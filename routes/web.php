<?php

use App\Domains\User\Http\Controllers\Web\UserController;
use App\Http\Controllers\CheckoutController;
use App\Http\Controllers\CheckUsageController;
use App\Http\Controllers\FaqController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\PageController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\SitemapController;
use App\Http\Controllers\TopupController;
use App\Http\Controllers\TravelController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|?
*/
Route::group(['prefix' => LaravelLocalization::setLocale()], function()
{
    Route::get('/', [HomeController::class, 'index'])->name('home');
    Route::get('/travel-esim/{slug}/{sort?}', [TravelController::class, 'index'])->name('travel-esim');
    Route::get('/product/{slug}', [ProductController::class, 'index'])->name('travel-product');

    Route::get('/topup', [TopupController::class, 'index'])->name('topup');
    Route::get('/topup/{provider?}/{countries?}/{sort?}', [TopupController::class, 'catalog'])->name('topup_catalog');

    Route::get('/check-usage', [CheckUsageController::class, 'index'])->name('check-usage');
    Route::get('/sitemap', [SitemapController::class, 'index'])->name('sitemap');
    Route::get('/page/{slug}', [PageController::class, 'index'])->name('page');
    Route::get('/faqs', [FaqController::class, 'index'])->name('faqs');
//========================= paymant
    Route::post('/createorder', [CheckoutController::class, 'createorder'])->name('ajaxcreateorder');
    Route::post('/confirm', [CheckoutController::class, 'confirm'])->name('ajaxconfirm');
    Route::post('/trans_reverse', [CheckoutController::class, 'reverse'])->name('reverse');

    Route::get('/checkout', [CheckoutController::class, 'index'])->name('checkout.index');

    Route::get('/createorder', [ProductController::class, 'payments'])->name('ajaxcreateorder');
    Route::get('/thanks_page', [CheckoutController::class, 'thanks'])->name('thanks');
    Route::get('/close_day', [CheckoutController::class, 'close'])->name('close');
//=========================
    //Route::get('/che', [PageController::class, 'mailorder']);

    Route::middleware('auth')->group(function () {
        Route::get('/user', [UserController::class, 'index'])->name('profile.index');
    });

    // Route::get('/user', function () {
    //     return view('dashboard');
    // })->middleware(['auth'])->name('user');


});
require __DIR__.'/auth.php';
