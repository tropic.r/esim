<?php

declare(strict_types=1);

use App\Orchid\Screens\Currency\CurrenciesCreateScreen;
use App\Orchid\Screens\Currency\CurrenciesEditScreen;
use App\Orchid\Screens\Currency\CurrenciesListScreen;
use App\Orchid\Screens\Faq\FaqCreateScreen;
use App\Orchid\Screens\Faq\FaqEditScreen;
use App\Orchid\Screens\Faq\FaqListScreen;
use App\Orchid\Screens\FaqCatalog\FaqCatalogCreateScreen;
use App\Orchid\Screens\FaqCatalog\FaqCatalogEditScreen;
use App\Orchid\Screens\FaqCatalog\FaqCatalogListScreen;
use App\Orchid\Screens\MetaSeo\MetaSeoCreateScreen;
use App\Orchid\Screens\MetaSeo\MetaSeoEditScreen;
use App\Orchid\Screens\MetaSeo\MetaSeoListScreen;
use App\Orchid\Screens\Page\PageCreateScreen;
use App\Orchid\Screens\Page\PageEditScreen;
use App\Orchid\Screens\Page\PageListScreen;
use App\Orchid\Screens\PlatformScreen;
use App\Orchid\Screens\Product\ProductEditScreen;
use App\Orchid\Screens\Product\ProductListScreen;
use App\Orchid\Screens\Region\RegionCreateScreen;
use App\Orchid\Screens\Region\RegionEditScreen;
use App\Orchid\Screens\Region\RegionListScreen;
use App\Orchid\Screens\Role\RoleEditScreen;
use App\Orchid\Screens\Role\RoleListScreen;
use App\Orchid\Screens\User\UserEditScreen;
use App\Orchid\Screens\User\UserListScreen;
use App\Orchid\Screens\User\UserProfileScreen;
use App\Orchid\Screens\Country\CountryCreateScreen;
use App\Orchid\Screens\Country\CountryEditScreen;
use App\Orchid\Screens\Country\CountryListScreen;
use Illuminate\Support\Facades\Route;
use Tabuna\Breadcrumbs\Trail;

/*
|--------------------------------------------------------------------------
| Dashboard Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the need "dashboard" middleware group. Now create something great!
|
*/

//Route::screen('/categories', CategoryListScreen::class)->name('platform.category.list');
// Route::screen('/categories/{category}/edit', CategoryEditScreen::class)->name('platform.category.edit');
// Route::screen('/categories-create', CategoryCreateScreen::class)->name('platform.category.create');

Route::screen('/products', ProductListScreen::class)->name('platform.product.list');
Route::screen('/products/{product}/edit', ProductEditScreen::class)->name('platform.product.edit');

Route::screen('/country', CountryListScreen::class)->name('platform.country.list');
Route::screen('/country-create', CountryCreateScreen::class)->name('platform.country.create');
Route::screen('/country/{country}/edit', CountryEditScreen::class)->name('platform.country.edit');

Route::screen('/regions', RegionListScreen::class)->name('platform.region.list');
Route::screen('/regions-create', RegionCreateScreen::class)->name('platform.region.create');
Route::screen('/regions/{region}/edit', RegionEditScreen::class)->name('platform.region.edit');

Route::screen('/seo', MetaSeoListScreen::class)->name('platform.meta_seo.list');
Route::screen('/seo-create', MetaSeoCreateScreen::class)->name('platform.meta_seo.create');
Route::screen('/seo/{meta_seo}/edit', MetaSeoEditScreen::class)->name('platform.meta_seo.edit');

Route::screen('/page', PageListScreen::class)->name('platform.page.list');
Route::screen('/page-create', PageCreateScreen::class)->name('platform.page.create');
Route::screen('/page/{page}/edit', PageEditScreen::class)->name('platform.page.edit');

Route::screen('/faq-catalog', FaqCatalogListScreen::class)->name('platform.faqcatalog.list');
Route::screen('/faq-catalog-create', FaqCatalogCreateScreen::class)->name('platform.faqcatalog.create');
Route::screen('/faq-catalog/{page}/edit', FaqCatalogEditScreen::class)->name('platform.faqcatalog.edit');

Route::screen('/faq', FaqListScreen::class)->name('platform.faq.list');
Route::screen('/faq-create', FaqCreateScreen::class)->name('platform.faq.create');
Route::screen('/faq/{page}/edit', FaqEditScreen::class)->name('platform.faq.edit');

Route::screen('/currencies', CurrenciesListScreen::class)->name('platform.currencies.list');
Route::screen('/currencies/{currency}/edit', CurrenciesEditScreen::class)->name('platform.currencies.edit');
Route::screen('/currencies-create', CurrenciesCreateScreen::class)->name('platform.currencies.create');

// Main
Route::screen('/main', PlatformScreen::class)
    ->name('platform.main');

// Platform > Profile
Route::screen('profile', UserProfileScreen::class)
    ->name('platform.profile')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.index')
            ->push(__('Profile'), route('platform.profile'));
    });

// Platform > System > Users
Route::screen('users/{user}/edit', UserEditScreen::class)
    ->name('platform.systems.users.edit')
    ->breadcrumbs(function (Trail $trail, $user) {
        return $trail
            ->parent('platform.systems.users')
            ->push(__('User'), route('platform.systems.users.edit', $user));
    });

// Platform > System > Users > Create
Route::screen('users/create', UserEditScreen::class)
    ->name('platform.systems.users.create')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.systems.users')
            ->push(__('Create'), route('platform.systems.users.create'));
    });

// Platform > System > Users > User
Route::screen('users', UserListScreen::class)
    ->name('platform.systems.users')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.index')
            ->push(__('Users'), route('platform.systems.users'));
    });

// Platform > System > Roles > Role
Route::screen('roles/{role}/edit', RoleEditScreen::class)
    ->name('platform.systems.roles.edit')
    ->breadcrumbs(function (Trail $trail, $role) {
        return $trail
            ->parent('platform.systems.roles')
            ->push(__('Role'), route('platform.systems.roles.edit', $role));
    });

// Platform > System > Roles > Create
Route::screen('roles/create', RoleEditScreen::class)
    ->name('platform.systems.roles.create')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.systems.roles')
            ->push(__('Create'), route('platform.systems.roles.create'));
    });

// Platform > System > Roles
Route::screen('roles', RoleListScreen::class)
    ->name('platform.systems.roles')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.index')
            ->push(__('Roles'), route('platform.systems.roles'));
    });


//Route::screen('idea', Idea::class, 'platform.screens.idea');
