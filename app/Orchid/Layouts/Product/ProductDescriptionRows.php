<?php
namespace App\Orchid\Layouts\Product;

use Orchid\Screen\Field;
use Orchid\Screen\Fields\Quill;
use Orchid\Screen\Fields\SimpleMDE;
use Orchid\Screen\Layouts\Rows;

class ProductDescriptionRows extends Rows
{
    /**
     * Used to create the title of a group of form elements.
     *
     * @var string|null
     */
    protected $title;

    /**
     * Get the fields elements to be displayed.
     *
     * @return Field[]
     */
    protected function fields(): array
    {
        
        $translations = $this->query->get('translations');

        return $translations->map(function($item){
            
            return Quill::make('translations.' . $item->locale . '.description')->title('Описание ' . strtoupper($item->locale))
            ->value($item->description);

        })->toArray();
    }
}
