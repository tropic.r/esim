<?php
namespace App\Orchid\Layouts\Product;

use App\Domains\Applicability\Models\Applicability;
use App\Domains\Category\Models\Category;
use App\Domains\Product\Models\ProductCategory;
use App\Domains\Product\Models\ProductsGroup;
use App\Domains\Provider\Models\Provider;
use App\Domains\Region\Models\Region;
use App\Domains\Country\Models\Country;
use Log;
use Orchid\Screen\Field;
use Orchid\Screen\Fields\CheckBox;
use Orchid\Screen\Fields\Group;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Label;
use Orchid\Screen\Fields\Relation;
use Orchid\Screen\Fields\Select;
use Orchid\Screen\Layouts\Rows;
use Orchid\Screen\Repository;

class ProductMainRows extends Rows
{
    /**
     * Used to create the title of a group of form elements.
     *
     * @var string|null
     */
    protected $title = 'Основное';
    
    /**
     * Get the fields elements to be displayed.
     *
     * @return Field[]
     */
    protected function fields(): array
    {
        
        $translations = $this->query->get('translations');
        $country_product = $this->query->get('country');
        $provider_product = $this->query->get('provider');
        $regions_product = $this->query->get('regions');
        $product = $this->query->get('product');

        $arRegionsList = [];
        $firstRegions = Region::get();
        foreach($firstRegions as $region){
            $arRegionsList[$region->id] = $region->name;
        }
        $arCountryList = [];
        $firstCountry = Country::get();
        foreach($firstCountry as $country){
            $arCountryList[$country->id] = $country->name ? $country->name : 'none';
        }
        $arProviderList = [];
        $firstProvider = Provider::get();
        foreach($firstProvider as $provider){
            $arProviderList[$provider->id] = $provider->name ? $provider->name : 'none';
        }

        return [
            Input::make('product.slug')->title('URL')->required(),
            CheckBox::make('product.active')->title('Активность'),
            Group::make(
                $translations->map(function($trans){
                    return Input::make('translations.' . $trans->locale . '.name')->value($trans->name)->title('Название ' . $trans->locale);
                })->toArray()
            ),

            Group::make([
                Select::make('product.provider_id')
                    ->options($arProviderList)
                    ->value($provider_product)
                    ->title('Provider')
                    ->required(),
            ]),

            Group::make([
                Select::make('product.country')
                    ->options($arCountryList)
                    ->value($country_product->map(function($item){ return $item->countri_id; })->toArray())
                    ->multiple()
                    ->title('Country')
                    ->required(),
                Select::make('product.regions')
                    ->options($arRegionsList)
                    ->value($regions_product->map(function($item){ return $item->region_id; })->toArray())
                    ->multiple()
                    ->title('Regions')
                    ->required(),
            ])
        ];
    }
}