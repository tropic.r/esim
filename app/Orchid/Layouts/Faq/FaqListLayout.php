<?php
namespace App\Orchid\Layouts\Faq;

use Log;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Fields\CheckBox;
use Orchid\Screen\Layouts\Table;
use Orchid\Screen\TD;

class FaqListLayout extends Table
{
    /**
     * Data source.
     *
     * The name of the key to fetch it from the query.
     * The results of which will be elements of the table.
     *
     * @var string
     */
    protected $target = 'faq';

    /**
     * Get the table cells to be displayed.
     *
     * @return TD[]
     */
    protected function columns(): array
    {
        return [
            TD::make('id', 'ID')->render(function($faq){
                return $faq->id;
            }),
            TD::make('faq_category_id', 'Faq Category')->render(function($faq){
                return $faq->faq_catalog->translation->name;
            }),
            TD::make('active', 'Active')->render(function($faq){
                return $faq->active ? "да" : "<b>нет</b>";
            }),
            TD::make('name', 'NAME')->render(function($faq){
                return Link::make($faq->translation->name)->route('platform.faq.edit', $faq);
            }),
        ];
    }
}
