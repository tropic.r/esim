<?php
namespace App\Orchid\Layouts\Faq;

use Log;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;
use Orchid\Screen\Field;
use Orchid\Screen\Fields\CheckBox;
use Orchid\Screen\Fields\Group;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Label;
use Orchid\Screen\Fields\Quill;
use Orchid\Screen\Fields\Relation;
use Orchid\Screen\Fields\Select;
use Orchid\Screen\Layouts\Rows;
use Orchid\Screen\Repository;

class FaqDescriptionRows extends Rows
{
    /**
     * Used to create the title of a group of form elements.
     *
     * @var string|null
     */
    protected $title = 'Description';
    
    /**
     * Get the fields elements to be displayed.
     *
     * @return Field[]
     */
    protected function fields(): array
    {
        $translations = $this->query->get('translations');

        $rows = [];

        if (count($translations) > 0) {
            foreach($translations as $translation){
                $rows = [
                    ...$rows,
                    ...[
                        Input::make('translations.' . $translation->locale . '.name')->title('Название ' . strtoupper($translation->locale))->required()->value($translation->name),
                        Quill::make('translations.' . $translation->locale . '.description')->title('Description ' . strtoupper($translation->locale))->required()->value($translation->description),
                    ]
                ];
            }
           
        }
        else{
            foreach (LaravelLocalization::getSupportedLocales() as $locale => $value) {
                $rows = [
                    ...$rows,
                    ...[
                        Input::make('translations.' . $locale . '.name')->title('Название ' . strtoupper($locale))->required(),
                        Quill::make('translations.' . $locale . '.description')->title('Description ' . strtoupper($locale))->required(),
                    ]
                ];
            }
        }
        return $rows;
    }
}