<?php
namespace App\Orchid\Layouts\Faq;

use App\Domains\Faq\Models\FaqCatalog;
use Log;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;
use Orchid\Screen\Field;
use Orchid\Screen\Fields\CheckBox;
use Orchid\Screen\Fields\Group;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Label;
use Orchid\Screen\Fields\Relation;
use Orchid\Screen\Fields\Select;
use Orchid\Screen\Layouts\Rows;
use Orchid\Screen\Repository;

class FaqMainRows extends Rows
{
    /**
     * Used to create the title of a group of form elements.
     *
     * @var string|null
     */
    protected $title = 'Основное';
    
    /**
     * Get the fields elements to be displayed.
     *
     * @return Field[]
     */
    protected function fields(): array
    {
        $translations = $this->query->get('translations');
        $faq = $this->query->get('faq');
        $faq_catalog = $this->query->get('faq_catalog');

        $arFaqCatalogList = [];
        $arFaqCatalogList[0] = 'Not selected';
        $firstProvider = FaqCatalog::get();
        foreach($firstProvider as $catalog){
            $arFaqCatalogList[$catalog->id] = $catalog->translation->name ? $catalog->translation->name : 'none';
        }

        return [
            CheckBox::make('faq.active')->title('Активность'),
            Group::make([
                Select::make('faq.faq_catalog_id')
                    ->options($arFaqCatalogList)
                    ->value($faq_catalog)
                    ->title('Faq catalog')
                    ->required(),
            ]),
        ];
        return $rows;
    }
}