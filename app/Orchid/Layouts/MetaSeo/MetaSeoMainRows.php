<?php
namespace App\Orchid\Layouts\MetaSeo;

use App\Domains\MetaSeo\Models\MetaSeo;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;
use Orchid\Screen\Field;
use Orchid\Screen\Fields\CheckBox;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Relation;
use Orchid\Screen\Fields\Select;
use Orchid\Screen\Layouts\Rows;

class MetaSeoMainRows extends Rows
{
    /**
     * Used to create the title of a group of form elements.
     *
     * @var string|null
     */
    protected $title;

    /**
     * Get the fields elements to be displayed.
     *
     * @return Field[]
     */
    protected function fields(): array
    {
        $translations = $this->query->get('translations');
        $rows = [];


        $rows = [
            ...$rows,
            ...[
                Input::make('meta_seo.slug')->title('Исходный URL')->required(),
                Select::make('meta_seo.type')->options([
                    'static' => 'Статическая страница',
                    'page' => 'Посадочная страница',
                    'catalog' => 'Категория каталога товаров',
                    'filter' => 'Фильтр',
                    'product' => 'Карточка товара',
                ])->title('Тип страници'),

                Select::make('meta_seo.meta_index')->options([
                    '1' => 'index',
                    '0' => 'noindex',
                ])->title('noindex/index'),
                CheckBox::make('meta_seo.active')->title('Активность'),
            ]
        ];

        return $rows;
    }
}
