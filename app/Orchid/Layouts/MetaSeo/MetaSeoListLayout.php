<?php
namespace App\Orchid\Layouts\MetaSeo;

use Log;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Fields\CheckBox;
use Orchid\Screen\Layouts\Table;
use Orchid\Screen\TD;

class MetaSeoListLayout extends Table
{
    /**
     * Data source.
     *
     * The name of the key to fetch it from the query.
     * The results of which will be elements of the table.
     *
     * @var string
     */
    protected $target = 'meta_seoies';

    /**
     * Get the table cells to be displayed.
     *
     * @return TD[]
     */
    protected function columns(): array
    {
        return [
            TD::make('id', 'ID')->render(function($meta_seo){
                return $meta_seo->meta_seo_id ;
            }),
            TD::make('type', 'Тип страници')->render(function($meta_seo){
                return $meta_seo->type;
            }),
            TD::make('meta_index', 'noindex/index')->render(function($meta_seo){
                return $meta_seo->meta_index ? 'index' : '<b>noindex</b>';
            }),
            TD::make('active', 'Активность')->render(function($meta_seo){
                return $meta_seo->active ? 'Активна' : '<b>Неактивна</b>';
            }),
            TD::make('slug', 'Видимый URL')->render(function($meta_seo){
                return Link::make(htmlspecialchars($meta_seo->slug))->route('platform.meta_seo.edit', $meta_seo->meta_seo_id);
            }),
        ];
    }
}
