<?php
namespace App\Orchid\Layouts\FaqCatalog;

use Log;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Fields\CheckBox;
use Orchid\Screen\Layouts\Table;
use Orchid\Screen\TD;

class FaqCatalogListLayout extends Table
{
    /**
     * Data source.
     *
     * The name of the key to fetch it from the query.
     * The results of which will be elements of the table.
     *
     * @var string
     */
    protected $target = 'faq_catalogs';

    /**
     * Get the table cells to be displayed.
     *
     * @return TD[]
     */
    protected function columns(): array
    {
        return [
            TD::make('id', 'ID')->render(function($faq_catalogs){
                return $faq_catalogs->id;
            }),
            TD::make('name', 'NAME')->render(function($faq_catalogs){
                return $faq_catalogs->translation->name;
            }),
            TD::make('slug', 'SLUG')->render(function($faq_catalogs){
                return Link::make($faq_catalogs->slug)->route('platform.faqcatalog.edit', $faq_catalogs);
            }),
        ];
    }
}
