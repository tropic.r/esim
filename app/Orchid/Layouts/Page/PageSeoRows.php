<?php
namespace App\Orchid\Layouts\Page;

use Mcamara\LaravelLocalization\Facades\LaravelLocalization;
use Orchid\Screen\Field;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Label;
use Orchid\Screen\Layouts\Rows;

class PageSeoRows extends Rows
{
    /**
     * Used to create the title of a group of form elements.
     *
     * @var string|null
     */
    protected $title;

    /**
     * Get the fields elements to be displayed.
     *
     * @return Field[]
     */
    protected function fields(): array
    {
        $translations = $this->query->get('translations');
        $rows = [];

        if (count($translations) > 0) {
            $translations->each(function ($item) use (&$rows) {
                $rows = [
                    ...$rows,
                    ...[
                        Label::make('label.' . $item->locale)->value('Версия ' . strtoupper($item->locale)),
                        Input::make('translations.' . $item->locale . '.meta_h1')
                        ->title('Meta H1')->value($item->meta_h1), 
                        Input::make('translations.' . $item->locale . '.meta_title')
                        ->title('Meta Title')->value($item->meta_title), 
                        Input::make('translations.' . $item->locale . '.meta_keywords')
                        ->title('Meta Keywords')->value($item->meta_keywords), 
                        Input::make('translations.' . $item->locale . '.meta_description')
                        ->title('Meta Description')->value($item->meta_description), 
                    ]
                ];
            });
        } else {
            foreach (LaravelLocalization::getSupportedLocales() as $locale => $value) {
                $rows = [
                    ...$rows,
                    ...[
                        Label::make('label.' . $locale)->value('Версия ' . strtoupper($locale)),
                        Input::make('translations.' . $locale . '.meta_h1')->title('Meta H1'), 
                        Input::make('translations.' . $locale . '.meta_title')->title('Meta Title'), 
                        Input::make('translations.' . $locale . '.meta_keywords')->title('Meta Keywords'), 
                        Input::make('translations.' . $locale . '.meta_description')->title('Meta Description'), 
                    ]
                ];
            }

            return $rows;
        }

        return $rows;
    }
}
