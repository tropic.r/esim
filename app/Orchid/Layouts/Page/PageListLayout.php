<?php
namespace App\Orchid\Layouts\Page;

use Log;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Fields\CheckBox;
use Orchid\Screen\Layouts\Table;
use Orchid\Screen\TD;

class PageListLayout extends Table
{
    /**
     * Data source.
     *
     * The name of the key to fetch it from the query.
     * The results of which will be elements of the table.
     *
     * @var string
     */
    protected $target = 'pages';

    /**
     * Get the table cells to be displayed.
     *
     * @return TD[]
     */
    protected function columns(): array
    {
        return [
            TD::make('id', 'ID')->render(function($pages){
                return $pages->id;
            }),
            TD::make('name', 'NAME')->render(function($pages){
                return $pages->translation->name;
            }),
            TD::make('slug', 'SLUG')->render(function($pages){
                return Link::make($pages->slug)->route('platform.page.edit', $pages);
            }),
        ];
    }
}
