<?php
namespace App\Orchid\Layouts\Page;

use Mcamara\LaravelLocalization\Facades\LaravelLocalization;
use Orchid\Screen\Field;
use Orchid\Screen\Fields\Quill;
use Orchid\Screen\Fields\SimpleMDE;
use Orchid\Screen\Layouts\Rows;

class PageDescriptionRows extends Rows
{
    /**
     * Used to create the title of a group of form elements.
     *
     * @var string|null
     */
    protected $title;

    /**
     * Get the fields elements to be displayed.
     *
     * @return Field[]
     */
    protected function fields(): array
    {
        
        $translations = $this->query->get('translations');
        $rows = [];

        if (count($translations) > 0) {
            return $translations->map(function($item){
                return Quill::make('translations.' . $item->locale . '.description')->title('Description ' . strtoupper($item->locale))
                ->value($item->description);
            })->toArray();
        } else {
            foreach (LaravelLocalization::getSupportedLocales() as $locale => $value) {
                $rows = [
                    ...$rows,
                    ...[
                        Quill::make('translations.' . $locale . '.description')
                        ->title('Описание ' . strtoupper($locale)),
                    ]
                ];
            }

            return $rows;
        }
    }
}
