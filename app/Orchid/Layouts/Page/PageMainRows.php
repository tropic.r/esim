<?php
namespace App\Orchid\Layouts\Page;

use Log;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;
use Orchid\Screen\Field;
use Orchid\Screen\Fields\CheckBox;
use Orchid\Screen\Fields\Group;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Label;
use Orchid\Screen\Fields\Relation;
use Orchid\Screen\Fields\Select;
use Orchid\Screen\Layouts\Rows;
use Orchid\Screen\Repository;

class PageMainRows extends Rows
{
    /**
     * Used to create the title of a group of form elements.
     *
     * @var string|null
     */
    protected $title = 'Основное';
    
    /**
     * Get the fields elements to be displayed.
     *
     * @return Field[]
     */
    protected function fields(): array
    {
        $translations = $this->query->get('translations');
        $page = $this->query->get('page');



        $rows = [];

        if (count($translations) > 0) {
            $rows = [
                ...$translations->map(function($translation){
                    return Input::make('translations.' . $translation->locale . '.name')
                    ->title('Name ' . strtoupper($translation->locale))
                    ->required()
                    ->value($translation->name);
                })->toArray()
            ];
        }
        else{
            foreach (LaravelLocalization::getSupportedLocales() as $locale => $value) {
                $rows = [
                    ...$rows,
                    ...[
                        Input::make('translations.' . $locale . '.name')
                        ->title('Название ' . strtoupper($locale))
                        ->required(),
                    ]
                ];
            }
        }

        $rows = [
            ...$rows,
            ...[
                Input::make('page.slug')->title('Символьный код')->required(),
                CheckBox::make('page.active')->title('Активность'),
            ]
        ];
        return $rows;
    }
}