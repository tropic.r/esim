<?php


namespace App\Orchid\Layouts\Country;


use Mcamara\LaravelLocalization\Facades\LaravelLocalization;
use Orchid\Screen\Field;
use Orchid\Screen\Fields\CheckBox;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Select;
use Orchid\Screen\Layouts\Rows;

class CountryMainRows extends Rows
{
    /**
     * Used to create the title of a group of form elements.
     *
     * @var string|null
     */
    protected $title;

    /**
     * Get the fields elements to be displayed.
     *
     * @return Field[]
     */
    protected function fields(): array
    {
       $rows = [];

        $rows = [
            ...$rows,
            ...[
                Input::make('country.name')->title('Название')->required(),
                Input::make('country.slug')->title('Символьный код')->required(),
                CheckBox::make('country.active')->title('Активность')
            ]
        ];

        return $rows;
    }
}
