<?php


namespace App\Orchid\Layouts\Region;


use Mcamara\LaravelLocalization\Facades\LaravelLocalization;
use Orchid\Screen\Field;
use Orchid\Screen\Fields\CheckBox;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Select;
use Orchid\Screen\Layouts\Rows;

class RegionMainRows extends Rows
{
    /**
     * Used to create the title of a group of form elements.
     *
     * @var string|null
     */
    protected $title;

    /**
     * Get the fields elements to be displayed.
     *
     * @return Field[]
     */
    protected function fields(): array
    {
       $rows = [];

        $rows = [
            ...$rows,
            ...[
                Input::make('region.name')->title('Название')->required(),
                Input::make('region.slug')->title('Символьный код')->required(),
                CheckBox::make('region.active')->title('Активность')
            ]
        ];

        return $rows;
    }
}
