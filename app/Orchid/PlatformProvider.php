<?php

declare(strict_types=1);

namespace App\Orchid;

use Orchid\Platform\Dashboard;
use Orchid\Platform\ItemPermission;
use Orchid\Platform\OrchidServiceProvider;
use Orchid\Screen\Actions\Menu;
use Orchid\Support\Color;

class PlatformProvider extends OrchidServiceProvider
{
    /**
     * @param Dashboard $dashboard
     */
    public function boot(Dashboard $dashboard): void
    {
        parent::boot($dashboard);

        // ...
    }

    /**
     * @return Menu[]
     */
    public function registerMainMenu(): array
    {
        return [

            Menu::make('List Product')->icon('list')->route('platform.product.list'),
            Menu::make('List County')->icon('list')->route('platform.country.list'),
            Menu::make('List Region')->icon('list')->route('platform.region.list'),
            Menu::make('Meta Seo')->icon('list')->route('platform.meta_seo.list'),
            Menu::make('List Page')->icon('list')->route('platform.page.list'),
            Menu::make('List Faq Catalog')->icon('list')->route('platform.faqcatalog.list'),
            Menu::make('List Faq Item')->icon('list')->route('platform.faq.list'),
            Menu::make('List Currency')
                ->route('platform.currencies.list')
                ->icon('list'),

            Menu::make(__('Users'))
                ->icon('user')
                ->route('platform.systems.users')
                ->permission('platform.systems.users')
                ->title(__('Access rights')),

            Menu::make(__('Roles'))
                ->icon('lock')
                ->route('platform.systems.roles')
                ->permission('platform.systems.roles'),
        ];
    }

    /**
     * @return Menu[]
     */
    public function registerProfileMenu(): array
    {
        return [
            Menu::make('Profile')
                ->route('platform.profile')
                ->icon('user'),
        ];
    }

    /**
     * @return ItemPermission[]
     */
    public function registerPermissions(): array
    {
        return [
            ItemPermission::group(__('System'))
                ->addPermission('platform.systems.roles', __('Roles'))
                ->addPermission('platform.systems.users', __('Users')),
        ];
    }
}
