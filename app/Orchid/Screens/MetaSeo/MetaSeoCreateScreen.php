<?php

namespace App\Orchid\Screens\MetaSeo;

use App\Domains\MetaSeo\Models\MetaSeo;
use App\Domains\MetaSeo\Services\MetaSeoService;
use App\Orchid\Layouts\MetaSeo\MetaSeoMainRows;
use App\Orchid\Layouts\MetaSeo\MetaSeoSeoRows;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Alert;
use Orchid\Support\Facades\Layout;

class MetaSeoCreateScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Добавление';
    public $description = 'Добавление мета данных';

    /**
     * Query data.
     *
     * @return array
     */
    public function query(MetaSeo $meta_seo): array
    {
        $this->exists = $meta_seo->exists;

        return [
            'category' => collect([]),
            'translations' => collect([]),
        ];
    }

    /**
     * Button commands.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): array
    {
        return [
            Button::make('Сохранить')
                ->method('save')->icon('save')
        ];
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): array
    {
        return [
            Layout::tabs([
                'Параметры' => [ MetaSeoMainRows::class ],
                'Seo' => [ MetaSeoSeoRows::class ]
            ])
        ];
    }

    public function save(
        MetaSeo $meta_seo,
        Request $request,
        MetaSeoService $service
    )
    {
        $service->setModel($meta_seo);
        $validate = $request->validate([
            'meta_seo.slug' => 'required',

            'meta_seo.*' => '',
            'translations.*.*' =>'',
            'translations.*.*.*' =>'',
        ]);
         Log::info($validate);

        $service->save($validate['meta_seo']);
        $service->saveTranslations($validate['translations']);

        Alert::success('Изменения успешно сохранены');
        return redirect()->route('platform.meta_seo.edit', $meta_seo);

    }
}
