<?php

namespace App\Orchid\Screens\MetaSeo;

use App\Domains\MetaSeo\Models\MetaSeo;
use App\Domains\MetaSeo\Services\MetaSeoService;
use App\Orchid\Layouts\MetaSeo\MetaSeoMainRows;
use App\Orchid\Layouts\MetaSeo\MetaSeoSeoRows;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Alert;
use Orchid\Support\Facades\Layout;
use Illuminate\Support\Facades\Validator;

class MetaSeoEditScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Редактирование';

    public $description = 'Редактирование мета данных';

    public $exists = false;

    /**
     * Query data.
     *
     * @return array
     */
    public function query(MetaSeo $meta_seo): array
    {
        $this->exists = $meta_seo->exists;

        if ($this->exists) {
            $this->description = 'Редактировать категорию';
            if ($meta_seo) {
                $this->name = $meta_seo->name;
            }

            return [
                'meta_seo' => $meta_seo,
                'translations' => $meta_seo ? $meta_seo->translations : [],
            ];
        }


        return [
            'meta_seo' => collect([]),
            'translations' => collect([]),
        ];
    }

    /**
     * Button commands.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): array
    {
        return [
            Button::make('Сохранить')
                ->method('save')->icon('save'),

            Button::make('Удалить')
                ->method('remove')->icon('trash')
        ];
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): array
    {
        return [
            Layout::tabs([
                'Настройки' => [MetaSeoMainRows::class],
                'Seo' => [MetaSeoSeoRows::class]
            ])
        ];
    }

    public function save(
        MetaSeo $metaSeo,
        Request $request,
        MetaSeoService $service
    )
    {
        $service->setModel($metaSeo);

        $validator = Validator::make($request->all(), [
            'meta_seo.*' => '',
            'translations.*.*' =>'',
            'translations.*.*.*' =>'',
        ]);

        $validator->after(function ($validator) use ($request) {
            $input = $request->all();
            if (substr_count($input['meta_seo']['slug'], 'http') > 0) {
                $validator->errors()->add(
                    'meta_seo.slug', 'Не должен содержать протокол и домен! Убрать '.config('app.url')
                );
            }
        });


        if ($validator->fails()) {
            return redirect()->route('platform.meta_seo.edit', $metaSeo)
                ->withErrors($validator)
                ->withInput();
        }

        // Получить проверенные данные...
        $validate = $validator->validated();

        $service->save($validate['meta_seo']);
        $service->saveTranslations($validate['translations']);

        Alert::success('Изменения успешно сохранены');
        return redirect()->route('platform.meta_seo.edit', $metaSeo);

    }

    public function remove(MetaSeo $metaSeo)
    {
        $metaSeo->delete()
            ? Alert::info('Вы успешно удалили запись.')
            : Alert::warning('Произошла ошибка');

        Cache::tags(['meta_seo'])->flush();

        return redirect()->route('platform.meta_seo.list');
    }
}
