<?php

namespace App\Orchid\Screens\MetaSeo;

use App\Domains\MetaSeo\Models\MetaSeo;
use App\Orchid\Layouts\MetaSeo\MetaSeoListLayout;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Screen;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;
use Orchid\Screen\Field;
use Orchid\Screen\Fields\CheckBox;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Relation;
use Orchid\Screen\Fields\Select;
use Orchid\Screen\Layouts\Rows;

class MetaSeoListScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Мета Данные';
    public $description = 'Наполнение мета данные';

    /**
     * Query data.
     *
     * @return array
     */
    public function query(): array
    {
        
        return [
            'meta_seoies' => MetaSeo::rightJoin('meta_seo_translations', 'meta_seo.id', '=', 'meta_seo_translations.meta_seo_id')
                ->where('meta_seo_translations.locale', app()->getLocale())->select('*')->paginate(12)
        ];
    }

    /**
     * Button commands.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): array
    {
        return [
            Link::make('Добавить')
                ->icon('pencil')
                ->route('platform.meta_seo.create'),
        ];
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): array
    {
        return [
            MetaSeoListLayout::class
        ];
    }
}
