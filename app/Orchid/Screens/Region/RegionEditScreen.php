<?php


namespace App\Orchid\Screens\Region;

use App\Domains\Region\Services\RegionService;
use App\Domains\Region\Models\Region;
use App\Orchid\Layouts\Region\RegionMainRows;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Alert;
use Orchid\Support\Facades\Layout;

class RegionEditScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Регион';

    /**
     * Display header description.
     *
     * @var string|null
     */
    public $description = 'Редактирование регион';

    public $exists = false;

    /**
     * Query data.
     *
     * @return array
     */
    public function query(Region $region){
        $this->exists = $region->exists;
        if ($this->exists) {
            $this->description = 'Редактировать регионов';
            if ($region) {
                $this->name = $region->name;
            }

            return [
                'region' => $region
            ];
        }
        else{
            return [
                'region' => collect([])
            ];
        }
    }

    /**
     * Button commands.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): array
    {
        return [
            Button::make('Сохранить')
                ->method('save')->icon('save'),

            Button::make('Удалить')
                ->method('remove')->icon('trash')
        ];
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): array
    {
        return [
            Layout::tabs([
                'Основное' => [
                    RegionMainRows::class
                ]
            ])
        ];
    }
    public function save(
        Region $region,
        Request $request,
        RegionService $service
    ){
        $service->setModel($region);
        $validate = $request->validate([
            'region.slug' => 'required',
            'region.name' =>'required',
            'region.*' => ''
        ]);
        $service->update($region, $validate);

        Alert::success('Изменения успешно сохранены');
        return redirect()->route('platform.region.edit', $region);
    }

    /**
     * @param Region $item
     *
     * @return \Illuminate\Http\RedirectResponse
     *
     * @throws \Exception
     */
    public function remove(Region $region)
    {
        $region->delete()
            ? Alert::info('Вы успешно удалили запись.')
            : Alert::warning('Произошла ошибка');

        return redirect()->route('platform.region.list');
    }
}
