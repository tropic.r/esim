<?php


namespace App\Orchid\Screens\Region;

use App\Domains\Region\Models\Region;
use App\Orchid\Layouts\Region\CurrenciesListLayout;
use App\Orchid\Layouts\Region\RegionListLayout;
use Orchid\Screen\Screen;
use Orchid\Screen\Actions\Link;

class RegionListScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Регионов';

    /**
     * Display header description.
     *
     * @var string|null
     */
    public $description = 'Список всех регионов';

    /**
     * Query data.
     *
     * @return array
     */
    public function query(): array
    {
        return [
            'region' => Region::orderBy('id', 'ASC')->paginate(12)
        ];
    }

    /**
     * Button commands.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): array
    {
        return [
            Link::make('Добавить')
                ->icon('pencil')
                ->route('platform.region.create'),
        ];
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): array
    {
        return [
            RegionListLayout::class
        ];
    }
}
