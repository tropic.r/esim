<?php


namespace App\Orchid\Screens\Region;

use App\Domains\Region\Models\Region;
use App\Domains\Region\Services\RegionService;
use App\Orchid\Layouts\Region\RegionMainRows;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Alert;
use Orchid\Support\Facades\Layout;

class RegionCreateScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Регион';

    /**
     * Display header description.
     *
     * @var string|null
     */
    public $description = 'Создание региона';

    public $exists = false;

    /**
     * Query data.
     *
     * @return array
     */
    public function query(Region $region): array
    {
        $this->exists = $region->exists;

        return [
            'region' => collect([])
        ];
    }

    /**
     * Button commands.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): array
    {
        return [
            Button::make('Сохранить')
                ->method('save')->icon('save')
        ];
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): array
    {
        return [
            Layout::tabs([
                'Основное' => [
                    RegionMainRows::class
                ]
            ])
        ];
    }

    public function save(
        Region $region,
        Request $request,
        RegionService $service
    )
    {
        $service->setModel($region);
        $validate = $request->validate([
            'region.slug' => 'required',
            'region.name' => 'required',
            'region.*' => ''
        ]);

        $service->store($validate);
        Alert::success('Изменения успешно сохранены');
        return redirect()->route('platform.region.edit', $region);

    }
}
