<?php


namespace App\Orchid\Screens\Country;

use App\Domains\Country\Models\Country;
use App\Domains\Country\Services\CountryService;
use App\Orchid\Layouts\Country\CountryMainRows;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Alert;
use Orchid\Support\Facades\Layout;

class CountryEditScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Страны';

    /**
     * Display header description.
     *
     * @var string|null
     */
    public $description = 'Редактирование страны';

    public $exists = false;

    /**
     * Query data.
     *
     * @return array
     */
    public function query(Country $country){
        $this->exists = $country->exists;
        if ($this->exists) {
            $this->description = 'Редактировать страны';
            if ($country) {
                $this->name = $country->name;
            }

            return [
                'country' => $country
            ];
        }
        else{
            return [
                'country' => collect([])
            ];
        }
    }

    /**
     * Button commands.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): array
    {
        return [
            Button::make('Сохранить')
                ->method('save')->icon('save'),

            Button::make('Удалить')
                ->method('remove')->icon('trash')
        ];
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): array
    {
        return [
            Layout::tabs([
                'Основное' => [
                    CountryMainRows::class
                ]
            ])
        ];
    }
    
    public function save(
        Country $country,
        Request $request,
        CountryService $service
    ){
        $service->setModel($country);
        $validate = $request->validate([
            'country.slug' => 'required',
            'country.name' =>'required',
            'country.*' => ''
        ]);
        $service->update($country, $validate);

        Alert::success('Изменения успешно сохранены');
        return redirect()->route('platform.country.edit', $country);
    }

    /**
     * @param Country $item
     *
     * @return \Illuminate\Http\RedirectResponse
     *
     * @throws \Exception
     */
    public function remove(Country $country)
    {
        $country->delete()
            ? Alert::info('Вы успешно удалили запись.')
            : Alert::warning('Произошла ошибка');

        return redirect()->route('platform.country.list');
    }
}
