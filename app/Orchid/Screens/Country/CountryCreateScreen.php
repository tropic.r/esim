<?php
namespace App\Orchid\Screens\Country;

use App\Domains\Country\Models\Country;
use App\Domains\Country\Services\CountryService;
use App\Orchid\Layouts\Country\CountryMainRows;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Alert;
use Orchid\Support\Facades\Layout;

class CountryCreateScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Страны';

    /**
     * Display header description.
     *
     * @var string|null
     */
    public $description = 'Создание Страны';

    public $exists = false;

    /**
     * Query data.
     *
     * @return array
     */
    public function query(Country $country): array
    {
        $this->exists = $country->exists;

        return [
            'country' => collect([])
        ];
    }

    /**
     * Button commands.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): array
    {
        return [
            Button::make('Сохранить')
                ->method('save')->icon('save')
        ];
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): array
    {
        return [
            Layout::tabs([
                'Основное' => [
                    CountryMainRows::class
                ]
            ])
        ];
    }

    public function save(
        Country $country,
        Request $request,
        CountryService $service
    )
    {
        $service->setModel($country);
        $validate = $request->validate([
            'country.slug' => 'required',
            'country.name' => 'required',
            'country.*' => ''
        ]);
        $country = $service->store($validate);

        Alert::success('Изменения успешно сохранены');
        return redirect()->route('platform.country.edit', $country->id);

    }
}
