<?php


namespace App\Orchid\Screens\Country;

use App\Domains\Country\Models\Country;
use App\Orchid\Layouts\Country\CountryListLayout;
use Orchid\Screen\Screen;
use Orchid\Screen\Actions\Link;

class CountryListScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Страны';

    /**
     * Display header description.
     *
     * @var string|null
     */
    public $description = 'Список всех cтран';

    /**
     * Query data.
     *
     * @return array
     */
    public function query(): array
    {
        return [
            'country' => Country::orderBy('id', 'ASC')->paginate(12)
        ];
    }

    /**
     * Button commands.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): array
    {
        return [
            Link::make('Добавить')
                ->icon('pencil')
                ->route('platform.country.create'),
        ];
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): array
    {
        return [
            CountryListLayout::class
        ];
    }
}
