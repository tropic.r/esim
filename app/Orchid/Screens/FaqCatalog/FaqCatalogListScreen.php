<?php

namespace App\Orchid\Screens\FaqCatalog;

use App\Domains\Faq\Models\FaqCatalog;
use App\Orchid\Layouts\FaqCatalog\FaqCatalogListLayout;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Screen;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;
use Orchid\Screen\Field;
use Orchid\Screen\Fields\CheckBox;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Relation;
use Orchid\Screen\Fields\Select;
use Orchid\Screen\Layouts\Rows;

class FaqCatalogListScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Faq catalog list';
    public $description = 'Faq catalog';

    /**
     * Query data.
     *
     * @return array
     */
    public function query(): array
    {
        $faq_catalogs = FaqCatalog::paginate(20);        
        return [
            'faq_catalogs' => $faq_catalogs
        ];
    }

    /**
     * Button commands.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): array
    {
        return [
            Link::make('Добавить')
                ->icon('pencil')
                ->route('platform.faqcatalog.create'),
        ];
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): array
    {
        return [
            FaqCatalogListLayout::class
        ];
    }
}
