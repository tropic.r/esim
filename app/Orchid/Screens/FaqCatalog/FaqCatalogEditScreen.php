<?php

namespace App\Orchid\Screens\FaqCatalog;

use App\Domains\Faq\Models\FaqCatalog;
use App\Domains\Faq\Services\FaqCatalogService;
use App\Orchid\Layouts\FaqCatalog\FaqCatalogMainRows;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Alert;
use Orchid\Support\Facades\Layout;

class FaqCatalogEditScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Edit';
    public $description = 'Edit Faq Catalog';

    /**
     * Query data.
     *
     * @return array
     */
    public function query(FaqCatalog $faq_catalog): array
    {
        $this->exists = $faq_catalog->exists;

        if ($this->exists) {
            $this->description = 'Редактировать категорию';
            if ($faq_catalog) {
                $this->name = $faq_catalog->name;
            }

            return [
                'faq_catalog' => $faq_catalog,
                'translations' => $faq_catalog ? $faq_catalog->translations : [],
            ];
        }


        return [
            'faq_catalog' => collect([]),
            'translations' => collect([]),
        ];

    }

    /**
     * Button commands.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): array
    {
        return [
            Button::make('Сохранить')
                ->method('save')->icon('save'),

            Button::make('Удалить')
                ->method('remove')->icon('trash')
        ];
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): array
    {
        return [
            Layout::tabs([
                'Основное' => [ FaqCatalogMainRows::class ],
            ])
        ];
    }

    public function save(
        FaqCatalog $faq_catalog,
        Request $request,
        FaqCatalogService $service
    )
    {
        $service->setModel($faq_catalog);
        $validate = $request->validate([
            'faq_catalog.slug' => 'required',
            'faq_catalog.*' => '',
            'translations.*.*' =>'',
            'translations.*.*.*' =>'',
        ]);
        Log::info($validate);

        $service->save($validate['faq_catalog']);
        $service->saveTranslations($validate['translations']);

        Alert::success('Изменения успешно сохранены');
        return redirect()->route('platform.faqcatalog.edit', $faq_catalog);

    }

    public function remove(FaqCatalog $faq_catalog)
    {
        $faq_catalog->delete()
            ? Alert::info('Вы успешно удалили запись.')
            : Alert::warning('Произошла ошибка');



        return redirect()->route('platform.faqcatalog.list');
    }
}
