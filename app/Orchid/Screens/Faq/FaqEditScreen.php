<?php

namespace App\Orchid\Screens\Faq;

use App\Domains\Faq\Models\Faq;
use App\Domains\Faq\Services\FaqService;
use App\Orchid\Layouts\Faq\FaqDescriptionRows;
use App\Orchid\Layouts\Faq\FaqMainRows;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Alert;
use Orchid\Support\Facades\Layout;

class FaqEditScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Edit';
    public $description = 'Edit Faq';

    /**
     * Query data.
     *
     * @return array
     */
    public function query(Faq $faq): array
    {
        $this->exists = $faq->exists;

        if ($this->exists) {
            $this->description = 'Редактировать категорию';
            if ($faq) {
                $this->name = $faq->name;
            }

            return [
                'faq' => $faq,
                'translations' => $faq ? $faq->translations : [],
                'faq_catalog' => $faq ? $faq->faq_catalog_id : [],
            ];
        }


        return [
            'faq' => collect([]),
            'translations' => collect([]),
            'faq_catalog' => collect([]),
        ];

    }

    /**
     * Button commands.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): array
    {
        return [
            Button::make('Сохранить')
                ->method('save')->icon('save'),

            Button::make('Удалить')
                ->method('remove')->icon('trash')
        ];
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): array
    {
        return [
            Layout::tabs([
                'Основное' => [ FaqMainRows::class ],
                'Description' => [ FaqDescriptionRows::class ],
            ])
        ];
    }

    public function save(
        Faq $faq,
        Request $request,
        FaqService $service
    )
    {
        $service->setModel($faq);
        $validate = $request->validate([
            'faq.faq_catalog_id' => 'required',
            'faq.*' => '',
            'translations.*.*' =>'',
            'translations.*.*.*' =>'',
        ]);
        Log::info($validate);

        $service->save($validate['faq']);
        $service->saveTranslations($validate['translations']);

        Alert::success('Изменения успешно сохранены');
        return redirect()->route('platform.faq.edit', $faq);

    }

    public function remove(Faq $faq)
    {
        $faq->delete()
            ? Alert::info('Вы успешно удалили запись.')
            : Alert::warning('Произошла ошибка');

        return redirect()->route('platform.faq.list');
    }
}
