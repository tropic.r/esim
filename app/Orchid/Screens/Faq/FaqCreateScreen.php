<?php

namespace App\Orchid\Screens\Faq;

use App\Domains\Faq\Models\Faq;
use App\Domains\Faq\Services\FaqService;
use App\Orchid\Layouts\Faq\FaqDescriptionRows;
use App\Orchid\Layouts\Faq\FaqMainRows;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Alert;
use Orchid\Support\Facades\Layout;

class FaqCreateScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Create';
    public $description = 'Create Faq';

    /**
     * Query data.
     *
     * @return array
     */
    public function query(Faq $faq): array
    {
        $this->exists = $faq->exists;

        return [
            'faq' => collect([]),
            'translations' => collect([]),
            'faq_catalog' => collect([]),
        ];
    }

    /**
     * Button commands.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): array
    {
        return [
            Button::make('Сохранить')
                ->method('save')->icon('save')
        ];
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): array
    {
        return [
            Layout::tabs([
                'Основное' => [ FaqMainRows::class ],
                'Description' => [ FaqDescriptionRows::class ],
            ])
        ];
    }

    public function save(
        Faq $faq,
        Request $request,
        FaqService $service
    )
    {
        $service->setModel($faq);
        $validate = $request->validate([
            'faq.faq_catalog_id' => 'required',
            'faq.*' => '',
            'translations.*.*' =>'',
            'translations.*.*.*' =>'',
        ]);
        Log::info($validate);
        $service->save($validate['faq']);
        $service->saveTranslations($validate['translations']);

        Alert::success('Изменения успешно сохранены');
        return redirect()->route('platform.faq.edit', $faq);

    }
}
