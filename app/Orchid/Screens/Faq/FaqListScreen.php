<?php

namespace App\Orchid\Screens\Faq;

use App\Domains\Faq\Models\Faq;
use App\Orchid\Layouts\Faq\FaqListLayout;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Screen;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;
use Orchid\Screen\Field;
use Orchid\Screen\Fields\CheckBox;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Relation;
use Orchid\Screen\Fields\Select;
use Orchid\Screen\Layouts\Rows;

class FaqListScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Faq list';
    public $description = 'Faq';

    /**
     * Query data.
     *
     * @return array
     */
    public function query(): array
    {
        $faq = Faq::paginate(20);        
        return [
            'faq' => $faq
        ];
    }

    /**
     * Button commands.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): array
    {
        return [
            Link::make('Добавить')
                ->icon('pencil')
                ->route('platform.faq.create'),
        ];
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): array
    {
        return [
            FaqListLayout::class
        ];
    }
}
