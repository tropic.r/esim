<?php

namespace App\Orchid\Screens\Page;

use App\Domains\Page\Models\Page;
use App\Domains\Page\Services\PageService;
use App\Orchid\Layouts\Page\PageDescriptionRows;
use App\Orchid\Layouts\Page\PageMainRows;
use App\Orchid\Layouts\Page\PageSeoRows;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Alert;
use Orchid\Support\Facades\Layout;

class PageCreateScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Create';
    public $description = 'Create static page';

    /**
     * Query data.
     *
     * @return array
     */
    public function query(Page $page): array
    {
        $this->exists = $page->exists;

        return [
            'page' => collect([]),
            'translations' => collect([]),
        ];
    }

    /**
     * Button commands.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): array
    {
        return [
            Button::make('Сохранить')
                ->method('save')->icon('save')
        ];
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): array
    {
        return [
            Layout::tabs([
                'Основное' => [ PageMainRows::class ],
                'Description' => [ PageDescriptionRows::class ],
                'Seo' => [ PageSeoRows::class ]
            ])
        ];
    }

    public function save(
        Page $page,
        Request $request,
        PageService $service
    )
    {
        $service->setModel($page);
        $validate = $request->validate([
            'page.slug' => 'required',
            'page.*' => '',
            'translations.*.*' =>'',
            'translations.*.*.*' =>'',
        ]);
         Log::info($validate);

        $service->save($validate['page']);
        $service->saveTranslations($validate['translations']);

        Alert::success('Изменения успешно сохранены');
        return redirect()->route('platform.page.edit', $page);

    }
}
