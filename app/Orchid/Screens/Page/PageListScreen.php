<?php

namespace App\Orchid\Screens\Page;

use App\Domains\Page\Models\Page;
use App\Orchid\Layouts\Page\PageListLayout;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Screen;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;
use Orchid\Screen\Field;
use Orchid\Screen\Fields\CheckBox;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Relation;
use Orchid\Screen\Fields\Select;
use Orchid\Screen\Layouts\Rows;

class PageListScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Page list';
    public $description = 'Static page';

    /**
     * Query data.
     *
     * @return array
     */
    public function query(): array
    {
        $pages = Page::paginate(20);        
        return [
            'pages' => $pages
        ];
    }

    /**
     * Button commands.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): array
    {
        return [
            Link::make('Добавить')
                ->icon('pencil')
                ->route('platform.page.create'),
        ];
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): array
    {
        return [
            PageListLayout::class
        ];
    }
}
