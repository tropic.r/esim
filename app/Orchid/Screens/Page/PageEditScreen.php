<?php

namespace App\Orchid\Screens\Page;

use App\Domains\Page\Models\Page;
use App\Domains\Page\Services\PageService;
use App\Orchid\Layouts\Page\PageDescriptionRows;
use App\Orchid\Layouts\Page\PageMainRows;
use App\Orchid\Layouts\Page\PageSeoRows;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Alert;
use Orchid\Support\Facades\Layout;
use Illuminate\Support\Facades\Validator;

class PageEditScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Edit';

    public $description = 'Edit static page';

    public $exists = false;

    /**
     * Query data.
     *
     * @return array
     */
    public function query(Page $page): array
    {
        $this->exists = $page->exists;

        if ($this->exists) {
            $this->description = 'Редактировать категорию';
            if ($page) {
                $this->name = $page->name;
            }

            return [
                'page' => $page,
                'translations' => $page ? $page->translations : [],
            ];
        }


        return [
            'page' => collect([]),
            'translations' => collect([]),
        ];
    }

    /**
     * Button commands.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): array
    {
        return [
            Button::make('Сохранить')
                ->method('save')->icon('save'),

            Button::make('Удалить')
                ->method('remove')->icon('trash')
        ];
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): array
    {
        return [
            Layout::tabs([
                'Основное' => [ PageMainRows::class ],
                'Description' => [ PageDescriptionRows::class ],
                'Seo' => [ PageSeoRows::class ]
            ])
        ];
    }

    public function save(
        Page $page,
        Request $request,
        PageService $service
    )
    {
        $service->setModel($page);

        $validator = Validator::make($request->all(), [
            'page.*' => '',
            'translations.*.*' =>'',
            'translations.*.*.*' =>'',
        ]);


        if ($validator->fails()) {
            return redirect()->route('platform.page.edit', $page)
                ->withErrors($validator)
                ->withInput();
        }

        // Получить проверенные данные...
        $validate = $validator->validated();

        $service->save($validate['page']);
        $service->saveTranslations($validate['translations']);

        Alert::success('Изменения успешно сохранены');
        return redirect()->route('platform.page.edit', $page);

    }

    public function remove(Page $page)
    {
        $page->delete()
            ? Alert::info('Вы успешно удалили запись.')
            : Alert::warning('Произошла ошибка');



        return redirect()->route('platform.page.list');
    }
}
