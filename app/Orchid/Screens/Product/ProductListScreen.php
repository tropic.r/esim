<?php


namespace App\Orchid\Screens\Product;


use App\Domains\Product\Models\Product;
use App\Orchid\Layouts\Product\ProductListLayout;
use App\Orchid\Screens\Filters\ProductFiltersLayout;
use Orchid\Screen\Screen;
use Orchid\Screen\Actions\Link;

class ProductListScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Товары';

    /**
     * Display header description.
     *
     * @var string|null
     */
    public $description = 'Список всех товаров';

    /**
     * Query data.
     *
     * @return array
     */
    public function query(): array
    {
        return [
            'products' => Product::orderBy('id', 'ASC')->paginate(20)
        ];
    }

    /**
     * Button commands.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): array
    {
         return [];
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): array
    {
        return [
            ProductListLayout::class
        ];
    }
}
