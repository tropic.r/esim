<?php
namespace App\Orchid\Screens\Product;

use App\Domains\Product\Http\Requests\OrchidProductRequest;
use App\Domains\Product\Models\Product;
use App\Domains\Product\Services\ProductOrchidService;
use App\Orchid\Layouts\Product\ProductDescriptionRows;
use App\Orchid\Layouts\Product\ProductMainRows;
use App\Orchid\Layouts\Product\ProductSeoRows;

use Orchid\Screen\Actions\Button;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Alert;
use Orchid\Support\Facades\Layout;

class ProductEditScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'ProductEditScreen';
    public $service;

    /**
     * Display header description.
     *
     * @var string|null
     */
    protected $data;

    public $description = 'Карточка редактирования товара';

    /**
     * Query data.
     *
     * @return array
     */
    public function query(Product $product): array
    {
        $this->data = $product;

        $this->name = $product->translation != null ? $product->translation->name : "";
        
        return [
            'product' => $product,
            'translations' => $product->translations,
            'country' => $product->counties,
            'provider' => $product->provider_id,
            'regions' => $product->regions,
        ];
    }

    /**
     * Button commands.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): array
    {
        return [
            Link::make('Назад')
                 ->href(route('platform.product.list'))
                 ->icon('action-undo'),
            Button::make('Сохранить')
                ->method('save')
                ->icon('save'),
            Button::make('Удалить')
                ->method('remove')
                ->icon('close')
        ];
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): array
    {
        return [
            Layout::tabs([
                'Товар' => [
                    ProductMainRows::class
                ],
                'Детальное описание' => [
                    ProductDescriptionRows::class
                ],
                'SEO' => [
                    ProductSeoRows::class
                ]
            ])
        ];
    }

    public function save(
        Product $product,
        OrchidProductRequest $request,
        ProductOrchidService $service
    ) {

        $service->setModel($product);
        $validated = $request->validated();

        $service->save($validated['product']);
        if (array_key_exists('translations', $validated)) {
            $service->saveTranslations($validated['translations']);
        }
        if (array_key_exists('country', $validated)) {
            $service->saveCountrys($validated['country']);
        }
        if (array_key_exists('regions', $validated)) {
            $service->saveRegions($validated['regions']);
        }

        Alert::success('Товар успешно изменен');
        return redirect()->route('platform.product.edit', $product);
    }

    public function remove(
        Product $product,
        OrchidProductRequest $request,
        ProductOrchidService $service
    ) {

        $service->setModel($product);
        $validated = $request->validated();

        $service->deleteById($product->id);

        Alert::success('Товар успешно удален');
        return redirect()->route('platform.product.list');
    }
}
