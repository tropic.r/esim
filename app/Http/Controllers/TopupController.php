<?php

namespace App\Http\Controllers;

use App\Domains\Product\Models\Product;
use App\Domains\Provider\Models\Provider;
use App\Domains\Region\Models\Region;
use App\Domains\Country\Models\Country;
use App\Services\RouterService;

/**
 * Class HomeController.
 */
class TopupController
{

    public function __construct(RouterService $routerService)
    {
        $this->routerService = $routerService;
    }

    public function index()
    {
        return view('pages.topup.index');
    }

    public function catalog($provider = '', $countries = '', $sort ='')
    {
        list($provider_slug, $countries_slug, $sort_slug) = $this->routerService->detectParameters([$provider, $countries, $sort]);

        switch($sort_slug){
            case '':
                $name_ = 'products.retailPrice';
                $sort = 'desc';
                break;
            case 'sort-low-to-high':
                $name_ = 'products.retailPrice';
                $sort = 'asc';
                break;
            case 'sort-high-to-low':
                $name_ = 'products.plan_data_limit';
                $sort = 'desc';
                break;
            case 'sort-price':
                $name_ = 'products.price_gb';
                $sort = 'desc';
                break;
        }
       
        $info_provider = Provider::where('slug',$provider_slug)->first();
        if($countries_slug != ''){
            $info_countries = Country::where('slug',$countries_slug)->first();
            $products = Product::select('products.*')->leftJoin('products_countrie', function($join) {
                $join->on('products.id', '=', 'products_countrie.product_id');
              })->where('products.category', '4')->where('products.provider_id', $info_provider->id)->where('products_countrie.countri_id', $info_countries->id)->orderBy($name_, $sort)->get();
        } else {
            $products = Product::select('products.*')->where('products.category', '4')->where('products.provider_id', $info_provider->id)->orderBy($name_, $sort)->get();
        }
        $link = [];
        
        if($countries_slug != ''){
            $link = ['provider' => $provider_slug, 'countries' => $countries_slug];
        } else {
            $link = ['provider' => $provider_slug];
        }

        $sorts =[];
        $sorts[] = [
            'name' => __('catalog.sort.recommned'),
            'link' => route('topup_catalog', $link),
            'active' => ($sort_slug == '') ? ' active' : ''
        ];
        $sorts[] = [
            'name' => __('catalog.sort.lowToHigh'),
            'link' => route('topup_catalog', $link).'/sort-low-to-high',
            'active' => ($sort_slug == 'sort-low-to-high') ? ' active' : ''
        ];
        $sorts[] = [
            'name' => __('catalog.sort.highToLow'),
            'link' => route('topup_catalog', $link).'/sort-high-to-low',
            'active' => ($sort_slug == 'sort-high-to-low') ? ' active' : ''
        ];
        $sorts[] = [
            'name' => __('catalog.sort.price'),
            'link' => route('topup_catalog', $link).'/sort-price',
            'active' => ($sort_slug == 'sort-price') ? ' active' : ''
        ];
            

        return view('pages.topup.catalog',['info'=>$info_provider,'products'=>$products,'sorts'=>$sorts]);
    }
}