<?php

namespace App\Http\Controllers;

use App\Domains\MetaSeo\Services\MetaSeoFactory;
use App\Domains\Product\Models\Product;
use App\Domains\Region\Models\Region;
use App\Domains\Country\Models\Country;

/**
 * Class HomeController.
 */
class TravelController
{

    public function index($slug, $sort = '')
    {
        switch($sort){
            case '':
                $name_ = 'products.retailPrice';
                $sort_ = 'desc';
                break;
            case 'sort-low-to-high':
                $name_ = 'products.retailPrice';
                $sort_ = 'asc';
                break;
            case 'sort-high-to-low':
                $name_ = 'products.plan_data_limit';
                $sort_ = 'desc';
                break;
            case 'sort-price':
                $name_ = 'products.price_gb';
                $sort_ = 'desc';
                break;
        }

        $flag = false;
        $info = Country::where('slug', $slug)->first();
        if(!$info){
            $flag = true;
            $info = Region::where('slug', $slug)->first();
        }

        if($flag){
            $products = Product::select('products.*')->leftJoin('products_region', function($join) {
                $join->on('products.id', '=', 'products_region.product_id');
              })->where('products_region.region_id', $info->id)->whereIn('products.category', [1,2])->orderBy($name_,$sort_)->get();
        } else {
            $products = Product::select('products.*')->leftJoin('products_countrie', function($join) {
                $join->on('products.id', '=', 'products_countrie.product_id');
              })->where('products_countrie.countri_id', $info->id)->whereIn('products.category', [1,2])->orderBy($name_,$sort_)->get();
        }

        $sorts =[];
        $sorts[] = [
            'name' => __('catalog.sort.recommned'),
            'link' => route('travel-esim', ['slug' => $slug]),
            'active' => ($sort == '') ? ' active' : ''
        ];
        $sorts[] = [
            'name' => __('catalog.sort.lowToHigh'),
            'link' => route('travel-esim', ['slug' => $slug]).'/sort-low-to-high',
            'active' => ($sort == 'sort-low-to-high') ? ' active' : ''
        ];
        $sorts[] = [
            'name' => __('catalog.sort.highToLow'),
            'link' => route('travel-esim', ['slug' => $slug]).'/sort-high-to-low',
            'active' => ($sort == 'sort-high-to-low') ? ' active' : ''
        ];
        $sorts[] = [
            'name' => __('catalog.sort.price'),
            'link' => route('travel-esim', ['slug' => $slug]).'/sort-price',
            'active' => ($sort == 'sort-price') ? ' active' : ''
        ];
    
        return view('pages.travel.catalog', ['info' => $info, 'sorts' => $sorts, 'products' => $products, 'seo' => $this->getMeta()]);
    }
    private function getMeta(){
        $metaSeo = MetaSeoFactory::init('catalog');
        return $metaSeo->get();
    }
}