<?php

namespace App\Http\Controllers;

use App\Domains\MetaSeo\Services\MetaSeoFactory;
use App\Domains\Order\Models\Order;
use App\Domains\Page\Models\Page;

/**
 * Class HomeController.
 */
class PageController
{

    public function index($slug)
    {
        $page = Page::where('slug', $slug)->first();
        return view('pages.page', ['page' => $page]);
    }

    public function mailorder(){
        $order = Order::where('orderId', 'STV-570887')->first();
        $arr = [];
        foreach($order->data['orderLineItem']['lineItemDetails'] as $item){
            $arr[$item['name']] = $item['value'];
        }
        $order->lineItemDetails = $arr;
        return view('mails.order', ['order' => $order]);
    }
}