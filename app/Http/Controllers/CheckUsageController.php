<?php

namespace App\Http\Controllers;

use App\Domains\Product\Models\Product;
use Illuminate\Http\Request;

/**
 * Class HomeController.
 */
class CheckUsageController
{
    public function index(Request $request)
    {
        if(isset($request->orderId)){
            $order = $this->gurl($request->orderId);
            $data = Product::where('productId', $order->orderLineItem->productId)->first();
            
        } else {
            $data = [];
        }
        return view('pages.check-sage', ['data' => $data, 'orderId' => $request->orderId]);
    }

    public function gurl($orderId){
        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => 'https://api.mobimatter.com/mobimatter/api/v2/order/'.$orderId,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'GET',
          CURLOPT_HTTPHEADER => array(
            'Content-Type: application/json',
            'api-key:'. env('MOBIMATTER_API_KEY'),
            'MerchantId:'. env('MOBIMATTER_API_MERCHANT_ID'),
          ),
        ));
        
        $response = curl_exec($curl);
        
        curl_close($curl);
        return json_decode($response)->result;
    }
}