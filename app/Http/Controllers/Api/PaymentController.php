<?php 

namespace App\Http\Controllers\Api;

use Fruitware\VictoriaBankGateway\VictoriaBankGateway;
use Fruitware\VictoriaBankGateway\VictoriaBank\Exception;
use Fruitware\VictoriaBankGateway\VictoriaBank\Response;
use Fruitware\VictoriaBankGateway\VictoriaBank\AuthorizationResponse;
use Illuminate\Http\Request;

class PaymentController extends BaseController {

    public function afterPaymants(Request $request)
    {
        $victoriaBankGateway = new VictoriaBankGateway();
        $bankResponse = $victoriaBankGateway->getResponseObject($request);
        
        if (!$bankResponse->isValid()) {
            throw new Exception('Invalid bank Auth response');
        }
        
        switch ($bankResponse::TRX_TYPE) {
            case VictoriaBankGateway::TRX_TYPE_AUTHORIZATION:
                $amount         = $bankResponse->{Response::AMOUNT};
                $bankOrderCode  = $bankResponse->{Response::ORDER};
                $rrn            = $bankResponse->{Response::RRN};
                $intRef         = $bankResponse->{Response::INT_REF};
        
                $victoriaBankGateway->requestCompletion($bankOrderCode, $amount, $rrn, $intRef, $currency = null);
                break;
        
            case VictoriaBankGateway::TRX_TYPE_COMPLETION:
                break;
        
            case VictoriaBankGateway::TRX_TYPE_REVERSAL:
                break;
        
            default:
        throw new Exception('Unknown bank response transaction type');
    }

}
}