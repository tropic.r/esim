<?php

namespace App\Http\Controllers;

use App\Domains\Product\Models\Product;
use App\Domains\Region\Models\Region;
use App\Domains\Country\Models\Country;

/**
 * Class HomeController.
 */
class SitemapController
{

    public function index()
    {
        $products = Product::where('active', 1)->get();
        $regions = Region::where('active', 1)->get();
        $country = Country::where('active', 1)->get();
        return view('pages.sitemap', ['products' => $products,'regions' => $regions,'country' => $country,]);
    }
}