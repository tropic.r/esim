<?php

namespace App\Http\Controllers;

use App\Domains\Faq\Models\FaqCatalog;
use App\Domains\MetaSeo\Services\MetaSeoFactory;
use App\Domains\Page\Models\Page;

/**
 * Class HomeController.
 */
class FaqController
{

    public function index()
    {
        $faqs = FaqCatalog::get();
        return view('pages.faqs', ['faqs' => $faqs, 'seo' => $this->getMeta()]);
    }

    private function getMeta(){
        $metaSeo = MetaSeoFactory::init('static');
        return $metaSeo->get();
    }
}