<?php

namespace App\Http\Controllers;

use App\Domains\MetaSeo\Services\MetaSeoFactory;
use Fruitware\VictoriaBankGateway\VictoriaBank\AuthorizationResponse;
use Illuminate\Http\Request;

/**
 * Class HomeController.
 */
class CheckoutController
{
    protected $cert=false;





    public function __construct() {
        $this->cert = $_SERVER['DOCUMENT_ROOT'].'/public/cert.pem';
    }

    public function index(){
        return view('pages.checkout');
    }





    //@thanks page after transaction
    public function thanks()
    {
        return view('pages.thanks');
    }





    //@return money  ajax
    public function reverse(Request $request)
    {
       // https://maib.ecommerce.md:21440/ecomm/MerchantHandler?command=r&trans_id=owrlbEwEKv3FniBONeV2qtx1YRI=&amount=400
        if(file_exists($this->cert)){
            $post = [
                'command' => 'r',
                'client_ip_addr' => $_SERVER["REMOTE_ADDR"],
                'trans_id'=>$request['orderId'],
                'amount'=>$request['price']
            ];

            $addr = curl_init('https://maib.ecommerce.md:21440/ecomm/MerchantHandler');
            curl_setopt($addr, CURLOPT_CERTINFO, true);
            curl_setopt($addr, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($addr, CURLOPT_SSLCERT,$this->cert);
            curl_setopt($addr, CURLOPT_POST, TRUE);
            curl_setopt($addr, CURLOPT_POSTFIELDS, http_build_query($post));
            echo $response = curl_exec($addr);
            echo $errors = curl_error($addr);


            curl_close($addr);
        }else{
            echo json_encode(["status"=>"sertificate not found"]);
        }
    }




    //@close transaction day
    public function close()
    {
        if(file_exists($this->cert)){
            $post = [
                'command' => 'b',
                'client_ip_addr' => $_SERVER["REMOTE_ADDR"]
            ];

            $addr = curl_init('https://maib.ecommerce.md:21440/ecomm/MerchantHandler');
            curl_setopt($addr, CURLOPT_CERTINFO, true);
            curl_setopt($addr, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($addr, CURLOPT_SSLCERT,$this->cert);
            curl_setopt($addr, CURLOPT_POST, TRUE);
            curl_setopt($addr, CURLOPT_POSTFIELDS, http_build_query($post));
            echo $response = curl_exec($addr);
            echo $errors = curl_error($addr);


            curl_close($addr);
        }else{
            echo json_encode(["status"=>"sertificate not found"]);
        }
    }




    //@confirm transaction ajax
    public function confirm(Request $request) {

        if(file_exists($this->cert)){
            $post = [
                'command' => 'c',
                'trans_id' => $request['trans_id'],
                'client_ip_addr' => $_SERVER["REMOTE_ADDR"]
            ];

            $addr = curl_init('https://maib.ecommerce.md:21440/ecomm/MerchantHandler');
            curl_setopt($addr, CURLOPT_CERTINFO, true);
            curl_setopt($addr, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($addr, CURLOPT_SSLCERT,$this->cert);
            //curl_setopt($addr, CURLOPT_SSLKEY,'/var/www/html/hongbao/apiclient_key.pem');
            //curl_setopt($addr, CURLOPT_CAINFO, '/var/www/html/hongbao/rootca.pem');
            curl_setopt($addr, CURLOPT_POST, TRUE);
            curl_setopt($addr, CURLOPT_POSTFIELDS, http_build_query($post));
            echo $response = curl_exec($addr);
            echo $errors = curl_error($addr);


            curl_close($addr);
        }else{
            echo json_encode(["status"=>"sertificate not found"]);
        }
    }






    //@create transaction ajax

    public function createorder(Request $request)
    {

        if(file_exists($this->cert)){
            $post = [
                'command' => 'v',
                'amount' => $request['price']*100,
                'currency' => '840',
                'description' => 'e-sim-id:'.$request['productId'],
                'language' => 'en',
                'client_ip_addr' => $_SERVER["REMOTE_ADDR"]
            ];

            $addr = curl_init('https://maib.ecommerce.md:21440/ecomm/MerchantHandler');
            curl_setopt($addr, CURLOPT_CERTINFO, true);
            curl_setopt($addr, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($addr, CURLOPT_SSLCERT,$this->cert);
            curl_setopt($addr, CURLOPT_POST, TRUE);
            curl_setopt($addr, CURLOPT_POSTFIELDS, http_build_query($post));
            echo $response = curl_exec($addr);
            echo $errors = curl_error($addr);


            curl_close($addr);
        }else{
           echo json_encode(["status"=>"certificate not found"]);
        }


    }




    private function getMeta(){
        $metaSeo = MetaSeoFactory::init('static');
        return $metaSeo->get();
    }

}
