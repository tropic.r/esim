<?php

namespace App\Http\Controllers;

use App\Domains\MetaSeo\Services\MetaSeoFactory;
use App\Domains\Product\Models\Product;
use App\Domains\Product\Models\ProductCountie;
use App\Domains\Region\Models\Region;
use App\Domains\Country\Models\Country;

/**
 * Class HomeController.
 */
class HomeController
{
    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {

        return view('pages.home', ['seo' => $this->getMeta()]);
    }
    
    private function getMeta(){
        $metaSeo = MetaSeoFactory::init('static');
        return $metaSeo->get();
    }
}