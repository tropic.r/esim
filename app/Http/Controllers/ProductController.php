<?php

namespace App\Http\Controllers;

use App\Domains\Product\Models\Product;
use App\Domains\Product\Models\ProductCountie;
use App\Domains\Region\Models\Region;
use App\Domains\Country\Models\Country;

/**
 * Class HomeController.
 */
class ProductController
{
    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index($slug)
    {

        $product = Product::where('slug', $slug)->first();
        return view('pages.travel.cart',['product' => $product]);
    }

    public function topup($slug){

    }

    public function payments()
    {
        return 222;
    }
}
