<?php

namespace App\Console\Commands;

use App\Domains\Product\Models\Product;
use App\Domains\Product\Models\ProductCountie;
use App\Domains\Product\Models\ProductRegion;
use App\Domains\Product\Models\ProductTranslation;
use App\Domains\Provider\Models\Provider;
use App\Domains\Region\Models\Region;
use App\Domains\Country\Models\Country;
use Illuminate\Console\Command;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Orchid\Attachment\File;
use Orchid\Attachment\Models\Attachment;


class ExportProduct extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:export_product';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Export product mobimatter';

    private $langs = [];
    private $product_id = '';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->setLangs();
        $products = $this->curl();
        foreach($products as $product){
            $provider_id = $this->provider($product->providerName,$product->providerLogo);
            foreach($product->productDetails as $productDetails){
                switch($productDetails->name){
                    case 'PLAN_TITLE':
                        $PLAN_TITLE = $productDetails->value;
                        break;
                    case 'PLAN_DATA_LIMIT':
                        $PLAN_DATA_LIMIT = $productDetails->value;
                        break;
                    case 'PLAN_DATA_UNIT':
                        $PLAN_DATA_UNIT = $productDetails->value;
                        break;
                    case 'PLAN_VALIDITY':
                        $PLAN_VALIDITY = $productDetails->value;
                        break;
                    case 'TAGS':
                        $productDetails->value = json_decode($productDetails->value);
                        if(isset($productDetails->value[0])){
                            $TAGS = $productDetails->value[0];
                        }else{
                            $TAGS = '';
                        }
                        break;
                    case 'I_PRODUCT':
                        $I_PRODUCT = $productDetails->value;
                        break;
                    case 'PLAN_DETAILS ':
                        $PLAN_DETAILS = '';
                        $DETAILS = json_decode($productDetails->value);
                        $PLAN_DETAILS .= '<h3 class="h5 text-uppercase mb-3">'.$DETAILS->heading.'</h3>';
                        if($DETAILS->description){
                            $PLAN_DETAILS .= '<p>'.$DETAILS->description.'</p>';
                        }
                        if($DETAILS->items){
                            $PLAN_DETAILS .= '<ul class="v-list custom-list">';
                            foreach($DETAILS->items as $item){
                                $PLAN_DETAILS .= '<li>'.$item.'</li>';
                            }
                            $PLAN_DETAILS .= '</ul>';
                        }
                        break;
                }
            }
            if($PLAN_DATA_LIMIT){
                $price_gb = $product->retailPrice/$PLAN_DATA_LIMIT;
            } else {
                $price_gb = $product->retailPrice/0.1;
            }
            
            $produc = Product::updateOrCreate([
                'productId' => $product->productId
            ], [
                'slug' => Str::slug($PLAN_TITLE),
                'uniqueId' => $product->uniqueId,
                'merchantId' => $product->merchantId,
                'i_product' => $I_PRODUCT,
                'rank' => $product->rank,
                'category' => $product->productCategoryId,
                'productCategory' => $product->productCategory,
                'provider_id' => $provider_id,
                'retailPrice' => $product->retailPrice,
                'wholesalePrice' => $product->wholesalePrice,
                'currencyCode' => $product->currencyCode,
                'price_gb' => $price_gb,
                'plan_data_limit' => $PLAN_DATA_LIMIT,
                'plan_data_unit' => $PLAN_DATA_UNIT,
                'plan_validity' => $PLAN_VALIDITY,
                'tags' => json_encode($TAGS),
                'data' => json_encode($product),
                'active' => 1,
            ]);

            $this->product_id = $produc->id;

            foreach($this->langs as $lang){
                switch ($lang) {
                    case 'en':
                        ProductTranslation::updateOrCreate([
                            'product_id' => $produc->id,
                            'locale' => $lang
                        ], [
                            'name' => $PLAN_TITLE,
                            'meta_title' => $PLAN_TITLE,
                            'description' => $PLAN_DETAILS
                        ]);
                        break;
                    default:
                        $productTrans = ProductTranslation::where('product_id', $produc->id)->where('locale', $lang)->get();
                        switch ($productTrans->count()) {
                            case '0':
                                ProductTranslation::create([
                                    'product_id' => $produc->id,
                                    'locale' => $lang,
                                    'name' => $PLAN_TITLE,
                                    'meta_title' => $PLAN_TITLE,
                                    'description' => $PLAN_DETAILS,
                                    'meta_description' => $PLAN_TITLE,
                                ]);
                                break;
                        }
                        break;
                }
            }

            $this->setRegion($product->regions);
            $this->setCountie($product->countries);
        } 
    }

    private function setLangs()
    {
        foreach (LaravelLocalization::getSupportedLocales() as $key => $item) {
            $this->langs[] = $key;
        }
    }

    private function curl()
    {
        $curl = curl_init();

        curl_setopt_array($curl, [
            CURLOPT_URL => env('MOBIMATTER_API_URL'),
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => [
                'Content-Type: application/json',
                'api-key:'. env('MOBIMATTER_API_KEY'),
                'MerchantId:'. env('MOBIMATTER_API_MERCHANT_ID'),
                'Cookie: ARRAffinity=645e8f754b5c169bbf500aca97480ed8507d6dded47a9bdf4d4ce5a70e419436; ARRAffinitySameSite=645e8f754b5c169bbf500aca97480ed8507d6dded47a9bdf4d4ce5a70e419436'
            ],
        ]);

        $response = curl_exec($curl);
        curl_close($curl);
        return json_decode($response)->result;
    }

    private function provider($name, $logo)
    {
        $prov = Provider::where('slug', Str::slug($name))->get();
        switch($prov->count()){
            case '0':
                $info = getimagesize($logo);
                if (preg_match('{image/(.*)}is', $info['mime'], $p)) {
                }
                $src = "image" . $name .".".$p[1];
                $url = $logo;
                Storage::disk('public')->put("t" . DIRECTORY_SEPARATOR . $src, file_get_contents($url, true));
                $uploaded = new UploadedFile(public_path("storage" . DIRECTORY_SEPARATOR . "t" . DIRECTORY_SEPARATOR . $src), $src);
                $file = new File($uploaded, 'public');
                $attach = $file->load();

                if (Storage::disk('public')->exists("t/")) {
                    Storage::disk('public')->deleteDirectory("t/");
                }
                $provider = Provider::updateOrCreate([
                    'slug' => Str::slug($name)
                ],[
                    'name' => $name,
                    'attachment_id' => $attach->id,
                ]);
                break;
            default:
                $provider = Provider::where('slug', Str::slug($name))->first();
                break;
        }
        
        return $provider->id;
    }

    private function setRegion($regions)
    {
        ProductRegion::where('product_id', $this->product_id)->delete();
        foreach($regions as $item){
            if($item == 'Americas'){$item = 'America';}
            $region = Region::updateOrCreate(['slug' => Str::slug($item)],['name'=>$item,'action' => '1']);
            ProductRegion::updateOrCreate(['product_id' => $this->product_id, 'region_id' => $region->id]);
        }
        return;
    }
    private function setCountie($countries)
    {
        ProductCountie::where('product_id', $this->product_id)->delete();
        foreach($countries as $item){
            $countri = Country::where('two_letter_abbreviation', $item)->first();
            ProductCountie::updateOrCreate(['product_id' => $this->product_id, 'countri_id' => $countri->id]);
        }
        return;
    }
}
