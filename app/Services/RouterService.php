<?php
namespace App\Services;

use RuthgerIdema\UrlRewrite\Facades\UrlRewrite;

class RouterService
{
    /**
     * @param array $params
     * @return mixed|null[]  Returned values are in next order [category1, category2, category3, filters]
     */
    public function detectParameters(array $params):array {
        $return = ['', '', ''];

        $filterSelected = false;
        foreach ($params as $key => $param) {
            if (strpos($param, 'sort-') !== false) {
                $return[2] = $param;
                $filterSelected = true;
            } 
            elseif(!$filterSelected && !in_array($key, [2])) {
                $return[$key] = $param;
            }
        }   
        return $return;
    }
}