<?php

use Carbon\Carbon;

if (! function_exists('appName')) {
    /**
     * Helper to grab the application name.
     *
     * @return mixed
     */
    function appName()
    {
        return config('app.name', '"E-Sim"');
    }
}

if (! function_exists('carbon')) {
    /**
     * Create a new Carbon instance from a time.
     *
     * @param $time
     *
     * @return Carbon
     * @throws Exception
     */
    function carbon($time)
    {
        return new Carbon($time);
    }
}

if (! function_exists('homeRoute')) {
    /**
     * Return the route to the "home" page depending on authentication/authorization status.
     *
     * @return string
     */
    function homeRoute()
    {
        return 'index';
    }
}

if (! function_exists('clearCacheByArray')) {
    /**
     * @param array $keys
     * @return void
     * @throws \Exception
     */
    function clearCacheByArray(array $keys)
    {
        foreach ($keys as $key) {
            cache()->forget($key);
        }
    }
}

if (! function_exists('clearCacheByTags')) {
    /**
     * @param array $tags
     * @return void
     * @throws \Exception
     */
    function clearCacheByTags(array $tags)
    {
        cache()->tags($tags)->flush();
    }
}


if (! function_exists('goLkClass')) {
    /**
     * @return string
     */
    function goLkClass(): string
    {
        return auth()->check() ? 'showLK' : '';
    }
}


if (! function_exists('getLocalization')) {
    /**
     * @return string
     */
    function getLocalization()
    {
        $localization = '';
        $explode = explode('/', request()->path());
        if (isset($explode[0]) && in_array($explode[0], ['en', 'ro', 'ru'])) {
            $localization = $explode[0];
        }

        return $localization;
    }
}

if (! function_exists('getLocaleUrl')) {
    /**
     * @return string
     */
    function getLocaleUrl($lang)
    {
        $explode = array_filter(explode('/', request()->path()), function ($data) {
            return ! empty($data);
        });
        if (! empty($explode[0]) && in_array($explode[0], ['en', 'ro', 'ru'])) {
            array_shift($explode);
        }

        if ($lang == 'ro' || $lang == 'en') {
            array_unshift($explode, $lang);
        }

        return empty($explode) ? '/' : '/' . implode('/', $explode);
    }
}

if (! function_exists('isLocaleActive')) {
    /**
     * @param $lang
     * @return bool
     */
    function isLocaleActive($lang)
    {
        $locale = app()->getLocale();

        return $locale == $lang;
    }
}