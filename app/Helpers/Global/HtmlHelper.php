<?php

use App\Domains\Currency\Models\Currency;

if (! function_exists('activeClass')) {
    /**
     * Get the active class if the condition is not falsy.
     *
     * @param        $condition
     * @param string $activeClass
     * @param string $inactiveClass
     *
     * @return string
     */
    function activeClass($condition, $activeClass = 'active', $inactiveClass = '')
    {
        return $condition ? $activeClass : $inactiveClass;
    }
}

if (! function_exists('htmlLang')) {
    /**
     * Access the htmlLang helper.
     */
    function htmlLang()
    {
        return str_replace('_', '-', app()->getLocale());
    }
}

if(! function_exists('json_country')){
    function json_country(){
        $json = file_get_contents(public_path().'/country.json');
        $json = str_replace('&quot;','"',$json);
        return $json;
    }
}
if(! function_exists('json_currencies')){
    function json_currencies(){
        $currencies = Currency::where('active', 1)->get();
        $mas = [];
        foreach($currencies as $currency){
            $mas[$currency->name] = $currency->value;
        }
        return $mas;
    }
}
