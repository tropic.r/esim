<?php


namespace App\Domains\Page\Services;


use App\Domains\Page\Models\Page;
use App\Domains\Page\Models\PageTranslation;
use App\Exceptions\NoPageException;
use App\Services\BaseService;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;
use \RuthgerIdema\UrlRewrite\Facades\UrlRewrite;

class PageService extends BaseService
{
    /**
     * @var array
     */
    protected array $groupedIdsBySlug;

    public function __construct(Page $page)
    {
        $this->model = $page;
    }

    public function save(array $fields): self{

        $this->model->fill($fields);

        if (isset($fields['active'])) {
            $this->model->active = true;
        }
        else{
            $this->model->active = false;
        }

        $this->model->save();

        return $this;
    }

    public function saveTranslations(array $translations): self
    {

        foreach ($translations as $locale => $fields) {
            $entity = $this->model->translations->where('locale', $locale)
                ->first();

            if ($entity) {
                $fields['page_id'] = $this->model->id;
                $entity->fill($fields)->save();
            }
            else{
                $fields['page_id'] = $this->model->id;
                $fields['locale'] = $locale;
                PageTranslation::create($fields);
            }
        }
        return $this;
    }
}
