<?php
namespace App\Domains\Page\Models;


use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Orchid\Screen\AsSource;
use Orchid\Filters\Filterable;
use App;
use App\Domains\Page\Models\Traits\Relationship\PageRelationship;

class Page extends Model
{
    use HasFactory,
    PageRelationship,
    AsSource,
    Filterable;

    /**
     * fillable
     *
     * @var array
     */

    public $table = "page";
    
    protected $fillable = [
        "slug",
    ];

}
