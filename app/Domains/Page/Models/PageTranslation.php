<?php
namespace App\Domains\Page\Models;


use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Orchid\Screen\AsSource;
use Orchid\Filters\Filterable;
use App;

class PageTranslation extends Model
{
    use HasFactory,
    AsSource,
    Filterable;

    public $table = "page_translations";

    protected $fillable = [
        "page_id",
        "name",
        "locale",
        "meta_title",
        "meta_keywords",
        "meta_description",
        "meta_h1",
        "description"
    ];
}
