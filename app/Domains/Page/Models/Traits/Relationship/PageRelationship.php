<?php
namespace App\Domains\Page\Models\Traits\Relationship;

use App\Domains\Page\Models\Page;
use App\Domains\Page\Models\PageTranslation;


trait PageRelationship{
    public function translation()
    {
        return $this->hasOne(PageTranslation::class, 'page_id', 'id')
            ->where('locale', app()->getLocale());
    }
    
    public function translations()
    {
        return $this->hasMany(PageTranslation::class, 'page_id', 'id');
    }
}