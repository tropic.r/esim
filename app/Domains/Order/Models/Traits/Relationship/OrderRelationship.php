<?php
namespace App\Domains\Order\Models\Traits\Relationship;

use App\Domains\Product\Models\Product;
use App\Domains\User\Models\User;

trait OrderRelationship{
    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id')
            ->where('locale', app()->getLocale());
    }
    
    public function product()
    {
        return $this->hasOne(Product::class, 'id', 'product_id');
    }
}