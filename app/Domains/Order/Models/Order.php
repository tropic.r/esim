<?php
namespace App\Domains\Order\Models;


use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Orchid\Screen\AsSource;
use Orchid\Filters\Filterable;
use App;
use App\Domains\Order\Models\Traits\Relationship\OrderRelationship;

class Order extends Model
{
    use HasFactory,
    OrderRelationship,
    AsSource,
    Filterable;

    /**
     * fillable
     *
     * @var array
     */

    public $table = "order";
    
    protected $fillable = [
        "id",
        "orderId",
        "user_id",
        "product_id",
        "name",
        "email",
        "status",
        "currencyCode",
        "retailPrice",
        "data",
    ];

    protected $casts = [
        'data' => 'collection'
    ];

}
