<?php


namespace App\Domains\Faq\Services;


use App\Domains\Faq\Models\Faq;
use App\Domains\Faq\Models\FaqTranslation;
use App\Exceptions\NoPageException;
use App\Services\BaseService;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;
use \RuthgerIdema\UrlRewrite\Facades\UrlRewrite;

class FaqService extends BaseService
{
    /**
     * @var array
     */
    protected array $groupedIdsBySlug;

    public function __construct(Faq $faq)
    {
        $this->model = $faq;
    }

    public function save(array $fields): self{

        $this->model->fill($fields);

        if (isset($fields['active'])) {
            $this->model->active = true;
        }
        else{
            $this->model->active = false;
        }

        $this->model->save();

        return $this;
    }

    public function saveTranslations(array $translations): self
    {

        foreach ($translations as $locale => $fields) {
            $entity = $this->model->translations->where('locale', $locale)
                ->first();

            if ($entity) {
                $fields['faq_id'] = $this->model->id;
                $entity->fill($fields)->save();
            }
            else{
                $fields['faq_id'] = $this->model->id;
                $fields['locale'] = $locale;
                FaqTranslation::create($fields);
            }
        }
        return $this;
    }
}
