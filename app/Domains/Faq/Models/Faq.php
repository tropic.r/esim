<?php
namespace App\Domains\Faq\Models;


use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Orchid\Screen\AsSource;
use Orchid\Filters\Filterable;
use App;
use App\Domains\Faq\Models\Traits\Relationship\FaqRelationship;

class Faq extends Model
{
    use HasFactory,
    FaqRelationship,
    AsSource,
    Filterable;

    /**
     * fillable
     *
     * @var array
     */

    public $table = "faq";
    
    protected $fillable = [
        "slug",
        "faq_catalog_id",
    ];

}
