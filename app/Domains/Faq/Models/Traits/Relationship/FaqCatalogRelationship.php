<?php
namespace App\Domains\Faq\Models\Traits\Relationship;

use App\Domains\Faq\Models\Faq;
use App\Domains\Faq\Models\FaqCatalog;
use App\Domains\Faq\Models\FaqCatalogTranslation;


trait FaqCatalogRelationship{
    public function translation()
    {
        return $this->hasOne(FaqCatalogTranslation::class, 'faq_catalog_id', 'id')
            ->where('locale', app()->getLocale());
    }
    
    public function translations()
    {
        return $this->hasMany(FaqCatalogTranslation::class, 'faq_catalog_id', 'id');
    }
    public function faqs()
    {
        return $this->hasMany(Faq::class, 'faq_catalog_id', 'id');
    }
}