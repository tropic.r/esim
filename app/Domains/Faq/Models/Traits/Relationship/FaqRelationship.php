<?php
namespace App\Domains\Faq\Models\Traits\Relationship;

use App\Domains\Faq\Models\Faq;
use App\Domains\Faq\Models\FaqCatalog;
use App\Domains\Faq\Models\FaqTranslation;


trait FaqRelationship{
    public function translation()
    {
        return $this->hasOne(FaqTranslation::class, 'faq_id', 'id')
            ->where('locale', app()->getLocale());
    }
    
    public function translations()
    {
        return $this->hasMany(FaqTranslation::class, 'faq_id', 'id');
    }

    public function faq_catalog()
    {
        return $this->hasOne(FaqCatalog::class, 'id', 'faq_catalog_id');
    }
}