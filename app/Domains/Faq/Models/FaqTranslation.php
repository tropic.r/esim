<?php
namespace App\Domains\Faq\Models;


use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Orchid\Screen\AsSource;
use Orchid\Filters\Filterable;
use App;

class FaqTranslation extends Model
{
    use HasFactory,
    AsSource,
    Filterable;

    public $table = "faq_translations";

    protected $fillable = [
        "faq_id",
        "locale",
        "name",        
        "description",        
    ];
}
