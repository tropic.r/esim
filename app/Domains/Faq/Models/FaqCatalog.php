<?php
namespace App\Domains\Faq\Models;


use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Orchid\Screen\AsSource;
use Orchid\Filters\Filterable;
use App;
use App\Domains\Faq\Models\Traits\Relationship\FaqCatalogRelationship;

class FaqCatalog extends Model
{
    use HasFactory,
    FaqCatalogRelationship,
    AsSource,
    Filterable;

    /**
     * fillable
     *
     * @var array
     */

    public $table = "faq_catalog";
    
    protected $fillable = [
        "slug",
    ];

}
