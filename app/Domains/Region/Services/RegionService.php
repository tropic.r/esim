<?php

namespace App\Domains\Region\Services;

use App\Domains\Region\Models\Region;
use App\Services\BaseService;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use App\Exceptions\GeneralException;

/**
 * Class СurrencyService.
 */
class RegionService extends BaseService
{
    /**
     * СurrencyService constructor.
     *
     * @param  Region  $Region
     */
    public function __construct(Region $region)
    {
        $this->model = $region;
    }

	/**
     * @param  array  $data
     *
     * @return Region
     * @throws GeneralException
     * @throws \Throwable
     */
    public function store(array $data = []): Region
    {
        DB::beginTransaction();

        try {
            $region = $this->model::create([
                'name' => $data['region']['name'],
                'slug' => Str::slug($data['region']['slug']),
                'active' => isset($data['region']['active']) ? true : false
            ]);
        } catch (\Exception $e) {
            DB::rollBack();
            throw new GeneralException(__('There was a problem creating this Region. Please try again. '. $e->getMessage()));
        }

        DB::commit();

        return $region;
    }

    

    /**
     * @param  Region  $region
     * @param  array  $data
     *
     * @return User
     * @throws \Throwable
     */
    public function update(Region $region, array $data = []): Region
    {
        DB::beginTransaction();

        try {
            $region->update([
                'name' => $data['region']['name'],
                'slug' => Str::slug($data['region']['slug']),
                'active' => isset($data['region']['active']) ? true : false
            ]);
        } catch (\Exception $e) {
            DB::rollBack();

            throw new GeneralException(__('There was a problem updating this region. Please try again. '. $e->getMessage()));
        }

        DB::commit();

        return $region;
    }

    /**
     * @param  Region  $region
     *
     * @return Region
     * @throws GeneralException
     */
    public function delete(Region $region): Region
    {
        if ($this->deleteById($region->id)) {
            return $region;
        }

        throw new GeneralException('There was a problem deleting this region. Please try again.');
    }
}
