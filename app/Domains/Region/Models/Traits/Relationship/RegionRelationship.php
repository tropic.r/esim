<?php
namespace App\Domains\Region\Models\Traits\Relationship;

use App\Domains\Product\Models\ProductRegion;

trait RegionRelationship {
    public function products()
    {
        return $this->hasMany(ProductRegion::class, 'region_id', 'id');
    }
}