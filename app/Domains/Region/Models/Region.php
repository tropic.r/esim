<?php

namespace App\Domains\Region\Models;

use App\Domains\Region\Models\Traits\Relationship\RegionRelationship;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Orchid\Screen\AsSource;

class Region extends Model
{
    use HasFactory,
    RegionRelationship,
    AsSource;

    public $table = 'regions';

    protected $fillable = [
        "name",
        "slug",
        "active"
    ];
    
}
