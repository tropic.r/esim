<?php

namespace App\Domains\Category\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CategoriesTranslation extends Model
{
    use HasFactory;

    protected $fillable = [
        "category_id",
        "active",
        "locale",
        "name",
        "description",
        "meta_title",
        "meta_keywords",
        "meta_h1"
    ];

}
