<?php

namespace App\Domains\Category\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Orchid\Screen\AsSource;

class Category extends Model
{
    use HasFactory,
    AsSource;

    public $table = 'categoryes';

    protected $fillable = [
        "parent_id",
        "slug",
        "active",
    ];

}
