<?php
namespace App\Domains\Product\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class OrchidProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'product.slug' => 'required|max:255',
            'product.active' => '',
            'product.provider_id' => '',
            'product.article' => '',
            'product.*' => '', 
            'translations.*.name' => 'required|max:255',
            'translations.*.*' => '',
            'product.country' => 'required|min:1',
            'product.regions' => 'required|min:1',
        ];
    }
}
