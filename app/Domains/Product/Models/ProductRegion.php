<?php
namespace App\Domains\Product\Models;

use App\Domains\Product\Models\Traits\Relationship\ProductRegionRelationship;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Orchid\Screen\AsSource;
use Orchid\Screen\Layouts\Modal;

class ProductRegion extends Model
{
    use HasFactory,
    ProductRegionRelationship,
    AsSource;

    public $table = 'products_region';

    protected $fillable = [
        "product_id",
        "region_id"
    ]; 

}