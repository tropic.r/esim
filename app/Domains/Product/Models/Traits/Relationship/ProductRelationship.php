<?php
namespace App\Domains\Product\Models\Traits\Relationship;

use App\Domains\Product\Models\Product;
use App\Domains\Product\Models\ProductCountie;
use App\Domains\Product\Models\ProductRegion;
use App\Domains\Product\Models\ProductTranslation;
use App\Domains\Provider\Models\Provider;

trait ProductRelationship{
    public function translation()
    {
        return $this->hasOne(ProductTranslation::class, 'product_id', 'id')
            ->where('locale', app()->getLocale());
    }
    
    public function translations()
    {
        return $this->hasMany(ProductTranslation::class, 'product_id', 'id');
    }

    public function regions()
    {
        return $this->hasMany(ProductRegion::class, 'product_id', 'id');
    }

    public function counties()
    {
        return $this->hasMany(ProductCountie::class, 'product_id', 'id');
    }

    public function provider()
    {
        return $this->hasOne(Provider::class, 'id', 'provider_id');
    }
}