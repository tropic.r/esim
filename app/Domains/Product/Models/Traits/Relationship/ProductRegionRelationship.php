<?php
namespace App\Domains\Product\Models\Traits\Relationship;

use App\Domains\Product\Models\Product;
use App\Domains\Region\Models\Region;

trait ProductRegionRelationship{

    public function region()
    {
        return $this->hasOne(Region::class, 'id', 'countri_id');
    }

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id', 'id');
    }
}