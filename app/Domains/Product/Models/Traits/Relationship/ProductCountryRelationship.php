<?php
namespace App\Domains\Product\Models\Traits\Relationship;

use App\Domains\Product\Models\Product;
use App\Domains\Country\Models\Country;

trait ProductCountryRelationship{

    public function country()
    {
        return $this->hasOne(Country::class, 'id', 'countri_id');
    }

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id', 'id');
    }
}