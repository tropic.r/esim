<?php
namespace App\Domains\Product\Models;

use App\Domains\Product\Models\Traits\Relationship\ProductCountryRelationship;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Orchid\Screen\AsSource;
use Orchid\Screen\Layouts\Modal;

class ProductCountie extends Model
{
    use HasFactory,
    ProductCountryRelationship,
    AsSource;

    public $table = 'products_countrie';

    protected $fillable = [
        "product_id",
        "countri_id"
    ]; 

}