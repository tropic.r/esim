<?php
namespace App\Domains\Product\Models;


use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Orchid\Screen\AsSource;
use Orchid\Filters\Filterable;
use App;
use App\Domains\Product\Models\Traits\Relationship\ProductRelationship;

class Product extends Model
{
    use HasFactory,
    ProductRelationship,
    AsSource,
    Filterable;

    /**
     * fillable
     *
     * @var array
     */
    protected $fillable = [
        "active",
        "slug",
        "uniqueId",
        "merchantId",
        "productId",
        "i_product",
        "rank",
        "category",
        "productCategory",
        "provider_id",
        "retailPrice",
        "wholesalePrice",
        "currencyCode",
        "price_gb",
        "plan_data_limit",
        "plan_data_unit",
        "plan_validity",
        "tags",
        "data",
    ];

    protected $casts = [
        'tags' => 'array',
        'data' => 'array',
    ];
}
