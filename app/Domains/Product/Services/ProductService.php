<?php
namespace App\Domains\Product\Services;

use App\Domains\Product\Models\Product;
use App\Exceptions\NoPageException;
use App\Services\BaseService;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

class ProductService extends BaseService
{
    /**
     * __construct
     *
     * @param  Product $product
     * @return void
     */
    public function __construct(Product $product) {
        $this->model = $product;
    }

    /**
     * whereSlug
     *
     * @param  string $slug
     * @return Product
     * @throws NoPageException
     */
    public function whereSlug(string $slug) : Product
    {
        $model = $this->where('slug', $slug)->get()->first();
        if($model === null){
            throw new NoPageException('there is no product with slug like: ' . $slug);
        }

        return $model;
    }


}
