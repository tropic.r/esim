<?php
namespace App\Domains\Product\Services;

use App\Domains\Product\Models\ProductCountie;
use App\Domains\Product\Models\ProductRegion;
use App\Domains\Product\Services\ProductService;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Log;
use Orchid\Attachment\Models\Attachment;

class ProductOrchidService extends ProductService
{

    protected $group = null;

    /**
     * Сохраняет личные поля модели
     *
     * @param  array $fields
     * @return self
     */
    public function save(array $fields): self
    {
        $this->model->fill($fields)->save();
        return $this;
    }

    /**
     * Установка группы товаров
     *
     * @param ProductsGroup $group
     * @return self
     */
    // public function setGroup(ProductsGroup $group): self
    // {
    //     $this->group = $group;
    //     return $this;
    // }

    /**
     * Принимает двумерный массив локаль => поля и сохраняет каждую. 
     *
     * @param  mixed $fields
     * @return void
     */
    public function saveTranslations(array $fields)
    {
        foreach ($fields as $locale => $translation) {
            $this->saveTranslation($locale, $translation);
        }
        return $this;
    }


    /**
     * saveTranslation
     *
     * @param  string $locale
     * @param  array $translations
     * @return self
     */
    public function saveTranslation(string $locale, array $fields): self
    {
        $this->model->translations->where('locale', $locale)->first()
            ->fill($fields)->save();
        return $this;
    }

    public function saveCountrys(array $country){
        ProductCountie::where('product_id', $this->model->id)->delete();
        foreach($country as $countri_id){
            ProductCountie::updateOrCreate([
                'product_id' => $this->model->id,
                'countri_id' => $$countri_id
            ],[]);
        }
    }
    public function saveRegions(array $regions){
        ProductRegion::where('product_id', $this->model->id)->delete();
        foreach($regions as $region_id ){
            ProductRegion::updateOrCreate([
                'product_id' => $this->model->id,
                'countri_id' => $$region_id
            ],[]);
        }
    }


}
