<?php

namespace App\Domains\Provider\Models;

use App\Domains\Provider\Models\Traits\Relationship\ProviderRelationship;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Orchid\Screen\AsSource;

class Provider extends Model
{
    use HasFactory,
    ProviderRelationship,
    AsSource;

    public $table = 'provider';

    protected $fillable = [
        "slug",
        "name",
        "attachment_id"
    ];   
}