<?php
namespace App\Domains\Provider\Models\Traits\Relationship;

use App\Domains\Product\Models\ProductCountie;
use Orchid\Attachment\Models\Attachment;

trait ProviderRelationship{

    public function attachment()
    {
        return $this->hasOne(Attachment::class, 'id', 'attachment_id');
    }
}