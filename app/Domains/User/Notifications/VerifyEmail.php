<?php
namespace App\Domains\User\Notifications;

use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\URL;

/**
 * Class VerifyEmail.
 */
class VerifyEmail extends Notification
{
    /**
     * Get the notification's channels.
     *
     * @param  mixed  $notifiable
     * @return array|string
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Build the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject("Ви зареєстровані у магазині")
            ->line("Дякуємо «".$notifiable->name."», Ви успішно зареєстровані в інтернет-магазині Grandstep.com.ua.")
            ->line(__('Login: '). $notifiable->email)
            ->line(__('Password: '). $notifiable->not_code_pass)
            // ->action(__('Verify E-mail Address'), $this->verificationUrl($notifiable))
            ->line("Тепер вам доступна історія замовлень, їх статуси виконання, профілі доставки та багато іншого! Будь-ласка, дозаповніть свої дані в розділі \"Мої дані\" в Особистому кабінеті.")
            ->line("Вдалих покупок! ");
    }

    /**
     * Get the verification URL for the given notifiable.
     *
     * @param  mixed  $notifiable
     * @return string
     */
    protected function verificationUrl($notifiable)
    {
        return URL::temporarySignedRoute(
            'verification.verify',
            Carbon::now()->addMinutes(Config::get('auth.verification.expire', 60)),
            [
                'id' => $notifiable->getKey(),
                'hash' => sha1($notifiable->getEmailForVerification()),
            ]
        );
    }
}
