<?php

namespace App\Domains\Country\Services;

use App\Domains\Country\Models\Country;
use App\Services\BaseService;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use App\Exceptions\GeneralException;

/**
 * Class СurrencyService.
 */
class CountryService extends BaseService
{
    /**
     * СurrencyService constructor.
     *
     * @param  Country  $country
     */
    public function __construct(Country $country)
    {
        $this->model = $country;
    }

	/**
     * @param  array  $data
     *
     * @return Country
     * @throws GeneralException
     * @throws \Throwable
     */
    public function store(array $data = []): Country
    {
        DB::beginTransaction();

        try {
            $country = $this->model::create([
                'name' => $data['country']['name'],
                'slug' => Str::slug($data['country']['slug']),
                'active' => isset($data['country']['active']) ? true : false
            ]);
        } catch (\Exception $e) {
            DB::rollBack();
            throw new GeneralException(__('There was a problem creating this Country. Please try again. '. $e->getMessage()));
        }

        DB::commit();

        return $country;
    }

    

    /**
     * @param  Country  $country
     * @param  array  $data
     *
     * @return User
     * @throws \Throwable
     */
    public function update(Country $country, array $data = []): Country
    {
        DB::beginTransaction();

        try {
            $country->update([
                'name' => $data['country']['name'],
                'slug' => Str::slug($data['country']['slug']),
                'active' => isset($data['country']['active']) ? true : false
            ]);
        } catch (\Exception $e) {
            DB::rollBack();

            throw new GeneralException(__('There was a problem updating this country. Please try again. '. $e->getMessage()));
        }

        DB::commit();

        return $country;
    }

    /**
     * @param  Country  $country
     *
     * @return Country
     * @throws GeneralException
     */
    public function delete(Country $country): Country
    {
        if ($this->deleteById($country->id)) {
            return $country;
        }

        throw new GeneralException('There was a problem deleting this Country. Please try again.');
    }
}
