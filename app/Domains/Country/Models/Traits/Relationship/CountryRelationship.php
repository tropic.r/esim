<?php
namespace App\Domains\Country\Models\Traits\Relationship;

use App\Domains\Product\Models\ProductCountie;

trait CountryRelationship {
    public function products()
    {
        return $this->hasMany(ProductCountie::class, 'countri_id', 'id');
    }
}