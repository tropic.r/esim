<?php

namespace App\Domains\Country\Models;

use App\Domains\Country\Models\Traits\Relationship\CountryRelationship;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Orchid\Screen\AsSource;

class Country extends Model
{
    use HasFactory,
    CountryRelationship,
    AsSource;

    public $table = 'countries';

    protected $fillable = [
        "slug",
        "calling_code",
        "currency",
        "name",
        "three_letter_abbreviation",
        "two_letter_abbreviation",
        "objectID",
        "_highlightResult",
        "isOfferCountry",
        "isTopUpCountry",
        "active"
    ];   
}