<?php


namespace App\Domains\MetaSeo\Models;


use App\Domains\MetaSeo\Models\Traits\MetaSeoRelationship;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Orchid\Screen\AsSource;

class MetaSeo extends Model
{
    use HasFactory,
        MetaSeoRelationship,
        AsSource;

    public $table = 'meta_seo';

    protected $fillable = [
        "id",
        "slug",
        "type",
        "meta_index",
        "active"
    ];
}
