<?php
namespace App\Domains\MetaSeo\Models;

use Database\Factories\MetaSeoTranslationFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MetaSeoTranslation extends Model
{
    use HasFactory;

    public $table = 'meta_seo_translations';

    protected $fillable = [
        "id",
        "meta_seo_id",
        "locale",
        "description",
        "meta_title",
        "meta_description",
        "meta_keywords",
        "meta_h1",
    ];

    protected static function newFactory()
    {
        return MetaSeoTranslationFactory::new();
    }
}
