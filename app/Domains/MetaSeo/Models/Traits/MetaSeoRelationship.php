<?php
namespace App\Domains\MetaSeo\Models\Traits;

use App\Domains\MetaSeo\Models\MetaSeoTranslation;
use App\Domains\MetaSeo\Models\MetaSeo;

trait MetaSeoRelationship {

    public function translations()
    {
        return $this->hasMany(MetaSeoTranslation::class, 'meta_seo_id', 'id');
    }

    public function translation()
    {
        return $this->hasOne(MetaSeoTranslation::class, 'meta_seo_id', 'id')->where('locale', app()->getLocale());
    }
}
