<?php


namespace App\Domains\MetaSeo\Services;


use App\Domains\MetaSeo\Models\MetaSeo;
use App\Domains\MetaSeo\Models\MetaSeoTranslation;
use App\Exceptions\NoPageException;
use App\Services\BaseService;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;
use \RuthgerIdema\UrlRewrite\Facades\UrlRewrite;

class MetaSeoService extends BaseService
{
    /**
     * @var array
     */
    protected array $groupedIdsBySlug;

    public function __construct(MetaSeo $metaSeo)
    {
        $this->model = $metaSeo;
    }

    public function save(array $fields): self{

        $this->model->fill($fields);

        if (isset($fields['active'])) {
            $this->model->active = true;
        }
        else{
            $this->model->active = false;
        }

        $this->model->save();

        return $this;
    }

    public function saveTranslations(array $translations): self
    {

        foreach ($translations as $locale => $fields) {
            $entity = $this->model->translations->where('locale', $locale)
                ->first();

            if ($entity) {
                $fields['meta_seo_id'] = $this->model->id;
                $entity->fill($fields)->save();
            }
            else{
                $fields['meta_seo_id'] = $this->model->id;
                $fields['locale'] = $locale;
                MetaSeoTranslation::create($fields);
            }
        }
        return $this;
    }
}
