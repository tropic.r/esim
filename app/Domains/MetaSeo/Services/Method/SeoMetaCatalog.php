<?php

namespace App\Domains\MetaSeo\Services\Method;

use App\Domains\Category\Models\Category;
use App\Domains\MetaSeo\Models\MetaSeo;
use App\Services\RouterService;

class SeoMetaCatalog implements SeoMetaInterface
{
    protected $routeServis;
    protected $paths = '';
    protected $lang;
    protected $category;

    public function __construct()
    {
        $paths = explode('/', request()->path());
        $this->lang = getLocalization();
        foreach ($paths as $key => $path) {
            if ($path == $this->lang) {
                unset($paths[$key]);
            } else {
                $this->paths .= '/'.$path;
            }
        }
       
        $meta_seo = MetaSeo::where('slug', $this->paths)
                            ->where('type', 'catalog')
                            ->where('active', '1')
                            ->with(['translation'])->first();

        $this->meta = $meta_seo;
    }

    public function get(): array
    {
        $meta = [];
        if($this->meta && $this->meta->translation && $this->meta->translation->meta_title){
            $meta['title'] = $this->meta->translation->meta_title;
        } else {
            $meta['title'] = '';
        }
        if($this->meta && $this->meta->translation && $this->meta->translation->meta_description){
            $meta['description'] = $this->meta->translation->meta_description;
        } else {
            $meta['description'] = '';
        }

        if($this->meta && $this->meta->translation && $this->meta->translation->meta_keywords){
            $meta['meta_keywords'] = $this->meta->translation->meta_keywords;
        } else {
            $meta['meta_keywords'] = '';
        }

        if($this->meta && !$this->meta->meta_index){
            $meta['robots'] = 'noindex/nofollow';
        } else {
            $meta['robots'] = '';
        }


        return $meta;
    }
}
