<?php

namespace App\Domains\MetaSeo\Services\Method;

use App\Domains\Category\Models\Category;
use App\Domains\MetaSeo\Models\MetaSeo;
use App\Domains\MetaSeo\Services\MetaSeoService;
use App\Services\RouterService;

class SeoMetaFilter implements SeoMetaInterface
{
    protected $paths = [];
    protected $lang;
    protected $category;

    public function __construct()
    {
        $this->lang = getLocalization();
        $path = str_replace("ru/", "", request()->path());
        
        $params = MetaSeo::where('new_slug', 'like', '%' . $path . '%')->where('type', 'filter')->with('translation')->get()->first();
        if ($params) {
            $this->category = $params;
        } else {
            $this->category = null;
        }
    }

    public function get(): array
    {
        $meta = [];
        $meta = [];
        if ($this->category && $this->category->translation->meta_title) {
            $meta['meta_title'] = $this->category->translation->meta_title;
        }
        elseif($this->category && $this->category->translation->name){
            $meta['meta_title'] = $this->category->translation->name;
        } 
        else {
            $meta['meta_title'] = __('product.catalog');
        }

        if ($this->category && $this->category->translation->meta_description) {
            $meta['meta_description'] = $this->category->translation->meta_description;
        } else {
            $meta['meta_description'] = '';
        }
        if ($this->category && $this->category->translation->meta_h1) {
            $meta['meta_h1'] = $this->category->translation->meta_h1;
        } else {
            $meta['meta_h1'] = '';
        }
        if ($this->category && isset($this->category->translation->meta_index)) {
            $meta['meta_index'] = 'noindex/nofollow';
        } else {
            $meta['meta_index'] = '';
        }

        if ($this->category && isset($this->category->translation->description) && strlen($this->category->translation->description) > 30) {
            $meta['description'] = $this->category->translation->description;
        } else {
            $meta['description'] = '';
        }
        
        $hasOtherPageOfPaginate = collect($this->paths)->filter(function($value, $key){
            return substr_count($value, 'page-') > 0;
        })->count();
        if($hasOtherPageOfPaginate > 0){
            $meta['description'] = '';
        }

        return $meta;
    }
}
