<?php

namespace App\Domains\MetaSeo\Services\Method;


interface SeoMetaInterface
{
    public function get(): array;
}
