<?php


namespace App\Domains\MetaSeo\Services\Method;


use App\Domains\MetaSeo\Models\MetaSeo;
use App\Domains\MetaSeo\Models\Traits\MetaSeoRelationship;
use App\Services\RouterService;
use Illuminate\Support\Facades\DB;

class SeoMetaPage implements SeoMetaInterface{
    protected $routeServis;
    protected $paths = '';
    protected $meta;

    public function __construct(){
        $paths = explode('/', request()->path());
        $this->lang = getLocalization();

        foreach ($paths as $key => $path) {
            if ($path == $this->lang) {
                unset($paths[$key]);
            } else {
                $this->paths .= $path;
            }
        }

        $path = $this->paths;
        $meta_seo = MetaSeo::where('type', 'page')->where('active', 1)->where(function ($query) use ($path) {
            $query->where('slug', $path)
            ->orWhere('slug', $path.'/')
            ->orWhere('slug', '/'.$path)
            ->orWhere('slug', '/'.$path.'/')
            ->orWhere('slug', 'like', $path);
        })->with(['translation']);
                
        $this->meta = $meta_seo->get()->first();
    }

    public function get(): array
    {
        $meta = [];
        if($this->meta && $this->meta->translation && $this->meta->translation->meta_title){
            $meta['meta_title'] = $this->meta->translation->meta_title;
        } else {
            $meta['meta_title'] = '';
        }
        if($this->meta && $this->meta->translation && $this->meta->translation->meta_description){
            $meta['meta_description'] = $this->meta->translation->meta_description;
        } else {
            $meta['meta_description'] = '';
        }
       
        if($this->meta && $this->meta->meta_index){
            $meta['robots'] = 'noindex/nofollow';
        } else {
            $meta['robots'] = '';
        }
        
        return $meta;
    }
}
