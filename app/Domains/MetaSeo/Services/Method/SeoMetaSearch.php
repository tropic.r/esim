<?php

namespace App\Domains\MetaSeo\Services\Method;

use App\Domains\Category\Services\CatalogFilterService;
use App\Services\RouterService;
use Illuminate\Support\Facades\Request;

class SeoMetaSearch implements SeoMetaInterface
{
    protected $paths = [];
    protected $lang;
    protected $filters;

    public function __construct()
    {
        $this->lang = getLocalization();

        $filters = '';
        foreach(explode('/', Request::url()) as $part){
            if(substr_count($part, 'text=') > 0){
                $filters = $part;
            }
        }

        $catalogFilterService = new CatalogFilterService();
        $this->filters = $catalogFilterService->parseFilters($filters);
    }

    public function get(): array
    {
        $meta = [];
        if ($this->filters) {
            $meta['meta_h1'] = __('product.search.prefix').': '.urldecode($this->filters['text']);
            $meta['meta_title'] = __('product.search.prefix').' '.urldecode($this->filters['text']).' | ЕКОЖ';
            $meta['meta_description'] = __('product.search.prefix').' '.urldecode($this->filters['text']);
            $meta['meta_index'] = 'noindex/nofollow';
            $meta['description'] = '';

        } else {
            $meta['meta_title'] = '';
            $meta['meta_h1'] = '';
            $meta['meta_description'] = '';
            $meta['meta_index'] = 'noindex/nofollow';
            $meta['description'] = '';
        }
        
        return $meta;
    }
}
