<?php
namespace App\Domains\MetaSeo\Services;

use Illuminate\Support\Facades\Log;

class CanonicalService
{
    public function __construct()
    {
        
    }
    
    public function render()
    {
        $return = url()->full();
        return $return;
    }
}
