<?php
namespace App\Domains\MetaSeo\Services;

use App\Domains\MetaSeo\Services\Method\SeoMetaCatalog;
use App\Domains\MetaSeo\Services\Method\SeoMetaFilter;
use App\Domains\MetaSeo\Services\Method\SeoMetaStatic;
use App\Domains\MetaSeo\Services\Method\SeoMetaPage;
use App\Domains\MetaSeo\Services\Method\SeoMetaInterface;
use App\Domains\MetaSeo\Services\Method\SeoMetaSearch;

final class MetaSeoFactory
{
    public static function init(string $type): SeoMetaInterface{
        switch ($type):
            case 'catalog':
                    return new SeoMetaCatalog();
                break;
            case 'page':
                    return new SeoMetaPage();
                break;    
            case 'filter':
                    return new SeoMetaFilter();
                break;
            case 'search':
                    return new SeoMetaSearch();
                break;
            case 'static':
                    return new SeoMetaStatic();
                break;
        endswitch;
    }
}
