<?php

namespace App\Domains\Currency\Http\Controllers\Api;

use App\Domains\Currency\Models\Currency;
use App\Http\Controllers\Api\BaseController;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Validator;

class CurrencyController extends BaseController {

    public function import(Request $request){
        $data = json_decode($request->getContent(), true);

        if (! empty($data)) {
            $sourceFileName = 'currencies.json';
            $fileFullPath = 'app/tmp'; // Соответствует storage/app/public/

            Storage::disk('local')->makeDirectory('tmp');

            $file = storage_path($fileFullPath.'/'.$sourceFileName);

            if (file_exists($file)) {
                unlink($file);
            }

            file_put_contents($file, $request->getContent());
        }

        $rules = [
            'currencies' => 'required|array|min:1',
            'currencies.*.name' => 'required|max:255',
            'currencies.*.code' => 'required|max:3',
            'currencies.*.value' => 'required',
            'currencies.*.active' => 'required'
        ];
        $validator = Validator::make($data, $rules);
        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }
        foreach($data['currencies'] as $item){
            Currency::updateOrCreate([
                'code' => $item['code']
            ],[
                'name' => $item['name'],
                'value' => (float)$item['value'],
                'active' => $item['active']
            ]);
        }

        return $this->sendResponse($request->all(), 'Items retrieved successfully.');
    }
}
