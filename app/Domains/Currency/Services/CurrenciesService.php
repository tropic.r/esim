<?php

namespace App\Domains\Currency\Services;


use App\Domains\Currency\Models\Currency;
use App\Exceptions\NoPageException;
use App\Services\BaseService;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;



class CurrenciesService extends BaseService
{
    /**
     * __construct
     *
     * @param  Currency $currency
     * @return void
     */
    public function __construct(Currency $currency)
    {
        $this->model = $currency;
    }

    public function save(array $fields): self
    {
        $this->model->fill($fields['currencies']);

        if (isset($fields['currencies']['active'])) $this->model->active = true;
        else $this->model->active = false;

        $this->model->save();

        return $this;
    }
}
