<?php

namespace App\Domains\Currency\Models;

use App\Domains\Currency\Models\Traits\Attribute\CurrencyAttribute;
use Database\Factories\CurrencyFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Orchid\Screen\AsSource;

class Currency extends Model
{
    use HasFactory,
        CurrencyAttribute,
        AsSource;

    public $table = 'currencies';

    protected $fillable = [
        "name",
        "code",
        "iso",
        "value",
        "active"
    ];

    protected static function newFactory()
    {
        return CurrencyFactory::new();
    }
}