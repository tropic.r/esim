<?php

namespace App\Providers;

use App\Domains\Currency\Models\Currency;
use App\Domains\Product\Models\Product;
use App\Domains\Provider\Models\Provider;
use Illuminate\Support\ServiceProvider;
use Cache;
use File;
use Illuminate\Support\Facades\DB;
use Orchid\Attachment\Models\Attachment;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer(['includes.popular_product'], function ($view) {
            $view->with('products', Product::where('active', 1)->whereIn('category', [1, 2])->orderBy('rank', 'desc')->limit(6)->get());
        });

        view()->composer(['includes.header_desktop'], function ($view) {
            $view->with('currencys', Currency::where('active', 1)->orderBy('id', 'asc')->get());
        });

        view()->composer(['includes.topup_product'], function ($view) {
            $view->with('products', Product::where('active', 1)->where('category', 4)->orderBy('rank', 'desc')->limit(6)->get());
        });

        view()->composer(['includes.topup_providers'], function ($view) {
            $view->with('providers', Provider::select('provider.*')->leftJoin('products', function($join) {
                $join->on('provider.id', '=', 'products.provider_id');
              })->where('products.category', '4')->orderBy('id', 'asc')->limit(3)->distinct()->get());
        });
    }

    private function phpTranslations($locale)
    {
        $path = resource_path("lang/$locale");

        return collect(File::allFiles($path))->flatMap(function ($file) use ($locale) {
            $key = ($translation = $file->getBasename('.php'));

            return [$key => trans($translation, [], $locale)];
        });
    }

    private function jsonTranslations($locale)
    {
        $path = resource_path("lang/$locale.json");

        if (is_string($path) && is_readable($path)) {
            return json_decode(file_get_contents($path), true);
        }

        return [];
    }
}
