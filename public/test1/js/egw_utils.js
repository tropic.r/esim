
/* Genearate random number */
function getRandom(min, max){return Math.floor(Math.random()*(max-min)+min);}
/* Genearate transaction order id */
function generateOrderId(){var n = getRandom(1,999999)+"";for(var i=n.length;i<6;i++) n = '0' + n;return n;}
/* Function to generate timestamp in the format: YYYYMMDDhhmmss  */
function getTimeStamp(){var now = new Date();return ("" + now.getUTCFullYear() +(((now.getUTCMonth() + 1) < 10)?("0" + (now.getUTCMonth() + 1)):(now.getUTCMonth() + 1))+((now.getUTCDate() < 10)?("0" + now.getUTCDate()):now.getUTCDate())+((now.getUTCHours() < 10)? ("0" + now.getUTCHours()):(now.getUTCHours()))+((now.getUTCMinutes() < 10)? ("0" + now.getUTCMinutes()):(now.getUTCMinutes()))+((now.getUTCSeconds() < 10)?("0" + now.getUTCSeconds()):(now.getUTCSeconds())) );}
/* Return length in bytes */
function getLengthInBytes(str){return (new TextEncoder('utf-8').encode(str)).length;}


