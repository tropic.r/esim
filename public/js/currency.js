 $(window).on('load', function () {
    var curnyValfrmSesinStrg = localStorage.getItem("currencyChageVal");
    if (curnyValfrmSesinStrg != '' && curnyValfrmSesinStrg != null && curnyValfrmSesinStrg != undefined) {
        setTimeout(currencyConverion(counterElement, curnyValfrmSesinStrg, elementId), 5000);
        $('.multi-currency-select').find('option[value="' + curnyValfrmSesinStrg + '"]').attr('selected', 'selected');
    }
    $('.multi-currency-select').change(function () {
        var option = $(this).find('option:selected');
        var text = $(this).find('option:selected').text()
        var val = $('.multi-currency-select').val();
        localStorage.setItem("currencyChageVal", val);
        var $aux = $('<select/>').append($('<option/>').text(text))
        $(this).after($aux)
        $(this).width($aux.width())
        $aux.remove();
        $('.currency-text').css('display', 'block');
        if (val == 'USD') {
            $('.currency-text').hide();
        } else {
            currencyConverion(counterElement, val, elementId);
        }
    });
});

function currencyConverion(counterElement, val, elementId) {
    var actualAmount = '';
    var rate = '';
    var currencyAmount = '';
    var currentRate = sessionStorage.getItem('currencyChageVal');
    if (currentRate != null && currentRate != undefined) {
        rate = window._currencies.currentRate;
    } else {
        rate = window._currencies.val;
    }
    alert(rate);
    for (var i = 1; i <= counterElement; i++) {
        actualAmount = $("#" + elementId + "-" + i).text();
        actualAmount = actualAmount.substring(4);
        var firstValue = 1;
        if (rate != "error") {
            var currencyObj = JSON.parse(rate);
            firstValue = currencyObj[Object.keys(currencyObj)[0]];
        } else {
            firstValue = 1;
            val = "USD";
            $('.multi-currency-select').find('option[value="' + val + '"]').attr('selected', 'selected')
        }

        currencyAmount = firstValue * actualAmount;
        $("#currencyAmountElement-" + i).empty();
        $("#currencyAmountElement-" + i).append(`<strong>(${val} ${currencyAmount.toFixed(2)})</strong>`);
        $('#currencyAmountElement-' + i).show();
        currencyAmount = 0;
        if (val == "USD") {
            $('.currency-text').hide();
        }
    }
}