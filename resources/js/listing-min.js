var countries = "";
$(document).ready(function () {
    countries =
        '[
            {
                "calling_code":"93",
                "flag":"https://mobimatter.com/assets/flag/flag-AF.png",
                "currency":"AFN",
                "name":"Afghanistan",
                "three_letter_abbreviation":"AFG",
                "two_letter_abbreviation":"AF",
                "objectID":"103587692",
                "_highlightResult":null,
                "isOfferCountry":true,
                "isTopUpCountry":true
            },
            {
                "calling_code":"355",
                "flag":"https://mobimatter.com/assets/flag/flag-AL.png",
                "currency":"ALL",
                "name":"Albania",
                "three_letter_abbreviation":"ALB",
                "two_letter_abbreviation":"AL",
                "objectID":"103587702",
                "_highlightResult":null,
                "isOfferCountry":true,
                "isTopUpCountry":true
            },
            {
                "calling_code":"213",
                "flag":"https://mobimatter.com/assets/flag/flag-DZ.png",
                "currency":"DZD",
                "name":"Algeria",
                "three_letter_abbreviation":"DZA",
                "two_letter_abbreviation":"DZ",
                "objectID":"103587712",
                "_highlightResult":null,
                "isOfferCountry":true,
                "isTopUpCountry":true
            },
            {
                "calling_code":"1684",
                "flag":"https://mobimatter.com/assets/flag/flag-AS.png",
                "currency":"USD",
                "name":"American Samoa",
                "three_letter_abbreviation":"ASM",
                "two_letter_abbreviation":"AS",
                "objectID":"103587722",
                "_highlightResult":null,
                "isTopUpCountry":true
            },
            {
                "calling_code":"376",
                "flag":"https://mobimatter.com/assets/flag/flag-AD.png",
                "currency":"EUR",
                "name":"Andorra",
                "three_letter_abbreviation":"AND",
                "two_letter_abbreviation":"AD",
                "objectID":"103587732",
                "_highlightResult":null,
                "isOfferCountry":true
            },
            {
                "calling_code":"244",
                "flag":"https://mobimatter.com/assets/flag/flag-AO.png","currency":"AOA","name":"Angola","three_letter_abbreviation":"AGO","two_letter_abbreviation":"AO","objectID":"103587742","_highlightResult":null,"isTopUpCountry":true},{"calling_code":"1264","flag":"https://mobimatter.com/assets/flag/flag-AI.png","currency":"XCD","name":"Anguilla","three_letter_abbreviation":"AIA","two_letter_abbreviation":"AI","objectID":"103587752","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"flag":"https://mobimatter.com/assets/flag/flag-AQ.png","name":"Antarctica","three_letter_abbreviation":"ATA","two_letter_abbreviation":"AQ","calling_code":"672","objectID":"103587762","_highlightResult":null},{"calling_code":"1268","flag":"https://mobimatter.com/assets/flag/flag-AG.png","currency":"XCD","name":"Antigua and Barbuda","three_letter_abbreviation":"ATG","two_letter_abbreviation":"AG","objectID":"103587772","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"54","flag":"https://mobimatter.com/assets/flag/flag-AR.png","currency":"ARS","name":"Argentina","three_letter_abbreviation":"ARG","two_letter_abbreviation":"AR","objectID":"103587782","_highlightResult":null,"isTopUpCountry":true},{"calling_code":"374","flag":"https://mobimatter.com/assets/flag/flag-AM.png","currency":"AMD","name":"Armenia","three_letter_abbreviation":"ARM","two_letter_abbreviation":"AM","objectID":"103587792","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"297","flag":"https://mobimatter.com/assets/flag/flag-AW.png","currency":"AWG","name":"Aruba","three_letter_abbreviation":"ABW","two_letter_abbreviation":"AW","objectID":"103587802","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"61","flag":"https://mobimatter.com/assets/flag/flag-AU.png","currency":"AUD","name":"Australia","three_letter_abbreviation":"AUS","two_letter_abbreviation":"AU","objectID":"103587812","_highlightResult":null,"isOfferCountry":true},{"calling_code":"43","flag":"https://mobimatter.com/assets/flag/flag-AT.png","currency":"EUR","name":"Austria","three_letter_abbreviation":"AUT","two_letter_abbreviation":"AT","objectID":"103587822","_highlightResult":null,"isOfferCountry":true},{"calling_code":"994","flag":"https://mobimatter.com/assets/flag/flag-AZ.png","currency":"AZN","name":"Azerbaijan","three_letter_abbreviation":"AZE","two_letter_abbreviation":"AZ","objectID":"103587832","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"1242","flag":"https://mobimatter.com/assets/flag/flag-BS.png","currency":"BSD","name":"Bahamas","three_letter_abbreviation":"BHS","two_letter_abbreviation":"BS","objectID":"103587842","_highlightResult":null,"isTopUpCountry":true},{"calling_code":"973","flag":"https://mobimatter.com/assets/flag/flag-BH.png","currency":"BHD","name":"Bahrain","three_letter_abbreviation":"BHR","two_letter_abbreviation":"BH","objectID":"103587852","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"880","flag":"https://mobimatter.com/assets/flag/flag-BD.png","currency":"BDT","name":"Bangladesh","three_letter_abbreviation":"BGD","two_letter_abbreviation":"BD","objectID":"103587862","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"1246","flag":"https://mobimatter.com/assets/flag/flag-BB.png","currency":"BBD","name":"Barbados","three_letter_abbreviation":"BRB","two_letter_abbreviation":"BB","objectID":"103587872","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"375","flag":"https://mobimatter.com/assets/flag/flag-BY.png","currency":"BYR","name":"Belarus","three_letter_abbreviation":"BLR","two_letter_abbreviation":"BY","objectID":"103587882","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"32","flag":"https://mobimatter.com/assets/flag/flag-BE.png","currency":"EUR","name":"Belgium","three_letter_abbreviation":"BEL","two_letter_abbreviation":"BE","objectID":"103587892","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"501","flag":"https://mobimatter.com/assets/flag/flag-BZ.png","currency":"BZD","name":"Belize","three_letter_abbreviation":"BLZ","two_letter_abbreviation":"BZ","objectID":"103587902","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"229","flag":"https://mobimatter.com/assets/flag/flag-BJ.png","currency":"XOF","name":"Benin","three_letter_abbreviation":"BEN","two_letter_abbreviation":"BJ","objectID":"103587912","_highlightResult":null,"isTopUpCountry":true},{"calling_code":"1441","flag":"https://mobimatter.com/assets/flag/flag-BM.png","currency":"BMD","name":"Bermuda","three_letter_abbreviation":"BMU","two_letter_abbreviation":"BM","objectID":"103587922","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"975","flag":"https://mobimatter.com/assets/flag/flag-BT.png","currency":"BTN","name":"Bhutan","three_letter_abbreviation":"BTN","two_letter_abbreviation":"BT","objectID":"103587932","_highlightResult":null},{"calling_code":"591","flag":"https://mobimatter.com/assets/flag/flag-BO.png","currency":"BOB","name":"Bolivia","three_letter_abbreviation":"BOL","two_letter_abbreviation":"BO","objectID":"103587942","_highlightResult":null,"isTopUpCountry":true},{"calling_code":"387","flag":"https://mobimatter.com/assets/flag/flag-BA.png","currency":"BAM","name":"Bosnia and Herzegovina","three_letter_abbreviation":"BIH","two_letter_abbreviation":"BA","objectID":"103587952","_highlightResult":null,"isOfferCountry":true},{"calling_code":"267","flag":"https://mobimatter.com/assets/flag/flag-BW.png","currency":"BWP","name":"Botswana","three_letter_abbreviation":"BWA","two_letter_abbreviation":"BW","objectID":"103587962","_highlightResult":null,"isTopUpCountry":true},{"flag":"https://mobimatter.com/assets/flag/flag-BV.png","currency":"NOK","name":"Bouvet Island","three_letter_abbreviation":"BVT","two_letter_abbreviation":"BV","calling_code":"47","objectID":"103587972","_highlightResult":null},{"calling_code":"55","flag":"https://mobimatter.com/assets/flag/flag-BR.png","currency":"BRL","name":"Brazil","three_letter_abbreviation":"BRA","two_letter_abbreviation":"BR","objectID":"103587982","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"246","flag":"https://mobimatter.com/assets/flag/flag-IO.png","currency":"USD","name":"British Indian Ocean Territory","three_letter_abbreviation":"IOT","two_letter_abbreviation":"IO","objectID":"103587992","_highlightResult":null},{"calling_code":"1284","flag":"https://mobimatter.com/assets/flag/flag-VG.png","currency":"USD","name":"British Virgin Islands","three_letter_abbreviation":"VGB","two_letter_abbreviation":"VG","objectID":"103588002","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"673","flag":"https://mobimatter.com/assets/flag/flag-BN.png","currency":"BND","name":"Brunei","three_letter_abbreviation":"BRN","two_letter_abbreviation":"BN","objectID":"103588012","_highlightResult":null,"isOfferCountry":true},{"calling_code":"359","flag":"https://mobimatter.com/assets/flag/flag-BG.png","currency":"BGN","name":"Bulgaria","three_letter_abbreviation":"BGR","two_letter_abbreviation":"BG","objectID":"103588022","_highlightResult":null,"isOfferCountry":true},{"calling_code":"226","flag":"https://mobimatter.com/assets/flag/flag-BF.png","currency":"XOF","name":"Burkina Faso","three_letter_abbreviation":"BFA","two_letter_abbreviation":"BF","objectID":"103588032","_highlightResult":null,"isTopUpCountry":true},{"calling_code":"257","flag":"https://mobimatter.com/assets/flag/flag-BI.png","currency":"BIF","name":"Burundi","three_letter_abbreviation":"BDI","two_letter_abbreviation":"BI","objectID":"103588042","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"855","flag":"https://mobimatter.com/assets/flag/flag-KH.png","currency":"KHR","name":"Cambodia","three_letter_abbreviation":"KHM","two_letter_abbreviation":"KH","objectID":"103588052","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"237","flag":"https://mobimatter.com/assets/flag/flag-CM.png","currency":"XAF","name":"Cameroon","three_letter_abbreviation":"CMR","two_letter_abbreviation":"CM","objectID":"103588062","_highlightResult":null,"isTopUpCountry":true},{"calling_code":"1","flag":"https://mobimatter.com/assets/flag/flag-CA.png","currency":"CAD","name":"Canada","three_letter_abbreviation":"CAN","two_letter_abbreviation":"CA","objectID":"103588072","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"238","flag":"https://mobimatter.com/assets/flag/flag-CV.png","currency":"CVE","name":"Cape Verde","three_letter_abbreviation":"CPV","two_letter_abbreviation":"CV","objectID":"103588082","_highlightResult":null,"isTopUpCountry":true},{"calling_code":"1345","flag":"https://mobimatter.com/assets/flag/flag-KY.png","currency":"KYD","name":"Cayman Islands","three_letter_abbreviation":"CYM","two_letter_abbreviation":"KY","objectID":"103588092","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"236","flag":"https://mobimatter.com/assets/flag/flag-CF.png","currency":"XAF","name":"Central African Republic","three_letter_abbreviation":"CAF","two_letter_abbreviation":"CF","objectID":"103588102","_highlightResult":null,"isTopUpCountry":true},{"calling_code":"235","flag":"https://mobimatter.com/assets/flag/flag-TD.png","currency":"XAF","name":"Chad","three_letter_abbreviation":"TCD","two_letter_abbreviation":"TD","objectID":"103588112","_highlightResult":null},{"calling_code":"56","flag":"https://mobimatter.com/assets/flag/flag-CL.png","currency":"CLF","name":"Chile","three_letter_abbreviation":"CHL","two_letter_abbreviation":"CL","objectID":"103588122","_highlightResult":null,
                "isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"86","flag":"https://mobimatter.com/assets/flag/flag-CN.png","currency":"CNY","name":"China","three_letter_abbreviation":"CHN","two_letter_abbreviation":"CN","objectID":"103588132","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"61","flag":"https://mobimatter.com/assets/flag/flag-CX.png","currency":"AUD","name":"Christmas Island","three_letter_abbreviation":"CXR","two_letter_abbreviation":"CX","objectID":"103588142","_highlightResult":null},{"calling_code":"61","flag":"https://mobimatter.com/assets/flag/flag-CC.png","currency":"AUD","name":"Cocos (Keeling) Islands","three_letter_abbreviation":"CCK","two_letter_abbreviation":"CC","objectID":"103588152","_highlightResult":null},{"calling_code":"57","flag":"https://mobimatter.com/assets/flag/flag-CO.png","currency":"COP","name":"Colombia","three_letter_abbreviation":"COL","two_letter_abbreviation":"CO","objectID":"103588162","_highlightResult":null,"isTopUpCountry":true},{"calling_code":"269","flag":"https://mobimatter.com/assets/flag/flag-KM.png","currency":"KMF","name":"Comoros","three_letter_abbreviation":"COM","two_letter_abbreviation":"KM","objectID":"103588172","_highlightResult":null,"isTopUpCountry":true},{"calling_code":"682","flag":"https://mobimatter.com/assets/flag/flag-CK.png","currency":"NZD","name":"Cook Islands","three_letter_abbreviation":"COK","two_letter_abbreviation":"CK","objectID":"103588182","_highlightResult":null},{"calling_code":"506","flag":"https://mobimatter.com/assets/flag/flag-CR.png","currency":"CRC","name":"Costa Rica","three_letter_abbreviation":"CRI","two_letter_abbreviation":"CR","objectID":"103588192","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"385","flag":"https://mobimatter.com/assets/flag/flag-HR.png","currency":"HRK","name":"Croatia","three_letter_abbreviation":"HRV","two_letter_abbreviation":"HR","objectID":"103588202","_highlightResult":null,"isOfferCountry":true},{"calling_code":"53","flag":"https://mobimatter.com/assets/flag/flag-CU.png","currency":"CUC","name":"Cuba","three_letter_abbreviation":"CUB","two_letter_abbreviation":"CU","objectID":"103588212","_highlightResult":null},{"calling_code":"5999","flag":"https://mobimatter.com/assets/flag/flag-CW.png","currency":"ANG","name":"Curacao","three_letter_abbreviation":"CUW","two_letter_abbreviation":"CW","objectID":"103588222","_highlightResult":null,"isOfferCountry":true},{"calling_code":"357","flag":"https://mobimatter.com/assets/flag/flag-CY.png","currency":"EUR","name":"Cyprus","three_letter_abbreviation":"CYP","two_letter_abbreviation":"CY","objectID":"103588232","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"420","flag":"https://mobimatter.com/assets/flag/flag-CZ.png","currency":"CZK","name":"Czech Republic","three_letter_abbreviation":"CZE","two_letter_abbreviation":"CZ","objectID":"103588242","_highlightResult":null,"isOfferCountry":true},{"calling_code":"243","flag":"https://mobimatter.com/assets/flag/flag-CD.png","currency":"CDF","name":"DR Congo","three_letter_abbreviation":"COD","two_letter_abbreviation":"CD","objectID":"103588252","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"45","flag":"https://mobimatter.com/assets/flag/flag-DK.png","currency":"DKK","name":"Denmark","three_letter_abbreviation":"DNK","two_letter_abbreviation":"DK","objectID":"103588262","_highlightResult":null,"isOfferCountry":true},{"calling_code":"253","flag":"https://mobimatter.com/assets/flag/flag-DJ.png","currency":"DJF","name":"Djibouti","three_letter_abbreviation":"DJI","two_letter_abbreviation":"DJ","objectID":"103588272","_highlightResult":null},{"calling_code":"1767","flag":"https://mobimatter.com/assets/flag/flag-DM.png","currency":"XCD","name":"Dominica","three_letter_abbreviation":"DMA","two_letter_abbreviation":"DM","objectID":"103588282","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"1809","flag":"https://mobimatter.com/assets/flag/flag-DO.png","currency":"DOP","name":"Dominican Republic","three_letter_abbreviation":"DOM","two_letter_abbreviation":"DO","objectID":"103588292","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"593","flag":"https://mobimatter.com/assets/flag/flag-EC.png","currency":"USD","name":"Ecuador","three_letter_abbreviation":"ECU","two_letter_abbreviation":"EC","objectID":"103588302","_highlightResult":null,"isTopUpCountry":true},{"calling_code":"20","flag":"https://mobimatter.com/assets/flag/flag-EG.png","currency":"EGP","name":"Egypt","three_letter_abbreviation":"EGY","two_letter_abbreviation":"EG","objectID":"103588312","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"503","flag":"https://mobimatter.com/assets/flag/flag-SV.png","currency":"SVC","name":"El Salvador","three_letter_abbreviation":"SLV","two_letter_abbreviation":"SV","objectID":"103588322","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"240","flag":"https://mobimatter.com/assets/flag/flag-GQ.png","currency":"XAF","name":"Equatorial Guinea","three_letter_abbreviation":"GNQ","two_letter_abbreviation":"GQ","objectID":"103588332","_highlightResult":null},{"calling_code":"291","flag":"https://mobimatter.com/assets/flag/flag-ER.png","currency":"ERN","name":"Eritrea","three_letter_abbreviation":"ERI","two_letter_abbreviation":"ER","objectID":"103588342","_highlightResult":null},{"calling_code":"372","flag":"https://mobimatter.com/assets/flag/flag-EE.png","currency":"EUR","name":"Estonia","three_letter_abbreviation":"EST","two_letter_abbreviation":"EE","objectID":"103588352","_highlightResult":null,"isOfferCountry":true},{"calling_code":"268","flag":"https://mobimatter.com/assets/flag/flag-SZ.png","currency":"SZL","name":"Eswatini","three_letter_abbreviation":"SWZ","two_letter_abbreviation":"SZ","objectID":"103589792","_highlightResult":null,"isTopUpCountry":true},{"calling_code":"251","flag":"https://mobimatter.com/assets/flag/flag-ET.png","currency":"ETB","name":"Ethiopia","three_letter_abbreviation":"ETH","two_letter_abbreviation":"ET","objectID":"103588362","_highlightResult":null,"isTopUpCountry":true},{"calling_code":"500","flag":"https://mobimatter.com/assets/flag/flag-FK.png","currency":"FKP","name":"Falkland Islands","three_letter_abbreviation":"FLK","two_letter_abbreviation":"FK","objectID":"103588372","_highlightResult":null},{"calling_code":"298","flag":"https://mobimatter.com/assets/flag/flag-FO.png","currency":"DKK","name":"Faroe Islands","three_letter_abbreviation":"FRO","two_letter_abbreviation":"FO","objectID":"103588382","_highlightResult":null},{"calling_code":"679","flag":"https://mobimatter.com/assets/flag/flag-FJ.png","currency":"FJD","name":"Fiji","three_letter_abbreviation":"FJI","two_letter_abbreviation":"FJ","objectID":"103588392","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"358","flag":"https://mobimatter.com/assets/flag/flag-FI.png","currency":"EUR","name":"Finland","three_letter_abbreviation":"FIN","two_letter_abbreviation":"FI","objectID":"103588402","_highlightResult":null,"isOfferCountry":true},{"calling_code":"33","flag":"https://mobimatter.com/assets/flag/flag-FR.png","currency":"EUR","name":"France","three_letter_abbreviation":"FRA","two_letter_abbreviation":"FR","objectID":"103588412","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"594","flag":"https://mobimatter.com/assets/flag/flag-GF.png","currency":"EUR","name":"French Guiana","three_letter_abbreviation":"GUF","two_letter_abbreviation":"GF","objectID":"103588422","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"689","flag":"https://mobimatter.com/assets/flag/flag-PF.png","currency":"XPF","name":"French Polynesia","three_letter_abbreviation":"PYF","two_letter_abbreviation":"PF","objectID":"103588432","_highlightResult":null},{"flag":"https://mobimatter.com/assets/flag/flag-TF.png","currency":"EUR","name":"French Southern and Antarctic Lands","three_letter_abbreviation":"ATF","two_letter_abbreviation":"TF","calling_code":"011","objectID":"103588442","_highlightResult":null},{"calling_code":"241","flag":"https://mobimatter.com/assets/flag/flag-GA.png","currency":"XAF","name":"Gabon","three_letter_abbreviation":"GAB","two_letter_abbreviation":"GA","objectID":"103588452","_highlightResult":null},{"calling_code":"220","flag":"https://mobimatter.com/assets/flag/flag-GM.png","currency":"GMD","name":"Gambia","three_letter_abbreviation":"GMB","two_letter_abbreviation":"GM","objectID":"103588462","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"995","flag":"https://mobimatter.com/assets/flag/flag-GE.png","currency":"GEL","name":"Georgia","three_letter_abbreviation":"GEO","two_letter_abbreviation":"GE","objectID":"103588472","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"49","flag":"https://mobimatter.com/assets/flag/flag-DE.png","currency":"EUR","name":"Germany","three_letter_abbreviation":"DEU","two_letter_abbreviation":"DE","objectID":"103588482","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"233","flag":"https://mobimatter.com/assets/flag/flag-GH.png","currency":"GHS","name":"Ghana","three_letter_abbreviation":"GHA","two_letter_abbreviation":"GH","objectID":"103588492","_highlightResult":null,"isTopUpCountry":true},{"calling_code":"350","flag":"https://mobimatter.com/assets/flag/flag-GI.png","currency":"GIP","name":"Gibraltar","three_letter_abbreviation":"GIB","two_letter_abbreviation":"GI","objectID":"103588502","_highlightResult":null,"isOfferCountry":true},{"calling_code":"30","flag":"https://mobimatter.com/assets/flag/flag-GR.png","currency":"EUR","name":"Greece","three_letter_abbreviation":"GRC","two_letter_abbreviation":"GR","objectID":"103588512","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"299","flag":"https://mobimatter.com/assets/flag/flag-GL.png","currency":"DKK","name":"Greenland","three_letter_abbreviation":"GRL","two_letter_abbreviation":"GL","objectID":"103588522","_highlightResult":null,"isOfferCountry":true},{"calling_code":"1473","flag":"https://mobimatter.com/assets/flag/flag-GD.png","currency":"XCD","name":"Grenada","three_letter_abbreviation":"GRD","two_letter_abbreviation":"GD","objectID":"103588532","_highlightResult":null,"isTopUpCountry":true},{"calling_code":"590","flag":"https://mobimatter.com/assets/flag/flag-GP.png","currency":"EUR","name":"Guadeloupe","three_letter_abbreviation":"GLP","two_letter_abbreviation":"GP","objectID":"103588542","_highlightResult":null,"isOfferCountry":true},{"calling_code":"1671","flag":"https://mobimatter.com/assets/flag/flag-GU.png","currency":"USD","name":"Guam","three_letter_abbreviation":"GUM","two_letter_abbreviation":"GU","objectID":"103588552","_highlightResult":null},{"calling_code":"502","flag":"https://mobimatter.com/assets/flag/flag-GT.png","currency":"GTQ","name":"Guatemala","three_letter_abbreviation":"GTM","two_letter_abbreviation":"GT","objectID":"103588562","_highlightResult":null,"isTopUpCountry":true},{"calling_code":"44","flag":"https://mobimatter.com/assets/flag/flag-GG.png","currency":"GBP","name":"Guernsey","three_letter_abbreviation":"GGY","two_letter_abbreviation":"GG","objectID":"103588572","_highlightResult":null,"isOfferCountry":true},{"calling_code":"224","flag":"https://mobimatter.com/assets/flag/flag-GN.png","currency":"GNF","name":"Guinea","three_letter_abbreviation":"GIN","two_letter_abbreviation":"GN","objectID":"103588582","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"245","flag":"https://mobimatter.com/assets/flag/flag-GW.png","currency":"XOF","name":"Guinea Bissau","three_letter_abbreviation":"GNB","two_letter_abbreviation":"GW","objectID":"103588592","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"592","flag":"https://mobimatter.com/assets/flag/flag-GY.png","currency":"GYD","name":"Guyana","three_letter_abbreviation":"GUY","two_letter_abbreviation":"GY","objectID":"103588602","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"509","flag":"https://mobimatter.com/assets/flag/flag-HT.png","currency":"HTG","name":"Haiti","three_letter_abbreviation":"HTI","two_letter_abbreviation":"HT","objectID":"103588612","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"flag":"https://mobimatter.com/assets/flag/flag-HM.png","currency":"AUD","name":"Heard Island and McDonald Islands","three_letter_abbreviation":"HMD","two_letter_abbreviation":"HM","calling_code":"672","objectID":"103588622","_highlightResult":null},{"calling_code":"504","flag":"https://mobimatter.com/assets/flag/flag-HN.png","currency":"HNL","name":"Honduras","three_letter_abbreviation":"HND","two_letter_abbreviation":"HN","objectID":"103588632","_highlightResult":null,"isTopUpCountry":true},{"calling_code":"852","flag":"https://mobimatter.com/assets/flag/flag-HK.png","currency":"HKD","name":"Hong Kong","three_letter_abbreviation":"HKG","two_letter_abbreviation":"HK","objectID":"103588642","_highlightResult":null,"isOfferCountry":true},{"calling_code":"36","flag":"https://mobimatter.com/assets/flag/flag-HU.png","currency":"HUF","name":"Hungary","three_letter_abbreviation":"HUN","two_letter_abbreviation":"HU","objectID":"103588652","_highlightResult":null,"isOfferCountry":true},{"calling_code":"354","flag":"https://mobimatter.com/assets/flag/flag-IS.png","currency":"ISK","name":"Iceland","three_letter_abbreviation":"ISL","two_letter_abbreviation":"IS","objectID":"103588662","_highlightResult":null,"isOfferCountry":true},{"calling_code":"91","flag":"https://mobimatter.com/assets/flag/flag-IN.png","currency":"INR","name":"India","three_letter_abbreviation":"IND","two_letter_abbreviation":"IN","objectID":"103588672","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"62","flag":"https://mobimatter.com/assets/flag/flag-ID.png","currency":"IDR","name":"Indonesia","three_letter_abbreviation":"IDN","two_letter_abbreviation":"ID","objectID":"103588682","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"98","flag":"https://mobimatter.com/assets/flag/flag-IR.png","currency":"IRR","name":"Iran","three_letter_abbreviation":"IRN","two_letter_abbreviation":"IR","objectID":"103588692","_highlightResult":null,"isOfferCountry":true},{"calling_code":"964","flag":"https://mobimatter.com/assets/flag/flag-IQ.png","currency":"IQD","name":"Iraq","three_letter_abbreviation":"IRQ","two_letter_abbreviation":"IQ","objectID":"103588702","_highlightResult":null,"isTopUpCountry":true},{"calling_code":"353","flag":"https://mobimatter.com/assets/flag/flag-IE.png","currency":"EUR","name":"Ireland","three_letter_abbreviation":"IRL","two_letter_abbreviation":"IE","objectID":"103588712","_highlightResult":null,"isOfferCountry":true},{"calling_code":"44","flag":"https://mobimatter.com/assets/flag/flag-IM.png","currency":"GBP","name":"Isle of Man","three_letter_abbreviation":"IMN","two_letter_abbreviation":"IM","objectID":"103588722","_highlightResult":null,"isOfferCountry":true},{"calling_code":"972","flag":"https://mobimatter.com/assets/flag/flag-IL.png","currency":"ILS","name":"Israel","three_letter_abbreviation":"ISR","two_letter_abbreviation":"IL","objectID":"103588732","_highlightResult":null,"isOfferCountry":true},{"calling_code":"39","flag":"https://mobimatter.com/assets/flag/flag-IT.png","currency":"EUR","name":"Italy","three_letter_abbreviation":"ITA","two_letter_abbreviation":"IT","objectID":"103588742","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"225","flag":"https://mobimatter.com/assets/flag/flag-CI.png","currency":"XOF","name":"Ivory Coast","three_letter_abbreviation":"CIV","two_letter_abbreviation":"CI","objectID":"103588752","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"1876","flag":"https://mobimatter.com/assets/flag/flag-JM.png","currency":"JMD","name":"Jamaica","three_letter_abbreviation":"JAM","two_letter_abbreviation":"JM","objectID":"103588762","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"81","flag":"https://mobimatter.com/assets/flag/flag-JP.png","currency":"JPY","name":"Japan","three_letter_abbreviation":"JPN","two_letter_abbreviation":"JP","objectID":"103588772","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"44","flag":"https://mobimatter.com/assets/flag/flag-JE.png","currency":"GBP","name":"Jersey","three_letter_abbreviation":"JEY","two_letter_abbreviation":"JE","objectID":"103588782","_highlightResult":null,"isOfferCountry":true},{"calling_code":"962","flag":"https://mobimatter.com/assets/flag/flag-JO.png","currency":"JOD","name":"Jordan","three_letter_abbreviation":"JOR","two_letter_abbreviation":"JO","objectID":"103588792","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"7","flag":"https://mobimatter.com/assets/flag/flag-KZ.png","currency":"KZT","name":"Kazakhstan","three_letter_abbreviation":"KAZ","two_letter_abbreviation":"KZ","objectID":"103588802","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"254","flag":"https://mobimatter.com/assets/flag/flag-KE.png","currency":"KES","name":"Kenya","three_letter_abbreviation":"KEN","two_letter_abbreviation":"KE","objectID":"103588812","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"686","flag":"https://mobimatter.com/assets/flag/flag-KI.png","currency":"AUD","name":"Kiribati","three_letter_abbreviation":"KIR","two_letter_abbreviation":"KI","objectID":"103588822","_highlightResult":null},{"calling_code":"383","flag":"https://mobimatter.com/assets/flag/flag-XK.png","currency":"EUR","name":"Kosovo","three_letter_abbreviation":"XKX","two_letter_abbreviation":"XK","objectID":"103588832","_highlightResult":null,"isTopUpCountry":true},{"calling_code":"965","flag":"https://mobimatter.com/assets/flag/flag-KW.png","currency":"KWD","name":"Kuwait","three_letter_abbreviation":"KWT","two_letter_abbreviation":"KW","objectID":"103588842","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"996","flag":"https://mobimatter.com/assets/flag/flag-KG.png","currency":"KGS","name":"Kyrgyzstan","three_letter_abbreviation":"KGZ","two_letter_abbreviation":"KG","objectID":"103588852","_highlightResult":null,"isTopUpCountry":true},{"calling_code":"856","flag":"https://mobimatter.com/assets/flag/flag-LA.png","currency":"LAK","name":"Laos","three_letter_abbreviation":"LAO","two_letter_abbreviation":"LA","objectID":"103588862","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"371","flag":"https://mobimatter.com/assets/flag/flag-LV.png","currency":"EUR","name":"Latvia","three_letter_abbreviation":"LVA","two_letter_abbreviation":"LV","objectID":"103588872","_highlightResult":null,"isOfferCountry":true},{"calling_code":"961","flag":"https://mobimatter.com/assets/flag/flag-LB.png","currency":"LBP","name":"Lebanon","three_letter_abbreviation":"LBN","two_letter_abbreviation":"LB","objectID":"103588882","_highlightResult":null,"isTopUpCountry":true},{"calling_code":"266","flag":"https://mobimatter.com/assets/flag/flag-LS.png","currency":"LSL","name":"Lesotho","three_letter_abbreviation":"LSO","two_letter_abbreviation":"LS","objectID":"103588892","_highlightResult":null},{"calling_code":"231","flag":"https://mobimatter.com/assets/flag/flag-LR.png","currency":"LRD","name":"Liberia","three_letter_abbreviation":"LBR","two_letter_abbreviation":"LR","objectID":"103588902","_highlightResult":null,"isTopUpCountry":true},{"calling_code":"218","flag":"https://mobimatter.com/assets/flag/flag-LY.png","currency":"LYD","name":"Libya","three_letter_abbreviation":"LBY","two_letter_abbreviation":"LY","objectID":"103588912","_highlightResult":null},{"calling_code":"423","flag":"https://mobimatter.com/assets/flag/flag-LI.png","currency":"CHF","name":"Liechtenstein","three_letter_abbreviation":"LIE","two_letter_abbreviation":"LI","objectID":"103588922","_highlightResult":null,"isOfferCountry":true},{"calling_code":"370","flag":"https://mobimatter.com/assets/flag/flag-LT.png","currency":"EUR","name":"Lithuania","three_letter_abbreviation":"LTU","two_letter_abbreviation":"LT","objectID":"103588932","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"352","flag":"https://mobimatter.com/assets/flag/flag-LU.png","currency":"EUR","name":"Luxembourg","three_letter_abbreviation":"LUX","two_letter_abbreviation":"LU","objectID":"103588942","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"853","flag":"https://mobimatter.com/assets/flag/flag-MO.png","currency":"MOP","name":"Macau","three_letter_abbreviation":"MAC","two_letter_abbreviation":"MO","objectID":"103588952","_highlightResult":null,"isOfferCountry":true},{"calling_code":"389","flag":"https://mobimatter.com/assets/flag/flag-MK.png","currency":"MKD","name":"Macedonia","three_letter_abbreviation":"MKD","two_letter_abbreviation":"MK","objectID":"103588962","_highlightResult":null,"isOfferCountry":true},{"calling_code":"261","flag":"https://mobimatter.com/assets/flag/flag-MG.png","currency":"MGA","name":"Madagascar","three_letter_abbreviation":"MDG","two_letter_abbreviation":"MG","objectID":"103588972","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"265","flag":"https://mobimatter.com/assets/flag/flag-MW.png","currency":"MWK","name":"Malawi","three_letter_abbreviation":"MWI","two_letter_abbreviation":"MW","objectID":"103588982","_highlightResult":null,"isTopUpCountry":true},{"calling_code":"60","flag":"https://mobimatter.com/assets/flag/flag-MY.png","currency":"MYR","name":"Malaysia","three_letter_abbreviation":"MYS","two_letter_abbreviation":"MY","objectID":"103588992","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"960","flag":"https://mobimatter.com/assets/flag/flag-MV.png","currency":"MVR","name":"Maldives","three_letter_abbreviation":"MDV","two_letter_abbreviation":"MV","objectID":"103589002","_highlightResult":null},{"calling_code":"223","flag":"https://mobimatter.com/assets/flag/flag-ML.png","currency":"XOF","name":"Mali","three_letter_abbreviation":"MLI","two_letter_abbreviation":"ML","objectID":"103589012","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"356","flag":"https://mobimatter.com/assets/flag/flag-MT.png","currency":"EUR","name":"Malta","three_letter_abbreviation":"MLT","two_letter_abbreviation":"MT","objectID":"103589022","_highlightResult":null,"isOfferCountry":true},{"calling_code":"692","flag":"https://mobimatter.com/assets/flag/flag-MH.png","currency":"USD","name":"Marshall Islands","three_letter_abbreviation":"MHL","two_letter_abbreviation":"MH","objectID":"103589032","_highlightResult":null},{"calling_code":"596","flag":"https://mobimatter.com/assets/flag/flag-MQ.png","currency":"EUR","name":"Martinique","three_letter_abbreviation":"MTQ","two_letter_abbreviation":"MQ","objectID":"103589042","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"222","flag":"https://mobimatter.com/assets/flag/flag-MR.png","currency":"MRO","name":"Mauritania","three_letter_abbreviation":"MRT","two_letter_abbreviation":"MR","objectID":"103589052","_highlightResult":null},{"calling_code":"230","flag":"https://mobimatter.com/assets/flag/flag-MU.png","currency":"MUR","name":"Mauritius","three_letter_abbreviation":"MUS","two_letter_abbreviation":"MU","objectID":"103589062","_highlightResult":null},{"calling_code":"262","flag":"https://mobimatter.com/assets/flag/flag-YT.png","currency":"EUR","name":"Mayotte","three_letter_abbreviation":"MYT","two_letter_abbreviation":"YT","objectID":"103589072","_highlightResult":null,"isOfferCountry":true},{"calling_code":"52","flag":"https://mobimatter.com/assets/flag/flag-MX.png","currency":"MXN","name":"Mexico","three_letter_abbreviation":"MEX","two_letter_abbreviation":"MX","objectID":"103589082","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"691","flag":"https://mobimatter.com/assets/flag/flag-FM.png","currency":"USD","name":"Micronesia","three_letter_abbreviation":"FSM","two_letter_abbreviation":"FM","objectID":"103589092","_highlightResult":null},{"calling_code":"373","flag":"https://mobimatter.com/assets/flag/flag-MD.png","currency":"MDL","name":"Moldova","three_letter_abbreviation":"MDA","two_letter_abbreviation":"MD","objectID":"103589102","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"377","flag":"https://mobimatter.com/assets/flag/flag-MC.png","currency":"EUR","name":"Monaco","three_letter_abbreviation":"MCO","two_letter_abbreviation":"MC","objectID":"103589112","_highlightResult":null},{"calling_code":"976","flag":"https://mobimatter.com/assets/flag/flag-MN.png","currency":"MNT","name":"Mongolia","three_letter_abbreviation":"MNG","two_letter_abbreviation":"MN","objectID":"103589122","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"382","flag":"https://mobimatter.com/assets/flag/flag-ME.png","currency":"EUR","name":"Montenegro","three_letter_abbreviation":"MNE","two_letter_abbreviation":"ME","objectID":"103589132","_highlightResult":null,"isOfferCountry":true},{"calling_code":"1664","flag":"https://mobimatter.com/assets/flag/flag-MS.png","currency":"XCD","name":"Montserrat","three_letter_abbreviation":"MSR","two_letter_abbreviation":"MS","objectID":"103589142","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"212","flag":"https://mobimatter.com/assets/flag/flag-MA.png","currency":"MAD","name":"Morocco","three_letter_abbreviation":"MAR","two_letter_abbreviation":"MA","objectID":"103589152","_highlightResult":null,"isTopUpCountry":true},{"calling_code":"258","flag":"https://mobimatter.com/assets/flag/flag-MZ.png","currency":"MZN","name":"Mozambique","three_letter_abbreviation":"MOZ","two_letter_abbreviation":"MZ","objectID":"103589162","_highlightResult":null,"isTopUpCountry":true},{"calling_code":"95","flag":"https://mobimatter.com/assets/flag/flag-MM.png","currency":"MMK","name":"Myanmar","three_letter_abbreviation":"MMR","two_letter_abbreviation":"MM","objectID":"103589172","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"264","flag":"https://mobimatter.com/assets/flag/flag-NA.png","currency":"NAD","name":"Namibia","three_letter_abbreviation":"NAM","two_letter_abbreviation":"NA","objectID":"103589182","_highlightResult":null,"isTopUpCountry":true},{"calling_code":"674","flag":"https://mobimatter.com/assets/flag/flag-NR.png","currency":"AUD","name":"Nauru","three_letter_abbreviation":"NRU","two_letter_abbreviation":"NR","objectID":"103589192","_highlightResult":null,"isTopUpCountry":true},{"calling_code":"977","flag":"https://mobimatter.com/assets/flag/flag-NP.png","currency":"NPR","name":"Nepal","three_letter_abbreviation":"NPL","two_letter_abbreviation":"NP","objectID":"103589202","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"31","flag":"https://mobimatter.com/assets/flag/flag-NL.png","currency":"EUR","name":"Netherlands","three_letter_abbreviation":"NLD","two_letter_abbreviation":"NL","objectID":"103589212","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"687","flag":"https://mobimatter.com/assets/flag/flag-NC.png","currency":"XPF","name":"New Caledonia","three_letter_abbreviation":"NCL","two_letter_abbreviation":"NC","objectID":"103589222","_highlightResult":null},{"calling_code":"64","flag":"https://mobimatter.com/assets/flag/flag-NZ.png","currency":"NZD","name":"New Zealand","three_letter_abbreviation":"NZL","two_letter_abbreviation":"NZ","objectID":"103589232","_highlightResult":null,"isOfferCountry":true},{"calling_code":"505","flag":"https://mobimatter.com/assets/flag/flag-NI.png","currency":"NIO","name":"Nicaragua","three_letter_abbreviation":"NIC","two_letter_abbreviation":"NI","objectID":"103589242","_highlightResult":null},{"calling_code":"227","flag":"https://mobimatter.com/assets/flag/flag-NE.png","currency":"XOF","name":"Niger","three_letter_abbreviation":"NER","two_letter_abbreviation":"NE","objectID":"103589252","_highlightResult":null},{"calling_code":"234","flag":"https://mobimatter.com/assets/flag/flag-NG.png","currency":"NGN","name":"Nigeria","three_letter_abbreviation":"NGA","two_letter_abbreviation":"NG","objectID":"103589262","_highlightResult":null},{"calling_code":"683","flag":"https://mobimatter.com/assets/flag/flag-NU.png","currency":"NZD","name":"Niue","three_letter_abbreviation":"NIU","two_letter_abbreviation":"NU","objectID":"103589272","_highlightResult":null},{"calling_code":"672","flag":"https://mobimatter.com/assets/flag/flag-NF.png","currency":"AUD","name":"Norfolk Island","three_letter_abbreviation":"NFK","two_letter_abbreviation":"NF","objectID":"103589282","_highlightResult":null},{"calling_code":"850","flag":"https://mobimatter.com/assets/flag/flag-KP.png","currency":"KPW","name":"North Korea","three_letter_abbreviation":"PRK","two_letter_abbreviation":"KP","objectID":"103589292","_highlightResult":null},{"calling_code":"1670","flag":"https://mobimatter.com/assets/flag/flag-MP.png","currency":"USD","name":"Northern Mariana Islands","three_letter_abbreviation":"MNP","two_letter_abbreviation":"MP","objectID":"103589302","_highlightResult":null},{"calling_code":"47","flag":"https://mobimatter.com/assets/flag/flag-NO.png","currency":"NOK","name":"Norway","three_letter_abbreviation":"NOR","two_letter_abbreviation":"NO","objectID":"103589312","_highlightResult":null,"isOfferCountry":true},{"calling_code":"968","flag":"https://mobimatter.com/assets/flag/flag-OM.png","currency":"OMR","name":"Oman","three_letter_abbreviation":"OMN","two_letter_abbreviation":"OM","objectID":"103589322","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"92","flag":"https://mobimatter.com/assets/flag/flag-PK.png","currency":"PKR","name":"Pakistan","three_letter_abbreviation":"PAK","two_letter_abbreviation":"PK","objectID":"103589332","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"680","flag":"https://mobimatter.com/assets/flag/flag-PW.png","currency":"USD","name":"Palau","three_letter_abbreviation":"PLW","two_letter_abbreviation":"PW","objectID":"103589342","_highlightResult":null},{"calling_code":"970","flag":"https://mobimatter.com/assets/flag/flag-PS.png","currency":"ILS","name":"Palestine","three_letter_abbreviation":"PSE","two_letter_abbreviation":"PS","objectID":"103589352","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"507","flag":"https://mobimatter.com/assets/flag/flag-PA.png","currency":"PAB","name":"Panama","three_letter_abbreviation":"PAN","two_letter_abbreviation":"PA","objectID":"103589362","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"675","flag":"https://mobimatter.com/assets/flag/flag-PG.png","currency":"PGK","name":"Papua New Guinea","three_letter_abbreviation":"PNG","two_letter_abbreviation":"PG","objectID":"103589372","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"595","flag":"https://mobimatter.com/assets/flag/flag-PY.png","currency":"PYG","name":"Paraguay","three_letter_abbreviation":"PRY","two_letter_abbreviation":"PY","objectID":"103589382","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"51","flag":"https://mobimatter.com/assets/flag/flag-PE.png","currency":"PEN","name":"Peru","three_letter_abbreviation":"PER","two_letter_abbreviation":"PE","objectID":"103589392","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"63","flag":"https://mobimatter.com/assets/flag/flag-PH.png","currency":"PHP","name":"Philippines","three_letter_abbreviation":"PHL","two_letter_abbreviation":"PH","objectID":"103589402","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"64","flag":"https://mobimatter.com/assets/flag/flag-PN.png","currency":"NZD","name":"Pitcairn Islands","three_letter_abbreviation":"PCN","two_letter_abbreviation":"PN","objectID":"103589412","_highlightResult":null},{"calling_code":"48","flag":"https://mobimatter.com/assets/flag/flag-PL.png","currency":"PLN","name":"Poland","three_letter_abbreviation":"POL","two_letter_abbreviation":"PL","objectID":"103589422","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"351","flag":"https://mobimatter.com/assets/flag/flag-PT.png","currency":"EUR","name":"Portugal","three_letter_abbreviation":"PRT","two_letter_abbreviation":"PT","objectID":"103589432","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"1787","flag":"https://mobimatter.com/assets/flag/flag-PR.png","currency":"USD","name":"Puerto Rico","three_letter_abbreviation":"PRI","two_letter_abbreviation":"PR","objectID":"103589442","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"974","flag":"https://mobimatter.com/assets/flag/flag-QA.png","currency":"QAR","name":"Qatar","three_letter_abbreviation":"QAT","two_letter_abbreviation":"QA","objectID":"103589452","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"242","flag":"https://mobimatter.com/assets/flag/flag-CG.png","currency":"XAF","name":"Republic of the Congo","three_letter_abbreviation":"COG","two_letter_abbreviation":"CG","objectID":"103589462","_highlightResult":null,"isOfferCountry":true},{"calling_code":"40","flag":"https://mobimatter.com/assets/flag/flag-RO.png","currency":"RON","name":"Romania","three_letter_abbreviation":"ROU","two_letter_abbreviation":"RO","objectID":"103589472","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"7","flag":"https://mobimatter.com/assets/flag/flag-RU.png","currency":"RUB","name":"Russia","three_letter_abbreviation":"RUS","two_letter_abbreviation":"RU","objectID":"103589482","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"250","flag":"https://mobimatter.com/assets/flag/flag-RW.png","currency":"RWF","name":"Rwanda","three_letter_abbreviation":"RWA","two_letter_abbreviation":"RW","objectID":"103589492","_highlightResult":null,"isTopUpCountry":true},{"calling_code":"262","flag":"https://mobimatter.com/assets/flag/flag-RE.png","currency":"EUR","name":"Reunion","three_letter_abbreviation":"REU","two_letter_abbreviation":"RE","objectID":"103589502","_highlightResult":null,"isOfferCountry":true},{"calling_code":"590","flag":"https://mobimatter.com/assets/flag/flag-BL.png","currency":"EUR","name":"Saint Barthelemy","three_letter_abbreviation":"BLM","two_letter_abbreviation":"BL","objectID":"103589512","_highlightResult":null,"isOfferCountry":true},{"calling_code":"1869","flag":"https://mobimatter.com/assets/flag/flag-KN.png","currency":"XCD","name":"Saint Kitts and Nevis","three_letter_abbreviation":"KNA","two_letter_abbreviation":"KN","objectID":"103589522","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"1758","flag":"https://mobimatter.com/assets/flag/flag-LC.png","currency":"XCD","name":"Saint Lucia","three_letter_abbreviation":"LCA","two_letter_abbreviation":"LC","objectID":"103589532","_highlightResult":null,"isTopUpCountry":true},{"calling_code":"590","flag":"https://mobimatter.com/assets/flag/flag-MF.png","currency":"EUR","name":"Saint Martin","three_letter_abbreviation":"MAF","two_letter_abbreviation":"MF","objectID":"103589542","_highlightResult":null,"isOfferCountry":true},{"calling_code":"508","flag":"https://mobimatter.com/assets/flag/flag-PM.png","currency":"EUR","name":"Saint Pierre and Miquelon","three_letter_abbreviation":"SPM","two_letter_abbreviation":"PM","objectID":"103589552","_highlightResult":null},{"calling_code":"1784","flag":"https://mobimatter.com/assets/flag/flag-VC.png","currency":"XCD","name":"Saint Vincent and the Grenadines","three_letter_abbreviation":"VCT","two_letter_abbreviation":"VC","objectID":"103589562","_highlightResult":null,"isTopUpCountry":true},{"calling_code":"685","flag":"https://mobimatter.com/assets/flag/flag-WS.png","currency":"WST","name":"Samoa","three_letter_abbreviation":"WSM","two_letter_abbreviation":"WS","objectID":"103589572","_highlightResult":null,"isTopUpCountry":true},{"calling_code":"378","flag":"https://mobimatter.com/assets/flag/flag-SM.png","currency":"EUR","name":"San Marino","three_letter_abbreviation":"SMR","two_letter_abbreviation":"SM","objectID":"103589582","_highlightResult":null,"isOfferCountry":true},{"calling_code":"966","flag":"https://mobimatter.com/assets/flag/flag-SA.png","currency":"SAR","name":"Saudi Arabia","three_letter_abbreviation":"SAU","two_letter_abbreviation":"SA","objectID":"103589592","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"221","flag":"https://mobimatter.com/assets/flag/flag-SN.png","currency":"XOF","name":"Senegal","three_letter_abbreviation":"SEN","two_letter_abbreviation":"SN","objectID":"103589602","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"381","flag":"https://mobimatter.com/assets/flag/flag-RS.png","currency":"RSD","name":"Serbia","three_letter_abbreviation":"SRB","two_letter_abbreviation":"RS","objectID":"103589612","_highlightResult":null,"isOfferCountry":true},{"calling_code":"248","flag":"https://mobimatter.com/assets/flag/flag-SC.png","currency":"SCR","name":"Seychelles","three_letter_abbreviation":"SYC","two_letter_abbreviation":"SC","objectID":"103589622","_highlightResult":null},{"calling_code":"232","flag":"https://mobimatter.com/assets/flag/flag-SL.png","currency":"SLL","name":"Sierra Leone","three_letter_abbreviation":"SLE","two_letter_abbreviation":"SL","objectID":"103589632","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"65","flag":"https://mobimatter.com/assets/flag/flag-SG.png","currency":"SGD","name":"Singapore","three_letter_abbreviation":"SGP","two_letter_abbreviation":"SG","objectID":"103589642","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"1721","flag":"https://mobimatter.com/assets/flag/flag-SX.png","currency":"ANG","name":"Sint Maarten","three_letter_abbreviation":"SXM","two_letter_abbreviation":"SX","objectID":"103589652","_highlightResult":null},{"calling_code":"421","flag":"https://mobimatter.com/assets/flag/flag-SK.png","currency":"EUR","name":"Slovakia","three_letter_abbreviation":"SVK","two_letter_abbreviation":"SK","objectID":"103589662","_highlightResult":null,"isOfferCountry":true},{"calling_code":"386","flag":"https://mobimatter.com/assets/flag/flag-SI.png","currency":"EUR","name":"Slovenia","three_letter_abbreviation":"SVN","two_letter_abbreviation":"SI","objectID":"103589672","_highlightResult":null,"isOfferCountry":true},{"calling_code":"677","flag":"https://mobimatter.com/assets/flag/flag-SB.png","currency":"SBD","name":"Solomon Islands","three_letter_abbreviation":"SLB","two_letter_abbreviation":"SB","objectID":"103589682","_highlightResult":null,"isOfferCountry":true},{"calling_code":"252","flag":"https://mobimatter.com/assets/flag/flag-SO.png","currency":"SOS","name":"Somalia","three_letter_abbreviation":"SOM","two_letter_abbreviation":"SO","objectID":"103589692","_highlightResult":null},{"calling_code":"27","flag":"https://mobimatter.com/assets/flag/flag-ZA.png","currency":"ZAR","name":"South Africa","three_letter_abbreviation":"ZAF","two_letter_abbreviation":"ZA","objectID":"103589702","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"500","flag":"https://mobimatter.com/assets/flag/flag-GS.png","currency":"GBP","name":"South Georgia","three_letter_abbreviation":"SGS","two_letter_abbreviation":"GS","objectID":"103589712","_highlightResult":null},{"calling_code":"82","flag":"https://mobimatter.com/assets/flag/flag-KR.png","currency":"KRW","name":"South Korea","three_letter_abbreviation":"KOR","two_letter_abbreviation":"KR","objectID":"103589722","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"211","flag":"https://mobimatter.com/assets/flag/flag-SS.png","currency":"SSP","name":"South Sudan","three_letter_abbreviation":"SSD","two_letter_abbreviation":"SS","objectID":"103589732","_highlightResult":null},{"calling_code":"34","flag":"https://mobimatter.com/assets/flag/flag-ES.png","currency":"EUR","name":"Spain","three_letter_abbreviation":"ESP","two_letter_abbreviation":"ES","objectID":"103589742","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"94","flag":"https://mobimatter.com/assets/flag/flag-LK.png","currency":"LKR","name":"Sri Lanka","three_letter_abbreviation":"LKA","two_letter_abbreviation":"LK","objectID":"103589752","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"249","flag":"https://mobimatter.com/assets/flag/flag-SD.png","currency":"SDG","name":"Sudan","three_letter_abbreviation":"SDN","two_letter_abbreviation":"SD","objectID":"103589762","_highlightResult":null},{"calling_code":"597","flag":"https://mobimatter.com/assets/flag/flag-SR.png","currency":"SRD","name":"Suriname","three_letter_abbreviation":"SUR","two_letter_abbreviation":"SR","objectID":"103589772","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"4779","flag":"https://mobimatter.com/assets/flag/flag-SJ.png","currency":"NOK","name":"Svalbard and Jan Mayen","three_letter_abbreviation":"SJM","two_letter_abbreviation":"SJ","objectID":"103589782","_highlightResult":null},{"calling_code":"46","flag":"https://mobimatter.com/assets/flag/flag-SE.png","currency":"SEK","name":"Sweden","three_letter_abbreviation":"SWE","two_letter_abbreviation":"SE","objectID":"103589802","_highlightResult":null,"isOfferCountry":true},{"calling_code":"41","flag":"https://mobimatter.com/assets/flag/flag-CH.png","currency":"CHE","name":"Switzerland","three_letter_abbreviation":"CHE","two_letter_abbreviation":"CH","objectID":"103589812","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"963","flag":"https://mobimatter.com/assets/flag/flag-SY.png","currency":"SYP","name":"Syria","three_letter_abbreviation":"SYR","two_letter_abbreviation":"SY","objectID":"103589822","_highlightResult":null},{"calling_code":"239","flag":"https://mobimatter.com/assets/flag/flag-ST.png","currency":"STD","name":"SГЈo TomГ© and PrГ­ncipe","three_letter_abbreviation":"STP","two_letter_abbreviation":"ST","objectID":"103589832","_highlightResult":null},{"calling_code":"886","flag":"https://mobimatter.com/assets/flag/flag-TW.png","currency":"TWD","name":"Taiwan","three_letter_abbreviation":"TWN","two_letter_abbreviation":"TW","objectID":"103589842","_highlightResult":null,"isOfferCountry":true},{"calling_code":"992","flag":"https://mobimatter.com/assets/flag/flag-TJ.png","currency":"TJS","name":"Tajikistan","three_letter_abbreviation":"TJK","two_letter_abbreviation":"TJ","objectID":"103589852","_highlightResult":null,"isTopUpCountry":true},{"calling_code":"255","flag":"https://mobimatter.com/assets/flag/flag-TZ.png","currency":"TZS","name":"Tanzania","three_letter_abbreviation":"TZA","two_letter_abbreviation":"TZ","objectID":"103589862","_highlightResult":null,"isTopUpCountry":true},{"calling_code":"66","flag":"https://mobimatter.com/assets/flag/flag-TH.png","currency":"THB","name":"Thailand","three_letter_abbreviation":"THA","two_letter_abbreviation":"TH","objectID":"103589872","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"670","flag":"https://mobimatter.com/assets/flag/flag-TL.png","currency":"USD","name":"East Timor","three_letter_abbreviation":"TLS","two_letter_abbreviation":"TL","objectID":"103589882","_highlightResult":null},{"calling_code":"228","flag":"https://mobimatter.com/assets/flag/flag-TG.png","currency":"XOF","name":"Togo","three_letter_abbreviation":"TGO","two_letter_abbreviation":"TG","objectID":"103589892","_highlightResult":null,"isTopUpCountry":true},{"calling_code":"690","flag":"https://mobimatter.com/assets/flag/flag-TK.png","currency":"NZD","name":"Tokelau","three_letter_abbreviation":"TKL","two_letter_abbreviation":"TK","objectID":"103589902","_highlightResult":null},{"calling_code":"676","flag":"https://mobimatter.com/assets/flag/flag-TO.png","currency":"TOP","name":"Tonga","three_letter_abbreviation":"TON","two_letter_abbreviation":"TO","objectID":"103589912","_highlightResult":null,"isTopUpCountry":true},{"calling_code":"1868","flag":"https://mobimatter.com/assets/flag/flag-TT.png","currency":"TTD","name":"Trinidad and Tobago","three_letter_abbreviation":"TTO","two_letter_abbreviation":"TT","objectID":"103589922","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"216","flag":"https://mobimatter.com/assets/flag/flag-TN.png","currency":"TND","name":"Tunisia","three_letter_abbreviation":"TUN","two_letter_abbreviation":"TN","objectID":"103589932","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"90","flag":"https://mobimatter.com/assets/flag/flag-TR.png","currency":"TRY","name":"Turkey","three_letter_abbreviation":"TUR","two_letter_abbreviation":"TR","objectID":"103589942","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"993","flag":"https://mobimatter.com/assets/flag/flag-TM.png","currency":"TMT","name":"Turkmenistan","three_letter_abbreviation":"TKM","two_letter_abbreviation":"TM","objectID":"103589952","_highlightResult":null},{"calling_code":"1649","flag":"https://mobimatter.com/assets/flag/flag-TC.png","currency":"USD","name":"Turks and Caicos Islands","three_letter_abbreviation":"TCA","two_letter_abbreviation":"TC","objectID":"103589962","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"688","flag":"https://mobimatter.com/assets/flag/flag-TV.png","currency":"AUD","name":"Tuvalu","three_letter_abbreviation":"TUV","two_letter_abbreviation":"TV","objectID":"103589972","_highlightResult":null},{"calling_code":"256","flag":"https://mobimatter.com/assets/flag/flag-UG.png","currency":"UGX","name":"Uganda","three_letter_abbreviation":"UGA","two_letter_abbreviation":"UG","objectID":"103589982","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"380","flag":"https://mobimatter.com/assets/flag/flag-UA.png","currency":"UAH","name":"Ukraine","three_letter_abbreviation":"UKR","two_letter_abbreviation":"UA","objectID":"103589992","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"971","flag":"https://mobimatter.com/assets/flag/flag-AE.png","currency":"AED","name":"United Arab Emirates","three_letter_abbreviation":"UAE","two_letter_abbreviation":"AE","objectID":"103590002","_highlightResult":null,"isTopUpCountry":true},{"calling_code":"44","flag":"https://mobimatter.com/assets/flag/flag-GB.png","currency":"GBP","name":"United Kingdom","three_letter_abbreviation":"GBR","two_letter_abbreviation":"GB","objectID":"103590012","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"1","flag":"https://mobimatter.com/assets/flag/flag-US.png","currency":"USD","name":"United States","three_letter_abbreviation":"USA","two_letter_abbreviation":"US","objectID":"103590022","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"flag":"https://mobimatter.com/assets/flag/flag-UM.png","currency":"USD","name":"United States Minor Outlying Islands","three_letter_abbreviation":"UMI","two_letter_abbreviation":"UM","calling_code":"246","objectID":"103590032","_highlightResult":null},{"calling_code":"1340","flag":"https://mobimatter.com/assets/flag/flag-VI.png","currency":"USD","name":"United States Virgin Islands","three_letter_abbreviation":"VIR","two_letter_abbreviation":"VI","objectID":"103590042","_highlightResult":null,"isOfferCountry":true},{"calling_code":"598","flag":"https://mobimatter.com/assets/flag/flag-UY.png","currency":"UYI","name":"Uruguay","three_letter_abbreviation":"URY","two_letter_abbreviation":"UY","objectID":"103590052","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"998","flag":"https://mobimatter.com/assets/flag/flag-UZ.png","currency":"UZS","name":"Uzbekistan","three_letter_abbreviation":"UZB","two_letter_abbreviation":"UZ","objectID":"103590062","_highlightResult":null,"isTopUpCountry":true},{"calling_code":"678","flag":"https://mobimatter.com/assets/flag/flag-VU.png","currency":"VUV","name":"Vanuatu","three_letter_abbreviation":"VUT","two_letter_abbreviation":"VU","objectID":"103590072","_highlightResult":null,"isTopUpCountry":true},{"calling_code":"3906698","flag":"https://mobimatter.com/assets/flag/flag-VA.png","currency":"EUR","name":"Vatican City","three_letter_abbreviation":"VAT","two_letter_abbreviation":"VA","objectID":"103590082","_highlightResult":null,"isOfferCountry":true},{"calling_code":"58","flag":"https://mobimatter.com/assets/flag/flag-VE.png","currency":"VEF","name":"Venezuela","three_letter_abbreviation":"VEN","two_letter_abbreviation":"VE","objectID":"103590092","_highlightResult":null,"isTopUpCountry":true},{"calling_code":"84","flag":"https://mobimatter.com/assets/flag/flag-VN.png","currency":"VND","name":"Vietnam","three_letter_abbreviation":"VNM","two_letter_abbreviation":"VN","objectID":"103590102","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"681","flag":"https://mobimatter.com/assets/flag/flag-WF.png","currency":"XPF","name":"Wallis and Futuna","three_letter_abbreviation":"WLF","two_letter_abbreviation":"WF","objectID":"103590112","_highlightResult":null},{"calling_code":"212","flag":"https://mobimatter.com/assets/flag/flag-EH.png","currency":"MAD","name":"Western Sahara","three_letter_abbreviation":"ESH","two_letter_abbreviation":"EH","objectID":"103590122","_highlightResult":null},{"calling_code":"967","flag":"https://mobimatter.com/assets/flag/flag-YE.png","currency":"YER","name":"Yemen","three_letter_abbreviation":"YEM","two_letter_abbreviation":"YE","objectID":"103590132","_highlightResult":null,"isTopUpCountry":true},{"calling_code":"260","flag":"https://mobimatter.com/assets/flag/flag-ZM.png","currency":"ZMW","name":"Zambia","three_letter_abbreviation":"ZMB","two_letter_abbreviation":"ZM","objectID":"103590142","_highlightResult":null,"isTopUpCountry":true},{"calling_code":"263","flag":"https://mobimatter.com/assets/flag/flag-ZW.png","currency":"ZWL","name":"Zimbabwe","three_letter_abbreviation":"ZWE","two_letter_abbreviation":"ZW","objectID":"103590152","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"358","flag":"https://mobimatter.com/assets/flag/flag-AX.png","currency":"EUR","name":"Г…land Islands","three_letter_abbreviation":"ALA","two_letter_abbreviation":"AX","objectID":"103590162","_highlightResult":null},{"calling_code":"599","flag":"data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiPz4NCjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgd2lkdGg9IjYwMCIgaGVpZ2h0PSI0MDAiPg0KPHJlY3Qgd2lkdGg9IjYwMCIgaGVpZ2h0PSI0MDAiIGZpbGw9IiNmZmYiIC8+DQo8cGF0aCBkPSJNIDYwMCw0MDAgMCw0MDAgNjAwLDAgeiIgZmlsbD0iIzAxMmE4NyIgLz4NCjxwYXRoIGQ9Ik0gMCwwIDAsMTY2LjY2NjY2NyAyNTAsMCB6IiBmaWxsPSIjZjlkOTBmIiAvPg0KPGNpcmNsZSBjeD0iMTY1IiBjeT0iMTczIiByPSI4MCIgZmlsbD0ibm9uZSIgc3Ryb2tlPSIjMDAwIiBzdHJva2Utd2lkdGg9IjExIiAvPg0KPGcgaWQ9ImciPg0KPHBhdGggZD0ibSAxNjUsNjkuODM0NDc0IDE1LDI1Ljk4MDc2MjExIC0zMCwwIHoiIGZpbGw9IiMwMDAiIGlkPSJhIi8+DQo8dXNlIHhsaW5rOmhyZWY9IiNhIiB0cmFuc2Zvcm09InJvdGF0ZSgxODAsMTY1LDE3MykiIC8+DQo8L2c+DQo8dXNlIHhsaW5rOmhyZWY9IiNnIiB0cmFuc2Zvcm09InJvdGF0ZSg5MCwxNjUsMTczKSIgLz4NCjxwYXRoIGQ9Ik0gMTY1LDEyMiAyMDkuMTY3Mjk1NTksMTk4LjUgMTIwLjgzMjcwNDQxLDE5OC41IHoiIGZpbGw9IiNkYzE3MWQiIGlkPSJiIiAvPg0KPHVzZSB4bGluazpocmVmPSIjYiIgdHJhbnNmb3JtPSJyb3RhdGUoNjAsMTY1LDE3MykiIC8+DQo8L3N2Zz4=","currency":"USD","name":"Bonaire","three_letter_abbreviation":"BES","two_letter_abbreviation":"BQ"},{"calling_code":"1","flag":"https://mobimatter.com/assets/flag/flag-US-HI.png","currency":"USD","name":"Hawaii","three_letter_abbreviation":"US-HI","two_letter_abbreviation":"US-HI","objectID":"1034211164","isOfferCountry":true,"_highlightResult":null}]';
});
var countries_data = "";
$(document).ready(function () {
    "/" == window.location.pathname || "/travel-esim" == window.location.pathname || "/topup" == window.location.pathname
        ? $(".country-search").select2({ templateResult: r, matcher: e, placeholder: "Search for a country" })
        : setTimeout(function () {
              $(".country-search").select2({ templateResult: r, matcher: e, placeholder: "Search for a country" }), $("#search-form").css({ visibility: "unset" });
          }, 2e3);
    countries_data = JSON.parse(
        '[{"calling_code":"93","flag":"https://mobimatter.com/assets/flag/flag-AF.png","currency":"AFN","name":"Afghanistan","three_letter_abbreviation":"AFG","two_letter_abbreviation":"AF","objectID":"103587692","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"355","flag":"https://mobimatter.com/assets/flag/flag-AL.png","currency":"ALL","name":"Albania","three_letter_abbreviation":"ALB","two_letter_abbreviation":"AL","objectID":"103587702","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"213","flag":"https://mobimatter.com/assets/flag/flag-DZ.png","currency":"DZD","name":"Algeria","three_letter_abbreviation":"DZA","two_letter_abbreviation":"DZ","objectID":"103587712","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"1684","flag":"https://mobimatter.com/assets/flag/flag-AS.png","currency":"USD","name":"American Samoa","three_letter_abbreviation":"ASM","two_letter_abbreviation":"AS","objectID":"103587722","_highlightResult":null,"isTopUpCountry":true},{"calling_code":"376","flag":"https://mobimatter.com/assets/flag/flag-AD.png","currency":"EUR","name":"Andorra","three_letter_abbreviation":"AND","two_letter_abbreviation":"AD","objectID":"103587732","_highlightResult":null,"isOfferCountry":true},{"calling_code":"244","flag":"https://mobimatter.com/assets/flag/flag-AO.png","currency":"AOA","name":"Angola","three_letter_abbreviation":"AGO","two_letter_abbreviation":"AO","objectID":"103587742","_highlightResult":null,"isTopUpCountry":true},{"calling_code":"1264","flag":"https://mobimatter.com/assets/flag/flag-AI.png","currency":"XCD","name":"Anguilla","three_letter_abbreviation":"AIA","two_letter_abbreviation":"AI","objectID":"103587752","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"flag":"https://mobimatter.com/assets/flag/flag-AQ.png","name":"Antarctica","three_letter_abbreviation":"ATA","two_letter_abbreviation":"AQ","calling_code":"672","objectID":"103587762","_highlightResult":null},{"calling_code":"1268","flag":"https://mobimatter.com/assets/flag/flag-AG.png","currency":"XCD","name":"Antigua and Barbuda","three_letter_abbreviation":"ATG","two_letter_abbreviation":"AG","objectID":"103587772","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"54","flag":"https://mobimatter.com/assets/flag/flag-AR.png","currency":"ARS","name":"Argentina","three_letter_abbreviation":"ARG","two_letter_abbreviation":"AR","objectID":"103587782","_highlightResult":null,"isTopUpCountry":true},{"calling_code":"374","flag":"https://mobimatter.com/assets/flag/flag-AM.png","currency":"AMD","name":"Armenia","three_letter_abbreviation":"ARM","two_letter_abbreviation":"AM","objectID":"103587792","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"297","flag":"https://mobimatter.com/assets/flag/flag-AW.png","currency":"AWG","name":"Aruba","three_letter_abbreviation":"ABW","two_letter_abbreviation":"AW","objectID":"103587802","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"61","flag":"https://mobimatter.com/assets/flag/flag-AU.png","currency":"AUD","name":"Australia","three_letter_abbreviation":"AUS","two_letter_abbreviation":"AU","objectID":"103587812","_highlightResult":null,"isOfferCountry":true},{"calling_code":"43","flag":"https://mobimatter.com/assets/flag/flag-AT.png","currency":"EUR","name":"Austria","three_letter_abbreviation":"AUT","two_letter_abbreviation":"AT","objectID":"103587822","_highlightResult":null,"isOfferCountry":true},{"calling_code":"994","flag":"https://mobimatter.com/assets/flag/flag-AZ.png","currency":"AZN","name":"Azerbaijan","three_letter_abbreviation":"AZE","two_letter_abbreviation":"AZ","objectID":"103587832","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"1242","flag":"https://mobimatter.com/assets/flag/flag-BS.png","currency":"BSD","name":"Bahamas","three_letter_abbreviation":"BHS","two_letter_abbreviation":"BS","objectID":"103587842","_highlightResult":null,"isTopUpCountry":true},{"calling_code":"973","flag":"https://mobimatter.com/assets/flag/flag-BH.png","currency":"BHD","name":"Bahrain","three_letter_abbreviation":"BHR","two_letter_abbreviation":"BH","objectID":"103587852","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"880","flag":"https://mobimatter.com/assets/flag/flag-BD.png","currency":"BDT","name":"Bangladesh","three_letter_abbreviation":"BGD","two_letter_abbreviation":"BD","objectID":"103587862","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"1246","flag":"https://mobimatter.com/assets/flag/flag-BB.png","currency":"BBD","name":"Barbados","three_letter_abbreviation":"BRB","two_letter_abbreviation":"BB","objectID":"103587872","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"375","flag":"https://mobimatter.com/assets/flag/flag-BY.png","currency":"BYR","name":"Belarus","three_letter_abbreviation":"BLR","two_letter_abbreviation":"BY","objectID":"103587882","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"32","flag":"https://mobimatter.com/assets/flag/flag-BE.png","currency":"EUR","name":"Belgium","three_letter_abbreviation":"BEL","two_letter_abbreviation":"BE","objectID":"103587892","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"501","flag":"https://mobimatter.com/assets/flag/flag-BZ.png","currency":"BZD","name":"Belize","three_letter_abbreviation":"BLZ","two_letter_abbreviation":"BZ","objectID":"103587902","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"229","flag":"https://mobimatter.com/assets/flag/flag-BJ.png","currency":"XOF","name":"Benin","three_letter_abbreviation":"BEN","two_letter_abbreviation":"BJ","objectID":"103587912","_highlightResult":null,"isTopUpCountry":true},{"calling_code":"1441","flag":"https://mobimatter.com/assets/flag/flag-BM.png","currency":"BMD","name":"Bermuda","three_letter_abbreviation":"BMU","two_letter_abbreviation":"BM","objectID":"103587922","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"975","flag":"https://mobimatter.com/assets/flag/flag-BT.png","currency":"BTN","name":"Bhutan","three_letter_abbreviation":"BTN","two_letter_abbreviation":"BT","objectID":"103587932","_highlightResult":null},{"calling_code":"591","flag":"https://mobimatter.com/assets/flag/flag-BO.png","currency":"BOB","name":"Bolivia","three_letter_abbreviation":"BOL","two_letter_abbreviation":"BO","objectID":"103587942","_highlightResult":null,"isTopUpCountry":true},{"calling_code":"387","flag":"https://mobimatter.com/assets/flag/flag-BA.png","currency":"BAM","name":"Bosnia and Herzegovina","three_letter_abbreviation":"BIH","two_letter_abbreviation":"BA","objectID":"103587952","_highlightResult":null,"isOfferCountry":true},{"calling_code":"267","flag":"https://mobimatter.com/assets/flag/flag-BW.png","currency":"BWP","name":"Botswana","three_letter_abbreviation":"BWA","two_letter_abbreviation":"BW","objectID":"103587962","_highlightResult":null,"isTopUpCountry":true},{"flag":"https://mobimatter.com/assets/flag/flag-BV.png","currency":"NOK","name":"Bouvet Island","three_letter_abbreviation":"BVT","two_letter_abbreviation":"BV","calling_code":"47","objectID":"103587972","_highlightResult":null},{"calling_code":"55","flag":"https://mobimatter.com/assets/flag/flag-BR.png","currency":"BRL","name":"Brazil","three_letter_abbreviation":"BRA","two_letter_abbreviation":"BR","objectID":"103587982","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"246","flag":"https://mobimatter.com/assets/flag/flag-IO.png","currency":"USD","name":"British Indian Ocean Territory","three_letter_abbreviation":"IOT","two_letter_abbreviation":"IO","objectID":"103587992","_highlightResult":null},{"calling_code":"1284","flag":"https://mobimatter.com/assets/flag/flag-VG.png","currency":"USD","name":"British Virgin Islands","three_letter_abbreviation":"VGB","two_letter_abbreviation":"VG","objectID":"103588002","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"673","flag":"https://mobimatter.com/assets/flag/flag-BN.png","currency":"BND","name":"Brunei","three_letter_abbreviation":"BRN","two_letter_abbreviation":"BN","objectID":"103588012","_highlightResult":null,"isOfferCountry":true},{"calling_code":"359","flag":"https://mobimatter.com/assets/flag/flag-BG.png","currency":"BGN","name":"Bulgaria","three_letter_abbreviation":"BGR","two_letter_abbreviation":"BG","objectID":"103588022","_highlightResult":null,"isOfferCountry":true},{"calling_code":"226","flag":"https://mobimatter.com/assets/flag/flag-BF.png","currency":"XOF","name":"Burkina Faso","three_letter_abbreviation":"BFA","two_letter_abbreviation":"BF","objectID":"103588032","_highlightResult":null,"isTopUpCountry":true},{"calling_code":"257","flag":"https://mobimatter.com/assets/flag/flag-BI.png","currency":"BIF","name":"Burundi","three_letter_abbreviation":"BDI","two_letter_abbreviation":"BI","objectID":"103588042","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"855","flag":"https://mobimatter.com/assets/flag/flag-KH.png","currency":"KHR","name":"Cambodia","three_letter_abbreviation":"KHM","two_letter_abbreviation":"KH","objectID":"103588052","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"237","flag":"https://mobimatter.com/assets/flag/flag-CM.png","currency":"XAF","name":"Cameroon","three_letter_abbreviation":"CMR","two_letter_abbreviation":"CM","objectID":"103588062","_highlightResult":null,"isTopUpCountry":true},{"calling_code":"1","flag":"https://mobimatter.com/assets/flag/flag-CA.png","currency":"CAD","name":"Canada","three_letter_abbreviation":"CAN","two_letter_abbreviation":"CA","objectID":"103588072","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"238","flag":"https://mobimatter.com/assets/flag/flag-CV.png","currency":"CVE","name":"Cape Verde","three_letter_abbreviation":"CPV","two_letter_abbreviation":"CV","objectID":"103588082","_highlightResult":null,"isTopUpCountry":true},{"calling_code":"1345","flag":"https://mobimatter.com/assets/flag/flag-KY.png","currency":"KYD","name":"Cayman Islands","three_letter_abbreviation":"CYM","two_letter_abbreviation":"KY","objectID":"103588092","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"236","flag":"https://mobimatter.com/assets/flag/flag-CF.png","currency":"XAF","name":"Central African Republic","three_letter_abbreviation":"CAF","two_letter_abbreviation":"CF","objectID":"103588102","_highlightResult":null,"isTopUpCountry":true},{"calling_code":"235","flag":"https://mobimatter.com/assets/flag/flag-TD.png","currency":"XAF","name":"Chad","three_letter_abbreviation":"TCD","two_letter_abbreviation":"TD","objectID":"103588112","_highlightResult":null},{"calling_code":"56","flag":"https://mobimatter.com/assets/flag/flag-CL.png","currency":"CLF","name":"Chile","three_letter_abbreviation":"CHL","two_letter_abbreviation":"CL","objectID":"103588122","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"86","flag":"https://mobimatter.com/assets/flag/flag-CN.png","currency":"CNY","name":"China","three_letter_abbreviation":"CHN","two_letter_abbreviation":"CN","objectID":"103588132","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"61","flag":"https://mobimatter.com/assets/flag/flag-CX.png","currency":"AUD","name":"Christmas Island","three_letter_abbreviation":"CXR","two_letter_abbreviation":"CX","objectID":"103588142","_highlightResult":null},{"calling_code":"61","flag":"https://mobimatter.com/assets/flag/flag-CC.png","currency":"AUD","name":"Cocos (Keeling) Islands","three_letter_abbreviation":"CCK","two_letter_abbreviation":"CC","objectID":"103588152","_highlightResult":null},{"calling_code":"57","flag":"https://mobimatter.com/assets/flag/flag-CO.png","currency":"COP","name":"Colombia","three_letter_abbreviation":"COL","two_letter_abbreviation":"CO","objectID":"103588162","_highlightResult":null,"isTopUpCountry":true},{"calling_code":"269","flag":"https://mobimatter.com/assets/flag/flag-KM.png","currency":"KMF","name":"Comoros","three_letter_abbreviation":"COM","two_letter_abbreviation":"KM","objectID":"103588172","_highlightResult":null,"isTopUpCountry":true},{"calling_code":"682","flag":"https://mobimatter.com/assets/flag/flag-CK.png","currency":"NZD","name":"Cook Islands","three_letter_abbreviation":"COK","two_letter_abbreviation":"CK","objectID":"103588182","_highlightResult":null},{"calling_code":"506","flag":"https://mobimatter.com/assets/flag/flag-CR.png","currency":"CRC","name":"Costa Rica","three_letter_abbreviation":"CRI","two_letter_abbreviation":"CR","objectID":"103588192","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"385","flag":"https://mobimatter.com/assets/flag/flag-HR.png","currency":"HRK","name":"Croatia","three_letter_abbreviation":"HRV","two_letter_abbreviation":"HR","objectID":"103588202","_highlightResult":null,"isOfferCountry":true},{"calling_code":"53","flag":"https://mobimatter.com/assets/flag/flag-CU.png","currency":"CUC","name":"Cuba","three_letter_abbreviation":"CUB","two_letter_abbreviation":"CU","objectID":"103588212","_highlightResult":null},{"calling_code":"5999","flag":"https://mobimatter.com/assets/flag/flag-CW.png","currency":"ANG","name":"Curacao","three_letter_abbreviation":"CUW","two_letter_abbreviation":"CW","objectID":"103588222","_highlightResult":null,"isOfferCountry":true},{"calling_code":"357","flag":"https://mobimatter.com/assets/flag/flag-CY.png","currency":"EUR","name":"Cyprus","three_letter_abbreviation":"CYP","two_letter_abbreviation":"CY","objectID":"103588232","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"420","flag":"https://mobimatter.com/assets/flag/flag-CZ.png","currency":"CZK","name":"Czech Republic","three_letter_abbreviation":"CZE","two_letter_abbreviation":"CZ","objectID":"103588242","_highlightResult":null,"isOfferCountry":true},{"calling_code":"243","flag":"https://mobimatter.com/assets/flag/flag-CD.png","currency":"CDF","name":"DR Congo","three_letter_abbreviation":"COD","two_letter_abbreviation":"CD","objectID":"103588252","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"45","flag":"https://mobimatter.com/assets/flag/flag-DK.png","currency":"DKK","name":"Denmark","three_letter_abbreviation":"DNK","two_letter_abbreviation":"DK","objectID":"103588262","_highlightResult":null,"isOfferCountry":true},{"calling_code":"253","flag":"https://mobimatter.com/assets/flag/flag-DJ.png","currency":"DJF","name":"Djibouti","three_letter_abbreviation":"DJI","two_letter_abbreviation":"DJ","objectID":"103588272","_highlightResult":null},{"calling_code":"1767","flag":"https://mobimatter.com/assets/flag/flag-DM.png","currency":"XCD","name":"Dominica","three_letter_abbreviation":"DMA","two_letter_abbreviation":"DM","objectID":"103588282","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"1809","flag":"https://mobimatter.com/assets/flag/flag-DO.png","currency":"DOP","name":"Dominican Republic","three_letter_abbreviation":"DOM","two_letter_abbreviation":"DO","objectID":"103588292","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"593","flag":"https://mobimatter.com/assets/flag/flag-EC.png","currency":"USD","name":"Ecuador","three_letter_abbreviation":"ECU","two_letter_abbreviation":"EC","objectID":"103588302","_highlightResult":null,"isTopUpCountry":true},{"calling_code":"20","flag":"https://mobimatter.com/assets/flag/flag-EG.png","currency":"EGP","name":"Egypt","three_letter_abbreviation":"EGY","two_letter_abbreviation":"EG","objectID":"103588312","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"503","flag":"https://mobimatter.com/assets/flag/flag-SV.png","currency":"SVC","name":"El Salvador","three_letter_abbreviation":"SLV","two_letter_abbreviation":"SV","objectID":"103588322","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"240","flag":"https://mobimatter.com/assets/flag/flag-GQ.png","currency":"XAF","name":"Equatorial Guinea","three_letter_abbreviation":"GNQ","two_letter_abbreviation":"GQ","objectID":"103588332","_highlightResult":null},{"calling_code":"291","flag":"https://mobimatter.com/assets/flag/flag-ER.png","currency":"ERN","name":"Eritrea","three_letter_abbreviation":"ERI","two_letter_abbreviation":"ER","objectID":"103588342","_highlightResult":null},{"calling_code":"372","flag":"https://mobimatter.com/assets/flag/flag-EE.png","currency":"EUR","name":"Estonia","three_letter_abbreviation":"EST","two_letter_abbreviation":"EE","objectID":"103588352","_highlightResult":null,"isOfferCountry":true},{"calling_code":"268","flag":"https://mobimatter.com/assets/flag/flag-SZ.png","currency":"SZL","name":"Eswatini","three_letter_abbreviation":"SWZ","two_letter_abbreviation":"SZ","objectID":"103589792","_highlightResult":null,"isTopUpCountry":true},{"calling_code":"251","flag":"https://mobimatter.com/assets/flag/flag-ET.png","currency":"ETB","name":"Ethiopia","three_letter_abbreviation":"ETH","two_letter_abbreviation":"ET","objectID":"103588362","_highlightResult":null,"isTopUpCountry":true},{"calling_code":"500","flag":"https://mobimatter.com/assets/flag/flag-FK.png","currency":"FKP","name":"Falkland Islands","three_letter_abbreviation":"FLK","two_letter_abbreviation":"FK","objectID":"103588372","_highlightResult":null},{"calling_code":"298","flag":"https://mobimatter.com/assets/flag/flag-FO.png","currency":"DKK","name":"Faroe Islands","three_letter_abbreviation":"FRO","two_letter_abbreviation":"FO","objectID":"103588382","_highlightResult":null},{"calling_code":"679","flag":"https://mobimatter.com/assets/flag/flag-FJ.png","currency":"FJD","name":"Fiji","three_letter_abbreviation":"FJI","two_letter_abbreviation":"FJ","objectID":"103588392","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"358","flag":"https://mobimatter.com/assets/flag/flag-FI.png","currency":"EUR","name":"Finland","three_letter_abbreviation":"FIN","two_letter_abbreviation":"FI","objectID":"103588402","_highlightResult":null,"isOfferCountry":true},{"calling_code":"33","flag":"https://mobimatter.com/assets/flag/flag-FR.png","currency":"EUR","name":"France","three_letter_abbreviation":"FRA","two_letter_abbreviation":"FR","objectID":"103588412","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"594","flag":"https://mobimatter.com/assets/flag/flag-GF.png","currency":"EUR","name":"French Guiana","three_letter_abbreviation":"GUF","two_letter_abbreviation":"GF","objectID":"103588422","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"689","flag":"https://mobimatter.com/assets/flag/flag-PF.png","currency":"XPF","name":"French Polynesia","three_letter_abbreviation":"PYF","two_letter_abbreviation":"PF","objectID":"103588432","_highlightResult":null},{"flag":"https://mobimatter.com/assets/flag/flag-TF.png","currency":"EUR","name":"French Southern and Antarctic Lands","three_letter_abbreviation":"ATF","two_letter_abbreviation":"TF","calling_code":"011","objectID":"103588442","_highlightResult":null},{"calling_code":"241","flag":"https://mobimatter.com/assets/flag/flag-GA.png","currency":"XAF","name":"Gabon","three_letter_abbreviation":"GAB","two_letter_abbreviation":"GA","objectID":"103588452","_highlightResult":null},{"calling_code":"220","flag":"https://mobimatter.com/assets/flag/flag-GM.png","currency":"GMD","name":"Gambia","three_letter_abbreviation":"GMB","two_letter_abbreviation":"GM","objectID":"103588462","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"995","flag":"https://mobimatter.com/assets/flag/flag-GE.png","currency":"GEL","name":"Georgia","three_letter_abbreviation":"GEO","two_letter_abbreviation":"GE","objectID":"103588472","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"49","flag":"https://mobimatter.com/assets/flag/flag-DE.png","currency":"EUR","name":"Germany","three_letter_abbreviation":"DEU","two_letter_abbreviation":"DE","objectID":"103588482","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"233","flag":"https://mobimatter.com/assets/flag/flag-GH.png","currency":"GHS","name":"Ghana","three_letter_abbreviation":"GHA","two_letter_abbreviation":"GH","objectID":"103588492","_highlightResult":null,"isTopUpCountry":true},{"calling_code":"350","flag":"https://mobimatter.com/assets/flag/flag-GI.png","currency":"GIP","name":"Gibraltar","three_letter_abbreviation":"GIB","two_letter_abbreviation":"GI","objectID":"103588502","_highlightResult":null,"isOfferCountry":true},{"calling_code":"30","flag":"https://mobimatter.com/assets/flag/flag-GR.png","currency":"EUR","name":"Greece","three_letter_abbreviation":"GRC","two_letter_abbreviation":"GR","objectID":"103588512","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"299","flag":"https://mobimatter.com/assets/flag/flag-GL.png","currency":"DKK","name":"Greenland","three_letter_abbreviation":"GRL","two_letter_abbreviation":"GL","objectID":"103588522","_highlightResult":null,"isOfferCountry":true},{"calling_code":"1473","flag":"https://mobimatter.com/assets/flag/flag-GD.png","currency":"XCD","name":"Grenada","three_letter_abbreviation":"GRD","two_letter_abbreviation":"GD","objectID":"103588532","_highlightResult":null,"isTopUpCountry":true},{"calling_code":"590","flag":"https://mobimatter.com/assets/flag/flag-GP.png","currency":"EUR","name":"Guadeloupe","three_letter_abbreviation":"GLP","two_letter_abbreviation":"GP","objectID":"103588542","_highlightResult":null,"isOfferCountry":true},{"calling_code":"1671","flag":"https://mobimatter.com/assets/flag/flag-GU.png","currency":"USD","name":"Guam","three_letter_abbreviation":"GUM","two_letter_abbreviation":"GU","objectID":"103588552","_highlightResult":null},{"calling_code":"502","flag":"https://mobimatter.com/assets/flag/flag-GT.png","currency":"GTQ","name":"Guatemala","three_letter_abbreviation":"GTM","two_letter_abbreviation":"GT","objectID":"103588562","_highlightResult":null,"isTopUpCountry":true},{"calling_code":"44","flag":"https://mobimatter.com/assets/flag/flag-GG.png","currency":"GBP","name":"Guernsey","three_letter_abbreviation":"GGY","two_letter_abbreviation":"GG","objectID":"103588572","_highlightResult":null,"isOfferCountry":true},{"calling_code":"224","flag":"https://mobimatter.com/assets/flag/flag-GN.png","currency":"GNF","name":"Guinea","three_letter_abbreviation":"GIN","two_letter_abbreviation":"GN","objectID":"103588582","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"245","flag":"https://mobimatter.com/assets/flag/flag-GW.png","currency":"XOF","name":"Guinea Bissau","three_letter_abbreviation":"GNB","two_letter_abbreviation":"GW","objectID":"103588592","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"592","flag":"https://mobimatter.com/assets/flag/flag-GY.png","currency":"GYD","name":"Guyana","three_letter_abbreviation":"GUY","two_letter_abbreviation":"GY","objectID":"103588602","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"509","flag":"https://mobimatter.com/assets/flag/flag-HT.png","currency":"HTG","name":"Haiti","three_letter_abbreviation":"HTI","two_letter_abbreviation":"HT","objectID":"103588612","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"flag":"https://mobimatter.com/assets/flag/flag-HM.png","currency":"AUD","name":"Heard Island and McDonald Islands","three_letter_abbreviation":"HMD","two_letter_abbreviation":"HM","calling_code":"672","objectID":"103588622","_highlightResult":null},{"calling_code":"504","flag":"https://mobimatter.com/assets/flag/flag-HN.png","currency":"HNL","name":"Honduras","three_letter_abbreviation":"HND","two_letter_abbreviation":"HN","objectID":"103588632","_highlightResult":null,"isTopUpCountry":true},{"calling_code":"852","flag":"https://mobimatter.com/assets/flag/flag-HK.png","currency":"HKD","name":"Hong Kong","three_letter_abbreviation":"HKG","two_letter_abbreviation":"HK","objectID":"103588642","_highlightResult":null,"isOfferCountry":true},{"calling_code":"36","flag":"https://mobimatter.com/assets/flag/flag-HU.png","currency":"HUF","name":"Hungary","three_letter_abbreviation":"HUN","two_letter_abbreviation":"HU","objectID":"103588652","_highlightResult":null,"isOfferCountry":true},{"calling_code":"354","flag":"https://mobimatter.com/assets/flag/flag-IS.png","currency":"ISK","name":"Iceland","three_letter_abbreviation":"ISL","two_letter_abbreviation":"IS","objectID":"103588662","_highlightResult":null,"isOfferCountry":true},{"calling_code":"91","flag":"https://mobimatter.com/assets/flag/flag-IN.png","currency":"INR","name":"India","three_letter_abbreviation":"IND","two_letter_abbreviation":"IN","objectID":"103588672","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"62","flag":"https://mobimatter.com/assets/flag/flag-ID.png","currency":"IDR","name":"Indonesia","three_letter_abbreviation":"IDN","two_letter_abbreviation":"ID","objectID":"103588682","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"98","flag":"https://mobimatter.com/assets/flag/flag-IR.png","currency":"IRR","name":"Iran","three_letter_abbreviation":"IRN","two_letter_abbreviation":"IR","objectID":"103588692","_highlightResult":null,"isOfferCountry":true},{"calling_code":"964","flag":"https://mobimatter.com/assets/flag/flag-IQ.png","currency":"IQD","name":"Iraq","three_letter_abbreviation":"IRQ","two_letter_abbreviation":"IQ","objectID":"103588702","_highlightResult":null,"isTopUpCountry":true},{"calling_code":"353","flag":"https://mobimatter.com/assets/flag/flag-IE.png","currency":"EUR","name":"Ireland","three_letter_abbreviation":"IRL","two_letter_abbreviation":"IE","objectID":"103588712","_highlightResult":null,"isOfferCountry":true},{"calling_code":"44","flag":"https://mobimatter.com/assets/flag/flag-IM.png","currency":"GBP","name":"Isle of Man","three_letter_abbreviation":"IMN","two_letter_abbreviation":"IM","objectID":"103588722","_highlightResult":null,"isOfferCountry":true},{"calling_code":"972","flag":"https://mobimatter.com/assets/flag/flag-IL.png","currency":"ILS","name":"Israel","three_letter_abbreviation":"ISR","two_letter_abbreviation":"IL","objectID":"103588732","_highlightResult":null,"isOfferCountry":true},{"calling_code":"39","flag":"https://mobimatter.com/assets/flag/flag-IT.png","currency":"EUR","name":"Italy","three_letter_abbreviation":"ITA","two_letter_abbreviation":"IT","objectID":"103588742","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"225","flag":"https://mobimatter.com/assets/flag/flag-CI.png","currency":"XOF","name":"Ivory Coast","three_letter_abbreviation":"CIV","two_letter_abbreviation":"CI","objectID":"103588752","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"1876","flag":"https://mobimatter.com/assets/flag/flag-JM.png","currency":"JMD","name":"Jamaica","three_letter_abbreviation":"JAM","two_letter_abbreviation":"JM","objectID":"103588762","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"81","flag":"https://mobimatter.com/assets/flag/flag-JP.png","currency":"JPY","name":"Japan","three_letter_abbreviation":"JPN","two_letter_abbreviation":"JP","objectID":"103588772","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"44","flag":"https://mobimatter.com/assets/flag/flag-JE.png","currency":"GBP","name":"Jersey","three_letter_abbreviation":"JEY","two_letter_abbreviation":"JE","objectID":"103588782","_highlightResult":null,"isOfferCountry":true},{"calling_code":"962","flag":"https://mobimatter.com/assets/flag/flag-JO.png","currency":"JOD","name":"Jordan","three_letter_abbreviation":"JOR","two_letter_abbreviation":"JO","objectID":"103588792","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"7","flag":"https://mobimatter.com/assets/flag/flag-KZ.png","currency":"KZT","name":"Kazakhstan","three_letter_abbreviation":"KAZ","two_letter_abbreviation":"KZ","objectID":"103588802","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"254","flag":"https://mobimatter.com/assets/flag/flag-KE.png","currency":"KES","name":"Kenya","three_letter_abbreviation":"KEN","two_letter_abbreviation":"KE","objectID":"103588812","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"686","flag":"https://mobimatter.com/assets/flag/flag-KI.png","currency":"AUD","name":"Kiribati","three_letter_abbreviation":"KIR","two_letter_abbreviation":"KI","objectID":"103588822","_highlightResult":null},{"calling_code":"383","flag":"https://mobimatter.com/assets/flag/flag-XK.png","currency":"EUR","name":"Kosovo","three_letter_abbreviation":"XKX","two_letter_abbreviation":"XK","objectID":"103588832","_highlightResult":null,"isTopUpCountry":true},{"calling_code":"965","flag":"https://mobimatter.com/assets/flag/flag-KW.png","currency":"KWD","name":"Kuwait","three_letter_abbreviation":"KWT","two_letter_abbreviation":"KW","objectID":"103588842","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"996","flag":"https://mobimatter.com/assets/flag/flag-KG.png","currency":"KGS","name":"Kyrgyzstan","three_letter_abbreviation":"KGZ","two_letter_abbreviation":"KG","objectID":"103588852","_highlightResult":null,"isTopUpCountry":true},{"calling_code":"856","flag":"https://mobimatter.com/assets/flag/flag-LA.png","currency":"LAK","name":"Laos","three_letter_abbreviation":"LAO","two_letter_abbreviation":"LA","objectID":"103588862","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"371","flag":"https://mobimatter.com/assets/flag/flag-LV.png","currency":"EUR","name":"Latvia","three_letter_abbreviation":"LVA","two_letter_abbreviation":"LV","objectID":"103588872","_highlightResult":null,"isOfferCountry":true},{"calling_code":"961","flag":"https://mobimatter.com/assets/flag/flag-LB.png","currency":"LBP","name":"Lebanon","three_letter_abbreviation":"LBN","two_letter_abbreviation":"LB","objectID":"103588882","_highlightResult":null,"isTopUpCountry":true},{"calling_code":"266","flag":"https://mobimatter.com/assets/flag/flag-LS.png","currency":"LSL","name":"Lesotho","three_letter_abbreviation":"LSO","two_letter_abbreviation":"LS","objectID":"103588892","_highlightResult":null},{"calling_code":"231","flag":"https://mobimatter.com/assets/flag/flag-LR.png","currency":"LRD","name":"Liberia","three_letter_abbreviation":"LBR","two_letter_abbreviation":"LR","objectID":"103588902","_highlightResult":null,"isTopUpCountry":true},{"calling_code":"218","flag":"https://mobimatter.com/assets/flag/flag-LY.png","currency":"LYD","name":"Libya","three_letter_abbreviation":"LBY","two_letter_abbreviation":"LY","objectID":"103588912","_highlightResult":null},{"calling_code":"423","flag":"https://mobimatter.com/assets/flag/flag-LI.png","currency":"CHF","name":"Liechtenstein","three_letter_abbreviation":"LIE","two_letter_abbreviation":"LI","objectID":"103588922","_highlightResult":null,"isOfferCountry":true},{"calling_code":"370","flag":"https://mobimatter.com/assets/flag/flag-LT.png","currency":"EUR","name":"Lithuania","three_letter_abbreviation":"LTU","two_letter_abbreviation":"LT","objectID":"103588932","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"352","flag":"https://mobimatter.com/assets/flag/flag-LU.png","currency":"EUR","name":"Luxembourg","three_letter_abbreviation":"LUX","two_letter_abbreviation":"LU","objectID":"103588942","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"853","flag":"https://mobimatter.com/assets/flag/flag-MO.png","currency":"MOP","name":"Macau","three_letter_abbreviation":"MAC","two_letter_abbreviation":"MO","objectID":"103588952","_highlightResult":null,"isOfferCountry":true},{"calling_code":"389","flag":"https://mobimatter.com/assets/flag/flag-MK.png","currency":"MKD","name":"Macedonia","three_letter_abbreviation":"MKD","two_letter_abbreviation":"MK","objectID":"103588962","_highlightResult":null,"isOfferCountry":true},{"calling_code":"261","flag":"https://mobimatter.com/assets/flag/flag-MG.png","currency":"MGA","name":"Madagascar","three_letter_abbreviation":"MDG","two_letter_abbreviation":"MG","objectID":"103588972","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"265","flag":"https://mobimatter.com/assets/flag/flag-MW.png","currency":"MWK","name":"Malawi","three_letter_abbreviation":"MWI","two_letter_abbreviation":"MW","objectID":"103588982","_highlightResult":null,"isTopUpCountry":true},{"calling_code":"60","flag":"https://mobimatter.com/assets/flag/flag-MY.png","currency":"MYR","name":"Malaysia","three_letter_abbreviation":"MYS","two_letter_abbreviation":"MY","objectID":"103588992","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"960","flag":"https://mobimatter.com/assets/flag/flag-MV.png","currency":"MVR","name":"Maldives","three_letter_abbreviation":"MDV","two_letter_abbreviation":"MV","objectID":"103589002","_highlightResult":null},{"calling_code":"223","flag":"https://mobimatter.com/assets/flag/flag-ML.png","currency":"XOF","name":"Mali","three_letter_abbreviation":"MLI","two_letter_abbreviation":"ML","objectID":"103589012","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"356","flag":"https://mobimatter.com/assets/flag/flag-MT.png","currency":"EUR","name":"Malta","three_letter_abbreviation":"MLT","two_letter_abbreviation":"MT","objectID":"103589022","_highlightResult":null,"isOfferCountry":true},{"calling_code":"692","flag":"https://mobimatter.com/assets/flag/flag-MH.png","currency":"USD","name":"Marshall Islands","three_letter_abbreviation":"MHL","two_letter_abbreviation":"MH","objectID":"103589032","_highlightResult":null},{"calling_code":"596","flag":"https://mobimatter.com/assets/flag/flag-MQ.png","currency":"EUR","name":"Martinique","three_letter_abbreviation":"MTQ","two_letter_abbreviation":"MQ","objectID":"103589042","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"222","flag":"https://mobimatter.com/assets/flag/flag-MR.png","currency":"MRO","name":"Mauritania","three_letter_abbreviation":"MRT","two_letter_abbreviation":"MR","objectID":"103589052","_highlightResult":null},{"calling_code":"230","flag":"https://mobimatter.com/assets/flag/flag-MU.png","currency":"MUR","name":"Mauritius","three_letter_abbreviation":"MUS","two_letter_abbreviation":"MU","objectID":"103589062","_highlightResult":null},{"calling_code":"262","flag":"https://mobimatter.com/assets/flag/flag-YT.png","currency":"EUR","name":"Mayotte","three_letter_abbreviation":"MYT","two_letter_abbreviation":"YT","objectID":"103589072","_highlightResult":null,"isOfferCountry":true},{"calling_code":"52","flag":"https://mobimatter.com/assets/flag/flag-MX.png","currency":"MXN","name":"Mexico","three_letter_abbreviation":"MEX","two_letter_abbreviation":"MX","objectID":"103589082","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"691","flag":"https://mobimatter.com/assets/flag/flag-FM.png","currency":"USD","name":"Micronesia","three_letter_abbreviation":"FSM","two_letter_abbreviation":"FM","objectID":"103589092","_highlightResult":null},{"calling_code":"373","flag":"https://mobimatter.com/assets/flag/flag-MD.png","currency":"MDL","name":"Moldova","three_letter_abbreviation":"MDA","two_letter_abbreviation":"MD","objectID":"103589102","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"377","flag":"https://mobimatter.com/assets/flag/flag-MC.png","currency":"EUR","name":"Monaco","three_letter_abbreviation":"MCO","two_letter_abbreviation":"MC","objectID":"103589112","_highlightResult":null},{"calling_code":"976","flag":"https://mobimatter.com/assets/flag/flag-MN.png","currency":"MNT","name":"Mongolia","three_letter_abbreviation":"MNG","two_letter_abbreviation":"MN","objectID":"103589122","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"382","flag":"https://mobimatter.com/assets/flag/flag-ME.png","currency":"EUR","name":"Montenegro","three_letter_abbreviation":"MNE","two_letter_abbreviation":"ME","objectID":"103589132","_highlightResult":null,"isOfferCountry":true},{"calling_code":"1664","flag":"https://mobimatter.com/assets/flag/flag-MS.png","currency":"XCD","name":"Montserrat","three_letter_abbreviation":"MSR","two_letter_abbreviation":"MS","objectID":"103589142","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"212","flag":"https://mobimatter.com/assets/flag/flag-MA.png","currency":"MAD","name":"Morocco","three_letter_abbreviation":"MAR","two_letter_abbreviation":"MA","objectID":"103589152","_highlightResult":null,"isTopUpCountry":true},{"calling_code":"258","flag":"https://mobimatter.com/assets/flag/flag-MZ.png","currency":"MZN","name":"Mozambique","three_letter_abbreviation":"MOZ","two_letter_abbreviation":"MZ","objectID":"103589162","_highlightResult":null,"isTopUpCountry":true},{"calling_code":"95","flag":"https://mobimatter.com/assets/flag/flag-MM.png","currency":"MMK","name":"Myanmar","three_letter_abbreviation":"MMR","two_letter_abbreviation":"MM","objectID":"103589172","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"264","flag":"https://mobimatter.com/assets/flag/flag-NA.png","currency":"NAD","name":"Namibia","three_letter_abbreviation":"NAM","two_letter_abbreviation":"NA","objectID":"103589182","_highlightResult":null,"isTopUpCountry":true},{"calling_code":"674","flag":"https://mobimatter.com/assets/flag/flag-NR.png","currency":"AUD","name":"Nauru","three_letter_abbreviation":"NRU","two_letter_abbreviation":"NR","objectID":"103589192","_highlightResult":null,"isTopUpCountry":true},{"calling_code":"977","flag":"https://mobimatter.com/assets/flag/flag-NP.png","currency":"NPR","name":"Nepal","three_letter_abbreviation":"NPL","two_letter_abbreviation":"NP","objectID":"103589202","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"31","flag":"https://mobimatter.com/assets/flag/flag-NL.png","currency":"EUR","name":"Netherlands","three_letter_abbreviation":"NLD","two_letter_abbreviation":"NL","objectID":"103589212","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"687","flag":"https://mobimatter.com/assets/flag/flag-NC.png","currency":"XPF","name":"New Caledonia","three_letter_abbreviation":"NCL","two_letter_abbreviation":"NC","objectID":"103589222","_highlightResult":null},{"calling_code":"64","flag":"https://mobimatter.com/assets/flag/flag-NZ.png","currency":"NZD","name":"New Zealand","three_letter_abbreviation":"NZL","two_letter_abbreviation":"NZ","objectID":"103589232","_highlightResult":null,"isOfferCountry":true},{"calling_code":"505","flag":"https://mobimatter.com/assets/flag/flag-NI.png","currency":"NIO","name":"Nicaragua","three_letter_abbreviation":"NIC","two_letter_abbreviation":"NI","objectID":"103589242","_highlightResult":null},{"calling_code":"227","flag":"https://mobimatter.com/assets/flag/flag-NE.png","currency":"XOF","name":"Niger","three_letter_abbreviation":"NER","two_letter_abbreviation":"NE","objectID":"103589252","_highlightResult":null},{"calling_code":"234","flag":"https://mobimatter.com/assets/flag/flag-NG.png","currency":"NGN","name":"Nigeria","three_letter_abbreviation":"NGA","two_letter_abbreviation":"NG","objectID":"103589262","_highlightResult":null},{"calling_code":"683","flag":"https://mobimatter.com/assets/flag/flag-NU.png","currency":"NZD","name":"Niue","three_letter_abbreviation":"NIU","two_letter_abbreviation":"NU","objectID":"103589272","_highlightResult":null},{"calling_code":"672","flag":"https://mobimatter.com/assets/flag/flag-NF.png","currency":"AUD","name":"Norfolk Island","three_letter_abbreviation":"NFK","two_letter_abbreviation":"NF","objectID":"103589282","_highlightResult":null},{"calling_code":"850","flag":"https://mobimatter.com/assets/flag/flag-KP.png","currency":"KPW","name":"North Korea","three_letter_abbreviation":"PRK","two_letter_abbreviation":"KP","objectID":"103589292","_highlightResult":null},{"calling_code":"1670","flag":"https://mobimatter.com/assets/flag/flag-MP.png","currency":"USD","name":"Northern Mariana Islands","three_letter_abbreviation":"MNP","two_letter_abbreviation":"MP","objectID":"103589302","_highlightResult":null},{"calling_code":"47","flag":"https://mobimatter.com/assets/flag/flag-NO.png","currency":"NOK","name":"Norway","three_letter_abbreviation":"NOR","two_letter_abbreviation":"NO","objectID":"103589312","_highlightResult":null,"isOfferCountry":true},{"calling_code":"968","flag":"https://mobimatter.com/assets/flag/flag-OM.png","currency":"OMR","name":"Oman","three_letter_abbreviation":"OMN","two_letter_abbreviation":"OM","objectID":"103589322","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"92","flag":"https://mobimatter.com/assets/flag/flag-PK.png","currency":"PKR","name":"Pakistan","three_letter_abbreviation":"PAK","two_letter_abbreviation":"PK","objectID":"103589332","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"680","flag":"https://mobimatter.com/assets/flag/flag-PW.png","currency":"USD","name":"Palau","three_letter_abbreviation":"PLW","two_letter_abbreviation":"PW","objectID":"103589342","_highlightResult":null},{"calling_code":"970","flag":"https://mobimatter.com/assets/flag/flag-PS.png","currency":"ILS","name":"Palestine","three_letter_abbreviation":"PSE","two_letter_abbreviation":"PS","objectID":"103589352","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"507","flag":"https://mobimatter.com/assets/flag/flag-PA.png","currency":"PAB","name":"Panama","three_letter_abbreviation":"PAN","two_letter_abbreviation":"PA","objectID":"103589362","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"675","flag":"https://mobimatter.com/assets/flag/flag-PG.png","currency":"PGK","name":"Papua New Guinea","three_letter_abbreviation":"PNG","two_letter_abbreviation":"PG","objectID":"103589372","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"595","flag":"https://mobimatter.com/assets/flag/flag-PY.png","currency":"PYG","name":"Paraguay","three_letter_abbreviation":"PRY","two_letter_abbreviation":"PY","objectID":"103589382","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"51","flag":"https://mobimatter.com/assets/flag/flag-PE.png","currency":"PEN","name":"Peru","three_letter_abbreviation":"PER","two_letter_abbreviation":"PE","objectID":"103589392","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"63","flag":"https://mobimatter.com/assets/flag/flag-PH.png","currency":"PHP","name":"Philippines","three_letter_abbreviation":"PHL","two_letter_abbreviation":"PH","objectID":"103589402","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"64","flag":"https://mobimatter.com/assets/flag/flag-PN.png","currency":"NZD","name":"Pitcairn Islands","three_letter_abbreviation":"PCN","two_letter_abbreviation":"PN","objectID":"103589412","_highlightResult":null},{"calling_code":"48","flag":"https://mobimatter.com/assets/flag/flag-PL.png","currency":"PLN","name":"Poland","three_letter_abbreviation":"POL","two_letter_abbreviation":"PL","objectID":"103589422","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"351","flag":"https://mobimatter.com/assets/flag/flag-PT.png","currency":"EUR","name":"Portugal","three_letter_abbreviation":"PRT","two_letter_abbreviation":"PT","objectID":"103589432","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"1787","flag":"https://mobimatter.com/assets/flag/flag-PR.png","currency":"USD","name":"Puerto Rico","three_letter_abbreviation":"PRI","two_letter_abbreviation":"PR","objectID":"103589442","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"974","flag":"https://mobimatter.com/assets/flag/flag-QA.png","currency":"QAR","name":"Qatar","three_letter_abbreviation":"QAT","two_letter_abbreviation":"QA","objectID":"103589452","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"242","flag":"https://mobimatter.com/assets/flag/flag-CG.png","currency":"XAF","name":"Republic of the Congo","three_letter_abbreviation":"COG","two_letter_abbreviation":"CG","objectID":"103589462","_highlightResult":null,"isOfferCountry":true},{"calling_code":"40","flag":"https://mobimatter.com/assets/flag/flag-RO.png","currency":"RON","name":"Romania","three_letter_abbreviation":"ROU","two_letter_abbreviation":"RO","objectID":"103589472","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"7","flag":"https://mobimatter.com/assets/flag/flag-RU.png","currency":"RUB","name":"Russia","three_letter_abbreviation":"RUS","two_letter_abbreviation":"RU","objectID":"103589482","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"250","flag":"https://mobimatter.com/assets/flag/flag-RW.png","currency":"RWF","name":"Rwanda","three_letter_abbreviation":"RWA","two_letter_abbreviation":"RW","objectID":"103589492","_highlightResult":null,"isTopUpCountry":true},{"calling_code":"262","flag":"https://mobimatter.com/assets/flag/flag-RE.png","currency":"EUR","name":"Reunion","three_letter_abbreviation":"REU","two_letter_abbreviation":"RE","objectID":"103589502","_highlightResult":null,"isOfferCountry":true},{"calling_code":"590","flag":"https://mobimatter.com/assets/flag/flag-BL.png","currency":"EUR","name":"Saint Barthelemy","three_letter_abbreviation":"BLM","two_letter_abbreviation":"BL","objectID":"103589512","_highlightResult":null,"isOfferCountry":true},{"calling_code":"1869","flag":"https://mobimatter.com/assets/flag/flag-KN.png","currency":"XCD","name":"Saint Kitts and Nevis","three_letter_abbreviation":"KNA","two_letter_abbreviation":"KN","objectID":"103589522","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"1758","flag":"https://mobimatter.com/assets/flag/flag-LC.png","currency":"XCD","name":"Saint Lucia","three_letter_abbreviation":"LCA","two_letter_abbreviation":"LC","objectID":"103589532","_highlightResult":null,"isTopUpCountry":true},{"calling_code":"590","flag":"https://mobimatter.com/assets/flag/flag-MF.png","currency":"EUR","name":"Saint Martin","three_letter_abbreviation":"MAF","two_letter_abbreviation":"MF","objectID":"103589542","_highlightResult":null,"isOfferCountry":true},{"calling_code":"508","flag":"https://mobimatter.com/assets/flag/flag-PM.png","currency":"EUR","name":"Saint Pierre and Miquelon","three_letter_abbreviation":"SPM","two_letter_abbreviation":"PM","objectID":"103589552","_highlightResult":null},{"calling_code":"1784","flag":"https://mobimatter.com/assets/flag/flag-VC.png","currency":"XCD","name":"Saint Vincent and the Grenadines","three_letter_abbreviation":"VCT","two_letter_abbreviation":"VC","objectID":"103589562","_highlightResult":null,"isTopUpCountry":true},{"calling_code":"685","flag":"https://mobimatter.com/assets/flag/flag-WS.png","currency":"WST","name":"Samoa","three_letter_abbreviation":"WSM","two_letter_abbreviation":"WS","objectID":"103589572","_highlightResult":null,"isTopUpCountry":true},{"calling_code":"378","flag":"https://mobimatter.com/assets/flag/flag-SM.png","currency":"EUR","name":"San Marino","three_letter_abbreviation":"SMR","two_letter_abbreviation":"SM","objectID":"103589582","_highlightResult":null,"isOfferCountry":true},{"calling_code":"966","flag":"https://mobimatter.com/assets/flag/flag-SA.png","currency":"SAR","name":"Saudi Arabia","three_letter_abbreviation":"SAU","two_letter_abbreviation":"SA","objectID":"103589592","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"221","flag":"https://mobimatter.com/assets/flag/flag-SN.png","currency":"XOF","name":"Senegal","three_letter_abbreviation":"SEN","two_letter_abbreviation":"SN","objectID":"103589602","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"381","flag":"https://mobimatter.com/assets/flag/flag-RS.png","currency":"RSD","name":"Serbia","three_letter_abbreviation":"SRB","two_letter_abbreviation":"RS","objectID":"103589612","_highlightResult":null,"isOfferCountry":true},{"calling_code":"248","flag":"https://mobimatter.com/assets/flag/flag-SC.png","currency":"SCR","name":"Seychelles","three_letter_abbreviation":"SYC","two_letter_abbreviation":"SC","objectID":"103589622","_highlightResult":null},{"calling_code":"232","flag":"https://mobimatter.com/assets/flag/flag-SL.png","currency":"SLL","name":"Sierra Leone","three_letter_abbreviation":"SLE","two_letter_abbreviation":"SL","objectID":"103589632","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"65","flag":"https://mobimatter.com/assets/flag/flag-SG.png","currency":"SGD","name":"Singapore","three_letter_abbreviation":"SGP","two_letter_abbreviation":"SG","objectID":"103589642","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"1721","flag":"https://mobimatter.com/assets/flag/flag-SX.png","currency":"ANG","name":"Sint Maarten","three_letter_abbreviation":"SXM","two_letter_abbreviation":"SX","objectID":"103589652","_highlightResult":null},{"calling_code":"421","flag":"https://mobimatter.com/assets/flag/flag-SK.png","currency":"EUR","name":"Slovakia","three_letter_abbreviation":"SVK","two_letter_abbreviation":"SK","objectID":"103589662","_highlightResult":null,"isOfferCountry":true},{"calling_code":"386","flag":"https://mobimatter.com/assets/flag/flag-SI.png","currency":"EUR","name":"Slovenia","three_letter_abbreviation":"SVN","two_letter_abbreviation":"SI","objectID":"103589672","_highlightResult":null,"isOfferCountry":true},{"calling_code":"677","flag":"https://mobimatter.com/assets/flag/flag-SB.png","currency":"SBD","name":"Solomon Islands","three_letter_abbreviation":"SLB","two_letter_abbreviation":"SB","objectID":"103589682","_highlightResult":null,"isOfferCountry":true},{"calling_code":"252","flag":"https://mobimatter.com/assets/flag/flag-SO.png","currency":"SOS","name":"Somalia","three_letter_abbreviation":"SOM","two_letter_abbreviation":"SO","objectID":"103589692","_highlightResult":null},{"calling_code":"27","flag":"https://mobimatter.com/assets/flag/flag-ZA.png","currency":"ZAR","name":"South Africa","three_letter_abbreviation":"ZAF","two_letter_abbreviation":"ZA","objectID":"103589702","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"500","flag":"https://mobimatter.com/assets/flag/flag-GS.png","currency":"GBP","name":"South Georgia","three_letter_abbreviation":"SGS","two_letter_abbreviation":"GS","objectID":"103589712","_highlightResult":null},{"calling_code":"82","flag":"https://mobimatter.com/assets/flag/flag-KR.png","currency":"KRW","name":"South Korea","three_letter_abbreviation":"KOR","two_letter_abbreviation":"KR","objectID":"103589722","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"211","flag":"https://mobimatter.com/assets/flag/flag-SS.png","currency":"SSP","name":"South Sudan","three_letter_abbreviation":"SSD","two_letter_abbreviation":"SS","objectID":"103589732","_highlightResult":null},{"calling_code":"34","flag":"https://mobimatter.com/assets/flag/flag-ES.png","currency":"EUR","name":"Spain","three_letter_abbreviation":"ESP","two_letter_abbreviation":"ES","objectID":"103589742","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"94","flag":"https://mobimatter.com/assets/flag/flag-LK.png","currency":"LKR","name":"Sri Lanka","three_letter_abbreviation":"LKA","two_letter_abbreviation":"LK","objectID":"103589752","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"249","flag":"https://mobimatter.com/assets/flag/flag-SD.png","currency":"SDG","name":"Sudan","three_letter_abbreviation":"SDN","two_letter_abbreviation":"SD","objectID":"103589762","_highlightResult":null},{"calling_code":"597","flag":"https://mobimatter.com/assets/flag/flag-SR.png","currency":"SRD","name":"Suriname","three_letter_abbreviation":"SUR","two_letter_abbreviation":"SR","objectID":"103589772","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"4779","flag":"https://mobimatter.com/assets/flag/flag-SJ.png","currency":"NOK","name":"Svalbard and Jan Mayen","three_letter_abbreviation":"SJM","two_letter_abbreviation":"SJ","objectID":"103589782","_highlightResult":null},{"calling_code":"46","flag":"https://mobimatter.com/assets/flag/flag-SE.png","currency":"SEK","name":"Sweden","three_letter_abbreviation":"SWE","two_letter_abbreviation":"SE","objectID":"103589802","_highlightResult":null,"isOfferCountry":true},{"calling_code":"41","flag":"https://mobimatter.com/assets/flag/flag-CH.png","currency":"CHE","name":"Switzerland","three_letter_abbreviation":"CHE","two_letter_abbreviation":"CH","objectID":"103589812","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"963","flag":"https://mobimatter.com/assets/flag/flag-SY.png","currency":"SYP","name":"Syria","three_letter_abbreviation":"SYR","two_letter_abbreviation":"SY","objectID":"103589822","_highlightResult":null},{"calling_code":"239","flag":"https://mobimatter.com/assets/flag/flag-ST.png","currency":"STD","name":"SГЈo TomГ© and PrГ­ncipe","three_letter_abbreviation":"STP","two_letter_abbreviation":"ST","objectID":"103589832","_highlightResult":null},{"calling_code":"886","flag":"https://mobimatter.com/assets/flag/flag-TW.png","currency":"TWD","name":"Taiwan","three_letter_abbreviation":"TWN","two_letter_abbreviation":"TW","objectID":"103589842","_highlightResult":null,"isOfferCountry":true},{"calling_code":"992","flag":"https://mobimatter.com/assets/flag/flag-TJ.png","currency":"TJS","name":"Tajikistan","three_letter_abbreviation":"TJK","two_letter_abbreviation":"TJ","objectID":"103589852","_highlightResult":null,"isTopUpCountry":true},{"calling_code":"255","flag":"https://mobimatter.com/assets/flag/flag-TZ.png","currency":"TZS","name":"Tanzania","three_letter_abbreviation":"TZA","two_letter_abbreviation":"TZ","objectID":"103589862","_highlightResult":null,"isTopUpCountry":true},{"calling_code":"66","flag":"https://mobimatter.com/assets/flag/flag-TH.png","currency":"THB","name":"Thailand","three_letter_abbreviation":"THA","two_letter_abbreviation":"TH","objectID":"103589872","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"670","flag":"https://mobimatter.com/assets/flag/flag-TL.png","currency":"USD","name":"East Timor","three_letter_abbreviation":"TLS","two_letter_abbreviation":"TL","objectID":"103589882","_highlightResult":null},{"calling_code":"228","flag":"https://mobimatter.com/assets/flag/flag-TG.png","currency":"XOF","name":"Togo","three_letter_abbreviation":"TGO","two_letter_abbreviation":"TG","objectID":"103589892","_highlightResult":null,"isTopUpCountry":true},{"calling_code":"690","flag":"https://mobimatter.com/assets/flag/flag-TK.png","currency":"NZD","name":"Tokelau","three_letter_abbreviation":"TKL","two_letter_abbreviation":"TK","objectID":"103589902","_highlightResult":null},{"calling_code":"676","flag":"https://mobimatter.com/assets/flag/flag-TO.png","currency":"TOP","name":"Tonga","three_letter_abbreviation":"TON","two_letter_abbreviation":"TO","objectID":"103589912","_highlightResult":null,"isTopUpCountry":true},{"calling_code":"1868","flag":"https://mobimatter.com/assets/flag/flag-TT.png","currency":"TTD","name":"Trinidad and Tobago","three_letter_abbreviation":"TTO","two_letter_abbreviation":"TT","objectID":"103589922","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"216","flag":"https://mobimatter.com/assets/flag/flag-TN.png","currency":"TND","name":"Tunisia","three_letter_abbreviation":"TUN","two_letter_abbreviation":"TN","objectID":"103589932","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"90","flag":"https://mobimatter.com/assets/flag/flag-TR.png","currency":"TRY","name":"Turkey","three_letter_abbreviation":"TUR","two_letter_abbreviation":"TR","objectID":"103589942","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"993","flag":"https://mobimatter.com/assets/flag/flag-TM.png","currency":"TMT","name":"Turkmenistan","three_letter_abbreviation":"TKM","two_letter_abbreviation":"TM","objectID":"103589952","_highlightResult":null},{"calling_code":"1649","flag":"https://mobimatter.com/assets/flag/flag-TC.png","currency":"USD","name":"Turks and Caicos Islands","three_letter_abbreviation":"TCA","two_letter_abbreviation":"TC","objectID":"103589962","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"688","flag":"https://mobimatter.com/assets/flag/flag-TV.png","currency":"AUD","name":"Tuvalu","three_letter_abbreviation":"TUV","two_letter_abbreviation":"TV","objectID":"103589972","_highlightResult":null},{"calling_code":"256","flag":"https://mobimatter.com/assets/flag/flag-UG.png","currency":"UGX","name":"Uganda","three_letter_abbreviation":"UGA","two_letter_abbreviation":"UG","objectID":"103589982","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"380","flag":"https://mobimatter.com/assets/flag/flag-UA.png","currency":"UAH","name":"Ukraine","three_letter_abbreviation":"UKR","two_letter_abbreviation":"UA","objectID":"103589992","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"971","flag":"https://mobimatter.com/assets/flag/flag-AE.png","currency":"AED","name":"United Arab Emirates","three_letter_abbreviation":"UAE","two_letter_abbreviation":"AE","objectID":"103590002","_highlightResult":null,"isTopUpCountry":true},{"calling_code":"44","flag":"https://mobimatter.com/assets/flag/flag-GB.png","currency":"GBP","name":"United Kingdom","three_letter_abbreviation":"GBR","two_letter_abbreviation":"GB","objectID":"103590012","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"1","flag":"https://mobimatter.com/assets/flag/flag-US.png","currency":"USD","name":"United States","three_letter_abbreviation":"USA","two_letter_abbreviation":"US","objectID":"103590022","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"flag":"https://mobimatter.com/assets/flag/flag-UM.png","currency":"USD","name":"United States Minor Outlying Islands","three_letter_abbreviation":"UMI","two_letter_abbreviation":"UM","calling_code":"246","objectID":"103590032","_highlightResult":null},{"calling_code":"1340","flag":"https://mobimatter.com/assets/flag/flag-VI.png","currency":"USD","name":"United States Virgin Islands","three_letter_abbreviation":"VIR","two_letter_abbreviation":"VI","objectID":"103590042","_highlightResult":null,"isOfferCountry":true},{"calling_code":"598","flag":"https://mobimatter.com/assets/flag/flag-UY.png","currency":"UYI","name":"Uruguay","three_letter_abbreviation":"URY","two_letter_abbreviation":"UY","objectID":"103590052","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"998","flag":"https://mobimatter.com/assets/flag/flag-UZ.png","currency":"UZS","name":"Uzbekistan","three_letter_abbreviation":"UZB","two_letter_abbreviation":"UZ","objectID":"103590062","_highlightResult":null,"isTopUpCountry":true},{"calling_code":"678","flag":"https://mobimatter.com/assets/flag/flag-VU.png","currency":"VUV","name":"Vanuatu","three_letter_abbreviation":"VUT","two_letter_abbreviation":"VU","objectID":"103590072","_highlightResult":null,"isTopUpCountry":true},{"calling_code":"3906698","flag":"https://mobimatter.com/assets/flag/flag-VA.png","currency":"EUR","name":"Vatican City","three_letter_abbreviation":"VAT","two_letter_abbreviation":"VA","objectID":"103590082","_highlightResult":null,"isOfferCountry":true},{"calling_code":"58","flag":"https://mobimatter.com/assets/flag/flag-VE.png","currency":"VEF","name":"Venezuela","three_letter_abbreviation":"VEN","two_letter_abbreviation":"VE","objectID":"103590092","_highlightResult":null,"isTopUpCountry":true},{"calling_code":"84","flag":"https://mobimatter.com/assets/flag/flag-VN.png","currency":"VND","name":"Vietnam","three_letter_abbreviation":"VNM","two_letter_abbreviation":"VN","objectID":"103590102","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"681","flag":"https://mobimatter.com/assets/flag/flag-WF.png","currency":"XPF","name":"Wallis and Futuna","three_letter_abbreviation":"WLF","two_letter_abbreviation":"WF","objectID":"103590112","_highlightResult":null},{"calling_code":"212","flag":"https://mobimatter.com/assets/flag/flag-EH.png","currency":"MAD","name":"Western Sahara","three_letter_abbreviation":"ESH","two_letter_abbreviation":"EH","objectID":"103590122","_highlightResult":null},{"calling_code":"967","flag":"https://mobimatter.com/assets/flag/flag-YE.png","currency":"YER","name":"Yemen","three_letter_abbreviation":"YEM","two_letter_abbreviation":"YE","objectID":"103590132","_highlightResult":null,"isTopUpCountry":true},{"calling_code":"260","flag":"https://mobimatter.com/assets/flag/flag-ZM.png","currency":"ZMW","name":"Zambia","three_letter_abbreviation":"ZMB","two_letter_abbreviation":"ZM","objectID":"103590142","_highlightResult":null,"isTopUpCountry":true},{"calling_code":"263","flag":"https://mobimatter.com/assets/flag/flag-ZW.png","currency":"ZWL","name":"Zimbabwe","three_letter_abbreviation":"ZWE","two_letter_abbreviation":"ZW","objectID":"103590152","_highlightResult":null,"isOfferCountry":true,"isTopUpCountry":true},{"calling_code":"358","flag":"https://mobimatter.com/assets/flag/flag-AX.png","currency":"EUR","name":"Г…land Islands","three_letter_abbreviation":"ALA","two_letter_abbreviation":"AX","objectID":"103590162","_highlightResult":null},{"calling_code":"599","flag":"data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiPz4NCjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgd2lkdGg9IjYwMCIgaGVpZ2h0PSI0MDAiPg0KPHJlY3Qgd2lkdGg9IjYwMCIgaGVpZ2h0PSI0MDAiIGZpbGw9IiNmZmYiIC8+DQo8cGF0aCBkPSJNIDYwMCw0MDAgMCw0MDAgNjAwLDAgeiIgZmlsbD0iIzAxMmE4NyIgLz4NCjxwYXRoIGQ9Ik0gMCwwIDAsMTY2LjY2NjY2NyAyNTAsMCB6IiBmaWxsPSIjZjlkOTBmIiAvPg0KPGNpcmNsZSBjeD0iMTY1IiBjeT0iMTczIiByPSI4MCIgZmlsbD0ibm9uZSIgc3Ryb2tlPSIjMDAwIiBzdHJva2Utd2lkdGg9IjExIiAvPg0KPGcgaWQ9ImciPg0KPHBhdGggZD0ibSAxNjUsNjkuODM0NDc0IDE1LDI1Ljk4MDc2MjExIC0zMCwwIHoiIGZpbGw9IiMwMDAiIGlkPSJhIi8+DQo8dXNlIHhsaW5rOmhyZWY9IiNhIiB0cmFuc2Zvcm09InJvdGF0ZSgxODAsMTY1LDE3MykiIC8+DQo8L2c+DQo8dXNlIHhsaW5rOmhyZWY9IiNnIiB0cmFuc2Zvcm09InJvdGF0ZSg5MCwxNjUsMTczKSIgLz4NCjxwYXRoIGQ9Ik0gMTY1LDEyMiAyMDkuMTY3Mjk1NTksMTk4LjUgMTIwLjgzMjcwNDQxLDE5OC41IHoiIGZpbGw9IiNkYzE3MWQiIGlkPSJiIiAvPg0KPHVzZSB4bGluazpocmVmPSIjYiIgdHJhbnNmb3JtPSJyb3RhdGUoNjAsMTY1LDE3MykiIC8+DQo8L3N2Zz4=","currency":"USD","name":"Bonaire","three_letter_abbreviation":"BES","two_letter_abbreviation":"BQ"},{"calling_code":"1","flag":"https://mobimatter.com/assets/flag/flag-US-HI.png","currency":"USD","name":"Hawaii","three_letter_abbreviation":"US-HI","two_letter_abbreviation":"US-HI","objectID":"1034211164","isOfferCountry":true,"_highlightResult":null}]'
    );
    for (var t = 0; t < countries_data.length; t++) $(".country-search").append(`<option title="${countries_data[t].three_letter_abbreviation}" value="${countries_data[t].two_letter_abbreviation}">${countries_data[t].name} </option>`);
    function e(t, e) {
        return (
            void 0 !== t.term && (t.term = t.term.toLowerCase()),
            "" === $.trim(t.term)
                ? e
                : void 0 === e.text
                ? null
                : e.text.toLowerCase().trim().indexOf($.trim(t.term)) > -1 || e.id.toLowerCase().trim().indexOf($.trim(t.term)) > -1 || e.title.toLowerCase().trim().indexOf($.trim(t.term)) > -1
                ? $.extend({}, e, !0)
                : null
        );
    }
    function r(t) {
        if (!t.id) return t.text;
        return $('<span><img loading="lazy" src="/assets/flag/flag-' + t.element.value + '.png" class="img-flag" > ' + t.text + " </span>");
    }
    $(".country-search").change(function (t) {
        if ((t.preventDefault(), $(".country-search").val().length > 0)) {
            (window.dataLayer = window.dataLayer || []), window.dataLayer.push({ event: "RO_Country_Search", countries: $(".country-search").val() });
            var e = $(".country-search option:selected").text();
            if (
                ((a = e.trim()),
                (e = a
                    .toString()
                    .toLowerCase()
                    .replace("//", "-")
                    .split(/\&+/)
                    .join("-and-")
                    .split(/[^a-z0-9.]/)
                    .join("-")
                    .split(/-+/)
                    .join("-")
                    .trim("-")),
                window.location.pathname.includes("topup") || $("#provider").is(":visible"))
            ) {
                var r = $("#provider").val();
                location.href = "/topup/" + r + "/" + e;
            } else location.href = "/travel-esim/" + e;
        }
        var a;
    });
}),
    (function (t) {
        var e;
        "undefined" != typeof window ? (e = window) : "undefined" != typeof self && (e = self),
            (e.ALGOLIA_MIGRATION_LAYER = (function t(e, r, a) {
                function n(o, l) {
                    if (!r[o]) {
                        if (!e[o]) {
                            var s = "function" == typeof require && require;
                            if (!l && s) return s(o, !0);
                            if (i) return i(o, !0);
                            var c = new Error("Cannot find module '" + o + "'");
                            throw ((c.code = "MODULE_NOT_FOUND"), c);
                        }
                        var u = (r[o] = { exports: {} });
                        e[o][0].call(
                            u.exports,
                            function (t) {
                                var r = e[o][1][t];
                                return n(r || t);
                            },
                            u,
                            u.exports,
                            t,
                            e,
                            r,
                            a
                        );
                    }
                    return r[o].exports;
                }
                for (var i = "function" == typeof require && require, o = 0; o < a.length; o++) n(a[o]);
                return n;
            })(
                {
                    1: [
                        function (t, e, r) {
                            function a(t, e) {
                                (t.onload = function () {
                                    (this.onerror = this.onload = null), e(null, t);
                                }),
                                    (t.onerror = function () {
                                        (this.onerror = this.onload = null), e(new Error("Failed to load " + this.src), t);
                                    });
                            }
                            function n(t, e) {
                                t.onreadystatechange = function () {
                                    ("complete" != this.readyState && "loaded" != this.readyState) || ((this.onreadystatechange = null), e(null, t));
                                };
                            }
                            e.exports = function (t, e, r) {
                                var i = document.head || document.getElementsByTagName("head")[0],
                                    o = document.createElement("script");
                                "function" == typeof e && ((r = e), (e = {})),
                                    (e = e || {}),
                                    (r = r || function () {}),
                                    (o.type = e.type || "text/javascript"),
                                    (o.charset = e.charset || "utf8"),
                                    (o.async = !("async" in e && !e.async)),
                                    (o.src = t),
                                    e.attrs &&
                                        (function (t, e) {
                                            for (var r in e) t.setAttribute(r, e[r]);
                                        })(o, e.attrs),
                                    e.text && (o.text = "" + e.text);
                                var l = "onload" in o ? a : n;
                                l(o, r), o.onload || a(o, r), i.appendChild(o);
                            };
                        },
                        {},
                    ],
                    2: [
                        function (t, e, r) {
                            "use strict";
                            e.exports = function (t) {
                                for (var e = new RegExp("cdn\\.jsdelivr\\.net/algoliasearch/latest/" + t.replace(".", "\\.") + "(?:\\.min)?\\.js$"), r = document.getElementsByTagName("script"), a = !1, n = 0, i = r.length; n < i; n++)
                                    if (r[n].src && e.test(r[n].src)) {
                                        a = !0;
                                        break;
                                    }
                                return a;
                            };
                        },
                        {},
                    ],
                    3: [
                        function (t, e, r) {
                            "use strict";
                            function a(t) {
                                return function () {
                                    var e = "AlgoliaSearch: loaded V2 script using " + t;
                                    window.console && window.console.log && window.console.log(e);
                                };
                            }
                            e.exports = function (e) {
                                var r = t(1),
                                    n = "//cdn.jsdelivr.net/algoliasearch/2/" + e + ".min.js",
                                    i =
                                        "-- AlgoliaSearch `latest` warning --\nWarning, you are using the `latest` version string from jsDelivr to load the AlgoliaSearch library.\nUsing `latest` is no more recommended, you should load //cdn.jsdelivr.net/algoliasearch/2/algoliasearch.min.js\n\nAlso, we updated the AlgoliaSearch JavaScript client to V3. If you want to upgrade,\nplease read our migration guide at https://github.com/algolia/algoliasearch-client-js/wiki/Migration-guide-from-2.x.x-to-3.x.x\n-- /AlgoliaSearch  `latest` warning --";
                                window.console && (window.console.warn ? window.console.warn(i) : window.console.log && window.console.log(i));
                                try {
                                    document.write("<script>window.ALGOLIA_SUPPORTS_DOCWRITE = true</script>"),
                                        !0 === window.ALGOLIA_SUPPORTS_DOCWRITE ? (document.write('<script src="' + n + '"></script>'), a("document.write")()) : r(n, a("DOMElement"));
                                } catch (t) {
                                    r(n, a("DOMElement"));
                                }
                            };
                        },
                        { 1: 1 },
                    ],
                    4: [
                        function (t, e, r) {
                            "use strict";
                            e.exports = function () {
                                var t =
                                    "-- AlgoliaSearch V2 => V3 error --\nYou are trying to use a new version of the AlgoliaSearch JavaScript client with an old notation.\nPlease read our migration guide at https://github.com/algolia/algoliasearch-client-js/wiki/Migration-guide-from-2.x.x-to-3.x.x\n-- /AlgoliaSearch V2 => V3 error --";
                                (window.AlgoliaSearch = function () {
                                    throw new Error(t);
                                }),
                                    (window.AlgoliaSearchHelper = function () {
                                        throw new Error(t);
                                    }),
                                    (window.AlgoliaExplainResults = function () {
                                        throw new Error(t);
                                    });
                            };
                        },
                        {},
                    ],
                    5: [
                        function (t, e, r) {
                            "use strict";
                            !(function (e) {
                                var r = t(2),
                                    a = t(3),
                                    n = t(4);
                                r(e) ? a(e) : n();
                            })("algoliasearch");
                        },
                        { 2: 2, 3: 3, 4: 4 },
                    ],
                },
                {},
                [5]
            )(5));
    })(),
    (function (t) {
        if ("object" == typeof exports && "undefined" != typeof module) module.exports = t();
        else if ("function" == typeof define && define.amd) define([], t);
        else {
            ("undefined" != typeof window ? window : "undefined" != typeof global ? global : "undefined" != typeof self ? self : this).algoliasearch = t();
        }
    })(function () {
        return (function t(e, r, a) {
            function n(o, l) {
                if (!r[o]) {
                    if (!e[o]) {
                        var s = "function" == typeof require && require;
                        if (!l && s) return s(o, !0);
                        if (i) return i(o, !0);
                        var c = new Error("Cannot find module '" + o + "'");
                        throw ((c.code = "MODULE_NOT_FOUND"), c);
                    }
                    var u = (r[o] = { exports: {} });
                    e[o][0].call(
                        u.exports,
                        function (t) {
                            var r = e[o][1][t];
                            return n(r || t);
                        },
                        u,
                        u.exports,
                        t,
                        e,
                        r,
                        a
                    );
                }
                return r[o].exports;
            }
            for (var i = "function" == typeof require && require, o = 0; o < a.length; o++) n(a[o]);
            return n;
        })(
            {
                1: [
                    function (t, e, r) {
                        (function (a) {
                            function n() {
                                var t;
                                try {
                                    t = r.storage.debug;
                                } catch (t) {}
                                return !t && void 0 !== a && "env" in a && (t = a.env.DEBUG), t;
                            }
                            ((r = e.exports = t(2)).log = function () {
                                return "object" == typeof console && console.log && Function.prototype.apply.call(console.log, console, arguments);
                            }),
                                (r.formatArgs = function (t) {
                                    var e = this.useColors;
                                    if (((t[0] = (e ? "%c" : "") + this.namespace + (e ? " %c" : " ") + t[0] + (e ? "%c " : " ") + "+" + r.humanize(this.diff)), e)) {
                                        var a = "color: " + this.color;
                                        t.splice(1, 0, a, "color: inherit");
                                        var n = 0,
                                            i = 0;
                                        t[0].replace(/%[a-zA-Z%]/g, function (t) {
                                            "%%" !== t && (n++, "%c" === t && (i = n));
                                        }),
                                            t.splice(i, 0, a);
                                    }
                                }),
                                (r.save = function (t) {
                                    try {
                                        null == t ? r.storage.removeItem("debug") : (r.storage.debug = t);
                                    } catch (t) {}
                                }),
                                (r.load = n),
                                (r.useColors = function () {
                                    return (
                                        !("undefined" == typeof window || !window.process || "renderer" !== window.process.type) ||
                                        ("undefined" != typeof document && document.documentElement && document.documentElement.style && document.documentElement.style.WebkitAppearance) ||
                                        ("undefined" != typeof window && window.console && (window.console.firebug || (window.console.exception && window.console.table))) ||
                                        ("undefined" != typeof navigator && navigator.userAgent && navigator.userAgent.toLowerCase().match(/firefox\/(\d+)/) && parseInt(RegExp.$1, 10) >= 31) ||
                                        ("undefined" != typeof navigator && navigator.userAgent && navigator.userAgent.toLowerCase().match(/applewebkit\/(\d+)/))
                                    );
                                }),
                                (r.storage =
                                    "undefined" != typeof chrome && void 0 !== chrome.storage
                                        ? chrome.storage.local
                                        : (function () {
                                              try {
                                                  return window.localStorage;
                                              } catch (t) {}
                                          })()),
                                (r.colors = ["lightseagreen", "forestgreen", "goldenrod", "dodgerblue", "darkorchid", "crimson"]),
                                (r.formatters.j = function (t) {
                                    try {
                                        return JSON.stringify(t);
                                    } catch (t) {
                                        return "[UnexpectedJSONParseError]: " + t.message;
                                    }
                                }),
                                r.enable(n());
                        }.call(this, t(12)));
                    },
                    { 12: 12, 2: 2 },
                ],
                2: [
                    function (t, e, r) {
                        function a(t) {
                            function e() {
                                if (e.enabled) {
                                    var t = e,
                                        a = +new Date(),
                                        i = a - (n || a);
                                    (t.diff = i), (t.prev = n), (t.curr = a), (n = a);
                                    for (var o = new Array(arguments.length), l = 0; l < o.length; l++) o[l] = arguments[l];
                                    (o[0] = r.coerce(o[0])), "string" != typeof o[0] && o.unshift("%O");
                                    var s = 0;
                                    (o[0] = o[0].replace(/%([a-zA-Z%])/g, function (e, a) {
                                        if ("%%" === e) return e;
                                        s++;
                                        var n = r.formatters[a];
                                        if ("function" == typeof n) {
                                            var i = o[s];
                                            (e = n.call(t, i)), o.splice(s, 1), s--;
                                        }
                                        return e;
                                    })),
                                        r.formatArgs.call(t, o),
                                        (e.log || r.log || console.log.bind(console)).apply(t, o);
                                }
                            }
                            return (
                                (e.namespace = t),
                                (e.enabled = r.enabled(t)),
                                (e.useColors = r.useColors()),
                                (e.color = (function (t) {
                                    var e,
                                        a = 0;
                                    for (e in t) (a = (a << 5) - a + t.charCodeAt(e)), (a |= 0);
                                    return r.colors[Math.abs(a) % r.colors.length];
                                })(t)),
                                "function" == typeof r.init && r.init(e),
                                e
                            );
                        }
                        var n;
                        ((r = e.exports = a.debug = a.default = a).coerce = function (t) {
                            return t instanceof Error ? t.stack || t.message : t;
                        }),
                            (r.disable = function () {
                                r.enable("");
                            }),
                            (r.enable = function (t) {
                                r.save(t), (r.names = []), (r.skips = []);
                                for (var e = ("string" == typeof t ? t : "").split(/[\s,]+/), a = e.length, n = 0; n < a; n++)
                                    e[n] && ("-" === (t = e[n].replace(/\*/g, ".*?"))[0] ? r.skips.push(new RegExp("^" + t.substr(1) + "$")) : r.names.push(new RegExp("^" + t + "$")));
                            }),
                            (r.enabled = function (t) {
                                var e, a;
                                for (e = 0, a = r.skips.length; e < a; e++) if (r.skips[e].test(t)) return !1;
                                for (e = 0, a = r.names.length; e < a; e++) if (r.names[e].test(t)) return !0;
                                return !1;
                            }),
                            (r.humanize = t(9)),
                            (r.names = []),
                            (r.skips = []),
                            (r.formatters = {});
                    },
                    { 9: 9 },
                ],
                3: [
                    function (t, e, r) {
                        (function (a, n) {
                            !(function (t, a) {
                                "object" == typeof r && void 0 !== e ? (e.exports = a()) : (t.ES6Promise = a());
                            })(this, function () {
                                "use strict";
                                function e(t) {
                                    return "function" == typeof t;
                                }
                                function r() {
                                    var t = setTimeout;
                                    return function () {
                                        return t(i, 1);
                                    };
                                }
                                function i() {
                                    for (var t = 0; t < R; t += 2) {
                                        (0, M[t])(M[t + 1]), (M[t] = void 0), (M[t + 1] = void 0);
                                    }
                                    R = 0;
                                }
                                function o(t, e) {
                                    var r = arguments,
                                        a = this,
                                        n = new this.constructor(s);
                                    void 0 === n[N] && y(n);
                                    var i = a._state;
                                    return (
                                        i
                                            ? (function () {
                                                  var t = r[i - 1];
                                                  T(function () {
                                                      return d(i, n, t, a._result);
                                                  });
                                              })()
                                            : p(a, n, t, e),
                                        n
                                    );
                                }
                                function l(t) {
                                    if (t && "object" == typeof t && t.constructor === this) return t;
                                    var e = new this(s);
                                    return h(e, t), e;
                                }
                                function s() {}
                                function c(t) {
                                    try {
                                        return t.then;
                                    } catch (t) {
                                        return (G.error = t), G;
                                    }
                                }
                                function u(t, r, a) {
                                    r.constructor === t.constructor && a === o && r.constructor.resolve === l
                                        ? (function (t, e) {
                                              e._state === P
                                                  ? f(t, e._result)
                                                  : e._state === L
                                                  ? b(t, e._result)
                                                  : p(
                                                        e,
                                                        void 0,
                                                        function (e) {
                                                            return h(t, e);
                                                        },
                                                        function (e) {
                                                            return b(t, e);
                                                        }
                                                    );
                                          })(t, r)
                                        : a === G
                                        ? (b(t, G.error), (G.error = null))
                                        : void 0 === a
                                        ? f(t, r)
                                        : e(a)
                                        ? (function (t, e, r) {
                                              T(function (t) {
                                                  var a = !1,
                                                      n = (function (t, e, r, a) {
                                                          try {
                                                              t.call(e, r, a);
                                                          } catch (t) {
                                                              return t;
                                                          }
                                                      })(
                                                          r,
                                                          e,
                                                          function (r) {
                                                              a || ((a = !0), e !== r ? h(t, r) : f(t, r));
                                                          },
                                                          function (e) {
                                                              a || ((a = !0), b(t, e));
                                                          },
                                                          t._label
                                                      );
                                                  !a && n && ((a = !0), b(t, n));
                                              }, t);
                                          })(t, r, a)
                                        : f(t, r);
                                }
                                function h(t, e) {
                                    t === e
                                        ? b(t, new TypeError("You cannot resolve a promise with itself"))
                                        : (function (t) {
                                              var e = typeof t;
                                              return null !== t && ("object" === e || "function" === e);
                                          })(e)
                                        ? u(t, e, c(e))
                                        : f(t, e);
                                }
                                function g(t) {
                                    t._onerror && t._onerror(t._result), _(t);
                                }
                                function f(t, e) {
                                    t._state === x && ((t._result = e), (t._state = P), 0 !== t._subscribers.length && T(_, t));
                                }
                                function b(t, e) {
                                    t._state === x && ((t._state = L), (t._result = e), T(g, t));
                                }
                                function p(t, e, r, a) {
                                    var n = t._subscribers,
                                        i = n.length;
                                    (t._onerror = null), (n[i] = e), (n[i + P] = r), (n[i + L] = a), 0 === i && t._state && T(_, t);
                                }
                                function _(t) {
                                    var e = t._subscribers,
                                        r = t._state;
                                    if (0 !== e.length) {
                                        for (var a = void 0, n = void 0, i = t._result, o = 0; o < e.length; o += 3) (a = e[o]), (n = e[o + r]), a ? d(r, a, n, i) : n(i);
                                        t._subscribers.length = 0;
                                    }
                                }
                                function m() {
                                    this.error = null;
                                }
                                function d(t, r, a, n) {
                                    var i = e(a),
                                        o = void 0,
                                        l = void 0,
                                        s = void 0,
                                        c = void 0;
                                    if (i) {
                                        if (
                                            ((o = (function (t, e) {
                                                try {
                                                    return t(e);
                                                } catch (t) {
                                                    return (B.error = t), B;
                                                }
                                            })(a, n)) === B
                                                ? ((c = !0), (l = o.error), (o.error = null))
                                                : (s = !0),
                                            r === o)
                                        )
                                            return void b(r, new TypeError("A promises callback cannot return that same promise."));
                                    } else (o = n), (s = !0);
                                    r._state !== x || (i && s ? h(r, o) : c ? b(r, l) : t === P ? f(r, o) : t === L && b(r, o));
                                }
                                function y(t) {
                                    (t[N] = K++), (t._state = void 0), (t._result = void 0), (t._subscribers = []);
                                }
                                function v(t, e) {
                                    (this._instanceConstructor = t),
                                        (this.promise = new t(s)),
                                        this.promise[N] || y(this.promise),
                                        w(e)
                                            ? ((this.length = e.length),
                                              (this._remaining = e.length),
                                              (this._result = new Array(this.length)),
                                              0 === this.length ? f(this.promise, this._result) : ((this.length = this.length || 0), this._enumerate(e), 0 === this._remaining && f(this.promise, this._result)))
                                            : b(this.promise, new Error("Array Methods must be provided an Array"));
                                }
                                function C(t) {
                                    (this[N] = K++),
                                        (this._result = this._state = void 0),
                                        (this._subscribers = []),
                                        s !== t &&
                                            ("function" != typeof t &&
                                                (function () {
                                                    throw new TypeError("You must pass a resolver function as the first argument to the promise constructor");
                                                })(),
                                            this instanceof C
                                                ? (function (t, e) {
                                                      try {
                                                          e(
                                                              function (e) {
                                                                  h(t, e);
                                                              },
                                                              function (e) {
                                                                  b(t, e);
                                                              }
                                                          );
                                                      } catch (e) {
                                                          b(t, e);
                                                      }
                                                  })(this, t)
                                                : (function () {
                                                      throw new TypeError("Failed to construct 'Promise': Please use the 'new' operator, this object constructor cannot be called as a function.");
                                                  })());
                                }
                                var w = Array.isArray
                                        ? Array.isArray
                                        : function (t) {
                                              return "[object Array]" === Object.prototype.toString.call(t);
                                          },
                                    R = 0,
                                    I = void 0,
                                    D = void 0,
                                    T = function (t, e) {
                                        (M[R] = t), (M[R + 1] = e), 2 === (R += 2) && (D ? D(i) : E());
                                    },
                                    j = "undefined" != typeof window ? window : void 0,
                                    U = j || {},
                                    S = U.MutationObserver || U.WebKitMutationObserver,
                                    A = "undefined" == typeof self && void 0 !== a && "[object process]" === {}.toString.call(a),
                                    O = "undefined" != typeof Uint8ClampedArray && "undefined" != typeof importScripts && "undefined" != typeof MessageChannel,
                                    M = new Array(1e3),
                                    E = void 0;
                                E = A
                                    ? function () {
                                          return a.nextTick(i);
                                      }
                                    : S
                                    ? (function () {
                                          var t = 0,
                                              e = new S(i),
                                              r = document.createTextNode("");
                                          return (
                                              e.observe(r, { characterData: !0 }),
                                              function () {
                                                  r.data = t = ++t % 2;
                                              }
                                          );
                                      })()
                                    : O
                                    ? (function () {
                                          var t = new MessageChannel();
                                          return (
                                              (t.port1.onmessage = i),
                                              function () {
                                                  return t.port2.postMessage(0);
                                              }
                                          );
                                      })()
                                    : void 0 === j && "function" == typeof t
                                    ? (function () {
                                          try {
                                              var e = t("vertx");
                                              return void 0 !== (I = e.runOnLoop || e.runOnContext)
                                                  ? function () {
                                                        I(i);
                                                    }
                                                  : r();
                                          } catch (t) {
                                              return r();
                                          }
                                      })()
                                    : r();
                                var N = Math.random().toString(36).substring(16),
                                    x = void 0,
                                    P = 1,
                                    L = 2,
                                    G = new m(),
                                    B = new m(),
                                    K = 0;
                                return (
                                    (v.prototype._enumerate = function (t) {
                                        for (var e = 0; this._state === x && e < t.length; e++) this._eachEntry(t[e], e);
                                    }),
                                    (v.prototype._eachEntry = function (t, e) {
                                        var r = this._instanceConstructor,
                                            a = r.resolve;
                                        if (a === l) {
                                            var n = c(t);
                                            if (n === o && t._state !== x) this._settledAt(t._state, e, t._result);
                                            else if ("function" != typeof n) this._remaining--, (this._result[e] = t);
                                            else if (r === C) {
                                                var i = new r(s);
                                                u(i, t, n), this._willSettleAt(i, e);
                                            } else
                                                this._willSettleAt(
                                                    new r(function (e) {
                                                        return e(t);
                                                    }),
                                                    e
                                                );
                                        } else this._willSettleAt(a(t), e);
                                    }),
                                    (v.prototype._settledAt = function (t, e, r) {
                                        var a = this.promise;
                                        a._state === x && (this._remaining--, t === L ? b(a, r) : (this._result[e] = r)), 0 === this._remaining && f(a, this._result);
                                    }),
                                    (v.prototype._willSettleAt = function (t, e) {
                                        var r = this;
                                        p(
                                            t,
                                            void 0,
                                            function (t) {
                                                return r._settledAt(P, e, t);
                                            },
                                            function (t) {
                                                return r._settledAt(L, e, t);
                                            }
                                        );
                                    }),
                                    (C.all = function (t) {
                                        return new v(this, t).promise;
                                    }),
                                    (C.race = function (t) {
                                        var e = this;
                                        return new e(
                                            w(t)
                                                ? function (r, a) {
                                                      for (var n = t.length, i = 0; i < n; i++) e.resolve(t[i]).then(r, a);
                                                  }
                                                : function (t, e) {
                                                      return e(new TypeError("You must pass an array to race."));
                                                  }
                                        );
                                    }),
                                    (C.resolve = l),
                                    (C.reject = function (t) {
                                        var e = new this(s);
                                        return b(e, t), e;
                                    }),
                                    (C._setScheduler = function (t) {
                                        D = t;
                                    }),
                                    (C._setAsap = function (t) {
                                        T = t;
                                    }),
                                    (C._asap = T),
                                    (C.prototype = {
                                        constructor: C,
                                        then: o,
                                        catch: function (t) {
                                            return this.then(null, t);
                                        },
                                    }),
                                    (C.polyfill = function () {
                                        var t = void 0;
                                        if (void 0 !== n) t = n;
                                        else if ("undefined" != typeof self) t = self;
                                        else
                                            try {
                                                t = Function("return this")();
                                            } catch (t) {
                                                throw new Error("polyfill failed because global object is unavailable in this environment");
                                            }
                                        var e = t.Promise;
                                        if (e) {
                                            var r = null;
                                            try {
                                                r = Object.prototype.toString.call(e.resolve());
                                            } catch (t) {}
                                            if ("[object Promise]" === r && !e.cast) return;
                                        }
                                        t.Promise = C;
                                    }),
                                    (C.Promise = C),
                                    C
                                );
                            });
                        }.call(this, t(12), "undefined" != typeof global ? global : "undefined" != typeof self ? self : "undefined" != typeof window ? window : {}));
                    },
                    { 12: 12 },
                ],
                4: [
                    function (t, e, r) {
                        function a() {
                            (this._events = this._events || {}), (this._maxListeners = this._maxListeners || void 0);
                        }
                        function n(t) {
                            return "function" == typeof t;
                        }
                        function i(t) {
                            return "object" == typeof t && null !== t;
                        }
                        function o(t) {
                            return void 0 === t;
                        }
                        (e.exports = a),
                            (a.EventEmitter = a),
                            (a.prototype._events = void 0),
                            (a.prototype._maxListeners = void 0),
                            (a.defaultMaxListeners = 10),
                            (a.prototype.setMaxListeners = function (t) {
                                if (
                                    !(function (t) {
                                        return "number" == typeof t;
                                    })(t) ||
                                    t < 0 ||
                                    isNaN(t)
                                )
                                    throw TypeError("n must be a positive number");
                                return (this._maxListeners = t), this;
                            }),
                            (a.prototype.emit = function (t) {
                                var e, r, a, l, s, c;
                                if ((this._events || (this._events = {}), "error" === t && (!this._events.error || (i(this._events.error) && !this._events.error.length)))) {
                                    if ((e = arguments[1]) instanceof Error) throw e;
                                    var u = new Error('Uncaught, unspecified "error" event. (' + e + ")");
                                    throw ((u.context = e), u);
                                }
                                if (o((r = this._events[t]))) return !1;
                                if (n(r))
                                    switch (arguments.length) {
                                        case 1:
                                            r.call(this);
                                            break;
                                        case 2:
                                            r.call(this, arguments[1]);
                                            break;
                                        case 3:
                                            r.call(this, arguments[1], arguments[2]);
                                            break;
                                        default:
                                            (l = Array.prototype.slice.call(arguments, 1)), r.apply(this, l);
                                    }
                                else if (i(r)) for (l = Array.prototype.slice.call(arguments, 1), a = (c = r.slice()).length, s = 0; s < a; s++) c[s].apply(this, l);
                                return !0;
                            }),
                            (a.prototype.addListener = function (t, e) {
                                var r;
                                if (!n(e)) throw TypeError("listener must be a function");
                                return (
                                    this._events || (this._events = {}),
                                    this._events.newListener && this.emit("newListener", t, n(e.listener) ? e.listener : e),
                                    this._events[t] ? (i(this._events[t]) ? this._events[t].push(e) : (this._events[t] = [this._events[t], e])) : (this._events[t] = e),
                                    i(this._events[t]) &&
                                        !this._events[t].warned &&
                                        (r = o(this._maxListeners) ? a.defaultMaxListeners : this._maxListeners) &&
                                        r > 0 &&
                                        this._events[t].length > r &&
                                        ((this._events[t].warned = !0),
                                        console.error("(node) warning: possible EventEmitter memory leak detected. %d listeners added. Use emitter.setMaxListeners() to increase limit.", this._events[t].length),
                                        "function" == typeof console.trace && console.trace()),
                                    this
                                );
                            }),
                            (a.prototype.on = a.prototype.addListener),
                            (a.prototype.once = function (t, e) {
                                function r() {
                                    this.removeListener(t, r), a || ((a = !0), e.apply(this, arguments));
                                }
                                if (!n(e)) throw TypeError("listener must be a function");
                                var a = !1;
                                return (r.listener = e), this.on(t, r), this;
                            }),
                            (a.prototype.removeListener = function (t, e) {
                                var r, a, o, l;
                                if (!n(e)) throw TypeError("listener must be a function");
                                if (!this._events || !this._events[t]) return this;
                                if (((o = (r = this._events[t]).length), (a = -1), r === e || (n(r.listener) && r.listener === e))) delete this._events[t], this._events.removeListener && this.emit("removeListener", t, e);
                                else if (i(r)) {
                                    for (l = o; l-- > 0; )
                                        if (r[l] === e || (r[l].listener && r[l].listener === e)) {
                                            a = l;
                                            break;
                                        }
                                    if (a < 0) return this;
                                    1 === r.length ? ((r.length = 0), delete this._events[t]) : r.splice(a, 1), this._events.removeListener && this.emit("removeListener", t, e);
                                }
                                return this;
                            }),
                            (a.prototype.removeAllListeners = function (t) {
                                var e, r;
                                if (!this._events) return this;
                                if (!this._events.removeListener) return 0 === arguments.length ? (this._events = {}) : this._events[t] && delete this._events[t], this;
                                if (0 === arguments.length) {
                                    for (e in this._events) "removeListener" !== e && this.removeAllListeners(e);
                                    return this.removeAllListeners("removeListener"), (this._events = {}), this;
                                }
                                if (n((r = this._events[t]))) this.removeListener(t, r);
                                else if (r) for (; r.length; ) this.removeListener(t, r[r.length - 1]);
                                return delete this._events[t], this;
                            }),
                            (a.prototype.listeners = function (t) {
                                return this._events && this._events[t] ? (n(this._events[t]) ? [this._events[t]] : this._events[t].slice()) : [];
                            }),
                            (a.prototype.listenerCount = function (t) {
                                if (this._events) {
                                    var e = this._events[t];
                                    if (n(e)) return 1;
                                    if (e) return e.length;
                                }
                                return 0;
                            }),
                            (a.listenerCount = function (t, e) {
                                return t.listenerCount(e);
                            });
                    },
                    {},
                ],
                5: [
                    function (t, e, r) {
                        var a = Object.prototype.hasOwnProperty,
                            n = Object.prototype.toString;
                        e.exports = function (t, e, r) {
                            if ("[object Function]" !== n.call(e)) throw new TypeError("iterator must be a function");
                            var i = t.length;
                            if (i === +i) for (var o = 0; o < i; o++) e.call(r, t[o], o, t);
                            else for (var l in t) a.call(t, l) && e.call(r, t[l], l, t);
                        };
                    },
                    {},
                ],
                6: [
                    function (t, e, r) {
                        (function (t) {
                            var r;
                            (r = "undefined" != typeof window ? window : void 0 !== t ? t : "undefined" != typeof self ? self : {}), (e.exports = r);
                        }.call(this, "undefined" != typeof global ? global : "undefined" != typeof self ? self : "undefined" != typeof window ? window : {}));
                    },
                    {},
                ],
                7: [
                    function (t, e, r) {
                        "function" == typeof Object.create
                            ? (e.exports = function (t, e) {
                                  (t.super_ = e), (t.prototype = Object.create(e.prototype, { constructor: { value: t, enumerable: !1, writable: !0, configurable: !0 } }));
                              })
                            : (e.exports = function (t, e) {
                                  t.super_ = e;
                                  var r = function () {};
                                  (r.prototype = e.prototype), (t.prototype = new r()), (t.prototype.constructor = t);
                              });
                    },
                    {},
                ],
                8: [
                    function (t, e, r) {
                        var a = {}.toString;
                        e.exports =
                            Array.isArray ||
                            function (t) {
                                return "[object Array]" == a.call(t);
                            };
                    },
                    {},
                ],
                9: [
                    function (t, e, r) {
                        function a(t, e, r) {
                            if (!(t < e)) return t < 1.5 * e ? Math.floor(t / e) + " " + r : Math.ceil(t / e) + " " + r + "s";
                        }
                        var n = 1e3,
                            i = 60 * n,
                            o = 60 * i,
                            l = 24 * o,
                            s = 365.25 * l;
                        e.exports = function (t, e) {
                            e = e || {};
                            var r = typeof t;
                            if ("string" === r && t.length > 0)
                                return (function (t) {
                                    if (!((t = String(t)).length > 100)) {
                                        var e = /^((?:\d+)?\.?\d+) *(milliseconds?|msecs?|ms|seconds?|secs?|s|minutes?|mins?|m|hours?|hrs?|h|days?|d|years?|yrs?|y)?$/i.exec(t);
                                        if (e) {
                                            var r = parseFloat(e[1]);
                                            switch ((e[2] || "ms").toLowerCase()) {
                                                case "years":
                                                case "year":
                                                case "yrs":
                                                case "yr":
                                                case "y":
                                                    return r * s;
                                                case "days":
                                                case "day":
                                                case "d":
                                                    return r * l;
                                                case "hours":
                                                case "hour":
                                                case "hrs":
                                                case "hr":
                                                case "h":
                                                    return r * o;
                                                case "minutes":
                                                case "minute":
                                                case "mins":
                                                case "min":
                                                case "m":
                                                    return r * i;
                                                case "seconds":
                                                case "second":
                                                case "secs":
                                                case "sec":
                                                case "s":
                                                    return r * n;
                                                case "milliseconds":
                                                case "millisecond":
                                                case "msecs":
                                                case "msec":
                                                case "ms":
                                                    return r;
                                                default:
                                                    return;
                                            }
                                        }
                                    }
                                })(t);
                            if ("number" === r && !1 === isNaN(t))
                                return e.long
                                    ? (function (t) {
                                          return a(t, l, "day") || a(t, o, "hour") || a(t, i, "minute") || a(t, n, "second") || t + " ms";
                                      })(t)
                                    : (function (t) {
                                          return t >= l ? Math.round(t / l) + "d" : t >= o ? Math.round(t / o) + "h" : t >= i ? Math.round(t / i) + "m" : t >= n ? Math.round(t / n) + "s" : t + "ms";
                                      })(t);
                            throw new Error("val is not a non-empty string or a valid number. val=" + JSON.stringify(t));
                        };
                    },
                    {},
                ],
                10: [
                    function (t, e, r) {
                        "use strict";
                        var a = Object.prototype.hasOwnProperty,
                            n = Object.prototype.toString,
                            i = Array.prototype.slice,
                            o = t(11),
                            l = Object.prototype.propertyIsEnumerable,
                            s = !l.call({ toString: null }, "toString"),
                            c = l.call(function () {}, "prototype"),
                            u = ["toString", "toLocaleString", "valueOf", "hasOwnProperty", "isPrototypeOf", "propertyIsEnumerable", "constructor"],
                            h = function (t) {
                                var e = t.constructor;
                                return e && e.prototype === t;
                            },
                            g = {
                                $console: !0,
                                $external: !0,
                                $frame: !0,
                                $frameElement: !0,
                                $frames: !0,
                                $innerHeight: !0,
                                $innerWidth: !0,
                                $outerHeight: !0,
                                $outerWidth: !0,
                                $pageXOffset: !0,
                                $pageYOffset: !0,
                                $parent: !0,
                                $scrollLeft: !0,
                                $scrollTop: !0,
                                $scrollX: !0,
                                $scrollY: !0,
                                $self: !0,
                                $webkitIndexedDB: !0,
                                $webkitStorageInfo: !0,
                                $window: !0,
                            },
                            f = (function () {
                                if ("undefined" == typeof window) return !1;
                                for (var t in window)
                                    try {
                                        if (!g["$" + t] && a.call(window, t) && null !== window[t] && "object" == typeof window[t])
                                            try {
                                                h(window[t]);
                                            } catch (t) {
                                                return !0;
                                            }
                                    } catch (t) {
                                        return !0;
                                    }
                                return !1;
                            })(),
                            b = function (t) {
                                if ("undefined" == typeof window || !f) return h(t);
                                try {
                                    return h(t);
                                } catch (t) {
                                    return !1;
                                }
                            },
                            p = function (t) {
                                var e = null !== t && "object" == typeof t,
                                    r = "[object Function]" === n.call(t),
                                    i = o(t),
                                    l = e && "[object String]" === n.call(t),
                                    h = [];
                                if (!e && !r && !i) throw new TypeError("Object.keys called on a non-object");
                                var g = c && r;
                                if (l && t.length > 0 && !a.call(t, 0)) for (var f = 0; f < t.length; ++f) h.push(String(f));
                                if (i && t.length > 0) for (var p = 0; p < t.length; ++p) h.push(String(p));
                                else for (var _ in t) (g && "prototype" === _) || !a.call(t, _) || h.push(String(_));
                                if (s) for (var m = b(t), d = 0; d < u.length; ++d) (m && "constructor" === u[d]) || !a.call(t, u[d]) || h.push(u[d]);
                                return h;
                            };
                        (p.shim = function () {
                            if (Object.keys) {
                                if (
                                    !(function () {
                                        return 2 === (Object.keys(arguments) || "").length;
                                    })(1, 2)
                                ) {
                                    var t = Object.keys;
                                    Object.keys = function (e) {
                                        return t(o(e) ? i.call(e) : e);
                                    };
                                }
                            } else Object.keys = p;
                            return Object.keys || p;
                        }),
                            (e.exports = p);
                    },
                    { 11: 11 },
                ],
                11: [
                    function (t, e, r) {
                        "use strict";
                        var a = Object.prototype.toString;
                        e.exports = function (t) {
                            var e = a.call(t),
                                r = "[object Arguments]" === e;
                            return r || (r = "[object Array]" !== e && null !== t && "object" == typeof t && "number" == typeof t.length && t.length >= 0 && "[object Function]" === a.call(t.callee)), r;
                        };
                    },
                    {},
                ],
                12: [
                    function (t, e, r) {
                        function a() {
                            throw new Error("setTimeout has not been defined");
                        }
                        function n() {
                            throw new Error("clearTimeout has not been defined");
                        }
                        function i(t) {
                            if (u === setTimeout) return setTimeout(t, 0);
                            if ((u === a || !u) && setTimeout) return (u = setTimeout), setTimeout(t, 0);
                            try {
                                return u(t, 0);
                            } catch (e) {
                                try {
                                    return u.call(null, t, 0);
                                } catch (e) {
                                    return u.call(this, t, 0);
                                }
                            }
                        }
                        function o() {
                            p && f && ((p = !1), f.length ? (b = f.concat(b)) : (_ = -1), b.length && l());
                        }
                        function l() {
                            if (!p) {
                                var t = i(o);
                                p = !0;
                                for (var e = b.length; e; ) {
                                    for (f = b, b = []; ++_ < e; ) f && f[_].run();
                                    (_ = -1), (e = b.length);
                                }
                                (f = null),
                                    (p = !1),
                                    (function (t) {
                                        if (h === clearTimeout) return clearTimeout(t);
                                        if ((h === n || !h) && clearTimeout) return (h = clearTimeout), clearTimeout(t);
                                        try {
                                            h(t);
                                        } catch (e) {
                                            try {
                                                return h.call(null, t);
                                            } catch (e) {
                                                return h.call(this, t);
                                            }
                                        }
                                    })(t);
                            }
                        }
                        function s(t, e) {
                            (this.fun = t), (this.array = e);
                        }
                        function c() {}
                        var u,
                            h,
                            g = (e.exports = {});
                        !(function () {
                            try {
                                u = "function" == typeof setTimeout ? setTimeout : a;
                            } catch (t) {
                                u = a;
                            }
                            try {
                                h = "function" == typeof clearTimeout ? clearTimeout : n;
                            } catch (t) {
                                h = n;
                            }
                        })();
                        var f,
                            b = [],
                            p = !1,
                            _ = -1;
                        (g.nextTick = function (t) {
                            var e = new Array(arguments.length - 1);
                            if (arguments.length > 1) for (var r = 1; r < arguments.length; r++) e[r - 1] = arguments[r];
                            b.push(new s(t, e)), 1 !== b.length || p || i(l);
                        }),
                            (s.prototype.run = function () {
                                this.fun.apply(null, this.array);
                            }),
                            (g.title = "browser"),
                            (g.browser = !0),
                            (g.env = {}),
                            (g.argv = []),
                            (g.version = ""),
                            (g.versions = {}),
                            (g.on = c),
                            (g.addListener = c),
                            (g.once = c),
                            (g.off = c),
                            (g.removeListener = c),
                            (g.removeAllListeners = c),
                            (g.emit = c),
                            (g.binding = function (t) {
                                throw new Error("process.binding is not supported");
                            }),
                            (g.cwd = function () {
                                return "/";
                            }),
                            (g.chdir = function (t) {
                                throw new Error("process.chdir is not supported");
                            }),
                            (g.umask = function () {
                                return 0;
                            });
                    },
                    {},
                ],
                13: [
                    function (t, e, r) {
                        "use strict";
                        function a(t, e) {
                            return Object.prototype.hasOwnProperty.call(t, e);
                        }
                        e.exports = function (t, e, r, i) {
                            (e = e || "&"), (r = r || "=");
                            var o = {};
                            if ("string" != typeof t || 0 === t.length) return o;
                            var l = /\+/g;
                            t = t.split(e);
                            var s = 1e3;
                            i && "number" == typeof i.maxKeys && (s = i.maxKeys);
                            var c = t.length;
                            s > 0 && c > s && (c = s);
                            for (var u = 0; u < c; ++u) {
                                var h,
                                    g,
                                    f,
                                    b,
                                    p = t[u].replace(l, "%20"),
                                    _ = p.indexOf(r);
                                _ >= 0 ? ((h = p.substr(0, _)), (g = p.substr(_ + 1))) : ((h = p), (g = "")), (f = decodeURIComponent(h)), (b = decodeURIComponent(g)), a(o, f) ? (n(o[f]) ? o[f].push(b) : (o[f] = [o[f], b])) : (o[f] = b);
                            }
                            return o;
                        };
                        var n =
                            Array.isArray ||
                            function (t) {
                                return "[object Array]" === Object.prototype.toString.call(t);
                            };
                    },
                    {},
                ],
                14: [
                    function (t, e, r) {
                        "use strict";
                        function a(t, e) {
                            if (t.map) return t.map(e);
                            for (var r = [], a = 0; a < t.length; a++) r.push(e(t[a], a));
                            return r;
                        }
                        var n = function (t) {
                            switch (typeof t) {
                                case "string":
                                    return t;
                                case "boolean":
                                    return t ? "true" : "false";
                                case "number":
                                    return isFinite(t) ? t : "";
                                default:
                                    return "";
                            }
                        };
                        e.exports = function (t, e, r, l) {
                            return (
                                (e = e || "&"),
                                (r = r || "="),
                                null === t && (t = void 0),
                                "object" == typeof t
                                    ? a(o(t), function (o) {
                                          var l = encodeURIComponent(n(o)) + r;
                                          return i(t[o])
                                              ? a(t[o], function (t) {
                                                    return l + encodeURIComponent(n(t));
                                                }).join(e)
                                              : l + encodeURIComponent(n(t[o]));
                                      }).join(e)
                                    : l
                                    ? encodeURIComponent(n(l)) + r + encodeURIComponent(n(t))
                                    : ""
                            );
                        };
                        var i =
                                Array.isArray ||
                                function (t) {
                                    return "[object Array]" === Object.prototype.toString.call(t);
                                },
                            o =
                                Object.keys ||
                                function (t) {
                                    var e = [];
                                    for (var r in t) Object.prototype.hasOwnProperty.call(t, r) && e.push(r);
                                    return e;
                                };
                    },
                    {},
                ],
                15: [
                    function (t, e, r) {
                        "use strict";
                        (r.decode = r.parse = t(13)), (r.encode = r.stringify = t(14));
                    },
                    { 13: 13, 14: 14 },
                ],
                16: [
                    function (t, e, r) {
                        function a() {
                            s.apply(this, arguments);
                        }
                        function n() {
                            throw new u.AlgoliaSearchError("Not implemented in this environment.\nIf you feel this is a mistake, write to support@algolia.com");
                        }
                        e.exports = a;
                        var i = t(18),
                            o = t(28),
                            l = t(29),
                            s = t(17),
                            c = t(7),
                            u = t(30);
                        c(a, s),
                            (a.prototype.deleteIndex = function (t, e) {
                                return this._jsonRequest({ method: "DELETE", url: "/1/indexes/" + encodeURIComponent(t), hostType: "write", callback: e });
                            }),
                            (a.prototype.moveIndex = function (t, e, r) {
                                var a = { operation: "move", destination: e };
                                return this._jsonRequest({ method: "POST", url: "/1/indexes/" + encodeURIComponent(t) + "/operation", body: a, hostType: "write", callback: r });
                            }),
                            (a.prototype.copyIndex = function (t, e, r, a) {
                                var n = { operation: "copy", destination: e },
                                    i = a;
                                if ("function" == typeof r) i = r;
                                else if (Array.isArray(r) && r.length > 0) n.scope = r;
                                else if (void 0 !== r) throw new Error("the scope given to `copyIndex` was not an array with settings, synonyms or rules");
                                return this._jsonRequest({ method: "POST", url: "/1/indexes/" + encodeURIComponent(t) + "/operation", body: n, hostType: "write", callback: i });
                            }),
                            (a.prototype.getLogs = function (e, r, a) {
                                var n = t(26),
                                    i = {};
                                return (
                                    "object" == typeof e
                                        ? ((i = n(e)), (a = r))
                                        : 0 === arguments.length || "function" == typeof e
                                        ? (a = e)
                                        : 1 === arguments.length || "function" == typeof r
                                        ? ((a = r), (i.offset = e))
                                        : ((i.offset = e), (i.length = r)),
                                    void 0 === i.offset && (i.offset = 0),
                                    void 0 === i.length && (i.length = 10),
                                    this._jsonRequest({ method: "GET", url: "/1/logs?" + this._getSearchParams(i, ""), hostType: "read", callback: a })
                                );
                            }),
                            (a.prototype.listIndexes = function (t, e) {
                                var r = "";
                                return void 0 === t || "function" == typeof t ? (e = t) : (r = "?page=" + t), this._jsonRequest({ method: "GET", url: "/1/indexes" + r, hostType: "read", callback: e });
                            }),
                            (a.prototype.initIndex = function (t) {
                                return new i(this, t);
                            }),
                            (a.prototype.initAnalytics = function (e) {
                                return t(27)(this.applicationID, this.apiKey, e);
                            }),
                            (a.prototype.listUserKeys = o(function (t) {
                                return this.listApiKeys(t);
                            }, l("client.listUserKeys()", "client.listApiKeys()"))),
                            (a.prototype.listApiKeys = function (t) {
                                return this._jsonRequest({ method: "GET", url: "/1/keys", hostType: "read", callback: t });
                            }),
                            (a.prototype.getUserKeyACL = o(function (t, e) {
                                return this.getApiKey(t, e);
                            }, l("client.getUserKeyACL()", "client.getApiKey()"))),
                            (a.prototype.getApiKey = function (t, e) {
                                return this._jsonRequest({ method: "GET", url: "/1/keys/" + t, hostType: "read", callback: e });
                            }),
                            (a.prototype.deleteUserKey = o(function (t, e) {
                                return this.deleteApiKey(t, e);
                            }, l("client.deleteUserKey()", "client.deleteApiKey()"))),
                            (a.prototype.deleteApiKey = function (t, e) {
                                return this._jsonRequest({ method: "DELETE", url: "/1/keys/" + t, hostType: "write", callback: e });
                            }),
                            (a.prototype.addUserKey = o(function (t, e, r) {
                                return this.addApiKey(t, e, r);
                            }, l("client.addUserKey()", "client.addApiKey()"))),
                            (a.prototype.addApiKey = function (e, r, a) {
                                if (!t(8)(e)) throw new Error("Usage: client.addApiKey(arrayOfAcls[, params, callback])");
                                (1 !== arguments.length && "function" != typeof r) || ((a = r), (r = null));
                                var n = { acl: e };
                                return (
                                    r &&
                                        ((n.validity = r.validity),
                                        (n.maxQueriesPerIPPerHour = r.maxQueriesPerIPPerHour),
                                        (n.maxHitsPerQuery = r.maxHitsPerQuery),
                                        (n.indexes = r.indexes),
                                        (n.description = r.description),
                                        r.queryParameters && (n.queryParameters = this._getSearchParams(r.queryParameters, "")),
                                        (n.referers = r.referers)),
                                    this._jsonRequest({ method: "POST", url: "/1/keys", body: n, hostType: "write", callback: a })
                                );
                            }),
                            (a.prototype.addUserKeyWithValidity = o(function (t, e, r) {
                                return this.addApiKey(t, e, r);
                            }, l("client.addUserKeyWithValidity()", "client.addApiKey()"))),
                            (a.prototype.updateUserKey = o(function (t, e, r, a) {
                                return this.updateApiKey(t, e, r, a);
                            }, l("client.updateUserKey()", "client.updateApiKey()"))),
                            (a.prototype.updateApiKey = function (e, r, a, n) {
                                if (!t(8)(r)) throw new Error("Usage: client.updateApiKey(key, arrayOfAcls[, params, callback])");
                                (2 !== arguments.length && "function" != typeof a) || ((n = a), (a = null));
                                var i = { acl: r };
                                return (
                                    a &&
                                        ((i.validity = a.validity),
                                        (i.maxQueriesPerIPPerHour = a.maxQueriesPerIPPerHour),
                                        (i.maxHitsPerQuery = a.maxHitsPerQuery),
                                        (i.indexes = a.indexes),
                                        (i.description = a.description),
                                        a.queryParameters && (i.queryParameters = this._getSearchParams(a.queryParameters, "")),
                                        (i.referers = a.referers)),
                                    this._jsonRequest({ method: "PUT", url: "/1/keys/" + e, body: i, hostType: "write", callback: n })
                                );
                            }),
                            (a.prototype.startQueriesBatch = o(function () {
                                this._batch = [];
                            }, l("client.startQueriesBatch()", "client.search()"))),
                            (a.prototype.addQueryInBatch = o(function (t, e, r) {
                                this._batch.push({ indexName: t, query: e, params: r });
                            }, l("client.addQueryInBatch()", "client.search()"))),
                            (a.prototype.sendQueriesBatch = o(function (t) {
                                return this.search(this._batch, t);
                            }, l("client.sendQueriesBatch()", "client.search()"))),
                            (a.prototype.batch = function (e, r) {
                                if (!t(8)(e)) throw new Error("Usage: client.batch(operations[, callback])");
                                return this._jsonRequest({ method: "POST", url: "/1/indexes/*/batch", body: { requests: e }, hostType: "write", callback: r });
                            }),
                            (a.prototype.assignUserID = function (t, e) {
                                if (!t.userID || !t.cluster) throw new u.AlgoliaSearchError("You have to provide both a userID and cluster", t);
                                return this._jsonRequest({ method: "POST", url: "/1/clusters/mapping", hostType: "write", body: { cluster: t.cluster }, callback: e, headers: { "x-algolia-user-id": t.userID } });
                            }),
                            (a.prototype.getTopUserID = function (t) {
                                return this._jsonRequest({ method: "GET", url: "/1/clusters/mapping/top", hostType: "read", callback: t });
                            }),
                            (a.prototype.getUserID = function (t, e) {
                                if (!t.userID) throw new u.AlgoliaSearchError("You have to provide a userID", { debugData: t });
                                return this._jsonRequest({ method: "GET", url: "/1/clusters/mapping/" + t.userID, hostType: "read", callback: e });
                            }),
                            (a.prototype.listClusters = function (t) {
                                return this._jsonRequest({ method: "GET", url: "/1/clusters", hostType: "read", callback: t });
                            }),
                            (a.prototype.listUserIDs = function (t, e) {
                                return this._jsonRequest({ method: "GET", url: "/1/clusters/mapping", body: t, hostType: "read", callback: e });
                            }),
                            (a.prototype.removeUserID = function (t, e) {
                                if (!t.userID) throw new u.AlgoliaSearchError("You have to provide a userID", { debugData: t });
                                return this._jsonRequest({ method: "DELETE", url: "/1/clusters/mapping", hostType: "write", callback: e, headers: { "x-algolia-user-id": t.userID } });
                            }),
                            (a.prototype.searchUserIDs = function (t, e) {
                                return this._jsonRequest({ method: "POST", url: "/1/clusters/mapping/search", body: t, hostType: "read", callback: e });
                            }),
                            (a.prototype.setPersonalizationStrategy = function (t, e) {
                                return this._jsonRequest({ method: "POST", url: "/1/recommendation/personalization/strategy", body: t, hostType: "write", callback: e });
                            }),
                            (a.prototype.getPersonalizationStrategy = function (t) {
                                return this._jsonRequest({ method: "GET", url: "/1/recommendation/personalization/strategy", hostType: "write", callback: t });
                            }),
                            (a.prototype.destroy = n),
                            (a.prototype.enableRateLimitForward = n),
                            (a.prototype.disableRateLimitForward = n),
                            (a.prototype.useSecuredAPIKey = n),
                            (a.prototype.disableSecuredAPIKey = n),
                            (a.prototype.generateSecuredApiKey = n);
                    },
                    { 17: 17, 18: 18, 26: 26, 27: 27, 28: 28, 29: 29, 30: 30, 7: 7, 8: 8 },
                ],
                17: [
                    function (t, e, r) {
                        (function (r) {
                            function a(e, r, a) {
                                var i = t(1)("algoliasearch"),
                                    o = t(26),
                                    s = t(8),
                                    c = t(32),
                                    u = "Usage: algoliasearch(applicationID, apiKey, opts)";
                                if (!0 !== a._allowEmptyCredentials && !e) throw new l.AlgoliaSearchError("Please provide an application ID. " + u);
                                if (!0 !== a._allowEmptyCredentials && !r) throw new l.AlgoliaSearchError("Please provide an API key. " + u);
                                (this.applicationID = e),
                                    (this.apiKey = r),
                                    (this.hosts = { read: [], write: [] }),
                                    (a = a || {}),
                                    (this._timeouts = a.timeouts || { connect: 1e3, read: 2e3, write: 3e4 }),
                                    a.timeout && (this._timeouts.connect = this._timeouts.read = this._timeouts.write = a.timeout);
                                var h = a.protocol || "https:";
                                if ((/:$/.test(h) || (h += ":"), "http:" !== h && "https:" !== h)) throw new l.AlgoliaSearchError("protocol must be `http:` or `https:` (was `" + a.protocol + "`)");
                                if ((this._checkAppIdData(), a.hosts)) s(a.hosts) ? ((this.hosts.read = o(a.hosts)), (this.hosts.write = o(a.hosts))) : ((this.hosts.read = o(a.hosts.read)), (this.hosts.write = o(a.hosts.write)));
                                else {
                                    var g = c(this._shuffleResult, function (t) {
                                            return e + "-" + t + ".algolianet.com";
                                        }),
                                        f = (!1 === a.dsn ? "" : "-dsn") + ".algolia.net";
                                    (this.hosts.read = [this.applicationID + f].concat(g)), (this.hosts.write = [this.applicationID + ".algolia.net"].concat(g));
                                }
                                (this.hosts.read = c(this.hosts.read, n(h))),
                                    (this.hosts.write = c(this.hosts.write, n(h))),
                                    (this.extraHeaders = {}),
                                    (this.cache = a._cache || {}),
                                    (this._ua = a._ua),
                                    (this._useCache = !(void 0 !== a._useCache && !a._cache) || a._useCache),
                                    (this._useRequestCache = this._useCache && a._useRequestCache),
                                    (this._useFallback = void 0 === a.useFallback || a.useFallback),
                                    (this._setTimeout = a._setTimeout),
                                    i("init done, %j", this);
                            }
                            function n(t) {
                                return function (e) {
                                    return t + "//" + e.toLowerCase();
                                };
                            }
                            function i(t) {
                                if (void 0 === Array.prototype.toJSON) return JSON.stringify(t);
                                var e = Array.prototype.toJSON;
                                delete Array.prototype.toJSON;
                                var r = JSON.stringify(t);
                                return (Array.prototype.toJSON = e), r;
                            }
                            function o(t) {
                                var e = {};
                                for (var r in t)
                                    if (Object.prototype.hasOwnProperty.call(t, r)) {
                                        var a;
                                        (a = "x-algolia-api-key" === r || "x-algolia-application-id" === r ? "**hidden for security purposes**" : t[r]), (e[r] = a);
                                    }
                                return e;
                            }
                            e.exports = a;
                            var l = t(30),
                                s = t(31),
                                c = t(20),
                                u = t(36),
                                h = (r.env.RESET_APP_DATA_TIMER && parseInt(r.env.RESET_APP_DATA_TIMER, 10)) || 12e4;
                            (a.prototype.initIndex = function (t) {
                                return new c(this, t);
                            }),
                                (a.prototype.setExtraHeader = function (t, e) {
                                    this.extraHeaders[t.toLowerCase()] = e;
                                }),
                                (a.prototype.getExtraHeader = function (t) {
                                    return this.extraHeaders[t.toLowerCase()];
                                }),
                                (a.prototype.unsetExtraHeader = function (t) {
                                    delete this.extraHeaders[t.toLowerCase()];
                                }),
                                (a.prototype.addAlgoliaAgent = function (t) {
                                    -1 === this._ua.indexOf(";" + t) && (this._ua += ";" + t);
                                }),
                                (a.prototype._jsonRequest = function (e) {
                                    function r(t, e, r) {
                                        return b._useCache && t && e && void 0 !== e[r];
                                    }
                                    function a(t, a) {
                                        return (
                                            r(b._useRequestCache, f, c) &&
                                                t.catch(function () {
                                                    delete f[c];
                                                }),
                                            "function" != typeof e.callback
                                                ? t.then(a)
                                                : void t.then(
                                                      function (t) {
                                                          s(function () {
                                                              e.callback(null, a(t));
                                                          }, b._setTimeout || setTimeout);
                                                      },
                                                      function (t) {
                                                          s(function () {
                                                              e.callback(t);
                                                          }, b._setTimeout || setTimeout);
                                                      }
                                                  )
                                        );
                                    }
                                    this._checkAppIdData();
                                    var n,
                                        c,
                                        u,
                                        h = t(1)("algoliasearch:" + e.url),
                                        g = e.additionalUA || "",
                                        f = e.cache,
                                        b = this,
                                        p = 0,
                                        _ = !1,
                                        m = b._useFallback && b._request.fallback && e.fallback;
                                    this.apiKey.length > 500 && void 0 !== e.body && (void 0 !== e.body.params || void 0 !== e.body.requests)
                                        ? ((e.body.apiKey = this.apiKey), (u = this._computeRequestHeaders({ additionalUA: g, withApiKey: !1, headers: e.headers })))
                                        : (u = this._computeRequestHeaders({ additionalUA: g, headers: e.headers })),
                                        void 0 !== e.body && (n = i(e.body)),
                                        h("request start");
                                    var d = [];
                                    if ((b._useCache && b._useRequestCache && (c = e.url), b._useCache && b._useRequestCache && n && (c += "_body_" + n), r(b._useRequestCache, f, c))) {
                                        h("serving request from cache");
                                        var y = f[c];
                                        return a("function" != typeof y.then ? b._promise.resolve({ responseText: y }) : y, function (t) {
                                            return JSON.parse(t.responseText);
                                        });
                                    }
                                    var v = (function t(a, s) {
                                        function y() {
                                            return h("retrying request"), b._incrementHostIndex(e.hostType), t(a, s);
                                        }
                                        function v() {
                                            return h("retrying request with higher timeout"), b._incrementHostIndex(e.hostType), b._incrementTimeoutMultipler(), (s.timeouts = b._getTimeoutsForRequest(e.hostType)), t(a, s);
                                        }
                                        b._checkAppIdData();
                                        var C = new Date();
                                        if ((b._useCache && !b._useRequestCache && (c = e.url), b._useCache && !b._useRequestCache && n && (c += "_body_" + s.body), r(!b._useRequestCache, f, c))) {
                                            h("serving response from cache");
                                            var w = f[c];
                                            return b._promise.resolve({ body: JSON.parse(w), responseText: w });
                                        }
                                        if (p >= b.hosts[e.hostType].length)
                                            return !m || _
                                                ? (h("could not get any response"),
                                                  b._promise.reject(
                                                      new l.AlgoliaSearchError("Cannot connect to the AlgoliaSearch API. Send an email to support@algolia.com to report and resolve the issue. Application id was: " + b.applicationID, {
                                                          debugData: d,
                                                      })
                                                  ))
                                                : (h("switching to fallback"),
                                                  (p = 0),
                                                  (s.method = e.fallback.method),
                                                  (s.url = e.fallback.url),
                                                  (s.jsonBody = e.fallback.body),
                                                  s.jsonBody && (s.body = i(s.jsonBody)),
                                                  (u = b._computeRequestHeaders({ additionalUA: g, headers: e.headers })),
                                                  (s.timeouts = b._getTimeoutsForRequest(e.hostType)),
                                                  b._setHostIndexByType(0, e.hostType),
                                                  (_ = !0),
                                                  t(b._request.fallback, s));
                                        var R = b._getHostByType(e.hostType),
                                            I = R + s.url,
                                            D = { body: s.body, jsonBody: s.jsonBody, method: s.method, headers: u, timeouts: s.timeouts, debug: h, forceAuthHeaders: s.forceAuthHeaders };
                                        return (
                                            h("method: %s, url: %s, headers: %j, timeouts: %d", D.method, I, D.headers, D.timeouts),
                                            a === b._request.fallback && h("using fallback"),
                                            a.call(b, I, D).then(
                                                function (t) {
                                                    var e = (t && t.body && t.body.message && t.body.status) || t.statusCode || (t && t.body && 200);
                                                    h("received response: statusCode: %s, computed statusCode: %d, headers: %j", t.statusCode, e, t.headers);
                                                    var r = 2 === Math.floor(e / 100),
                                                        a = new Date();
                                                    if (
                                                        (d.push({
                                                            currentHost: R,
                                                            headers: o(u),
                                                            content: n || null,
                                                            contentLength: void 0 !== n ? n.length : null,
                                                            method: s.method,
                                                            timeouts: s.timeouts,
                                                            url: s.url,
                                                            startTime: C,
                                                            endTime: a,
                                                            duration: a - C,
                                                            statusCode: e,
                                                        }),
                                                        r)
                                                    )
                                                        return b._useCache && !b._useRequestCache && f && (f[c] = t.responseText), { responseText: t.responseText, body: t.body };
                                                    if (4 !== Math.floor(e / 100)) return (p += 1), y();
                                                    h("unrecoverable error");
                                                    var i = new l.AlgoliaSearchError(t.body && t.body.message, { debugData: d, statusCode: e });
                                                    return b._promise.reject(i);
                                                },
                                                function (t) {
                                                    h("error: %s, stack: %s", t.message, t.stack);
                                                    var r = new Date();
                                                    return (
                                                        d.push({
                                                            currentHost: R,
                                                            headers: o(u),
                                                            content: n || null,
                                                            contentLength: void 0 !== n ? n.length : null,
                                                            method: s.method,
                                                            timeouts: s.timeouts,
                                                            url: s.url,
                                                            startTime: C,
                                                            endTime: r,
                                                            duration: r - C,
                                                        }),
                                                        t instanceof l.AlgoliaSearchError || (t = new l.Unknown(t && t.message, t)),
                                                        (p += 1),
                                                        t instanceof l.Unknown || t instanceof l.UnparsableJSON || (p >= b.hosts[e.hostType].length && (_ || !m))
                                                            ? ((t.debugData = d), b._promise.reject(t))
                                                            : t instanceof l.RequestTimeout
                                                            ? v()
                                                            : y()
                                                    );
                                                }
                                            )
                                        );
                                    })(b._request, { url: e.url, method: e.method, body: n, jsonBody: e.body, timeouts: b._getTimeoutsForRequest(e.hostType), forceAuthHeaders: e.forceAuthHeaders });
                                    return (
                                        b._useCache && b._useRequestCache && f && (f[c] = v),
                                        a(v, function (t) {
                                            return t.body;
                                        })
                                    );
                                }),
                                (a.prototype._getSearchParams = function (t, e) {
                                    if (null == t) return e;
                                    for (var r in t)
                                        null !== r && void 0 !== t[r] && t.hasOwnProperty(r) && ((e += "" === e ? "" : "&"), (e += r + "=" + encodeURIComponent("[object Array]" === Object.prototype.toString.call(t[r]) ? i(t[r]) : t[r])));
                                    return e;
                                }),
                                (a.prototype._computeRequestHeaders = function (e) {
                                    var r = t(5),
                                        a = { "x-algolia-agent": e.additionalUA ? this._ua + ";" + e.additionalUA : this._ua, "x-algolia-application-id": this.applicationID };
                                    return (
                                        !1 !== e.withApiKey && (a["x-algolia-api-key"] = this.apiKey),
                                        this.userToken && (a["x-algolia-usertoken"] = this.userToken),
                                        this.securityTags && (a["x-algolia-tagfilters"] = this.securityTags),
                                        r(this.extraHeaders, function (t, e) {
                                            a[e] = t;
                                        }),
                                        e.headers &&
                                            r(e.headers, function (t, e) {
                                                a[e] = t;
                                            }),
                                        a
                                    );
                                }),
                                (a.prototype.search = function (e, r, a) {
                                    var n = t(8),
                                        i = t(32);
                                    if (!n(e)) throw new Error("Usage: client.search(arrayOfQueries[, callback])");
                                    "function" == typeof r ? ((a = r), (r = {})) : void 0 === r && (r = {});
                                    var o = this,
                                        l = {
                                            requests: i(e, function (t) {
                                                var e = "";
                                                return void 0 !== t.query && (e += "query=" + encodeURIComponent(t.query)), { indexName: t.indexName, params: o._getSearchParams(t.params, e) };
                                            }),
                                        },
                                        s = i(l.requests, function (t, e) {
                                            return e + "=" + encodeURIComponent("/1/indexes/" + encodeURIComponent(t.indexName) + "?" + t.params);
                                        }).join("&");
                                    return (
                                        void 0 !== r.strategy && (l.strategy = r.strategy),
                                        this._jsonRequest({ cache: this.cache, method: "POST", url: "/1/indexes/*/queries", body: l, hostType: "read", fallback: { method: "GET", url: "/1/indexes/*", body: { params: s } }, callback: a })
                                    );
                                }),
                                (a.prototype.searchForFacetValues = function (e) {
                                    var r = t(8),
                                        a = t(32),
                                        n = "Usage: client.searchForFacetValues([{indexName, params: {facetName, facetQuery, ...params}}, ...queries])";
                                    if (!r(e)) throw new Error(n);
                                    var i = this;
                                    return i._promise.all(
                                        a(e, function (e) {
                                            if (!e || void 0 === e.indexName || void 0 === e.params.facetName || void 0 === e.params.facetQuery) throw new Error(n);
                                            var r = t(26),
                                                a = t(34),
                                                o = e.indexName,
                                                l = e.params,
                                                s = l.facetName,
                                                c = a(r(l), function (t) {
                                                    return "facetName" === t;
                                                }),
                                                u = i._getSearchParams(c, "");
                                            return i._jsonRequest({ cache: i.cache, method: "POST", url: "/1/indexes/" + encodeURIComponent(o) + "/facets/" + encodeURIComponent(s) + "/query", hostType: "read", body: { params: u } });
                                        })
                                    );
                                }),
                                (a.prototype.setSecurityTags = function (t) {
                                    if ("[object Array]" === Object.prototype.toString.call(t)) {
                                        for (var e = [], r = 0; r < t.length; ++r)
                                            if ("[object Array]" === Object.prototype.toString.call(t[r])) {
                                                for (var a = [], n = 0; n < t[r].length; ++n) a.push(t[r][n]);
                                                e.push("(" + a.join(",") + ")");
                                            } else e.push(t[r]);
                                        t = e.join(",");
                                    }
                                    this.securityTags = t;
                                }),
                                (a.prototype.setUserToken = function (t) {
                                    this.userToken = t;
                                }),
                                (a.prototype.clearCache = function () {
                                    this.cache = {};
                                }),
                                (a.prototype.setRequestTimeout = function (t) {
                                    t && (this._timeouts.connect = this._timeouts.read = this._timeouts.write = t);
                                }),
                                (a.prototype.setTimeouts = function (t) {
                                    this._timeouts = t;
                                }),
                                (a.prototype.getTimeouts = function () {
                                    return this._timeouts;
                                }),
                                (a.prototype._getAppIdData = function () {
                                    var t = u.get(this.applicationID);
                                    return null !== t && this._cacheAppIdData(t), t;
                                }),
                                (a.prototype._setAppIdData = function (t) {
                                    return (t.lastChange = new Date().getTime()), this._cacheAppIdData(t), u.set(this.applicationID, t);
                                }),
                                (a.prototype._checkAppIdData = function () {
                                    var t = this._getAppIdData(),
                                        e = new Date().getTime();
                                    return null === t || e - t.lastChange > h ? this._resetInitialAppIdData(t) : t;
                                }),
                                (a.prototype._resetInitialAppIdData = function (t) {
                                    var e = t || {};
                                    return (
                                        (e.hostIndexes = { read: 0, write: 0 }),
                                        (e.timeoutMultiplier = 1),
                                        (e.shuffleResult =
                                            e.shuffleResult ||
                                            (function (t) {
                                                for (var e, r, a = t.length; 0 !== a; ) (r = Math.floor(Math.random() * a)), (e = t[(a -= 1)]), (t[a] = t[r]), (t[r] = e);
                                                return t;
                                            })([1, 2, 3])),
                                        this._setAppIdData(e)
                                    );
                                }),
                                (a.prototype._cacheAppIdData = function (t) {
                                    (this._hostIndexes = t.hostIndexes), (this._timeoutMultiplier = t.timeoutMultiplier), (this._shuffleResult = t.shuffleResult);
                                }),
                                (a.prototype._partialAppIdDataUpdate = function (e) {
                                    var r = t(5),
                                        a = this._getAppIdData();
                                    return (
                                        r(e, function (t, e) {
                                            a[e] = t;
                                        }),
                                        this._setAppIdData(a)
                                    );
                                }),
                                (a.prototype._getHostByType = function (t) {
                                    return this.hosts[t][this._getHostIndexByType(t)];
                                }),
                                (a.prototype._getTimeoutMultiplier = function () {
                                    return this._timeoutMultiplier;
                                }),
                                (a.prototype._getHostIndexByType = function (t) {
                                    return this._hostIndexes[t];
                                }),
                                (a.prototype._setHostIndexByType = function (e, r) {
                                    var a = t(26)(this._hostIndexes);
                                    return (a[r] = e), this._partialAppIdDataUpdate({ hostIndexes: a }), e;
                                }),
                                (a.prototype._incrementHostIndex = function (t) {
                                    return this._setHostIndexByType((this._getHostIndexByType(t) + 1) % this.hosts[t].length, t);
                                }),
                                (a.prototype._incrementTimeoutMultipler = function () {
                                    var t = Math.max(this._timeoutMultiplier + 1, 4);
                                    return this._partialAppIdDataUpdate({ timeoutMultiplier: t });
                                }),
                                (a.prototype._getTimeoutsForRequest = function (t) {
                                    return { connect: this._timeouts.connect * this._timeoutMultiplier, complete: this._timeouts[t] * this._timeoutMultiplier };
                                });
                        }.call(this, t(12)));
                    },
                    { 1: 1, 12: 12, 20: 20, 26: 26, 30: 30, 31: 31, 32: 32, 34: 34, 36: 36, 5: 5, 8: 8 },
                ],
                18: [
                    function (t, e, r) {
                        function a() {
                            o.apply(this, arguments);
                        }
                        function n(t, e, r) {
                            return (function r(a, n) {
                                var i = { page: a || 0, hitsPerPage: e || 100 },
                                    o = n || [];
                                return t(i).then(function (t) {
                                    var e = t.hits,
                                        a = t.nbHits,
                                        n = e.map(function (t) {
                                            return delete t._highlightResult, t;
                                        }),
                                        l = o.concat(n);
                                    return l.length < a ? r(i.page + 1, l) : l;
                                });
                            })().then(function (t) {
                                return "function" == typeof r ? void r(t) : t;
                            });
                        }
                        var i = t(7),
                            o = t(20),
                            l = t(28),
                            s = t(29),
                            c = t(31),
                            u = t(30),
                            h = l(function () {}, s("forwardToSlaves", "forwardToReplicas"));
                        (e.exports = a),
                            i(a, o),
                            (a.prototype.addObject = function (t, e, r) {
                                return (
                                    (1 !== arguments.length && "function" != typeof e) || ((r = e), (e = void 0)),
                                    this.as._jsonRequest({
                                        method: void 0 !== e ? "PUT" : "POST",
                                        url: "/1/indexes/" + encodeURIComponent(this.indexName) + (void 0 !== e ? "/" + encodeURIComponent(e) : ""),
                                        body: t,
                                        hostType: "write",
                                        callback: r,
                                    })
                                );
                            }),
                            (a.prototype.addObjects = function (e, r) {
                                if (!t(8)(e)) throw new Error("Usage: index.addObjects(arrayOfObjects[, callback])");
                                for (var a = { requests: [] }, n = 0; n < e.length; ++n) {
                                    var i = { action: "addObject", body: e[n] };
                                    a.requests.push(i);
                                }
                                return this.as._jsonRequest({ method: "POST", url: "/1/indexes/" + encodeURIComponent(this.indexName) + "/batch", body: a, hostType: "write", callback: r });
                            }),
                            (a.prototype.partialUpdateObject = function (t, e, r) {
                                (1 !== arguments.length && "function" != typeof e) || ((r = e), (e = void 0));
                                var a = "/1/indexes/" + encodeURIComponent(this.indexName) + "/" + encodeURIComponent(t.objectID) + "/partial";
                                return !1 === e && (a += "?createIfNotExists=false"), this.as._jsonRequest({ method: "POST", url: a, body: t, hostType: "write", callback: r });
                            }),
                            (a.prototype.partialUpdateObjects = function (e, r, a) {
                                (1 !== arguments.length && "function" != typeof r) || ((a = r), (r = !0));
                                if (!t(8)(e)) throw new Error("Usage: index.partialUpdateObjects(arrayOfObjects[, callback])");
                                for (var n = { requests: [] }, i = 0; i < e.length; ++i) {
                                    var o = { action: !0 === r ? "partialUpdateObject" : "partialUpdateObjectNoCreate", objectID: e[i].objectID, body: e[i] };
                                    n.requests.push(o);
                                }
                                return this.as._jsonRequest({ method: "POST", url: "/1/indexes/" + encodeURIComponent(this.indexName) + "/batch", body: n, hostType: "write", callback: a });
                            }),
                            (a.prototype.saveObject = function (t, e) {
                                return this.as._jsonRequest({ method: "PUT", url: "/1/indexes/" + encodeURIComponent(this.indexName) + "/" + encodeURIComponent(t.objectID), body: t, hostType: "write", callback: e });
                            }),
                            (a.prototype.saveObjects = function (e, r) {
                                if (!t(8)(e)) throw new Error("Usage: index.saveObjects(arrayOfObjects[, callback])");
                                for (var a = { requests: [] }, n = 0; n < e.length; ++n) {
                                    var i = { action: "updateObject", objectID: e[n].objectID, body: e[n] };
                                    a.requests.push(i);
                                }
                                return this.as._jsonRequest({ method: "POST", url: "/1/indexes/" + encodeURIComponent(this.indexName) + "/batch", body: a, hostType: "write", callback: r });
                            }),
                            (a.prototype.deleteObject = function (t, e) {
                                if ("function" == typeof t || ("string" != typeof t && "number" != typeof t)) {
                                    var r = new u.AlgoliaSearchError(t && "function" != typeof t ? "ObjectID must be a string" : "Cannot delete an object without an objectID");
                                    return "function" == typeof (e = t) ? e(r) : this.as._promise.reject(r);
                                }
                                return this.as._jsonRequest({ method: "DELETE", url: "/1/indexes/" + encodeURIComponent(this.indexName) + "/" + encodeURIComponent(t), hostType: "write", callback: e });
                            }),
                            (a.prototype.deleteObjects = function (e, r) {
                                var a = t(8),
                                    n = t(32);
                                if (!a(e)) throw new Error("Usage: index.deleteObjects(arrayOfObjectIDs[, callback])");
                                var i = {
                                    requests: n(e, function (t) {
                                        return { action: "deleteObject", objectID: t, body: { objectID: t } };
                                    }),
                                };
                                return this.as._jsonRequest({ method: "POST", url: "/1/indexes/" + encodeURIComponent(this.indexName) + "/batch", body: i, hostType: "write", callback: r });
                            }),
                            (a.prototype.deleteByQuery = l(function (e, r, a) {
                                function n(t) {
                                    return s.waitTask(t.taskID);
                                }
                                function i() {
                                    return s.deleteByQuery(e, r);
                                }
                                var o = t(26),
                                    l = t(32),
                                    s = this,
                                    u = s.as;
                                1 === arguments.length || "function" == typeof r ? ((a = r), (r = {})) : (r = o(r)), (r.attributesToRetrieve = "objectID"), (r.hitsPerPage = 1e3), (r.distinct = !1), this.clearCache();
                                var h = this.search(e, r).then(function (t) {
                                    if (0 === t.nbHits) return t;
                                    var e = l(t.hits, function (t) {
                                        return t.objectID;
                                    });
                                    return s.deleteObjects(e).then(n).then(i);
                                });
                                return a
                                    ? void h.then(
                                          function () {
                                              c(function () {
                                                  a(null);
                                              }, u._setTimeout || setTimeout);
                                          },
                                          function (t) {
                                              c(function () {
                                                  a(t);
                                              }, u._setTimeout || setTimeout);
                                          }
                                      )
                                    : h;
                            }, s("index.deleteByQuery()", "index.deleteBy()"))),
                            (a.prototype.deleteBy = function (t, e) {
                                return this.as._jsonRequest({ method: "POST", url: "/1/indexes/" + encodeURIComponent(this.indexName) + "/deleteByQuery", body: { params: this.as._getSearchParams(t, "") }, hostType: "write", callback: e });
                            }),
                            (a.prototype.browseAll = function (e, r) {
                                function a(t) {
                                    var e;
                                    o._stopped ||
                                        ((e = void 0 !== t ? { cursor: t } : { params: c }), l._jsonRequest({ method: "POST", url: "/1/indexes/" + encodeURIComponent(s.indexName) + "/browse", hostType: "read", body: e, callback: n }));
                                }
                                function n(t, e) {
                                    if (!o._stopped) return t ? void o._error(t) : (o._result(e), void 0 === e.cursor ? void o._end() : void a(e.cursor));
                                }
                                "object" == typeof e && ((r = e), (e = void 0));
                                var i = t(33),
                                    o = new (t(19))(),
                                    l = this.as,
                                    s = this,
                                    c = l._getSearchParams(i({}, r || {}, { query: e }), "");
                                return a(), o;
                            }),
                            (a.prototype.ttAdapter = l(function (t) {
                                var e = this;
                                return function (r, a, n) {
                                    var i;
                                    (i = "function" == typeof n ? n : a),
                                        e.search(r, t, function (t, e) {
                                            return t ? void i(t) : void i(e.hits);
                                        });
                                };
                            }, "ttAdapter is not necessary anymore and will be removed in the next version,\nhave a look at autocomplete.js (https://github.com/algolia/autocomplete.js)")),
                            (a.prototype.waitTask = function (t, e) {
                                var r = 100,
                                    a = 5e3,
                                    n = 0,
                                    i = this,
                                    o = i.as,
                                    l = (function e() {
                                        return o._jsonRequest({ method: "GET", hostType: "read", url: "/1/indexes/" + encodeURIComponent(i.indexName) + "/task/" + t }).then(function (t) {
                                            var i = r * ++n * n;
                                            return i > a && (i = a), "published" !== t.status ? o._promise.delay(i).then(e) : t;
                                        });
                                    })();
                                return e
                                    ? void l.then(
                                          function (t) {
                                              c(function () {
                                                  e(null, t);
                                              }, o._setTimeout || setTimeout);
                                          },
                                          function (t) {
                                              c(function () {
                                                  e(t);
                                              }, o._setTimeout || setTimeout);
                                          }
                                      )
                                    : l;
                            }),
                            (a.prototype.clearIndex = function (t) {
                                return this.as._jsonRequest({ method: "POST", url: "/1/indexes/" + encodeURIComponent(this.indexName) + "/clear", hostType: "write", callback: t });
                            }),
                            (a.prototype.getSettings = function (t, e) {
                                1 === arguments.length && "function" == typeof t && ((e = t), (t = {})), (t = t || {});
                                var r = encodeURIComponent(this.indexName);
                                return this.as._jsonRequest({ method: "GET", url: "/1/indexes/" + r + "/settings?getVersion=2" + (t.advanced ? "&advanced=" + t.advanced : ""), hostType: "read", callback: e });
                            }),
                            (a.prototype.searchSynonyms = function (t, e) {
                                return (
                                    "function" == typeof t ? ((e = t), (t = {})) : void 0 === t && (t = {}),
                                    this.as._jsonRequest({ method: "POST", url: "/1/indexes/" + encodeURIComponent(this.indexName) + "/synonyms/search", body: t, hostType: "read", callback: e })
                                );
                            }),
                            (a.prototype.exportSynonyms = function (t, e) {
                                return n(this.searchSynonyms.bind(this), t, e);
                            }),
                            (a.prototype.saveSynonym = function (t, e, r) {
                                "function" == typeof e ? ((r = e), (e = {})) : void 0 === e && (e = {}), void 0 !== e.forwardToSlaves && h();
                                var a = e.forwardToSlaves || e.forwardToReplicas ? "true" : "false";
                                return this.as._jsonRequest({
                                    method: "PUT",
                                    url: "/1/indexes/" + encodeURIComponent(this.indexName) + "/synonyms/" + encodeURIComponent(t.objectID) + "?forwardToReplicas=" + a,
                                    body: t,
                                    hostType: "write",
                                    callback: r,
                                });
                            }),
                            (a.prototype.getSynonym = function (t, e) {
                                return this.as._jsonRequest({ method: "GET", url: "/1/indexes/" + encodeURIComponent(this.indexName) + "/synonyms/" + encodeURIComponent(t), hostType: "read", callback: e });
                            }),
                            (a.prototype.deleteSynonym = function (t, e, r) {
                                "function" == typeof e ? ((r = e), (e = {})) : void 0 === e && (e = {}), void 0 !== e.forwardToSlaves && h();
                                var a = e.forwardToSlaves || e.forwardToReplicas ? "true" : "false";
                                return this.as._jsonRequest({ method: "DELETE", url: "/1/indexes/" + encodeURIComponent(this.indexName) + "/synonyms/" + encodeURIComponent(t) + "?forwardToReplicas=" + a, hostType: "write", callback: r });
                            }),
                            (a.prototype.clearSynonyms = function (t, e) {
                                "function" == typeof t ? ((e = t), (t = {})) : void 0 === t && (t = {}), void 0 !== t.forwardToSlaves && h();
                                var r = t.forwardToSlaves || t.forwardToReplicas ? "true" : "false";
                                return this.as._jsonRequest({ method: "POST", url: "/1/indexes/" + encodeURIComponent(this.indexName) + "/synonyms/clear?forwardToReplicas=" + r, hostType: "write", callback: e });
                            }),
                            (a.prototype.batchSynonyms = function (t, e, r) {
                                "function" == typeof e ? ((r = e), (e = {})) : void 0 === e && (e = {}), void 0 !== e.forwardToSlaves && h();
                                var a = e.forwardToSlaves || e.forwardToReplicas ? "true" : "false";
                                return this.as._jsonRequest({
                                    method: "POST",
                                    url: "/1/indexes/" + encodeURIComponent(this.indexName) + "/synonyms/batch?forwardToReplicas=" + a + "&replaceExistingSynonyms=" + (e.replaceExistingSynonyms ? "true" : "false"),
                                    hostType: "write",
                                    body: t,
                                    callback: r,
                                });
                            }),
                            (a.prototype.searchRules = function (t, e) {
                                return (
                                    "function" == typeof t ? ((e = t), (t = {})) : void 0 === t && (t = {}),
                                    this.as._jsonRequest({ method: "POST", url: "/1/indexes/" + encodeURIComponent(this.indexName) + "/rules/search", body: t, hostType: "read", callback: e })
                                );
                            }),
                            (a.prototype.exportRules = function (t, e) {
                                return n(this.searchRules.bind(this), t, e);
                            }),
                            (a.prototype.saveRule = function (t, e, r) {
                                if (("function" == typeof e ? ((r = e), (e = {})) : void 0 === e && (e = {}), !t.objectID)) throw new u.AlgoliaSearchError("Missing or empty objectID field for rule");
                                var a = !0 === e.forwardToReplicas ? "true" : "false";
                                return this.as._jsonRequest({
                                    method: "PUT",
                                    url: "/1/indexes/" + encodeURIComponent(this.indexName) + "/rules/" + encodeURIComponent(t.objectID) + "?forwardToReplicas=" + a,
                                    body: t,
                                    hostType: "write",
                                    callback: r,
                                });
                            }),
                            (a.prototype.getRule = function (t, e) {
                                return this.as._jsonRequest({ method: "GET", url: "/1/indexes/" + encodeURIComponent(this.indexName) + "/rules/" + encodeURIComponent(t), hostType: "read", callback: e });
                            }),
                            (a.prototype.deleteRule = function (t, e, r) {
                                "function" == typeof e ? ((r = e), (e = {})) : void 0 === e && (e = {});
                                var a = !0 === e.forwardToReplicas ? "true" : "false";
                                return this.as._jsonRequest({ method: "DELETE", url: "/1/indexes/" + encodeURIComponent(this.indexName) + "/rules/" + encodeURIComponent(t) + "?forwardToReplicas=" + a, hostType: "write", callback: r });
                            }),
                            (a.prototype.clearRules = function (t, e) {
                                "function" == typeof t ? ((e = t), (t = {})) : void 0 === t && (t = {});
                                var r = !0 === t.forwardToReplicas ? "true" : "false";
                                return this.as._jsonRequest({ method: "POST", url: "/1/indexes/" + encodeURIComponent(this.indexName) + "/rules/clear?forwardToReplicas=" + r, hostType: "write", callback: e });
                            }),
                            (a.prototype.batchRules = function (t, e, r) {
                                "function" == typeof e ? ((r = e), (e = {})) : void 0 === e && (e = {});
                                var a = !0 === e.forwardToReplicas ? "true" : "false";
                                return this.as._jsonRequest({
                                    method: "POST",
                                    url: "/1/indexes/" + encodeURIComponent(this.indexName) + "/rules/batch?forwardToReplicas=" + a + "&clearExistingRules=" + (!0 === e.clearExistingRules ? "true" : "false"),
                                    hostType: "write",
                                    body: t,
                                    callback: r,
                                });
                            }),
                            (a.prototype.setSettings = function (t, e, r) {
                                (1 !== arguments.length && "function" != typeof e) || ((r = e), (e = {})), void 0 !== e.forwardToSlaves && h();
                                var a = e.forwardToSlaves || e.forwardToReplicas ? "true" : "false";
                                return this.as._jsonRequest({ method: "PUT", url: "/1/indexes/" + encodeURIComponent(this.indexName) + "/settings?forwardToReplicas=" + a, hostType: "write", body: t, callback: r });
                            }),
                            (a.prototype.listUserKeys = l(function (t) {
                                return this.listApiKeys(t);
                            }, s("index.listUserKeys()", "client.listApiKeys()"))),
                            (a.prototype.listApiKeys = l(function (t) {
                                return this.as._jsonRequest({ method: "GET", url: "/1/indexes/" + encodeURIComponent(this.indexName) + "/keys", hostType: "read", callback: t });
                            }, s("index.listApiKeys()", "client.listApiKeys()"))),
                            (a.prototype.getUserKeyACL = l(function (t, e) {
                                return this.getApiKey(t, e);
                            }, s("index.getUserKeyACL()", "client.getApiKey()"))),
                            (a.prototype.getApiKey = l(function (t, e) {
                                return this.as._jsonRequest({ method: "GET", url: "/1/indexes/" + encodeURIComponent(this.indexName) + "/keys/" + t, hostType: "read", callback: e });
                            }, s("index.getApiKey()", "client.getApiKey()"))),
                            (a.prototype.deleteUserKey = l(function (t, e) {
                                return this.deleteApiKey(t, e);
                            }, s("index.deleteUserKey()", "client.deleteApiKey()"))),
                            (a.prototype.deleteApiKey = l(function (t, e) {
                                return this.as._jsonRequest({ method: "DELETE", url: "/1/indexes/" + encodeURIComponent(this.indexName) + "/keys/" + t, hostType: "write", callback: e });
                            }, s("index.deleteApiKey()", "client.deleteApiKey()"))),
                            (a.prototype.addUserKey = l(function (t, e, r) {
                                return this.addApiKey(t, e, r);
                            }, s("index.addUserKey()", "client.addApiKey()"))),
                            (a.prototype.addApiKey = l(function (e, r, a) {
                                if (!t(8)(e)) throw new Error("Usage: index.addApiKey(arrayOfAcls[, params, callback])");
                                (1 !== arguments.length && "function" != typeof r) || ((a = r), (r = null));
                                var n = { acl: e };
                                return (
                                    r &&
                                        ((n.validity = r.validity),
                                        (n.maxQueriesPerIPPerHour = r.maxQueriesPerIPPerHour),
                                        (n.maxHitsPerQuery = r.maxHitsPerQuery),
                                        (n.description = r.description),
                                        r.queryParameters && (n.queryParameters = this.as._getSearchParams(r.queryParameters, "")),
                                        (n.referers = r.referers)),
                                    this.as._jsonRequest({ method: "POST", url: "/1/indexes/" + encodeURIComponent(this.indexName) + "/keys", body: n, hostType: "write", callback: a })
                                );
                            }, s("index.addApiKey()", "client.addApiKey()"))),
                            (a.prototype.addUserKeyWithValidity = l(function (t, e, r) {
                                return this.addApiKey(t, e, r);
                            }, s("index.addUserKeyWithValidity()", "client.addApiKey()"))),
                            (a.prototype.updateUserKey = l(function (t, e, r, a) {
                                return this.updateApiKey(t, e, r, a);
                            }, s("index.updateUserKey()", "client.updateApiKey()"))),
                            (a.prototype.updateApiKey = l(function (e, r, a, n) {
                                if (!t(8)(r)) throw new Error("Usage: index.updateApiKey(key, arrayOfAcls[, params, callback])");
                                (2 !== arguments.length && "function" != typeof a) || ((n = a), (a = null));
                                var i = { acl: r };
                                return (
                                    a &&
                                        ((i.validity = a.validity),
                                        (i.maxQueriesPerIPPerHour = a.maxQueriesPerIPPerHour),
                                        (i.maxHitsPerQuery = a.maxHitsPerQuery),
                                        (i.description = a.description),
                                        a.queryParameters && (i.queryParameters = this.as._getSearchParams(a.queryParameters, "")),
                                        (i.referers = a.referers)),
                                    this.as._jsonRequest({ method: "PUT", url: "/1/indexes/" + encodeURIComponent(this.indexName) + "/keys/" + e, body: i, hostType: "write", callback: n })
                                );
                            }, s("index.updateApiKey()", "client.updateApiKey()")));
                    },
                    { 19: 19, 20: 20, 26: 26, 28: 28, 29: 29, 30: 30, 31: 31, 32: 32, 33: 33, 7: 7, 8: 8 },
                ],
                19: [
                    function (t, e, r) {
                        "use strict";
                        function a() {}
                        (e.exports = a),
                            t(7)(a, t(4).EventEmitter),
                            (a.prototype.stop = function () {
                                (this._stopped = !0), this._clean();
                            }),
                            (a.prototype._end = function () {
                                this.emit("end"), this._clean();
                            }),
                            (a.prototype._error = function (t) {
                                this.emit("error", t), this._clean();
                            }),
                            (a.prototype._result = function (t) {
                                this.emit("result", t);
                            }),
                            (a.prototype._clean = function () {
                                this.removeAllListeners("stop"), this.removeAllListeners("end"), this.removeAllListeners("error"), this.removeAllListeners("result");
                            });
                    },
                    { 4: 4, 7: 7 },
                ],
                20: [
                    function (t, e, r) {
                        function a(t, e) {
                            (this.indexName = e), (this.as = t), (this.typeAheadArgs = null), (this.typeAheadValueOption = null), (this.cache = {});
                        }
                        var n = t(25),
                            i = t(28),
                            o = t(29);
                        (e.exports = a),
                            (a.prototype.clearCache = function () {
                                this.cache = {};
                            }),
                            (a.prototype.search = n("query")),
                            (a.prototype.similarSearch = i(n("similarQuery"), o("index.similarSearch(query[, callback])", "index.search({ similarQuery: query }[, callback])"))),
                            (a.prototype.browse = function (e, r, a) {
                                var n,
                                    i,
                                    o = t(33);
                                0 === arguments.length || (1 === arguments.length && "function" == typeof arguments[0])
                                    ? ((n = 0), (a = arguments[0]), (e = void 0))
                                    : "number" == typeof arguments[0]
                                    ? ((n = arguments[0]), "number" == typeof arguments[1] ? (i = arguments[1]) : "function" == typeof arguments[1] && ((a = arguments[1]), (i = void 0)), (e = void 0), (r = void 0))
                                    : "object" == typeof arguments[0]
                                    ? ("function" == typeof arguments[1] && (a = arguments[1]), (r = arguments[0]), (e = void 0))
                                    : "string" == typeof arguments[0] && "function" == typeof arguments[1] && ((a = arguments[1]), (r = void 0)),
                                    (r = o({}, r || {}, { page: n, hitsPerPage: i, query: e }));
                                var l = this.as._getSearchParams(r, "");
                                return this.as._jsonRequest({ method: "POST", url: "/1/indexes/" + encodeURIComponent(this.indexName) + "/browse", body: { params: l }, hostType: "read", callback: a });
                            }),
                            (a.prototype.browseFrom = function (t, e) {
                                return this.as._jsonRequest({ method: "POST", url: "/1/indexes/" + encodeURIComponent(this.indexName) + "/browse", body: { cursor: t }, hostType: "read", callback: e });
                            }),
                            (a.prototype.searchForFacetValues = function (e, r) {
                                var a = t(26),
                                    n = t(34);
                                if (void 0 === e.facetName || void 0 === e.facetQuery) throw new Error("Usage: index.searchForFacetValues({facetName, facetQuery, ...params}[, callback])");
                                var i = e.facetName,
                                    o = n(a(e), function (t) {
                                        return "facetName" === t;
                                    }),
                                    l = this.as._getSearchParams(o, "");
                                return this.as._jsonRequest({ method: "POST", url: "/1/indexes/" + encodeURIComponent(this.indexName) + "/facets/" + encodeURIComponent(i) + "/query", hostType: "read", body: { params: l }, callback: r });
                            }),
                            (a.prototype.searchFacet = i(function (t, e) {
                                return this.searchForFacetValues(t, e);
                            }, o("index.searchFacet(params[, callback])", "index.searchForFacetValues(params[, callback])"))),
                            (a.prototype._search = function (t, e, r, a) {
                                return this.as._jsonRequest({
                                    cache: this.cache,
                                    method: "POST",
                                    url: e || "/1/indexes/" + encodeURIComponent(this.indexName) + "/query",
                                    body: { params: t },
                                    hostType: "read",
                                    fallback: { method: "GET", url: "/1/indexes/" + encodeURIComponent(this.indexName), body: { params: t } },
                                    callback: r,
                                    additionalUA: a,
                                });
                            }),
                            (a.prototype.getObject = function (t, e, r) {
                                (1 !== arguments.length && "function" != typeof e) || ((r = e), (e = void 0));
                                var a = "";
                                if (void 0 !== e) {
                                    a = "?attributes=";
                                    for (var n = 0; n < e.length; ++n) 0 !== n && (a += ","), (a += e[n]);
                                }
                                return this.as._jsonRequest({ method: "GET", url: "/1/indexes/" + encodeURIComponent(this.indexName) + "/" + encodeURIComponent(t) + a, hostType: "read", callback: r });
                            }),
                            (a.prototype.getObjects = function (e, r, a) {
                                var n = t(8),
                                    i = t(32);
                                if (!n(e)) throw new Error("Usage: index.getObjects(arrayOfObjectIDs[, callback])");
                                var o = this;
                                (1 !== arguments.length && "function" != typeof r) || ((a = r), (r = void 0));
                                var l = {
                                    requests: i(e, function (t) {
                                        var e = { indexName: o.indexName, objectID: t };
                                        return r && (e.attributesToRetrieve = r.join(",")), e;
                                    }),
                                };
                                return this.as._jsonRequest({ method: "POST", url: "/1/indexes/*/objects", hostType: "read", body: l, callback: a });
                            }),
                            (a.prototype.as = null),
                            (a.prototype.indexName = null),
                            (a.prototype.typeAheadArgs = null),
                            (a.prototype.typeAheadValueOption = null);
                    },
                    { 25: 25, 26: 26, 28: 28, 29: 29, 32: 32, 33: 33, 34: 34, 8: 8 },
                ],
                21: [
                    function (t, e, r) {
                        "use strict";
                        var a = t(16),
                            n = t(22);
                        e.exports = n(a);
                    },
                    { 16: 16, 22: 22 },
                ],
                22: [
                    function (t, e, r) {
                        (function (r) {
                            "use strict";
                            var a = t(6),
                                n = a.Promise || t(3).Promise;
                            e.exports = function (e, i) {
                                function o(e, r, a) {
                                    return ((a = t(26)(a || {}))._ua = a._ua || o.ua), new l(e, r, a);
                                }
                                function l() {
                                    e.apply(this, arguments);
                                }
                                var s = t(7),
                                    c = t(30),
                                    u = t(23),
                                    h = t(24),
                                    g = t(35);
                                (i = i || ""),
                                    "debug" === r.env.NODE_ENV && t(1).enable("algoliasearch*"),
                                    (o.version = t(37)),
                                    (o.ua = "Algolia for vanilla JavaScript " + i + o.version),
                                    (o.initPlaces = g(o)),
                                    (a.__algolia = { debug: t(1), algoliasearch: o });
                                var f = { hasXMLHttpRequest: "XMLHttpRequest" in a, hasXDomainRequest: "XDomainRequest" in a };
                                return (
                                    f.hasXMLHttpRequest && (f.cors = "withCredentials" in new XMLHttpRequest()),
                                    s(l, e),
                                    (l.prototype._request = function (t, e) {
                                        return new n(function (r, a) {
                                            function n() {
                                                (l = !0), h.abort(), a(new c.RequestTimeout());
                                            }
                                            function i() {
                                                (g = !0), clearTimeout(o), (o = setTimeout(n, e.timeouts.complete));
                                            }
                                            if (f.cors || f.hasXDomainRequest) {
                                                t = u(t, e.headers);
                                                var o,
                                                    l,
                                                    s = e.body,
                                                    h = f.cors ? new XMLHttpRequest() : new XDomainRequest(),
                                                    g = !1;
                                                (o = setTimeout(n, e.timeouts.connect)),
                                                    (h.onprogress = function () {
                                                        g || i();
                                                    }),
                                                    "onreadystatechange" in h &&
                                                        (h.onreadystatechange = function () {
                                                            !g && h.readyState > 1 && i();
                                                        }),
                                                    (h.onload = function () {
                                                        if (!l) {
                                                            var t;
                                                            clearTimeout(o);
                                                            try {
                                                                t = { body: JSON.parse(h.responseText), responseText: h.responseText, statusCode: h.status, headers: (h.getAllResponseHeaders && h.getAllResponseHeaders()) || {} };
                                                            } catch (e) {
                                                                t = new c.UnparsableJSON({ more: h.responseText });
                                                            }
                                                            t instanceof c.UnparsableJSON ? a(t) : r(t);
                                                        }
                                                    }),
                                                    (h.onerror = function (t) {
                                                        l || (clearTimeout(o), a(new c.Network({ more: t })));
                                                    }),
                                                    h instanceof XMLHttpRequest
                                                        ? (h.open(e.method, t, !0),
                                                          e.forceAuthHeaders &&
                                                              (h.setRequestHeader("x-algolia-application-id", e.headers["x-algolia-application-id"]), h.setRequestHeader("x-algolia-api-key", e.headers["x-algolia-api-key"])))
                                                        : h.open(e.method, t),
                                                    f.cors &&
                                                        (s && ("POST" === e.method ? h.setRequestHeader("content-type", "application/x-www-form-urlencoded") : h.setRequestHeader("content-type", "application/json")),
                                                        h.setRequestHeader("accept", "application/json")),
                                                    s ? h.send(s) : h.send();
                                            } else a(new c.Network("CORS not supported"));
                                        });
                                    }),
                                    (l.prototype._request.fallback = function (t, e) {
                                        return (
                                            (t = u(t, e.headers)),
                                            new n(function (r, a) {
                                                h(t, e, function (t, e) {
                                                    return t ? void a(t) : void r(e);
                                                });
                                            })
                                        );
                                    }),
                                    (l.prototype._promise = {
                                        reject: function (t) {
                                            return n.reject(t);
                                        },
                                        resolve: function (t) {
                                            return n.resolve(t);
                                        },
                                        delay: function (t) {
                                            return new n(function (e) {
                                                setTimeout(e, t);
                                            });
                                        },
                                        all: function (t) {
                                            return n.all(t);
                                        },
                                    }),
                                    o
                                );
                            };
                        }.call(this, t(12)));
                    },
                    { 1: 1, 12: 12, 23: 23, 24: 24, 26: 26, 3: 3, 30: 30, 35: 35, 37: 37, 6: 6, 7: 7 },
                ],
                23: [
                    function (t, e, r) {
                        "use strict";
                        e.exports = function (t, e) {
                            return (t += /\?/.test(t) ? "&" : "?") + a(e);
                        };
                        var a = t(14);
                    },
                    { 14: 14 },
                ],
                24: [
                    function (t, e, r) {
                        "use strict";
                        e.exports = function (t, e, r) {
                            function i() {
                                e.debug("JSONP: success"), g || s || ((g = !0), l || (e.debug("JSONP: Fail. Script loaded but did not call the callback"), o(), r(new a.JSONPScriptFail())));
                            }
                            function o() {
                                clearTimeout(f), (u.onload = null), (u.onreadystatechange = null), (u.onerror = null), c.removeChild(u);
                            }
                            if ("GET" === e.method) {
                                e.debug("JSONP: start");
                                var l = !1,
                                    s = !1;
                                n += 1;
                                var c = document.getElementsByTagName("head")[0],
                                    u = document.createElement("script"),
                                    h = "algoliaJSONP_" + n,
                                    g = !1;
                                (window[h] = function (t) {
                                    return (
                                        (function () {
                                            try {
                                                delete window[h], delete window[h + "_loaded"];
                                            } catch (t) {
                                                window[h] = window[h + "_loaded"] = void 0;
                                            }
                                        })(),
                                        s ? void e.debug("JSONP: Late answer, ignoring") : ((l = !0), o(), void r(null, { body: t, responseText: JSON.stringify(t) }))
                                    );
                                }),
                                    (t += "&callback=" + h),
                                    e.jsonBody && e.jsonBody.params && (t += "&" + e.jsonBody.params);
                                var f = setTimeout(function () {
                                    e.debug("JSONP: Script timeout"), (s = !0), o(), r(new a.RequestTimeout());
                                }, e.timeouts.complete);
                                (u.onreadystatechange = function () {
                                    ("loaded" !== this.readyState && "complete" !== this.readyState) || i();
                                }),
                                    (u.onload = i),
                                    (u.onerror = function () {
                                        e.debug("JSONP: Script error"), g || s || (o(), r(new a.JSONPScriptError()));
                                    }),
                                    (u.async = !0),
                                    (u.defer = !0),
                                    (u.src = t),
                                    c.appendChild(u);
                            } else r(new Error("Method " + e.method + " " + t + " is not supported by JSONP."));
                        };
                        var a = t(30),
                            n = 0;
                    },
                    { 30: 30 },
                ],
                25: [
                    function (t, e, r) {
                        e.exports = function (t, e) {
                            return function (r, n, i) {
                                if (("function" == typeof r && "object" == typeof n) || "object" == typeof i) throw new a.AlgoliaSearchError("index.search usage is index.search(query, params, cb)");
                                0 === arguments.length || "function" == typeof r ? ((i = r), (r = "")) : (1 !== arguments.length && "function" != typeof n) || ((i = n), (n = void 0)),
                                    "object" == typeof r && null !== r ? ((n = r), (r = void 0)) : null != r || (r = "");
                                var o,
                                    l = "";
                                return (
                                    void 0 !== r && (l += t + "=" + encodeURIComponent(r)), void 0 !== n && (n.additionalUA && ((o = n.additionalUA), delete n.additionalUA), (l = this.as._getSearchParams(n, l))), this._search(l, e, i, o)
                                );
                            };
                        };
                        var a = t(30);
                    },
                    { 30: 30 },
                ],
                26: [
                    function (t, e, r) {
                        e.exports = function (t) {
                            return JSON.parse(JSON.stringify(t));
                        };
                    },
                    {},
                ],
                27: [
                    function (t, e, r) {
                        e.exports = function (t, e, r) {
                            var n = {};
                            return (
                                ((r = r || {}).hosts = r.hosts || ["analytics.algolia.com", "analytics.algolia.com", "analytics.algolia.com", "analytics.algolia.com"]),
                                (r.protocol = r.protocol || "https:"),
                                (n.as = a(t, e, r)),
                                (n.getABTests = function (t, e) {
                                    var r = r || {},
                                        a = r.offset || 0,
                                        n = r.limit || 10;
                                    return this.as._jsonRequest({ method: "GET", url: "/2/abtests?offset=" + encodeURIComponent(a) + "&limit=" + encodeURIComponent(n), hostType: "read", forceAuthHeaders: !0, callback: e });
                                }),
                                (n.getABTest = function (t, e) {
                                    return this.as._jsonRequest({ method: "GET", url: "/2/abtests/" + encodeURIComponent(t), hostType: "read", forceAuthHeaders: !0, callback: e });
                                }),
                                (n.addABTest = function (t, e) {
                                    return this.as._jsonRequest({ method: "POST", url: "/2/abtests", body: t, hostType: "read", forceAuthHeaders: !0, callback: e });
                                }),
                                (n.stopABTest = function (t, e) {
                                    return this.as._jsonRequest({ method: "POST", url: "/2/abtests/" + encodeURIComponent(t) + "/stop", hostType: "read", forceAuthHeaders: !0, callback: e });
                                }),
                                (n.deleteABTest = function (t, e) {
                                    return this.as._jsonRequest({ method: "DELETE", url: "/2/abtests/" + encodeURIComponent(t), hostType: "write", forceAuthHeaders: !0, callback: e });
                                }),
                                (n.waitTask = function (t, e, r) {
                                    return this.as.initIndex(t).waitTask(e, r);
                                }),
                                n
                            );
                        };
                        var a = t(21);
                    },
                    { 21: 21 },
                ],
                28: [
                    function (t, e, r) {
                        e.exports = function (t, e) {
                            var r = !1;
                            return function () {
                                return r || (console.warn(e), (r = !0)), t.apply(this, arguments);
                            };
                        };
                    },
                    {},
                ],
                29: [
                    function (t, e, r) {
                        e.exports = function (t, e) {
                            var r = t.toLowerCase().replace(/[\.\(\)]/g, "");
                            return "algoliasearch: `" + t + "` was replaced by `" + e + "`. Please see https://github.com/algolia/algoliasearch-client-javascript/wiki/Deprecated#" + r;
                        };
                    },
                    {},
                ],
                30: [
                    function (t, e, r) {
                        "use strict";
                        function a(e, r) {
                            var a = t(5),
                                n = this;
                            "function" == typeof Error.captureStackTrace ? Error.captureStackTrace(this, this.constructor) : (n.stack = new Error().stack || "Cannot get a stacktrace, browser is too old"),
                                (this.name = "AlgoliaSearchError"),
                                (this.message = e || "Unknown error"),
                                r &&
                                    a(r, function (t, e) {
                                        n[e] = t;
                                    });
                        }
                        function n(t, e) {
                            function r() {
                                var r = Array.prototype.slice.call(arguments, 0);
                                "string" != typeof r[0] && r.unshift(e), a.apply(this, r), (this.name = "AlgoliaSearch" + t + "Error");
                            }
                            return i(r, a), r;
                        }
                        var i = t(7);
                        i(a, Error),
                            (e.exports = {
                                AlgoliaSearchError: a,
                                UnparsableJSON: n("UnparsableJSON", "Could not parse the incoming response as JSON, see err.more for details"),
                                RequestTimeout: n("RequestTimeout", "Request timedout before getting a response"),
                                Network: n("Network", "Network issue, see err.more for details"),
                                JSONPScriptFail: n("JSONPScriptFail", "<script> was loaded but did not call our provided callback"),
                                JSONPScriptError: n("JSONPScriptError", "<script> unable to load due to an `error` event on it"),
                                Unknown: n("Unknown", "Unknown error occured"),
                            });
                    },
                    { 5: 5, 7: 7 },
                ],
                31: [
                    function (t, e, r) {
                        e.exports = function (t, e) {
                            e(t, 0);
                        };
                    },
                    {},
                ],
                32: [
                    function (t, e, r) {
                        var a = t(5);
                        e.exports = function (t, e) {
                            var r = [];
                            return (
                                a(t, function (a, n) {
                                    r.push(e(a, n, t));
                                }),
                                r
                            );
                        };
                    },
                    { 5: 5 },
                ],
                33: [
                    function (t, e, r) {
                        var a = t(5);
                        e.exports = function t(e) {
                            var r = Array.prototype.slice.call(arguments);
                            return (
                                a(r, function (r) {
                                    for (var a in r) r.hasOwnProperty(a) && ("object" == typeof e[a] && "object" == typeof r[a] ? (e[a] = t({}, e[a], r[a])) : void 0 !== r[a] && (e[a] = r[a]));
                                }),
                                e
                            );
                        };
                    },
                    { 5: 5 },
                ],
                34: [
                    function (t, e, r) {
                        e.exports = function (e, r) {
                            var a = t(10),
                                n = t(5),
                                i = {};
                            return (
                                n(a(e), function (t) {
                                    !0 !== r(t) && (i[t] = e[t]);
                                }),
                                i
                            );
                        };
                    },
                    { 10: 10, 5: 5 },
                ],
                35: [
                    function (t, e, r) {
                        e.exports = function (e) {
                            return function (r, i, o) {
                                var l = t(26);
                                ((o = (o && l(o)) || {}).hosts = o.hosts || ["places-dsn.algolia.net", "places-1.algolianet.com", "places-2.algolianet.com", "places-3.algolianet.com"]),
                                    (0 !== arguments.length && "object" != typeof r && void 0 !== r) || ((r = ""), (i = ""), (o._allowEmptyCredentials = !0));
                                var s = e(r, i, o).initIndex("places");
                                return (
                                    (s.search = n("query", "/1/places/query")),
                                    (s.reverse = function (t, e) {
                                        var r = a.encode(t);
                                        return this.as._jsonRequest({ method: "GET", url: "/1/places/reverse?" + r, hostType: "read", callback: e });
                                    }),
                                    (s.getObject = function (t, e) {
                                        return this.as._jsonRequest({ method: "GET", url: "/1/places/" + encodeURIComponent(t), hostType: "read", callback: e });
                                    }),
                                    s
                                );
                            };
                        };
                        var a = t(15),
                            n = t(25);
                    },
                    { 15: 15, 25: 25, 26: 26 },
                ],
                36: [
                    function (t, e, r) {
                        (function (r) {
                            function a(t, e) {
                                return (
                                    l("localStorage failed with", e),
                                    (function () {
                                        try {
                                            r.localStorage.removeItem(s);
                                        } catch (t) {}
                                    })(),
                                    (o = c).get(t)
                                );
                            }
                            function n(t, e) {
                                return 1 === arguments.length ? o.get(t) : o.set(t, e);
                            }
                            function i() {
                                try {
                                    return "localStorage" in r && null !== r.localStorage && (r.localStorage[s] || r.localStorage.setItem(s, JSON.stringify({})), !0);
                                } catch (t) {
                                    return !1;
                                }
                            }
                            var o,
                                l = t(1)("algoliasearch:src/hostIndexState.js"),
                                s = "algoliasearch-client-js",
                                c = {
                                    state: {},
                                    set: function (t, e) {
                                        return (this.state[t] = e), this.state[t];
                                    },
                                    get: function (t) {
                                        return this.state[t] || null;
                                    },
                                },
                                u = {
                                    set: function (t, e) {
                                        c.set(t, e);
                                        try {
                                            var n = JSON.parse(r.localStorage[s]);
                                            return (n[t] = e), (r.localStorage[s] = JSON.stringify(n)), n[t];
                                        } catch (e) {
                                            return a(t, e);
                                        }
                                    },
                                    get: function (t) {
                                        try {
                                            return JSON.parse(r.localStorage[s])[t] || null;
                                        } catch (e) {
                                            return a(t, e);
                                        }
                                    },
                                };
                            (o = i() ? u : c), (e.exports = { get: n, set: n, supportsLocalStorage: i });
                        }.call(this, "undefined" != typeof global ? global : "undefined" != typeof self ? self : "undefined" != typeof window ? window : {}));
                    },
                    { 1: 1 },
                ],
                37: [
                    function (t, e, r) {
                        "use strict";
                        e.exports = "3.32.0";
                    },
                    {},
                ],
            },
            {},
            [21]
        )(21);
    }),
    (function (t) {
        function e(a) {
            if (r[a]) return r[a].exports;
            var n = (r[a] = { exports: {}, id: a, loaded: !1 });
            return t[a].call(n.exports, n, n.exports, e), (n.loaded = !0), n.exports;
        }
        var r = {};
        (e.m = t), (e.c = r), (e.p = ""), e(0);
    })([
        function (t, e, r) {
            "use strict";
            t.exports = r(1);
        },
        function (t, e, r) {
            "use strict";
            var a = r(2),
                n = r(3);
            a.element = n;
            var i = r(4);
            (i.isArray = n.isArray),
                (i.isFunction = n.isFunction),
                (i.isObject = n.isPlainObject),
                (i.bind = n.proxy),
                (i.each = function (t, e) {
                    n.each(t, function (t, r) {
                        return e(r, t);
                    });
                }),
                (i.map = n.map),
                (i.mixin = n.extend),
                (i.Event = n.Event);
            var o,
                l,
                s,
                c = r(5),
                u = r(6);
            (o = n.fn.autocomplete),
                (l = "aaAutocomplete"),
                (s = {
                    initialize: function (t, e) {
                        return (
                            (e = i.isArray(e) ? e : [].slice.call(arguments, 1)),
                            (t = t || {}),
                            this.each(function () {
                                var r,
                                    a = n(this),
                                    i = new u({ el: a });
                                (r = new c({
                                    input: a,
                                    eventBus: i,
                                    dropdownMenuContainer: t.dropdownMenuContainer,
                                    hint: void 0 === t.hint || !!t.hint,
                                    minLength: t.minLength,
                                    autoselect: t.autoselect,
                                    autoselectOnBlur: t.autoselectOnBlur,
                                    tabAutocomplete: t.tabAutocomplete,
                                    openOnFocus: t.openOnFocus,
                                    templates: t.templates,
                                    debug: t.debug,
                                    clearOnSelected: t.clearOnSelected,
                                    cssClasses: t.cssClasses,
                                    datasets: e,
                                    keyboardShortcuts: t.keyboardShortcuts,
                                    appendTo: t.appendTo,
                                    autoWidth: t.autoWidth,
                                })),
                                    a.data(l, r);
                            })
                        );
                    },
                    open: function () {
                        return this.each(function () {
                            var t;
                            (t = n(this).data(l)) && t.open();
                        });
                    },
                    close: function () {
                        return this.each(function () {
                            var t;
                            (t = n(this).data(l)) && t.close();
                        });
                    },
                    val: function (t) {
                        return arguments.length
                            ? this.each(function () {
                                  var e;
                                  (e = n(this).data(l)) && e.setVal(t);
                              })
                            : (function (t) {
                                  var e, r;
                                  return (e = t.data(l)) && (r = e.getVal()), r;
                              })(this.first());
                    },
                    destroy: function () {
                        return this.each(function () {
                            var t,
                                e = n(this);
                            (t = e.data(l)) && (t.destroy(), e.removeData(l));
                        });
                    },
                }),
                (n.fn.autocomplete = function (t) {
                    var e;
                    return s[t] && "initialize" !== t
                        ? ((e = this.filter(function () {
                              return !!n(this).data(l);
                          })),
                          s[t].apply(e, [].slice.call(arguments, 1)))
                        : s.initialize.apply(this, arguments);
                }),
                (n.fn.autocomplete.noConflict = function () {
                    return (n.fn.autocomplete = o), this;
                }),
                (n.fn.autocomplete.sources = c.sources),
                (n.fn.autocomplete.escapeHighlightedString = i.escapeHighlightedString),
                (t.exports = n.fn.autocomplete);
        },
        function (t, e) {
            "use strict";
            t.exports = { element: null };
        },
        function (t, e) {
            t.exports = jQuery;
        },
        function (t, e, r) {
            "use strict";
            function a(t) {
                return t.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&");
            }
            var n = r(2);
            t.exports = {
                isArray: null,
                isFunction: null,
                isObject: null,
                bind: null,
                each: null,
                map: null,
                mixin: null,
                isMsie: function (t) {
                    if ((void 0 === t && (t = navigator.userAgent), /(msie|trident)/i.test(t))) {
                        var e = t.match(/(msie |rv:)(\d+(.\d+)?)/i);
                        if (e) return e[2];
                    }
                    return !1;
                },
                escapeRegExChars: function (t) {
                    return t.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&");
                },
                isNumber: function (t) {
                    return "number" == typeof t;
                },
                toStr: function (t) {
                    return null == t ? "" : t + "";
                },
                cloneDeep: function (t) {
                    var e = this.mixin({}, t),
                        r = this;
                    return (
                        this.each(e, function (t, a) {
                            t && (r.isArray(t) ? (e[a] = [].concat(t)) : r.isObject(t) && (e[a] = r.cloneDeep(t)));
                        }),
                        e
                    );
                },
                error: function (t) {
                    throw new Error(t);
                },
                every: function (t, e) {
                    var r = !0;
                    return t
                        ? (this.each(t, function (a, n) {
                              r && (r = e.call(null, a, n, t) && r);
                          }),
                          !!r)
                        : r;
                },
                any: function (t, e) {
                    var r = !1;
                    return t
                        ? (this.each(t, function (a, n) {
                              if (e.call(null, a, n, t)) return (r = !0), !1;
                          }),
                          r)
                        : r;
                },
                getUniqueId: (function () {
                    var t = 0;
                    return function () {
                        return t++;
                    };
                })(),
                templatify: function (t) {
                    if (this.isFunction(t)) return t;
                    var e = n.element(t);
                    return "SCRIPT" === e.prop("tagName")
                        ? function () {
                              return e.text();
                          }
                        : function () {
                              return String(t);
                          };
                },
                defer: function (t) {
                    setTimeout(t, 0);
                },
                noop: function () {},
                formatPrefix: function (t, e) {
                    return e ? "" : t + "-";
                },
                className: function (t, e, r) {
                    return (r ? "" : ".") + t + e;
                },
                escapeHighlightedString: function (t, e, r) {
                    e = e || "<em>";
                    var n = document.createElement("div");
                    n.appendChild(document.createTextNode(e)), (r = r || "</em>");
                    var i = document.createElement("div");
                    i.appendChild(document.createTextNode(r));
                    var o = document.createElement("div");
                    return o.appendChild(document.createTextNode(t)), o.innerHTML.replace(RegExp(a(n.innerHTML), "g"), e).replace(RegExp(a(i.innerHTML), "g"), r);
                },
            };
        },
        function (t, e, r) {
            "use strict";
            function a(t) {
                var e, r;
                if (
                    ((t = t || {}).input || o.error("missing input"),
                    (this.isActivated = !1),
                    (this.debug = !!t.debug),
                    (this.autoselect = !!t.autoselect),
                    (this.autoselectOnBlur = !!t.autoselectOnBlur),
                    (this.openOnFocus = !!t.openOnFocus),
                    (this.minLength = o.isNumber(t.minLength) ? t.minLength : 1),
                    (this.autoWidth = void 0 === t.autoWidth || !!t.autoWidth),
                    (this.clearOnSelected = !!t.clearOnSelected),
                    (this.tabAutocomplete = void 0 === t.tabAutocomplete || !!t.tabAutocomplete),
                    (t.hint = !!t.hint),
                    t.hint && t.appendTo)
                )
                    throw new Error("[autocomplete.js] hint and appendTo options can't be used at the same time");
                (this.css = t.css = o.mixin({}, g, t.appendTo ? g.appendTo : {})),
                    (this.cssClasses = t.cssClasses = o.mixin({}, g.defaultClasses, t.cssClasses || {})),
                    (this.cssClasses.prefix = t.cssClasses.formattedPrefix = o.formatPrefix(this.cssClasses.prefix, this.cssClasses.noPrefix)),
                    (this.listboxId = t.listboxId = [this.cssClasses.root, "listbox", o.getUniqueId()].join("-"));
                var i = n(t);
                this.$node = i.wrapper;
                var c = (this.$input = i.input);
                (e = i.menu),
                    (r = i.hint),
                    t.dropdownMenuContainer && l.element(t.dropdownMenuContainer).css("position", "relative").append(e.css("top", "0")),
                    c.on("blur.aa", function (t) {
                        var r = document.activeElement;
                        o.isMsie() &&
                            (e[0] === r || e[0].contains(r)) &&
                            (t.preventDefault(),
                            t.stopImmediatePropagation(),
                            o.defer(function () {
                                c.focus();
                            }));
                    }),
                    e.on("mousedown.aa", function (t) {
                        t.preventDefault();
                    }),
                    (this.eventBus = t.eventBus || new s({ el: c })),
                    (this.dropdown = new a.Dropdown({ appendTo: t.appendTo, wrapper: this.$node, menu: e, datasets: t.datasets, templates: t.templates, cssClasses: t.cssClasses, minLength: this.minLength })
                        .onSync("suggestionClicked", this._onSuggestionClicked, this)
                        .onSync("cursorMoved", this._onCursorMoved, this)
                        .onSync("cursorRemoved", this._onCursorRemoved, this)
                        .onSync("opened", this._onOpened, this)
                        .onSync("closed", this._onClosed, this)
                        .onSync("shown", this._onShown, this)
                        .onSync("empty", this._onEmpty, this)
                        .onSync("redrawn", this._onRedrawn, this)
                        .onAsync("datasetRendered", this._onDatasetRendered, this)),
                    (this.input = new a.Input({ input: c, hint: r })
                        .onSync("focused", this._onFocused, this)
                        .onSync("blurred", this._onBlurred, this)
                        .onSync("enterKeyed", this._onEnterKeyed, this)
                        .onSync("tabKeyed", this._onTabKeyed, this)
                        .onSync("escKeyed", this._onEscKeyed, this)
                        .onSync("upKeyed", this._onUpKeyed, this)
                        .onSync("downKeyed", this._onDownKeyed, this)
                        .onSync("leftKeyed", this._onLeftKeyed, this)
                        .onSync("rightKeyed", this._onRightKeyed, this)
                        .onSync("queryChanged", this._onQueryChanged, this)
                        .onSync("whitespaceChanged", this._onWhitespaceChanged, this)),
                    this._bindKeyboardShortcuts(t),
                    this._setLanguageDirection();
            }
            function n(t) {
                var e, r, a, n;
                (e = l.element(t.input)),
                    (r = l.element(h.wrapper.replace("%ROOT%", t.cssClasses.root)).css(t.css.wrapper)),
                    t.appendTo || "block" !== e.css("display") || "table" !== e.parent().css("display") || r.css("display", "table-cell");
                var s = h.dropdown.replace("%PREFIX%", t.cssClasses.prefix).replace("%DROPDOWN_MENU%", t.cssClasses.dropdownMenu);
                (a = l.element(s).css(t.css.dropdown).attr({ role: "listbox", id: t.listboxId })),
                    t.templates && t.templates.dropdownMenu && a.html(o.templatify(t.templates.dropdownMenu)()),
                    (n = e
                        .clone()
                        .css(t.css.hint)
                        .css(
                            (function (t) {
                                return {
                                    backgroundAttachment: t.css("background-attachment"),
                                    backgroundClip: t.css("background-clip"),
                                    backgroundColor: t.css("background-color"),
                                    backgroundImage: t.css("background-image"),
                                    backgroundOrigin: t.css("background-origin"),
                                    backgroundPosition: t.css("background-position"),
                                    backgroundRepeat: t.css("background-repeat"),
                                    backgroundSize: t.css("background-size"),
                                };
                            })(e)
                        ))
                        .val("")
                        .addClass(o.className(t.cssClasses.prefix, t.cssClasses.hint, !0))
                        .removeAttr("id name placeholder required")
                        .prop("readonly", !0)
                        .attr({ "aria-hidden": "true", autocomplete: "off", spellcheck: "false", tabindex: -1 }),
                    n.removeData && n.removeData(),
                    e.data(i, {
                        "aria-autocomplete": e.attr("aria-autocomplete"),
                        "aria-expanded": e.attr("aria-expanded"),
                        "aria-owns": e.attr("aria-owns"),
                        autocomplete: e.attr("autocomplete"),
                        dir: e.attr("dir"),
                        role: e.attr("role"),
                        spellcheck: e.attr("spellcheck"),
                        style: e.attr("style"),
                        type: e.attr("type"),
                    }),
                    e
                        .addClass(o.className(t.cssClasses.prefix, t.cssClasses.input, !0))
                        .attr({
                            autocomplete: "off",
                            spellcheck: !1,
                            role: "combobox",
                            "aria-autocomplete": t.datasets && t.datasets[0] && t.datasets[0].displayKey ? "both" : "list",
                            "aria-expanded": "false",
                            "aria-label": t.ariaLabel,
                            "aria-owns": t.listboxId,
                        })
                        .css(t.hint ? t.css.input : t.css.inputWithNoHint);
                try {
                    e.attr("dir") || e.attr("dir", "auto");
                } catch (t) {}
                return (r = t.appendTo ? r.appendTo(l.element(t.appendTo).eq(0)).eq(0) : e.wrap(r).parent()).prepend(t.hint ? n : null).append(a), { wrapper: r, input: e, hint: n, menu: a };
            }
            var i = "aaAttrs",
                o = r(4),
                l = r(2),
                s = r(6),
                c = r(7),
                u = r(16),
                h = r(18),
                g = r(19);
            o.mixin(a.prototype, {
                _bindKeyboardShortcuts: function (t) {
                    if (t.keyboardShortcuts) {
                        var e = this.$input,
                            r = [];
                        o.each(t.keyboardShortcuts, function (t) {
                            "string" == typeof t && (t = t.toUpperCase().charCodeAt(0)), r.push(t);
                        }),
                            l.element(document).keydown(function (t) {
                                var a = t.target || t.srcElement,
                                    n = a.tagName;
                                if (!a.isContentEditable && "INPUT" !== n && "SELECT" !== n && "TEXTAREA" !== n) {
                                    var i = t.which || t.keyCode;
                                    -1 !== r.indexOf(i) && (e.focus(), t.stopPropagation(), t.preventDefault());
                                }
                            });
                    }
                },
                _onSuggestionClicked: function (t, e) {
                    var r;
                    (r = this.dropdown.getDatumForSuggestion(e)) && this._select(r, { selectionMethod: "click" });
                },
                _onCursorMoved: function (t, e) {
                    var r = this.dropdown.getDatumForCursor(),
                        a = this.dropdown.getCurrentCursor().attr("id");
                    this.input.setActiveDescendant(a), r && (e && this.input.setInputValue(r.value, !0), this.eventBus.trigger("cursorchanged", r.raw, r.datasetName));
                },
                _onCursorRemoved: function () {
                    this.input.resetInputValue(), this._updateHint(), this.eventBus.trigger("cursorremoved");
                },
                _onDatasetRendered: function () {
                    this._updateHint(), this.eventBus.trigger("updated");
                },
                _onOpened: function () {
                    this._updateHint(), this.input.expand(), this.eventBus.trigger("opened");
                },
                _onEmpty: function () {
                    this.eventBus.trigger("empty");
                },
                _onRedrawn: function () {
                    this.$node.css("top", "0px"), this.$node.css("left", "0px");
                    var t = this.$input[0].getBoundingClientRect();
                    this.autoWidth && this.$node.css("width", t.width + "px");
                    var e = this.$node[0].getBoundingClientRect(),
                        r = t.bottom - e.top;
                    this.$node.css("top", r + "px");
                    var a = t.left - e.left;
                    this.$node.css("left", a + "px"), this.eventBus.trigger("redrawn");
                },
                _onShown: function () {
                    this.eventBus.trigger("shown"), this.autoselect && this.dropdown.cursorTopSuggestion();
                },
                _onClosed: function () {
                    this.input.clearHint(), this.input.removeActiveDescendant(), this.input.collapse(), this.eventBus.trigger("closed");
                },
                _onFocused: function () {
                    if (((this.isActivated = !0), this.openOnFocus)) {
                        var t = this.input.getQuery();
                        t.length >= this.minLength ? this.dropdown.update(t) : this.dropdown.empty(), this.dropdown.open();
                    }
                },
                _onBlurred: function () {
                    var t, e;
                    (t = this.dropdown.getDatumForCursor()), (e = this.dropdown.getDatumForTopSuggestion());
                    var r = { selectionMethod: "blur" };
                    this.debug || (this.autoselectOnBlur && t ? this._select(t, r) : this.autoselectOnBlur && e ? this._select(e, r) : ((this.isActivated = !1), this.dropdown.empty(), this.dropdown.close()));
                },
                _onEnterKeyed: function (t, e) {
                    var r, a;
                    (r = this.dropdown.getDatumForCursor()), (a = this.dropdown.getDatumForTopSuggestion());
                    var n = { selectionMethod: "enterKey" };
                    r ? (this._select(r, n), e.preventDefault()) : this.autoselect && a && (this._select(a, n), e.preventDefault());
                },
                _onTabKeyed: function (t, e) {
                    if (this.tabAutocomplete) {
                        var r;
                        (r = this.dropdown.getDatumForCursor()) ? (this._select(r, { selectionMethod: "tabKey" }), e.preventDefault()) : this._autocomplete(!0);
                    } else this.dropdown.close();
                },
                _onEscKeyed: function () {
                    this.dropdown.close(), this.input.resetInputValue();
                },
                _onUpKeyed: function () {
                    var t = this.input.getQuery();
                    this.dropdown.isEmpty && t.length >= this.minLength ? this.dropdown.update(t) : this.dropdown.moveCursorUp(), this.dropdown.open();
                },
                _onDownKeyed: function () {
                    var t = this.input.getQuery();
                    this.dropdown.isEmpty && t.length >= this.minLength ? this.dropdown.update(t) : this.dropdown.moveCursorDown(), this.dropdown.open();
                },
                _onLeftKeyed: function () {
                    "rtl" === this.dir && this._autocomplete();
                },
                _onRightKeyed: function () {
                    "ltr" === this.dir && this._autocomplete();
                },
                _onQueryChanged: function (t, e) {
                    this.input.clearHintIfInvalid(), e.length >= this.minLength ? this.dropdown.update(e) : this.dropdown.empty(), this.dropdown.open(), this._setLanguageDirection();
                },
                _onWhitespaceChanged: function () {
                    this._updateHint(), this.dropdown.open();
                },
                _setLanguageDirection: function () {
                    var t = this.input.getLanguageDirection();
                    this.dir !== t && ((this.dir = t), this.$node.css("direction", t), this.dropdown.setLanguageDirection(t));
                },
                _updateHint: function () {
                    var t, e, r, a, n;
                    (t = this.dropdown.getDatumForTopSuggestion()) && this.dropdown.isVisible() && !this.input.hasOverflow()
                        ? ((e = this.input.getInputValue()), (r = c.normalizeQuery(e)), (a = o.escapeRegExChars(r)), (n = new RegExp("^(?:" + a + ")(.+$)", "i").exec(t.value)) ? this.input.setHint(e + n[1]) : this.input.clearHint())
                        : this.input.clearHint();
                },
                _autocomplete: function (t) {
                    var e, r, a, n;
                    (e = this.input.getHint()),
                        (r = this.input.getQuery()),
                        (a = t || this.input.isCursorAtEnd()),
                        e && r !== e && a && ((n = this.dropdown.getDatumForTopSuggestion()) && this.input.setInputValue(n.value), this.eventBus.trigger("autocompleted", n.raw, n.datasetName));
                },
                _select: function (t, e) {
                    void 0 !== t.value && this.input.setQuery(t.value),
                        this.clearOnSelected ? this.setVal("") : this.input.setInputValue(t.value, !0),
                        this._setLanguageDirection(),
                        !1 === this.eventBus.trigger("selected", t.raw, t.datasetName, e).isDefaultPrevented() && (this.dropdown.close(), o.defer(o.bind(this.dropdown.empty, this.dropdown)));
                },
                open: function () {
                    if (!this.isActivated) {
                        var t = this.input.getInputValue();
                        t.length >= this.minLength ? this.dropdown.update(t) : this.dropdown.empty();
                    }
                    this.dropdown.open();
                },
                close: function () {
                    this.dropdown.close();
                },
                setVal: function (t) {
                    (t = o.toStr(t)), this.isActivated ? this.input.setInputValue(t) : (this.input.setQuery(t), this.input.setInputValue(t, !0)), this._setLanguageDirection();
                },
                getVal: function () {
                    return this.input.getQuery();
                },
                destroy: function () {
                    var t, e, r;
                    this.input.destroy(),
                        this.dropdown.destroy(),
                        (t = this.$node),
                        (e = this.cssClasses),
                        (r = t.find(o.className(e.prefix, e.input))),
                        o.each(r.data(i), function (t, e) {
                            void 0 === t ? r.removeAttr(e) : r.attr(e, t);
                        }),
                        r.detach().removeClass(o.className(e.prefix, e.input, !0)).insertAfter(t),
                        r.removeData && r.removeData(i),
                        t.remove(),
                        (this.$node = null);
                },
                getWrapper: function () {
                    return this.dropdown.$container[0];
                },
            }),
                (a.Dropdown = u),
                (a.Input = c),
                (a.sources = r(20)),
                (t.exports = a);
        },
        function (t, e, r) {
            "use strict";
            function a(t) {
                (t && t.el) || n.error("EventBus initialized without el"), (this.$el = i.element(t.el));
            }
            var n = r(4),
                i = r(2);
            n.mixin(a.prototype, {
                trigger: function (t, e, r, a) {
                    var i = n.Event("autocomplete:" + t);
                    return this.$el.trigger(i, [e, r, a]), i;
                },
            }),
                (t.exports = a);
        },
        function (t, e, r) {
            "use strict";
            function a(t) {
                var e,
                    r,
                    a,
                    n,
                    s = this;
                (t = t || {}).input || o.error("input is missing"),
                    (e = o.bind(this._onBlur, this)),
                    (r = o.bind(this._onFocus, this)),
                    (a = o.bind(this._onKeydown, this)),
                    (n = o.bind(this._onInput, this)),
                    (this.$hint = l.element(t.hint)),
                    (this.$input = l.element(t.input).on("blur.aa", e).on("focus.aa", r).on("keydown.aa", a)),
                    0 === this.$hint.length && (this.setHint = this.getHint = this.clearHint = this.clearHintIfInvalid = o.noop),
                    o.isMsie()
                        ? this.$input.on("keydown.aa keypress.aa cut.aa paste.aa", function (t) {
                              i[t.which || t.keyCode] || o.defer(o.bind(s._onInput, s, t));
                          })
                        : this.$input.on("input.aa", n),
                    (this.query = this.$input.val()),
                    (this.$overflowHelper = (function (t) {
                        return l
                            .element('<pre aria-hidden="true"></pre>')
                            .css({
                                position: "absolute",
                                visibility: "hidden",
                                whiteSpace: "pre",
                                fontFamily: t.css("font-family"),
                                fontSize: t.css("font-size"),
                                fontStyle: t.css("font-style"),
                                fontVariant: t.css("font-variant"),
                                fontWeight: t.css("font-weight"),
                                wordSpacing: t.css("word-spacing"),
                                letterSpacing: t.css("letter-spacing"),
                                textIndent: t.css("text-indent"),
                                textRendering: t.css("text-rendering"),
                                textTransform: t.css("text-transform"),
                            })
                            .insertAfter(t);
                    })(this.$input));
            }
            function n(t) {
                return t.altKey || t.ctrlKey || t.metaKey || t.shiftKey;
            }
            var i;
            i = { 9: "tab", 27: "esc", 37: "left", 39: "right", 13: "enter", 38: "up", 40: "down" };
            var o = r(4),
                l = r(2),
                s = r(8);
            (a.normalizeQuery = function (t) {
                return (t || "").replace(/^\s*/g, "").replace(/\s{2,}/g, " ");
            }),
                o.mixin(a.prototype, s, {
                    _onBlur: function () {
                        this.resetInputValue(), this.$input.removeAttr("aria-activedescendant"), this.trigger("blurred");
                    },
                    _onFocus: function () {
                        this.trigger("focused");
                    },
                    _onKeydown: function (t) {
                        var e = i[t.which || t.keyCode];
                        this._managePreventDefault(e, t), e && this._shouldTrigger(e, t) && this.trigger(e + "Keyed", t);
                    },
                    _onInput: function () {
                        this._checkInputValue();
                    },
                    _managePreventDefault: function (t, e) {
                        var r, a, i;
                        switch (t) {
                            case "tab":
                                (a = this.getHint()), (i = this.getInputValue()), (r = a && a !== i && !n(e));
                                break;
                            case "up":
                            case "down":
                                r = !n(e);
                                break;
                            default:
                                r = !1;
                        }
                        r && e.preventDefault();
                    },
                    _shouldTrigger: function (t, e) {
                        var r;
                        switch (t) {
                            case "tab":
                                r = !n(e);
                                break;
                            default:
                                r = !0;
                        }
                        return r;
                    },
                    _checkInputValue: function () {
                        var t, e, r;
                        (r =
                            !(
                                !(e = (function (t, e) {
                                    return a.normalizeQuery(t) === a.normalizeQuery(e);
                                })((t = this.getInputValue()), this.query)) || !this.query
                            ) && this.query.length !== t.length),
                            (this.query = t),
                            e ? r && this.trigger("whitespaceChanged", this.query) : this.trigger("queryChanged", this.query);
                    },
                    focus: function () {
                        this.$input.focus();
                    },
                    blur: function () {
                        this.$input.blur();
                    },
                    getQuery: function () {
                        return this.query;
                    },
                    setQuery: function (t) {
                        this.query = t;
                    },
                    getInputValue: function () {
                        return this.$input.val();
                    },
                    setInputValue: function (t, e) {
                        void 0 === t && (t = this.query), this.$input.val(t), e ? this.clearHint() : this._checkInputValue();
                    },
                    expand: function () {
                        this.$input.attr("aria-expanded", "true");
                    },
                    collapse: function () {
                        this.$input.attr("aria-expanded", "false");
                    },
                    setActiveDescendant: function (t) {
                        this.$input.attr("aria-activedescendant", t);
                    },
                    removeActiveDescendant: function () {
                        this.$input.removeAttr("aria-activedescendant");
                    },
                    resetInputValue: function () {
                        this.setInputValue(this.query, !0);
                    },
                    getHint: function () {
                        return this.$hint.val();
                    },
                    setHint: function (t) {
                        this.$hint.val(t);
                    },
                    clearHint: function () {
                        this.setHint("");
                    },
                    clearHintIfInvalid: function () {
                        var t, e, r;
                        (r = (t = this.getInputValue()) !== (e = this.getHint()) && 0 === e.indexOf(t)), ("" !== t && r && !this.hasOverflow()) || this.clearHint();
                    },
                    getLanguageDirection: function () {
                        return (this.$input.css("direction") || "ltr").toLowerCase();
                    },
                    hasOverflow: function () {
                        var t = this.$input.width() - 2;
                        return this.$overflowHelper.text(this.getInputValue()), this.$overflowHelper.width() >= t;
                    },
                    isCursorAtEnd: function () {
                        var t, e, r;
                        return (t = this.$input.val().length), (e = this.$input[0].selectionStart), o.isNumber(e) ? e === t : !document.selection || ((r = document.selection.createRange()).moveStart("character", -t), t === r.text.length);
                    },
                    destroy: function () {
                        this.$hint.off(".aa"), this.$input.off(".aa"), (this.$hint = this.$input = this.$overflowHelper = null);
                    },
                }),
                (t.exports = a);
        },
        function (t, e, r) {
            "use strict";
            function a(t, e, r, a) {
                var n;
                if (!r) return this;
                for (
                    e = e.split(o),
                        r = a
                            ? (function (t, e) {
                                  return t.bind
                                      ? t.bind(e)
                                      : function () {
                                            t.apply(e, [].slice.call(arguments, 0));
                                        };
                              })(r, a)
                            : r,
                        this._callbacks = this._callbacks || {};
                    (n = e.shift());

                )
                    (this._callbacks[n] = this._callbacks[n] || { sync: [], async: [] }), this._callbacks[n][t].push(r);
                return this;
            }
            function n(t, e, r) {
                return function () {
                    for (var a, n = 0, i = t.length; !a && n < i; n += 1) a = !1 === t[n].apply(e, r);
                    return !a;
                };
            }
            var i = r(9),
                o = /\s+/;
            t.exports = {
                onSync: function (t, e, r) {
                    return a.call(this, "sync", t, e, r);
                },
                onAsync: function (t, e, r) {
                    return a.call(this, "async", t, e, r);
                },
                off: function (t) {
                    var e;
                    if (!this._callbacks) return this;
                    for (t = t.split(o); (e = t.shift()); ) delete this._callbacks[e];
                    return this;
                },
                trigger: function (t) {
                    var e, r, a, l, s;
                    if (!this._callbacks) return this;
                    for (t = t.split(o), a = [].slice.call(arguments, 1); (e = t.shift()) && (r = this._callbacks[e]); ) (l = n(r.sync, this, [e].concat(a))), (s = n(r.async, this, [e].concat(a))), l() && i(s);
                    return this;
                },
            };
        },
        function (t, e, r) {
            "use strict";
            function a() {
                o && l && ((o = !1), l.length ? (h = l.concat(h)) : (u = -1), h.length && n());
            }
            function n() {
                if (!o) {
                    (g = !1), (o = !0);
                    for (var t = h.length, e = setTimeout(a); t; ) {
                        for (l = h, h = []; l && ++u < t; ) l[u].run();
                        (u = -1), (t = h.length);
                    }
                    (l = null), (u = -1), (o = !1), clearTimeout(e);
                }
            }
            function i(t, e) {
                (this.fun = t), (this.array = e);
            }
            for (var o, l, s, c = [r(10), r(12), r(13), r(14), r(15)], u = -1, h = [], g = !1, f = -1, b = c.length; ++f < b; )
                if (c[f] && c[f].test && c[f].test()) {
                    s = c[f].install(n);
                    break;
                }
            (i.prototype.run = function () {
                var t = this.fun,
                    e = this.array;
                switch (e.length) {
                    case 0:
                        return t();
                    case 1:
                        return t(e[0]);
                    case 2:
                        return t(e[0], e[1]);
                    case 3:
                        return t(e[0], e[1], e[2]);
                    default:
                        return t.apply(null, e);
                }
            }),
                (t.exports = function (t) {
                    var e = new Array(arguments.length - 1);
                    if (arguments.length > 1) for (var r = 1; r < arguments.length; r++) e[r - 1] = arguments[r];
                    h.push(new i(t, e)), g || o || ((g = !0), s());
                });
        },
        function (t, e, r) {
            (function (t) {
                "use strict";
                (e.test = function () {
                    return void 0 !== t && !t.browser;
                }),
                    (e.install = function (e) {
                        return function () {
                            t.nextTick(e);
                        };
                    });
            }.call(e, r(11)));
        },
        function (t, e) {
            function r() {
                throw new Error("setTimeout has not been defined");
            }
            function a() {
                throw new Error("clearTimeout has not been defined");
            }
            function n(t) {
                if (c === setTimeout) return setTimeout(t, 0);
                if ((c === r || !c) && setTimeout) return (c = setTimeout), setTimeout(t, 0);
                try {
                    return c(t, 0);
                } catch (e) {
                    try {
                        return c.call(null, t, 0);
                    } catch (e) {
                        return c.call(this, t, 0);
                    }
                }
            }
            function i() {
                b && g && ((b = !1), g.length ? (f = g.concat(f)) : (p = -1), f.length && o());
            }
            function o() {
                if (!b) {
                    var t = n(i);
                    b = !0;
                    for (var e = f.length; e; ) {
                        for (g = f, f = []; ++p < e; ) g && g[p].run();
                        (p = -1), (e = f.length);
                    }
                    (g = null),
                        (b = !1),
                        (function (t) {
                            if (u === clearTimeout) return clearTimeout(t);
                            if ((u === a || !u) && clearTimeout) return (u = clearTimeout), clearTimeout(t);
                            try {
                                u(t);
                            } catch (e) {
                                try {
                                    return u.call(null, t);
                                } catch (e) {
                                    return u.call(this, t);
                                }
                            }
                        })(t);
                }
            }
            function l(t, e) {
                (this.fun = t), (this.array = e);
            }
            function s() {}
            var c,
                u,
                h = (t.exports = {});
            !(function () {
                try {
                    c = "function" == typeof setTimeout ? setTimeout : r;
                } catch (t) {
                    c = r;
                }
                try {
                    u = "function" == typeof clearTimeout ? clearTimeout : a;
                } catch (t) {
                    u = a;
                }
            })();
            var g,
                f = [],
                b = !1,
                p = -1;
            (h.nextTick = function (t) {
                var e = new Array(arguments.length - 1);
                if (arguments.length > 1) for (var r = 1; r < arguments.length; r++) e[r - 1] = arguments[r];
                f.push(new l(t, e)), 1 !== f.length || b || n(o);
            }),
                (l.prototype.run = function () {
                    this.fun.apply(null, this.array);
                }),
                (h.title = "browser"),
                (h.browser = !0),
                (h.env = {}),
                (h.argv = []),
                (h.version = ""),
                (h.versions = {}),
                (h.on = s),
                (h.addListener = s),
                (h.once = s),
                (h.off = s),
                (h.removeListener = s),
                (h.removeAllListeners = s),
                (h.emit = s),
                (h.binding = function (t) {
                    throw new Error("process.binding is not supported");
                }),
                (h.cwd = function () {
                    return "/";
                }),
                (h.chdir = function (t) {
                    throw new Error("process.chdir is not supported");
                }),
                (h.umask = function () {
                    return 0;
                });
        },
        function (t, e) {
            (function (t) {
                "use strict";
                var r = t.MutationObserver || t.WebKitMutationObserver;
                (e.test = function () {
                    return r;
                }),
                    (e.install = function (e) {
                        var a = 0,
                            n = new r(e),
                            i = t.document.createTextNode("");
                        return (
                            n.observe(i, { characterData: !0 }),
                            function () {
                                i.data = a = ++a % 2;
                            }
                        );
                    });
            }.call(
                e,
                (function () {
                    return this;
                })()
            ));
        },
        function (t, e) {
            (function (t) {
                "use strict";
                (e.test = function () {
                    return !t.setImmediate && void 0 !== t.MessageChannel;
                }),
                    (e.install = function (e) {
                        var r = new t.MessageChannel();
                        return (
                            (r.port1.onmessage = e),
                            function () {
                                r.port2.postMessage(0);
                            }
                        );
                    });
            }.call(
                e,
                (function () {
                    return this;
                })()
            ));
        },
        function (t, e) {
            (function (t) {
                "use strict";
                (e.test = function () {
                    return "document" in t && "onreadystatechange" in t.document.createElement("script");
                }),
                    (e.install = function (e) {
                        return function () {
                            var r = t.document.createElement("script");
                            return (
                                (r.onreadystatechange = function () {
                                    e(), (r.onreadystatechange = null), r.parentNode.removeChild(r), (r = null);
                                }),
                                t.document.documentElement.appendChild(r),
                                e
                            );
                        };
                    });
            }.call(
                e,
                (function () {
                    return this;
                })()
            ));
        },
        function (t, e) {
            "use strict";
            (e.test = function () {
                return !0;
            }),
                (e.install = function (t) {
                    return function () {
                        setTimeout(t, 0);
                    };
                });
        },
        function (t, e, r) {
            "use strict";
            function a(t) {
                var e,
                    r,
                    a,
                    l = this;
                (t = t || {}).menu || i.error("menu is required"),
                    i.isArray(t.datasets) || i.isObject(t.datasets) || i.error("1 or more datasets required"),
                    t.datasets || i.error("datasets is required"),
                    (this.isOpen = !1),
                    (this.isEmpty = !0),
                    (this.minLength = t.minLength || 0),
                    (this.templates = {}),
                    (this.appendTo = t.appendTo || !1),
                    (this.css = i.mixin({}, c, t.appendTo ? c.appendTo : {})),
                    (this.cssClasses = t.cssClasses = i.mixin({}, c.defaultClasses, t.cssClasses || {})),
                    (this.cssClasses.prefix = t.cssClasses.formattedPrefix || i.formatPrefix(this.cssClasses.prefix, this.cssClasses.noPrefix)),
                    (e = i.bind(this._onSuggestionClick, this)),
                    (r = i.bind(this._onSuggestionMouseEnter, this)),
                    (a = i.bind(this._onSuggestionMouseLeave, this));
                var s = i.className(this.cssClasses.prefix, this.cssClasses.suggestion);
                (this.$menu = o.element(t.menu).on("mouseenter.aa", s, r).on("mouseleave.aa", s, a).on("click.aa", s, e)),
                    (this.$container = t.appendTo ? t.wrapper : this.$menu),
                    t.templates && t.templates.header && ((this.templates.header = i.templatify(t.templates.header)), this.$menu.prepend(this.templates.header())),
                    t.templates &&
                        t.templates.empty &&
                        ((this.templates.empty = i.templatify(t.templates.empty)),
                        (this.$empty = o.element('<div class="' + i.className(this.cssClasses.prefix, this.cssClasses.empty, !0) + '"></div>')),
                        this.$menu.append(this.$empty),
                        this.$empty.hide()),
                    (this.datasets = i.map(t.datasets, function (e) {
                        return n(l.$menu, e, t.cssClasses);
                    })),
                    i.each(this.datasets, function (t) {
                        var e = t.getRoot();
                        e && 0 === e.parent().length && l.$menu.append(e), t.onSync("rendered", l._onRendered, l);
                    }),
                    t.templates && t.templates.footer && ((this.templates.footer = i.templatify(t.templates.footer)), this.$menu.append(this.templates.footer()));
                var u = this;
                o.element(window).resize(function () {
                    u._redraw();
                });
            }
            function n(t, e, r) {
                return new a.Dataset(i.mixin({ $menu: t, cssClasses: r }, e));
            }
            var i = r(4),
                o = r(2),
                l = r(8),
                s = r(17),
                c = r(19);
            i.mixin(a.prototype, l, {
                _onSuggestionClick: function (t) {
                    this.trigger("suggestionClicked", o.element(t.currentTarget));
                },
                _onSuggestionMouseEnter: function (t) {
                    var e = o.element(t.currentTarget);
                    if (!e.hasClass(i.className(this.cssClasses.prefix, this.cssClasses.cursor, !0))) {
                        this._removeCursor();
                        var r = this;
                        setTimeout(function () {
                            r._setCursor(e, !1);
                        }, 0);
                    }
                },
                _onSuggestionMouseLeave: function (t) {
                    (t.relatedTarget && o.element(t.relatedTarget).closest("." + i.className(this.cssClasses.prefix, this.cssClasses.cursor, !0)).length > 0) || (this._removeCursor(), this.trigger("cursorRemoved"));
                },
                _onRendered: function (t, e) {
                    if (
                        ((this.isEmpty = i.every(this.datasets, function (t) {
                            return t.isEmpty();
                        })),
                        this.isEmpty)
                    )
                        if ((e.length >= this.minLength && this.trigger("empty"), this.$empty))
                            if (e.length < this.minLength) this._hide();
                            else {
                                var r = this.templates.empty({ query: this.datasets[0] && this.datasets[0].query });
                                this.$empty.html(r), this.$empty.show(), this._show();
                            }
                        else
                            i.any(this.datasets, function (t) {
                                return t.templates && t.templates.empty;
                            })
                                ? e.length < this.minLength
                                    ? this._hide()
                                    : this._show()
                                : this._hide();
                    else this.isOpen && (this.$empty && (this.$empty.empty(), this.$empty.hide()), e.length >= this.minLength ? this._show() : this._hide());
                    this.trigger("datasetRendered");
                },
                _hide: function () {
                    this.$container.hide();
                },
                _show: function () {
                    this.$container.css("display", "block"), this._redraw(), this.trigger("shown");
                },
                _redraw: function () {
                    this.isOpen && this.appendTo && this.trigger("redrawn");
                },
                _getSuggestions: function () {
                    return this.$menu.find(i.className(this.cssClasses.prefix, this.cssClasses.suggestion));
                },
                _getCursor: function () {
                    return this.$menu.find(i.className(this.cssClasses.prefix, this.cssClasses.cursor)).first();
                },
                _setCursor: function (t, e) {
                    t.first().addClass(i.className(this.cssClasses.prefix, this.cssClasses.cursor, !0)).attr("aria-selected", "true"), this.trigger("cursorMoved", e);
                },
                _removeCursor: function () {
                    this._getCursor().removeClass(i.className(this.cssClasses.prefix, this.cssClasses.cursor, !0)).removeAttr("aria-selected");
                },
                _moveCursor: function (t) {
                    var e, r, a, n;
                    if (this.isOpen) {
                        if (((r = this._getCursor()), (e = this._getSuggestions()), this._removeCursor(), -1 == (a = (((a = e.index(r) + t) + 1) % (e.length + 1)) - 1))) return void this.trigger("cursorRemoved");
                        a < -1 && (a = e.length - 1), this._setCursor((n = e.eq(a)), !0), this._ensureVisible(n);
                    }
                },
                _ensureVisible: function (t) {
                    var e, r, a, n;
                    (r = (e = t.position().top) + t.height() + parseInt(t.css("margin-top"), 10) + parseInt(t.css("margin-bottom"), 10)),
                        (a = this.$menu.scrollTop()),
                        (n = this.$menu.height() + parseInt(this.$menu.css("padding-top"), 10) + parseInt(this.$menu.css("padding-bottom"), 10)),
                        e < 0 ? this.$menu.scrollTop(a + e) : n < r && this.$menu.scrollTop(a + (r - n));
                },
                close: function () {
                    this.isOpen && ((this.isOpen = !1), this._removeCursor(), this._hide(), this.trigger("closed"));
                },
                open: function () {
                    this.isOpen || ((this.isOpen = !0), this.isEmpty || this._show(), this.trigger("opened"));
                },
                setLanguageDirection: function (t) {
                    this.$menu.css("ltr" === t ? this.css.ltr : this.css.rtl);
                },
                moveCursorUp: function () {
                    this._moveCursor(-1);
                },
                moveCursorDown: function () {
                    this._moveCursor(1);
                },
                getDatumForSuggestion: function (t) {
                    var e = null;
                    return t.length && (e = { raw: s.extractDatum(t), value: s.extractValue(t), datasetName: s.extractDatasetName(t) }), e;
                },
                getCurrentCursor: function () {
                    return this._getCursor().first();
                },
                getDatumForCursor: function () {
                    return this.getDatumForSuggestion(this._getCursor().first());
                },
                getDatumForTopSuggestion: function () {
                    return this.getDatumForSuggestion(this._getSuggestions().first());
                },
                cursorTopSuggestion: function () {
                    this._setCursor(this._getSuggestions().first(), !1);
                },
                update: function (t) {
                    i.each(this.datasets, function (e) {
                        e.update(t);
                    });
                },
                empty: function () {
                    i.each(this.datasets, function (t) {
                        t.clear();
                    }),
                        (this.isEmpty = !0);
                },
                isVisible: function () {
                    return this.isOpen && !this.isEmpty;
                },
                destroy: function () {
                    this.$menu.off(".aa"),
                        (this.$menu = null),
                        i.each(this.datasets, function (t) {
                            t.destroy();
                        });
                },
            }),
                (a.Dataset = s),
                (t.exports = a);
        },
        function (t, e, r) {
            "use strict";
            function a(t) {
                ((t = t || {}).templates = t.templates || {}),
                    t.source || l.error("missing source"),
                    t.name &&
                        !(function (t) {
                            return /^[_a-zA-Z0-9-]+$/.test(t);
                        })(t.name) &&
                        l.error("invalid dataset name: " + t.name),
                    (this.query = null),
                    (this._isEmpty = !0),
                    (this.highlight = !!t.highlight),
                    (this.name = void 0 === t.name || null === t.name ? l.getUniqueId() : t.name),
                    (this.source = t.source),
                    (this.displayFn = (function (t) {
                        return (
                            (t = t || "value"),
                            l.isFunction(t)
                                ? t
                                : function (e) {
                                      return e[t];
                                  }
                        );
                    })(t.display || t.displayKey)),
                    (this.debounce = t.debounce),
                    (this.cache = !1 !== t.cache),
                    (this.templates = (function (t, e) {
                        return {
                            empty: t.empty && l.templatify(t.empty),
                            header: t.header && l.templatify(t.header),
                            footer: t.footer && l.templatify(t.footer),
                            suggestion:
                                t.suggestion ||
                                function (t) {
                                    return "<p>" + e(t) + "</p>";
                                },
                        };
                    })(t.templates, this.displayFn)),
                    (this.css = l.mixin({}, u, t.appendTo ? u.appendTo : {})),
                    (this.cssClasses = t.cssClasses = l.mixin({}, u.defaultClasses, t.cssClasses || {})),
                    (this.cssClasses.prefix = t.cssClasses.formattedPrefix || l.formatPrefix(this.cssClasses.prefix, this.cssClasses.noPrefix));
                var e = l.className(this.cssClasses.prefix, this.cssClasses.dataset);
                (this.$el =
                    t.$menu && t.$menu.find(e + "-" + this.name).length > 0
                        ? s.element(t.$menu.find(e + "-" + this.name)[0])
                        : s.element(c.dataset.replace("%CLASS%", this.name).replace("%PREFIX%", this.cssClasses.prefix).replace("%DATASET%", this.cssClasses.dataset))),
                    (this.$menu = t.$menu),
                    this.clearCachedSuggestions();
            }
            var n = "aaDataset",
                i = "aaValue",
                o = "aaDatum",
                l = r(4),
                s = r(2),
                c = r(18),
                u = r(19),
                h = r(8);
            (a.extractDatasetName = function (t) {
                return s.element(t).data(n);
            }),
                (a.extractValue = function (t) {
                    return s.element(t).data(i);
                }),
                (a.extractDatum = function (t) {
                    var e = s.element(t).data(o);
                    return "string" == typeof e && (e = JSON.parse(e)), e;
                }),
                l.mixin(a.prototype, h, {
                    _render: function (t, e) {
                        function r() {
                            var e = [].slice.call(arguments, 0);
                            return (e = [{ query: t, isEmpty: !u }].concat(e)), h.templates.header.apply(this, e);
                        }
                        function a() {
                            var e = [].slice.call(arguments, 0);
                            return (e = [{ query: t, isEmpty: !u }].concat(e)), h.templates.footer.apply(this, e);
                        }
                        if (this.$el) {
                            var u,
                                h = this,
                                g = [].slice.call(arguments, 2);
                            if ((this.$el.empty(), (u = e && e.length), (this._isEmpty = !u), !u && this.templates.empty))
                                this.$el
                                    .html(
                                        function () {
                                            var e = [].slice.call(arguments, 0);
                                            return (e = [{ query: t, isEmpty: !0 }].concat(e)), h.templates.empty.apply(this, e);
                                        }.apply(this, g)
                                    )
                                    .prepend(h.templates.header ? r.apply(this, g) : null)
                                    .append(h.templates.footer ? a.apply(this, g) : null);
                            else if (u)
                                this.$el
                                    .html(
                                        function () {
                                            var t,
                                                r,
                                                a = [].slice.call(arguments, 0),
                                                u = this,
                                                g = c.suggestions.replace("%PREFIX%", this.cssClasses.prefix).replace("%SUGGESTIONS%", this.cssClasses.suggestions);
                                            return (
                                                (t = s.element(g).css(this.css.suggestions)),
                                                (r = l.map(e, function (t) {
                                                    var e,
                                                        r = c.suggestion.replace("%PREFIX%", u.cssClasses.prefix).replace("%SUGGESTION%", u.cssClasses.suggestion);
                                                    return (
                                                        (e = s
                                                            .element(r)
                                                            .attr({ role: "option", id: ["option", Math.floor(1e8 * Math.random())].join("-") })
                                                            .append(h.templates.suggestion.apply(this, [t].concat(a)))).data(n, h.name),
                                                        e.data(i, h.displayFn(t) || void 0),
                                                        e.data(o, JSON.stringify(t)),
                                                        e.children().each(function () {
                                                            s.element(this).css(u.css.suggestionChild);
                                                        }),
                                                        e
                                                    );
                                                })),
                                                t.append.apply(t, r),
                                                t
                                            );
                                        }.apply(this, g)
                                    )
                                    .prepend(h.templates.header ? r.apply(this, g) : null)
                                    .append(h.templates.footer ? a.apply(this, g) : null);
                            else if (e && !Array.isArray(e)) throw new TypeError("suggestions must be an array");
                            this.$menu && this.$menu.addClass(this.cssClasses.prefix + (u ? "with" : "without") + "-" + this.name).removeClass(this.cssClasses.prefix + (u ? "without" : "with") + "-" + this.name),
                                this.trigger("rendered", t);
                        }
                    },
                    getRoot: function () {
                        return this.$el;
                    },
                    update: function (t) {
                        function e(e) {
                            if (!this.canceled && t === this.query) {
                                var r = [].slice.call(arguments, 1);
                                this.cacheSuggestions(t, e, r), this._render.apply(this, [t, e].concat(r));
                            }
                        }
                        if (((this.query = t), (this.canceled = !1), this.shouldFetchFromCache(t))) e.apply(this, [this.cachedSuggestions].concat(this.cachedRenderExtraArgs));
                        else {
                            var r = this,
                                a = function () {
                                    r.canceled || r.source(t, e.bind(r));
                                };
                            if (this.debounce) {
                                clearTimeout(this.debounceTimeout),
                                    (this.debounceTimeout = setTimeout(function () {
                                        (r.debounceTimeout = null), a();
                                    }, this.debounce));
                            } else a();
                        }
                    },
                    cacheSuggestions: function (t, e, r) {
                        (this.cachedQuery = t), (this.cachedSuggestions = e), (this.cachedRenderExtraArgs = r);
                    },
                    shouldFetchFromCache: function (t) {
                        return this.cache && this.cachedQuery === t && this.cachedSuggestions && this.cachedSuggestions.length;
                    },
                    clearCachedSuggestions: function () {
                        delete this.cachedQuery, delete this.cachedSuggestions, delete this.cachedRenderExtraArgs;
                    },
                    cancel: function () {
                        this.canceled = !0;
                    },
                    clear: function () {
                        this.cancel(), this.$el.empty(), this.trigger("rendered", "");
                    },
                    isEmpty: function () {
                        return this._isEmpty;
                    },
                    destroy: function () {
                        this.clearCachedSuggestions(), (this.$el = null);
                    },
                }),
                (t.exports = a);
        },
        function (t, e) {
            "use strict";
            t.exports = {
                wrapper: '<span class="%ROOT%"></span>',
                dropdown: '<span class="%PREFIX%%DROPDOWN_MENU%"></span>',
                dataset: '<div class="%PREFIX%%DATASET%-%CLASS%"></div>',
                suggestions: '<span class="%PREFIX%%SUGGESTIONS%"></span>',
                suggestion: '<div class="%PREFIX%%SUGGESTION%"></div>',
            };
        },
        function (t, e, r) {
            "use strict";
            var a = r(4),
                n = {
                    wrapper: { position: "relative", display: "inline-block" },
                    hint: { position: "absolute", top: "0", left: "0", borderColor: "transparent", boxShadow: "none", opacity: "1" },
                    input: { position: "relative", verticalAlign: "top", backgroundColor: "transparent" },
                    inputWithNoHint: { position: "relative", verticalAlign: "top" },
                    dropdown: { position: "absolute", top: "100%", left: "0", zIndex: "100", display: "none" },
                    suggestions: { display: "block" },
                    suggestion: { whiteSpace: "nowrap", cursor: "pointer" },
                    suggestionChild: { whiteSpace: "normal" },
                    ltr: { left: "0", right: "auto" },
                    rtl: { left: "auto", right: "0" },
                    defaultClasses: {
                        root: "algolia-autocomplete",
                        prefix: "aa",
                        noPrefix: !1,
                        dropdownMenu: "dropdown-menu",
                        input: "input",
                        hint: "hint",
                        suggestions: "suggestions",
                        suggestion: "suggestion",
                        cursor: "cursor",
                        dataset: "dataset",
                        empty: "empty",
                    },
                    appendTo: { wrapper: { position: "absolute", zIndex: "100", display: "none" }, input: {}, inputWithNoHint: {}, dropdown: { display: "block" } },
                };
            a.isMsie() && a.mixin(n.input, { backgroundImage: "url(data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7)" }),
                a.isMsie() && a.isMsie() <= 7 && a.mixin(n.input, { marginTop: "-1px" }),
                (t.exports = n);
        },
        function (t, e, r) {
            "use strict";
            t.exports = { hits: r(21), popularIn: r(24) };
        },
        function (t, e, r) {
            "use strict";
            var a = r(4),
                n = r(22),
                i = r(23);
            t.exports = function (t, e) {
                var r = i(t.as._ua);
                return (
                    r && r[0] >= 3 && r[1] > 20 && ((e = e || {}).additionalUA = "autocomplete.js " + n),
                    function (r, n) {
                        t.search(r, e, function (t, e) {
                            t ? a.error(t.message) : n(e.hits, e);
                        });
                    }
                );
            };
        },
        function (t, e) {
            t.exports = "0.36.0";
        },
        function (t, e) {
            "use strict";
            t.exports = function (t) {
                var e = t.match(/Algolia for vanilla JavaScript (\d+\.)(\d+\.)(\d+)/);
                if (e) return [e[1], e[2], e[3]];
            };
        },
        function (t, e, r) {
            "use strict";
            var a = r(4),
                n = r(22),
                i = r(23);
            t.exports = function (t, e, r, o) {
                var l = i(t.as._ua);
                if ((l && l[0] >= 3 && l[1] > 20 && ((e = e || {}).additionalUA = "autocomplete.js " + n), !r.source)) return a.error("Missing 'source' key");
                var s = a.isFunction(r.source)
                    ? r.source
                    : function (t) {
                          return t[r.source];
                      };
                if (!r.index) return a.error("Missing 'index' key");
                var c = r.index;
                return (
                    (o = o || {}),
                    function (l, u) {
                        t.search(l, e, function (t, l) {
                            if (t) a.error(t.message);
                            else {
                                if (l.hits.length > 0) {
                                    var h = l.hits[0],
                                        g = a.mixin({ hitsPerPage: 0 }, r);
                                    delete g.source, delete g.index;
                                    var f = i(c.as._ua);
                                    return (
                                        f && f[0] >= 3 && f[1] > 20 && (e.additionalUA = "autocomplete.js " + n),
                                        void c.search(s(h), g, function (t, e) {
                                            if (t) a.error(t.message);
                                            else {
                                                var r = [];
                                                if (o.includeAll) {
                                                    var n = o.allTitle || "All departments";
                                                    r.push(a.mixin({ facet: { value: n, count: e.nbHits } }, a.cloneDeep(h)));
                                                }
                                                a.each(e.facets, function (t, e) {
                                                    a.each(t, function (t, n) {
                                                        r.push(a.mixin({ facet: { facet: e, value: n, count: t } }, a.cloneDeep(h)));
                                                    });
                                                });
                                                for (var i = 1; i < l.hits.length; ++i) r.push(l.hits[i]);
                                                u(r, l);
                                            }
                                        })
                                    );
                                }
                                u([]);
                            }
                        });
                    }
                );
            };
        },
    ]);
