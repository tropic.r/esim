<?php
return [
    'desc' => 'Sineco Invest SRL адрес:Bd. Mircea cel Bătrân 22/5, of.200',
    'quick' => [
        'title' => 'Quick links',
        'home' => 'Home',
        'terms-condition' => 'Terms',
        'privacy-policy' => 'Privacy Policy',
        'delivery-refunds' => 'Shipping, Return and Cancellation Policy',
        'sitemap' => 'Sitemap',
    ],
    'regions' => [
        'title' => 'Regions',
        'europe' => 'eSIM for Europe',
        'asia' => 'eSIM for Asia',
        'america' => 'eSIM for America',
        'middle-east' => 'eSIM for the Middle East',
        'oceania' => 'eSIM for Oceania',
        'africa' => 'eSIM for Africa',
    ],
    'countries' => [
        'title' => 'Countries',
        'united-states' => 'eSIM for USA',
        'japan' => 'eSIM for Japan',
        'canada' => 'eSIM for Canada',
        'spain' => 'eSIM for Spain',
        'italy' => 'eSIM for Italy',
        'united-kingdom' => 'eSIM for the United Kingdom',
        'jordan' => 'eSIM for Jordan',
        'switzerland' => 'eSIM for Switzerland',
        'saudi-arabia' => 'eSIM for Saudi Arabia',
        'netherlands' => 'eSIM for the Netherlands',
        'turkey' => 'eSIM for Turkey',
        'sitemap' => 'To learn more…',
    ],
    'follow_us_on' => 'Follow us on'
];