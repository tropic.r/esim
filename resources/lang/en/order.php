<?php 
    return [
        "mail" => [
            "order" =>[
                'body' => [
                    'title' => 'Hello :name, here is a summary of your order:',
                    'order_id' => 'Order ID:',
                    'paid' => 'Paid:',
                    'date' => 'Date:',
                    'item' => 'Item',
                    'total' => 'Total',
                    'аmount' => 'Amount',
                    'need_an_invoice' => 'Need an invoice?',
                    'esim_activation_steps' => 'eSIM ctivation steps',
                    'important_note' => 'IMPORTANT NOTE',
                    'important_note_p1' => 'This eSIM profile can only be downloaded once.',
                    'important_note_p2' => 'Please do not remove your eSIM cellular plan from your phone settings until you are done using it. Once a deleted eSIM profile cannot be restored.',
                    'important_note_p3' => 'Please follow the activation instructions below and contact us if you need assistance.',
                    'esim_profile_info' => 'eSIM Profile Information:',
                    'phone_number' => 'Phone number:',
                    'apn' => 'APN (Access Point Name):',
                    'manual_installation_for_ios' => 'Manual installation for iOS:',
                    'sm_dp_address' => 'SM-DP+ address:',
                    'activation_code' => 'Activation code:',
                    'manual_installation_for_android' => 'Manual installation for Android:',
                    'your_package_details' => 'Your package details:',
                    'data' => 'Data:',
                    'validity' => 'Validity:',
                    'your_package_validity_esim' => 'Your package will begin to expire after you download your eSIM profile.',
                    'to_check_your_balance_visit' => 'To check your balance visit',
                    'instructions_esim_download_and_activation' => '<p><strong>eSIM Download and Activation Instructions</strong></p>
                        <p><br></p>
                        <p>1- Make sure your phone is connected to the internet with a stable WiFi connection.</p>
                        <p>2- Download the eSIM profile:</p>
                        <p><br></p>
                        <ul>
                        <li>iOS devices: Open the camera app as if you were about to take a picture and point it at the QR code above.</li>
                        <li>Android and other OS: Go to the cellular/mobile network settings of your eSIM enabled device and follow the instructions to add a cellular plan/add a mobile network.</li>
                        <li>Smartwatches and other wearable devices: See manufacturer\'s instructions.</li>
                        </ul>
                        <p>3- Assign cellular/mobile data access to your eSIM.</p>
                        <p><br></p>
                        <p>4- Enable data roaming</p>
                        <p><br></p>
                        <p>5- Turn off Wi-Fi and give the eSIM a few minutes to properly set itself up according to the country you are in.</p>
                        <p><br></p>
                        <p><strong>Note:</strong> While the eSIM activation principles remain the same, the setup processes may vary slightly from one phone to another. Please contact your smartphone manufacturer\'s online support for a precise step-by-step guide.</p>
                        <p><br></p>
                        <p><strong>Troubleshooting and additional information</strong></p>
                        <p><br></p>
                        <ul>
                        <li>If your eSIM is not connecting to the network, make sure cellular/mobile data is assigned to your eSIM and data roaming is enabled. Turn off any Wi-Fi or other Internet connection your phone may be using and wait a few minutes for the eSIM to connect.</li>
                        <li>If you are online but your data connection is not working properly, check if the APN is mentioned in your eSIM profile information. If yes, set the correct APN for your eSIM. Turn Airplane Mode on and off and check the connection again.</li>
                        <li>You can track data usage data right in your phone settings. For iOS: Go to Settings -&gt; Cellular and scroll down. For Android and other OS: go to Settings -&gt; Network & Internet and view your eSIM details. Please refer to your device manual for exact instructions.</li>
                        </ul>
                        <p>If you have finished your package and need more data, just buy any package of your choice and repeat the activation steps.</p>
                        <p><br></p>',
                ]
            ]
        ]
    ];