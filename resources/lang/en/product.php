<?php 
return [
    'popular' => [
        'title' => 'Popular Offers',
        'desc' => 'Top Selling eSIM Offers Shown'
    ],
    'top_up' => [
        'title' => 'Supporting recharge providers',
        'desc' => 'Top up your eSIM Mobimatter with the same or a different package from these providers.',
        'readmore' => 'See offers'
    ],
    'top_popular' => 'Popular top-up offers',
    'mini' => [
        'readmore' => 'View offer',
        'price' => 'Price:',
        'data' => 'Data:',
        'validity' => 'Validity:',
        'date_d' => 'days',
        'works_in' => 'Works in',
        'less_three_countries' => 'only',
        'more_three_countries' => 'other countries',
    ],
    'cart' => [
        'detail' => 'Details',
        'rabota' => 'Works in',
        'count' => 'Quantity x',
        'free' => 'Free',
        'total' => 'Total',
        'buy' => 'buy',
        'createOrder' => 'Creating an order...',
        'search_buy' => 'Looking for another offer',
        'invistion' => 'Warehouse inventory',
        'invistion_text' => 'Looks like we\'re in stock for this package. <br> We have many offers for you.',
        'booking' => 'Booking your ESIM...'
    ],
    'faq' => [
        'title' => 'Frequently asked Questions',
        'faq_1' => [
            'title' => 'Which smartphones are ESIM eligible?',
            'desc' => '<p><span class="text-primary">A:</span> iPhone: XR, XS, 11, 12, 13 and SE 2020 series; Google Pixel 3 and later; Samsung S20 Series, Flex and Flip; Huawei P40 series; iPads: 10.2, Air Conditioner, Mini 2019, Pro 11, Pro 12.9.</p><p>Smartwatches with eSIM capability (except Apple Watch) are also supported</p><p>Note: Some of these devices have variants that do not support ESIM, please check <a href="/faqs" target="_blank">Help and FAQ</a> if in doubt.</p>',
        ],
        'faq_2' => [
            'title' => 'Can I use a physical SIM with an ESIM?',
            'desc' => '<p><span class="text-primary">A:</span> Yes! With dual SIM functionality, you can have both your physical SIM and ESIM active at the same time.</p>',
        ],
        'faq_3' => [
            'title' => 'I already have an active ESIM in my phone, can I use your services?',
            'desc' => '<p><span class="text-primary">A:</span> Yes, your phone can store many ESIM profiles at once. You can choose one to use in your phone settings.</p>',
        ],
        'faq_4' => [
            'title' => 'I have other questions',
            'desc' => '<p><span class="text-primary">A:</span> Ask us any question using our chat feature or email us at info@esim.md. We will be happy to help.</p>',
        ],
    ]
];