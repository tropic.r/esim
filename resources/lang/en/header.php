<?php return [
    'currents' => 'currency',
    'menu' => [
        'faq' => 'FAQ',
        'check_usage' => 'Check usage',
        'shop' => 'ESIM Shop',
        'shop_buy_esim' => 'Buy new eSIMs',
        'shop_up_esim' => 'Top up your eSIM',
        'shop_region' => 'Shop by region',
        'login' => 'Login',
        'order' => 'Order History',
        'logout' => 'Logout',
    ],
    'regions' => [
        'europe' => 'Europa',
        'asia' => 'Asia',
        'middle-east' => 'Middle East',
        'america' => 'America',
        'africa' => 'Africa',
        'oceania' => 'Oceania',
    ]
];