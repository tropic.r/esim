<?php
    return [
        'title_page' => "Order History",
        'not_order' => "No orders found",
        'title_order' => "Order",
        'order' => [
            'order_id' => 'OrderID',
            'product' => 'Product',
            'date' => 'Date',
            'status' => 'Status',
            'price' => 'Wholesale Price ($)',
        ],
    ];