<?php
return [
    'cart_regional' => [
        'title' => 'Regional offers',
        'desc' => 'Find the best ESIM deals for your trip',
        'regional' => [
            'asia' => [
                'title' => 'Asia',
                'desc' => 'Share moments from Asian beaches with generous data packages',
            ],
            'middle-east' => [
                'title' => 'Middle East',
                'desc' => 'From the desert to the skyscrapers of the Middle East, keep in touch',
            ],
            'oceania' => [
                'title' => 'Oceania',
                'desc' => 'Stay connected down under',
            ],
            'america' => [
                'title' => 'North and South America',
                'desc' => 'Stay Connected in America',
            ],
            'europe' => [
                'title' => 'Europe',
                'desc' => 'Stay connected in Europe',
            ],
            'africa' => [
                'title' => 'Africa',
                'desc' => 'Stay connected in the jungle, endless natural beauty and views of the cliffs of Africa',
            ],
        ]
    ],
    'travel' => [
        'title' => ':name eSIM offers',
        'desc' => 'Best eSIM deals for :name from the top providers worldwide',
        'sub_desc' => 'Loading travel eSIM offers for :name',
        'search' => 'Search',
    ]
];