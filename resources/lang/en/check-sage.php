<?php 
return [
    'title_page' => "eSIM Status and Usage Check",
    'text1' => "You can see your data usage directly from your phone settings.",
    'text2' => "Alternatively, enter your order number:",
    'button' => "Check",

    'provider_name' => "Provider Name",
    'plan_name' => "Plan Name",
    'plan_data' => "Plan Data",
    'validity' => "Validity",
    'days' => "Days"
];