<?php return [
    'title' => "Shop for best <strong>mobile offers</strong> at home and when you travel",
    'top_up_your_esim' => "Top up your eSIM",
    'buy_a_new_eSIM' => "Buy a new eSIM",
    'button' => "Search",
    'jump_to' => "Jump to",
    'travel_esim_store' => "Travel eSIM store",
    'search_for_a_country' => "Search for a country",
];