<?php
    return [
        'title_page' => "История заказов",
        'not_order' => "Заказов не найдено",
        'title_order' => "Заказы",
        'order' => [
            'order_id' => 'Номер заказа',
            'product' => 'Товар',
            'date' => 'Дата',
            'status' => 'Статус',
            'price' => 'Цена ($)',
        ],
    ];