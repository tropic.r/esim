<?php
return [
    'desc' => 'Sineco Invest SRL адрес:Bd. Mircea cel Bătrân 22/5, of.200',
    'quick' => [
        'title' => 'Быстрые ссылки',
        'home' => 'Главная',
        'terms-condition' => 'Условия',
        'privacy-policy' => 'Политика конфиденциальности',
        'delivery-refunds' => 'Политика доставки, возврата и отмены',
        'sitemap' => 'Карта сайта',
    ],
    'regions' => [
        'title' => 'Регионы',
        'europe' => 'eSIM для Европы',
        'asia' => 'eSIM для Азии',
        'america' => 'eSIM для Америки',
        'middle-east' => 'eSIM для Ближнего Востока',
        'oceania' => 'eSIM для Океании',
        'africa' => 'eSIM для Африки',
    ],
    'countries' => [
        'title' => 'Страны',
        'united-states' => 'eSIM для США',
        'japan' => 'eSIM для Японии',
        'canada' => 'eSIM для Канады',
        'spain' => 'eSIM для Испании',
        'italy' => 'eSIM для Италии',
        'united-kingdom' => 'eSIM для Соединенного Королевства',
        'jordan' => 'eSIM для Иордании',
        'switzerland' => 'eSIM для Швейцарии',
        'saudi-arabia' => 'eSIM для Саудовской Аравии',
        'netherlands' => 'eSIM для Нидерландов',
        'turkey' => 'eSIM для Турции',
        'sitemap' => 'Узнать больше…',
    ],
    'follow_us_on' => 'Подпишитесь на нас в'
];