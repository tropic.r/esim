<?php return [
    'currents' => 'валюта',
    'menu' => [
        'faq' => 'Помощь и часто задаваемые вопросы',
        'check_usage' => 'Проверить использование',
        'shop' => 'ESIM Магазин',
        'shop_buy_esim' => 'Купить новые eSIM',
        'shop_up_esim' => 'Пополнить eSIM',
        'shop_region' => 'Магазин по регионам',
        'login' => 'Вход',
        'order' => 'История заказов',
        'logout' => 'Выход',
    ],
    'regions' => [
        'europe' => 'Европы',
        'asia' => 'Азии',
        'middle-east' => 'Ближнего Востока',
        'america' => 'Америки',
        'africa' => 'Африки',
        'oceania' => 'Океании',
    ]
];