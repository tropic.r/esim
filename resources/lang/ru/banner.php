<?php return [
    'title' => "Приобретайте лучшие <strong>предложения для мобильных устройств</strong> дома и в путешествии",
    'top_up_your_esim' => "Пополнить eSIM",
    'buy_a_new_eSIM' => "Купить новую eSIM",
    'button' => "Поиск",
    'jump_to' => "Прыгать на",
    'travel_esim_store' => "Магазин eSIM для путешествий",
    'search_for_a_country' => "Поиск страны",
];