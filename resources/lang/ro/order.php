<?php 
    return [
        "mail" => [
            "order" =>[
                'body' => [
                    'title' => 'Bună :name, iată un rezumat al comenzii tale:',
                    'order_id' => 'Comanda ID:',
                    'paid' => 'Plătit:',
                    'date' => 'Date:',
                    'item' => 'Articol',
                    'total' => 'Total',
                    'аmount' => 'Cantitate',
                    'need_an_invoice' => 'Am nevoie de o factură?',
                    'esim_activation_steps' => 'Pașii de activare a eSIM',
                    'important_note' => 'NOTĂ IMPORTANTĂ',
                    'important_note_p1' => 'Acest profil eSIM poate fi descărcat o singură dată.',
                    'important_note_p2' => 'Vă rugăm să nu eliminați planul de telefonie mobilă eSIM din setările telefonului până când nu îl utilizați. Odată ce un profil eSIM șters nu poate fi restaurat.',
                    'important_note_p3' => 'Vă rugăm să urmați instrucțiunile de activare de mai jos și să ne contactați dacă aveți nevoie de asistență.',
                    'esim_profile_info' => 'Informații de profil eSIM:',
                    'phone_number' => 'Număr de telefon:',
                    'apn' => 'APN (nume punct de acces):',
                    'manual_installation_for_ios' => 'Instalare manuală pentru iOS:',
                    'sm_dp_address' => 'adresa SM-DP+:',
                    'activation_code' => 'Cod de activare:',
                    'manual_installation_for_android' => 'Instalare manuală pentru Android:',
                    'your_package_details' => 'Detaliile pachetului dvs:',
                    'data' => 'Date:',
                    'validity' => 'Valabilitate:',
                    'your_package_validity_esim' => 'Pachetul dvs. va începe să expire după ce descărcați profilul eSIM.',
                    'to_check_your_balance_visit' => 'Pentru a vă verifica soldul, vizitați',
                    'instructions_esim_download_and_activation' => '<p><strong>Instrucțiuni de descărcare și activare eSIM</strong></p>
                        <p><br></p>
                        <p>1- Asigurați-vă că telefonul este conectat la internet cu o conexiune WiFi stabilă.</p>
                        <p>2- Descărcați profilul eSIM:</p>
                        <p><br></p>
                        <ul>
                        <li>Dispozitive iOS: deschideți aplicația pentru cameră ca și cum ați fi pe cale să faceți o fotografie și îndreptați-o către codul QR de mai sus.</li>
                        <li>Android și alt sistem de operare: Accesați setările rețelei celulare/mobile ale dispozitivului dvs. compatibil eSIM și urmați instrucțiunile pentru a adăuga un plan de telefonie mobilă/a adăuga o rețea mobilă.</li>
                        <li>Ceasuri inteligente și alte dispozitive portabile: consultați instrucțiunile producătorului.</li>
                        </ul>
                        <p>3- Atribuiți accesul la date celulare/mobile eSIM-ului dvs.</p>
                        <p><br></p>
                        <p>4- Activați roamingul de date</p>
                        <p><br></p>
                        <p>5- Opriți Wi-Fi și acordați eSIM-ului câteva minute pentru a se configura corect în funcție de țara în care vă aflați.</p>
                        <p><br></p>
                        <p><strong>Notă:</strong> În timp ce principiile de activare a eSIM rămân aceleași, procesele de configurare pot varia ușor de la un telefon la altul. Vă rugăm să contactați serviciul de asistență online al producătorului dvs. de smartphone pentru un ghid precis pas cu pas.</p>
                        <p><br></p>
                        <p><strong>Depanare și informații suplimentare</strong></p>
                        <p><br></p>
                        <ul>
                        <li>Dacă eSIM-ul dvs. nu se conectează la rețea, asigurați-vă că datele celulare/mobile sunt alocate eSIM-ului dvs. și că roamingul de date este activat. Opriți orice conexiune Wi-Fi sau altă conexiune la internet pe care telefonul dvs. o poate utiliza și așteptați câteva minute pentru ca eSIM să se conecteze.</li>
                        <li>Dacă sunteți online, dar conexiunea dvs. de date nu funcționează corect, verificați dacă APN-ul este menționat în informațiile de profil eSIM. Dacă da, setați APN-ul corect pentru eSIM-ul dvs. Activați și dezactivați modul Avion și verificați din nou conexiunea.</li>
                        <li>Puteți urmări datele de utilizare a datelor chiar în setările telefonului. Pentru iOS: Accesați Setări -&gt; Cellular și derulați în jos. Pentru Android și alt sistem de operare: accesați Setări -&gt; Rețea și Internet și vizualizați detaliile dvs. eSIM. Vă rugăm să consultați manualul dispozitivului pentru instrucțiuni exacte.</li>
                        </ul>
                        <p>Dacă ați terminat pachetul și aveți nevoie de mai multe date, cumpărați orice pachet la alegere și repetați pașii de activare.</p>
                        <p><br></p>',
                ]
            ]
        ]
    ];