<?php
return [
    'cart_regional' => [
        'title' => 'Oferte regionale',
        'desc' => 'Găsiți cele mai bune oferte ESIM pentru călătoria dvs',
        'regional' => [
            'asia' => [
                'title' => 'Asia',
                'desc' => 'Împărtășiți momente de pe plajele asiatice cu pachete generoase de date',
            ],
            'middle-east' => [
                'title' => 'Orientul Mijlociu',
                'desc' => 'De la deșert la zgârie-norii din Orientul Mijlociu, păstrați legătura',
            ],
            'oceania' => [
                'title' => 'Oceania',
                'desc' => 'Rămâneți conectat jos',
            ],
            'america' => [
                'title' => 'America de Nord și de Sud',
                'desc' => 'Rămâi conectat în America',
            ],
            'europe' => [
                'title' => 'Europa',
                'desc' => 'Rămâneți conectat în Europa',
            ],
            'africa' => [
                'title' => 'Africa',
                'desc' => 'Rămâneți conectat în junglă, frumusețea naturală nesfârșită și priveliștile asupra stâncilor din Africa',
            ],
        ]
    ],
    'travel' => [
        'title' => ':name eSIM oferă',
        'desc' => 'Cele mai bune oferte eSIM pentru :name de la cei mai buni furnizori din întreaga lume',
        'sub_desc' => 'Se încarcă oferte eSIM de călătorie pentru :name',
        'search' => 'Căutare',
    ]
];