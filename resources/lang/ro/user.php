<?php
    return [
        'title_page' => "Order History",
        'not_order' => "Comenzi nu au fost găsite",
        'title_order' => "Ordin",
        'order' => [
            'order_id' => 'Comanda ID',
            'product' => 'Produs',
            'date' => 'Data',
            'status' => 'Stare',
            'price' => 'Preț ($)',
        ],
    ];