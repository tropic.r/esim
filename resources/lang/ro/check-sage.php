<?php 
return [
    'title_page' => "Verificarea stării și utilizării eSIM",
    'text1' => "Puteți vedea utilizarea datelor direct din setările telefonului.",
    'text2' => "Alternativ, introduceți numărul dvs. de comandă:",
    'button' => "Verifica",

    'provider_name' => "Numele furnizorului",
    'plan_name' => "Numele planului",
    'plan_data' => "Datele planului",
    'validity' => "Valabilitate",
    'days' => "Zile"
];