<?php
return [
    'desc' => 'Sineco Invest SRL адрес:Bd. Mircea cel Bătrân 22/5, of.200',
    'quick' => [
        'title' => 'Legături rapide',
        'home' => 'Acasă',
        'terms-condition' => 'Termeni',
        'privacy-policy' => 'Politica de Confidențialitate',
        'delivery-refunds' => 'Politica de livrare, returnare și anulare',
        'sitemap' => 'Harta site-ului',
    ],
    'regions' => [
        'title' => 'Regiuni',
        'europe' => 'eSIM pentru Europa',
        'asia' => 'eSIM pentru Asia',
        'america' => 'eSIM pentru America',
        'middle-east' => 'eSIM pentru Orientul Mijlociu',
        'oceania' => 'eSIM pentru Oceania',
        'africa' => 'eSIM pentru Africa',
    ],
    'countries' => [
        'title' => 'Țări',
        'united-states' => 'eSIM pentru SUA',
        'japan' => 'eSIM pentru Japonia',
        'canada' => 'eSIM pentru Canada',
        'spain' => 'eSIM pentru Spania',
        'italy' => 'eSIM pentru Italia',
        'united-kingdom' => 'eSIM pentru Regatul Unit',
        'jordan' => 'eSIM pentru Iordania',
        'switzerland' => 'eSIM pentru Elveția',
        'saudi-arabia' => 'eSIM pentru Arabia Saudită',
        'netherlands' => 'eSIM pentru Țările de Jos',
        'turkey' => 'eSIM pentru Turcia',
        'sitemap' => 'Pentru a afla mai multe…',
    ],
    'follow_us_on' => 'Urmareste-ne pe'
];