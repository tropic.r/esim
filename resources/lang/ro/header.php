<?php return [
    'currents' => 'valută',
    'menu' => [
        'faq' => 'Ajutor și întrebări frecvente',
        'check_usage' => 'Verificați utilizarea',
        'shop' => 'ESIM Magazin',
        'shop_buy_esim' => 'Cumpărați eSIM',
        'shop_up_esim' => 'Încărcați eSIM',
        'shop_region' => 'Cumpărați după regiune',
        'login' => 'Intrare',
        'order' => 'Istoric comenzi',
        'logout' => 'Deconectare',
    ],
    'regions' => [
        'europe' => 'Europa',
        'asia' => 'Asia',
        'middle-east' => 'Orientul Mijlociu',
        'america' => 'America',
        'africa' => 'Africa',
        'oceania' => 'Oceania',
    ]
];