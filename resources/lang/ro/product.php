<?php 
return [
    'popular' => [
        'title' => 'Oferte populare',
        'desc' => 'Cele mai vândute oferte eSIM afișate'
    ],
    'top_up' => [
        'title' => 'Sprijinirea furnizorilor de reîncărcare',
        'desc' => 'Încărcați-vă eSIM Mobimatter cu același pachet sau cu un alt pachet de la acești furnizori.',
        'readmore' => 'Vezi oferte'
    ],
    'top_popular' => 'Oferte populare de reîncărcare',
    'mini' => [
        'readmore' => 'Vezi oferta',
        'price' => 'Preț:',
        'data' => 'Date:',
        'validity' => 'Valabilitate:',
        'date_d' => 'zile',
        'works_in' => 'Lucrează în',
        'less_three_countries' => 'numai',
        'more_three_countries' => 'alte țări',
    ],
    'cart' => [
        'detail' => 'Detalii',
        'rabota' => 'Lucrează în',
        'count' => 'Cantitatea x',
        'free' => 'Gratuit',
        'total' => 'Total',
        'buy' => 'Cumpără',
        'createOrder' => 'Se creează o comandă...',
        'search_buy' => 'Caut alta oferta',
        'invistion' => 'Inventarul depozitului',
        'invistion_text' => 'Se pare că suntem în stoc pentru acest pachet. <br> Avem multe oferte pentru tine.',
        'booking' => 'Rezervarea ESIM...'
    ],
    'faq' => [
        'title' => 'Întrebări frecvente',
        'faq_1' => [
            'title' => 'Ce smartphone-uri sunt eligibile pentru ESIM?',
            'desc' => '<p><span class="text-primary">A:</span> iPhone: seria XR, XS, 11, 12, 13 și SE 2020; Google Pixel 3 și versiuni ulterioare; Seria Samsung S20, Flex și Flip; seria Huawei P40; iPad-uri: 10.2, aer condiționat, Mini 2019, Pro 11, Pro 12.9.</p><p>Sunt acceptate și ceasurile inteligente cu capacitate eSIM (cu excepția Apple Watchului)</p><p>Notă: unele dintre aceste dispozitive au variante care nu acceptă ESIM, vă rugăm să verificați <a href="/faqs" target="_blank">Ajutor și Întrebări frecvente</a> dacă aveți îndoieli.</p>',
        ],
        'faq_2' => [
            'title' => 'Pot folosi un SIM fizic cu un ESIM?',
            'desc' => '<p><span class="text-primary">A:</span> Da! Cu funcționalitatea dual SIM, puteți avea atât SIM-ul fizic, cât și ESIM-ul activ în același timp.</p>',
        ],
        'faq_3' => [
            'title' => 'Am deja un ESIM activ în telefon, pot folosi serviciile dvs.?',
            'desc' => '<p><span class="text-primary">R:</span> Da, telefonul dvs. poate stoca mai multe profiluri ESIM simultan. Puteți alege unul pe care să îl utilizați în setările telefonului dvs.</p>',
        ],
        'faq_4' => [
            'title' => 'Am alte intrebari',
            'desc' => '<p><span class="text-primary">A:</span> Pune-ne orice întrebare folosind funcția noastră de chat sau trimite-ne un e-mail la info@esim.md. Vom fi bucuroși să vă ajutăm.</p>',
        ],
    ]
];