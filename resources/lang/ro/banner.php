<?php return [
    'title' => "Cumpărați cele mai bune <strong>oferte mobile</strong> acasă și când călătoriți",
    'top_up_your_esim' => "Încărcați-vă eSIM-ul",
    'buy_a_new_eSIM' => "Cumpărați un eSIM nou",
    'button' => "Căutare",
    'jump_to' => "Sari la",
    'travel_esim_store' => "Magazin eSIM de călătorie",
    'search_for_a_country' => "Căutați o țară",
];