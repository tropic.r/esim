<div style="margin:0px;min-width:320px">
  <table cellpadding="0" cellspacing="0" width="760" align="center" style="padding:50px 15px 0px;border-collapse:separate;font-size:14px;line-height:1.3;margin:0px auto;color:rgb(0,0,0)">
    <tbody>
      <tr>
        <td style="padding-bottom:20px">
          <table style="width:100%">
            <tbody>
              <tr>
                <td style="padding:10px 0px 5px 15px;text-align:center">
                  <img src="{{env('APP_URL')}}/images/logo.png" alt="icon" style="width:190px;border:0px none" class="CToWUd a6T" data-bit="iit" tabindex="0">
                </td>
              </tr>
            </tbody>
          </table>
        </td>
      </tr>
      <tr>
        <td style="font-weight:500;font-size:18px;padding:0px 0px 10px">{{ __('order.mail.order.body.title', ['name'=>$order->name]) }}</td>
      </tr>
      <tr>
        <td style="padding:10px 0px;border-bottom-width:1px;border-bottom-style:solid;border-bottom-color:rgb(242,246,248)">
          <table cellpadding="0" cellspacing="0" style="width:100%">
            <tbody><tr>
              <td style="width:50%">
                <table cellpadding="0" cellspacing="0">
                  <tbody><tr>
                    <td>
                        <img src="{{ $order->product->provider->attachment->url() }}" alt="icon" style="width:45px;border:0px none" class="CToWUd">
                    </td>
                    <td style="padding-left:20px">
                      <span style="font-weight:500;font-size:18px;display:block;margin-bottom:5px">{{ $order->product->translation->name }}</span>
                      <p style="font-weight:500;margin-bottom:5px;color:rgb(149,155,148)">{{ $order->product->provider->name }}</p>
                      <p style="margin-bottom:5px">{{ __('order.mail.order.body.order_id') }} <strong style="color:rgb(0,174,239)">{{ $order->orderId }}</strong></p>
                      <p style="margin-bottom:5px">{{ __('order.mail.order.body.paid') }} <strong style="color:rgb(0,174,239)">{{ $order->currencyCode }} {{ $order->retailPrice }}</strong></p>
                    </td>
                  </tr>
                </tbody></table>
              </td>
              <td style="width:50%;text-align:right">{{ __('order.mail.order.body.date') }} <strong style="color:rgb(0,174,239)">{{ date('d.m.Y',strtotime($order->created_at)) }}</strong></td>
            </tr>
          </tbody></table>
        </td>
      </tr>
      <tr>
        <td style="padding:10px 0px">
          <table cellpadding="0" cellspacing="0" style="width:100%">
            <tbody><tr>
              <td style="width:50%">
                <strong style="font-size:16px;display:block;margin-bottom:5px">{{ __('order.mail.order.body.item') }}</strong>
                <p style="margin-bottom:5px">{{ $order->product->translation->name }}</p>
                <p style="margin-bottom:5px">eSIM</p>
                <hr style="margin-bottom:5px">
                <p style="margin-bottom:5px">VAT</p>
                <strong style="display:block;margin-bottom:5px;color:rgb(36,181,2)">{{ __('order.mail.order.body.total') }}</strong>
              </td>
              <td style="width:50%;text-align:right">
                <strong style="font-size:16px;display:block;margin-bottom:5px">{{ __('order.mail.order.body.аmount') }} ({{ $order->currencyCode }})</strong>
                <p style="margin-bottom:5px">{{ $order->retailPrice }}</p>
                <p style="margin-bottom:5px">0.00</p>
                <hr style="margin-bottom:5px">
                <p style="margin-bottom:5px">0.00</p>
                <strong style="display:block;margin-bottom:5px;color:rgb(36,181,2)">{{ $order->currencyCode }} {{ $order->retailPrice }}</strong>
              </td>
            </tr>
          </tbody></table>
        </td>
      </tr>
      <tr>
        <td style="font-weight:500;font-size:18px;padding:0px 0px 10px;text-align:center">
          {{ __('order.mail.order.body.need_an_invoice') }}
        </td>
      </tr>
      <tr>
        <td style="font-weight:500;font-size:18px;padding:0px 0px 10px"> {{ __('order.mail.order.body.esim_activation_steps') }}</td>
      </tr>
      <tr>
        <td style="text-align:center;padding:10px 0px 20px">
          <img src="{{$order->lineItemDetails['QR_CODE']}}" alt="icon" style="max-width:300px;width:100%;border:0px none" class="CToWUd a6T" data-bit="iit" tabindex="0">
        </td>
      </tr>
      <tr style="height:20px;line-height:20px;font-size:0px">
        <td></td>
      </tr>
      <tr>
        <td>
          <table cellpadding="0" cellspacing="0" style="width:100%;text-align:center;border:2px solid rgb(0,174,239)">
            <tbody>
              <tr>
                <td style="padding:10px">
                  <p>
                    <img src="https://ci4.googleusercontent.com/proxy/jP5jF5nkiEok6vNS70obB4nfETtA3HQTavvz-T9TTAjpyQgwNWOneNlOj2iSe7XXwq8OHKRo9ixsmajGqAhuhMSb9QX9J_coTItC-6zflluljAB5MuMXP0qP_UQYY8o=s0-d-e1-ft#https://mobimatterstagingstorage.blob.core.windows.net/static/danger-img.png" alt="icon" style="width:50px;border:0px none" class="CToWUd" data-bit="iit">
                  </p>
                  <strong style="padding-bottom:10px;display:block;width:100%">{{ __('order.mail.order.body.important_note') }}</strong>
                  <p style="margin-bottom:10px">{{ __('order.mail.order.body.important_note_p1') }}</p>
                  <p style="margin-bottom:10px">{{ __('order.mail.order.body.important_note_p2') }}</p>
                  <p style="margin-bottom:10px">{{ __('order.mail.order.body.important_note_p3') }}</p>
                </td>
              </tr>
            </tbody>
          </table>
        </td>
      </tr>
      <tr style="height:20px;line-height:20px;font-size:0px">
        <td></td>
      </tr>
      <tr>
        <td>
          <strong>{{ __('order.mail.order.body.esim_profile_info') }}</strong>
          <br>{{ __('order.mail.order.body.phone_number') }} {{$order->lineItemDetails['PHONE_NUMBER']}}
          <br>{{ __('order.mail.order.body.apn') }} {{$order->lineItemDetails['ACCESS_POINT_NAME']}}
          <br>{{ __('order.mail.order.body.manual_installation_for_ios') }}
          <ul>
            <li>{{ __('order.mail.order.body.sm_dp_address') }} <a href="{{$order->lineItemDetails['SMDP_ADDRESS']}}" target="_blank">{{$order->lineItemDetails['SMDP_ADDRESS']}}</a></li>
            <li>{{ __('order.mail.order.body.activation_code') }} {{$order->lineItemDetails['ACTIVATION_CODE']}}</li>
          </ul>
          {{ __('order.mail.order.body.manual_installation_for_android') }}
          <ul>
            <li>{{ __('order.mail.order.body.activation_code') }} <a href="{{$order->lineItemDetails['LOCAL_PROFILE_ASSISTANT']}}" target="_blank">{{$order->lineItemDetails['LOCAL_PROFILE_ASSISTANT']}}</a></li>
          </ul>
          <br><b>{{ __('order.mail.order.body.your_package_details') }}</b>
          <ul><li>{{ __('order.mail.order.body.data') }} {{ $order->product->plan_data_limit }} {{ $order->product->plan_data_unit }}</li>
          <li>{{ __('order.mail.order.body.validity') }} {{ $order->product->plan_validity / 24 }} {{ __('product.mini.date_d') }}</li></ul>
          {{ __('order.mail.order.body.your_package_validity_esim') }}
          <p></p>
          <p>{{ __('order.mail.order.body.to_check_your_balance_visit') }} <a href="{{ route('check-usage') }}" target="_blank">{{ route('check-usage') }}</a></p>
          <p></p>
        </td>
      </tr>
      <tr>
        <td>
          {!! __('order.mail.order.body.instructions_esim_download_and_activation') !!}
        </td>
      </tr>
      <tr style="height:20px;line-height:20px;font-size:0px">
        <td></td>
      </tr>
    </tbody>
  </table>
</div>