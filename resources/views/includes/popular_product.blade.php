
<div class="card-popular-offers mb-4 mb-md-5">
    <h2 class="h4 font-weight-500 text-dark">{{ __('product.popular.title') }}</h2>
    <p class="text-dark-light">{{ __('product.popular.desc') }}</p>
    <div class="grid font-size-12" id="offer_section">
        @php
            $i = 0;
        @endphp
        @foreach($products as $product)
            @php
                $i++;
                $product->key = $i; 
            @endphp
            @include('pages.travel.mini_product', $product)
        @endforeach
    </div>
</div>