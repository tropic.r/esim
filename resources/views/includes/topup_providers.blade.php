<section class="py-4">
    <div class="container">
        <div class="card-popular-offers mb-4 mb-md-5">
            <h2 class="h4 font-weight-500 text-dark">{{ __('product.top_up.title') }}</h2>
            <p class="text-dark-light">{{ __('product.top_up.desc') }}</p>
            <div class="grid font-size-12" id="offer_section">
                @foreach($providers as $provider)
                    <a href="{{ route('topup_catalog',['provider'=>$provider->slug]) }}" class="grid-item text-decoration-none text-body">
                        <div class="d-flex align-items-start justify-content-between mb-3">
                            <div class="d-flex align-items-start">
                                <img class="flex-shrink-0" src="{{ $provider->attachment->url() }}" alt="img" width="64">
                                <div class="d-inline-block pl-2">
                                    <span class="font-weight-500 d-block font-size-18">{{ $provider->name }}</span>
                                    <span class="text-dark-light"></span>
                                </div>
                            </div>
                        </div>
                        <div class="btn btn-primary btn-block">{{ __('product.top_up.readmore') }}</div>
                    </a>
                @endforeach
            </div>
        </div>
    </div>
</section>