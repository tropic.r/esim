<header id="header">
<div class="topbar d-none d-md-block">
        <div class="container-fluid">
            <ul class="list-unstyled d-flex align-items-center justify-content-end m-0 p-0">
                <li class="nav-item dropdown">
                    <div class="d-flex align-items-center">
                        <span>{{ __('header.currents') }}:</span>
                        <select class="custom-select multi-currency-select" id="select-multi-currency" style="width: 93.2px;">
                            @foreach($currencys as $currency)
                                <option value="{{ $currency->name }}" data-price="{{ $currency->value }}">{{ $currency->name }} - {{ $currency->iso }}</option>
                            @endforeach
                            
                        </select>
                    </div>
                </li>
                <li class="nav-item">
                    <a rel="alternate" hreflang="en_GB" href="{{ getLocaleUrl('en') }}" @if (isLocaleActive('en')) class="active" @endif>EN</a>
                    <a rel="alternate" hreflang="ro_RO" href="{{ getLocaleUrl('ro') }}" @if (isLocaleActive('ro')) class="active" @endif>RO</a>
                    <a rel="alternate" hreflang="ru_RU" href="{{ getLocaleUrl('ru') }}" @if (isLocaleActive('ru')) class="active" @endif>RU</a>
                </li>
            </ul>
        </div>
    </div>
    <nav class="navbar navbar-expand-md navbar-toggleable-md navbar-light bg-white box-shadow" id="navbar_top">
        <div class="container-fluid">
            <a class="navbar-brand" href="{{ route('home') }}">
                <img src="/images/logo.png" style="height: 46px;">
            </a>
            @auth
                <a class="nav-link navbar-toggler" href="{{ route('profile.index') }}"style="font-size: 0.9rem;color: var(--primary-color-defult);">
                    <img class="avatar__image" src="/images/35B520FF30FC9FC335826BFA7A0F40A100196217B019A4CA09499FA7AF81C801.png">{{ auth()->user()->name }}
                </a>
            @else
                <a class="nav-link navbar-toggler" href="{{ route('auth.login') }}"style="font-size: 0.9rem;color: var(--primary-color-defult);">
                    {{ __('header.menu.login') }}
                </a>
            @endauth
            <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target=".navbar-collapse" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="bar1"></span><span class="bar2"></span><span class="bar3"></span>
            </button>
            <div class="navbar-collapse collapse d-md-inline-flex order-md-3">
                <ul class="navbar-nav flex-grow-1 justify-content-start justify-content-md-end align-items-center px-0">
                    <li class="nav-item" id="faqs">
                        <a href="{{ route('faqs') }}">{{ __('header.menu.faq') }}</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('check-usage') }}">{{ __('header.menu.check_usage') }}</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link d-flex align-items-center text-dark" href="user_dropdownMenu_2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            {{ __('header.menu.shop') }}
                            <svg class="ml-2" xmlns="http://www.w3.org/2000/svg" width="9.02" height="6.297" viewBox="0 0 9.02 6.297">
                                <path id="Path_2774" data-name="Path 2774" d="M937.06,39.291l3.492,4.079,3.414-4.079" transform="translate(-936.003 -38.233)" fill="none" stroke="#00aeef" stroke-linecap="round" stroke-width="1.5" />
                            </svg>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right p-0" aria-labelledby="user_dropdownMenu_2">
                            <a class="dropdown-item" href="{{ route('home') }}">{{ __('header.menu.shop_buy_esim') }}</a>
                            <a class="dropdown-item" href="{{ route('topup') }}">{{ __('header.menu.shop_up_esim') }}</a>
                        </div>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link d-flex align-items-center text-dark" href="shopbyregion_dropdownMenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            {{ __('header.menu.shop_region') }}
                            <svg class="ml-2" xmlns="http://www.w3.org/2000/svg" width="9.02" height="6.297" viewBox="0 0 9.02 6.297">
                                <path id="Path_2774" data-name="Path 2774" d="M937.06,39.291l3.492,4.079,3.414-4.079" transform="translate(-936.003 -38.233)" fill="none" stroke="#00aeef" stroke-linecap="round" stroke-width="1.5" />
                            </svg>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right p-0" aria-labelledby="shopbyregion_dropdownMenu">
                            <a href="{{ route('travel-esim', 'europe') }}" class="dropdown-item">{{ __('header.regions.europe') }}</a>
                            <a href="{{ route('travel-esim', 'asia') }}" class="dropdown-item">{{ __('header.regions.asia') }}</a>
                            <a href="{{ route('travel-esim', 'middle-east') }}" class="dropdown-item">{{ __('header.regions.middle-east') }}</a>
                            <a href="{{ route('travel-esim', 'america') }}" class="dropdown-item">{{ __('header.regions.america') }}</a>
                            <a href="{{ route('travel-esim', 'africa') }}" class="dropdown-item">{{ __('header.regions.africa') }}</a>
                            <a href="{{ route('travel-esim', 'oceania') }}" class="dropdown-item">{{ __('header.regions.oceania') }}</a>
                        </div>
                    </li>
                                       
                    @auth
                        <li class="nav-item dropdown shopbyregion-dropdown">
                            <a class="nav-link d-flex align-items-center text-dark" href="user_dropdownMenu_2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                {{ auth()->user()->name }}
                                <svg class="ml-2" xmlns="http://www.w3.org/2000/svg" width="9.02" height="6.297" viewBox="0 0 9.02 6.297">
                                    <path id="Path_2774" data-name="Path 2774" d="M937.06,39.291l3.492,4.079,3.414-4.079" transform="translate(-936.003 -38.233)" fill="none" stroke="#00aeef" stroke-linecap="round" stroke-width="1.5"></path>
                                </svg>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right p-0" aria-labelledby="user_dropdownMenu_2">
                                <a class="dropdown-item" href="{{ route('profile.index') }}">{{ __('header.menu.order') }}</a>
                                <a class="dropdown-item" href="{{route('logout')}}" id="logout-button">{{ __('header.menu.logout') }}</a>
                            </div>
                        </li>
                    @else
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('auth.login') }}">{{ __('header.menu.login') }}</a>
                        </li>
                    @endauth
                </ul>
            </div>
        </div>
    </nav>
</header>