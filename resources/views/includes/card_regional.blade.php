<div class="card-regional-offers">
    <h2 class="h4 font-weight-500 text-dark">{{ __('catalog.cart_regional.title') }}</h2>
    <p class="text-dark-light">{{ __('catalog.cart_regional.desc') }}</p>
    <div class="grid regional-offers-grid">
        <a href="{{ route('travel-esim',['slug' => 'asia']) }}" class="items text-decoration-none">
            <figure><img class="lozad" data-src="/images/asia.jpg" alt="image"></figure>
            <h4>{{ __('catalog.cart_regional.regional.asia.title') }}</h4>
            <p>{{ __('catalog.cart_regional.regional.asia.desc') }}</p>
        </a>
        <a href="{{ route('travel-esim',['slug' => 'middle-east']) }}" class="items text-decoration-none">
            <figure><img class="lozad" data-src="/images/middle-east.jpg" alt="image"></figure>
            <h4>{{ __('catalog.cart_regional.regional.middle-east.title') }}</h4>
            <p>{{ __('catalog.cart_regional.regional.middle-east.desc') }}</p>
        </a>
        <a href="{{ route('travel-esim',['slug' => 'oceania']) }}" class="items text-decoration-none">
            <figure><img class="lozad" data-src="/images/oceania.jpg" alt="image"></figure>
            <h4>{{ __('catalog.cart_regional.regional.oceania.title') }}</h4>
            <p>{{ __('catalog.cart_regional.regional.oceania.desc') }}</p>
        </a>
        <a href="{{ route('travel-esim',['slug' => 'america']) }}" class="items text-decoration-none">
            <figure><img class="lozad" data-src="/images/america.jpg" alt="image"></figure>
            <h4>{{ __('catalog.cart_regional.regional.america.title') }}</h4>
            <p>{{ __('catalog.cart_regional.regional.america.desc') }}</p>
        </a>
        <a href="{{ route('travel-esim',['slug' => 'europe']) }}" class="items text-decoration-none">
            <figure><img class="lozad" data-src="/images/europe.jpg" alt="image"></figure>
            <h4>{{ __('catalog.cart_regional.regional.europe.title') }}</h4>
            <p>{{ __('catalog.cart_regional.regional.europe.desc') }}</p>
        </a>
        <a href="{{ route('travel-esim',['slug' => 'africa']) }}" class="items text-decoration-none">
            <figure><img class="lozad" data-src="/images/africa.jpg" alt="image"></figure>
            <h4>{{ __('catalog.cart_regional.regional.africa.title') }}</h4>
            <p>{{ __('catalog.cart_regional.regional.africa.desc') }}</p>
        </a>
    </div>
</div>