<section class="py-4">
    <div class="container">
        <div class="card-popular-offers mb-4 mb-md-5">
            <h2 class="h4 font-weight-500 text-dark">{{ __('product.top_popular') }}</h2>
            <div class="grid font-size-12" id="offer_section">
            @php
                $i = 0;
            @endphp
            @foreach($products as $product)
                @php
                    $i++;
                    $product->key = $i; 
                @endphp
                    @include('pages.travel.mini_product', $product)
                @endforeach
            </div>
        </div>
    </div>
</section>