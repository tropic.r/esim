<footer id="footer">
    <div class="container">
        <div class="row border-bottom py-3 py-md-4">
            <div class="col-md-6 mb-3">

                <p>{{ __('footer.desc') }}</p>
                <span class="d-block">Email: <a href="mailto:info@esim.md">info@esim.md</a>, <a href="mailto:sales@esim.md">sales@esim.md</a>, <a href="mailto:admin@esim.md">admin@esim.md</a></span>
                
            </div>
            <div class="col-md-6 text-left">
                <div class="row">
                    <div class="col-lg-4 mb-3">
                        <span class="nav-title">{{ __('footer.quick.title') }}</span>
                        <ul class="v-list">
                            <li><a href="{{ route('home') }}">{{ __('footer.quick.home') }}</a></li>
                            <li><a href="{{ route('page',['slug'=>'terms-condition']) }}" target="_blank">{{ __('footer.quick.terms-condition') }}</a></li>
                            <li><a href="{{ route('page',['slug'=>'privacy-policy']) }}" target="_blank">{{ __('footer.quick.privacy-policy') }}</a></li>
                            <li><a href="{{ route('page',['slug'=>'delivery-refunds']) }}" target="_blank">{{ __('footer.quick.delivery-refunds') }}</a></li>
                            <li><a href="{{ route('sitemap') }}" target="_blank">{{ __('footer.quick.sitemap') }}</a></li>
                        </ul>
                    </div>
                    <div class="col-lg-4 mb-3">
                        <span class="nav-title">{{ __('footer.regions.title') }}</span>
                        <ul class="v-list">
                            <li><a href="{{ route('travel-esim',['slug' => 'europe']) }}">{{ __('footer.regions.europe') }}</a></li>
                            <li><a href="{{ route('travel-esim',['slug' => 'asia']) }}">{{ __('footer.regions.asia') }}</a></li>
                            <li><a href="{{ route('travel-esim',['slug' => 'america']) }}">{{ __('footer.regions.america') }}</a></li>
                            <li><a href="{{ route('travel-esim',['slug' => 'middle-east']) }}">{{ __('footer.regions.middle-east') }}</a></li>
                            <li><a href="{{ route('travel-esim',['slug' => 'oceania']) }}">{{ __('footer.regions.oceania') }}</a></li>
                            <li><a href="{{ route('travel-esim',['slug' => 'africa']) }}">{{ __('footer.regions.africa') }}</a></li>
                        </ul>
                    </div>
                    <div class="col-lg-4 mb-3">
                        <span class="nav-title">{{ __('footer.countries.title') }}</span>
                        <ul class="v-list">
                            <li><a href="{{ route('travel-esim',['slug' => 'united-states']) }}">{{ __('footer.countries.united-states') }}</a></li>
                            <li><a href="{{ route('travel-esim',['slug' => 'japan']) }}">{{ __('footer.countries.japan') }}</a></li>
                            <li><a href="{{ route('travel-esim',['slug' => 'canada']) }}">{{ __('footer.countries.canada') }}</a></li>
                            <li><a href="{{ route('travel-esim',['slug' => 'spain']) }}">{{ __('footer.countries.spain') }}</a></li>
                            <li><a href="{{ route('travel-esim',['slug' => 'italy']) }}">{{ __('footer.countries.italy') }}</a></li>
                            <li><a href="{{ route('travel-esim',['slug' => 'united-kingdom']) }}">{{ __('footer.countries.united-kingdom') }}</a></li>
                            <li><a href="{{ route('travel-esim',['slug' => 'jordan']) }}">{{ __('footer.countries.jordan') }}</a></li>
                            <li><a href="{{ route('travel-esim',['slug' => 'switzerland']) }}">{{ __('footer.countries.switzerland') }}</a></li>
                            <li><a href="{{ route('travel-esim',['slug' => 'saudi-arabia']) }}">{{ __('footer.countries.audi-arabia') }}</a></li>
                            <li><a href="{{ route('travel-esim',['slug' => 'netherlands']) }}">{{ __('footer.countries.netherlands') }}</a></li>
                            <li><a href="{{ route('travel-esim',['slug' => 'turkey']) }}">{{ __('footer.countries.turkey') }}</a></li>
                            <li><a href="{{ route('sitemap') }}">{{ __('footer.countries.sitemap') }}</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="f-bottom-area py-3 py-md-4">
            <p class="mb-2 order-3 order-md-1">Copyright @ {{ date('Y') }} </p>
            <div class="d-flex align-items-center justify-content-center mb-2 order-2">
                <span>{{ __('footer.follow_us_on') }}</span>
                <ul class="h-list pl-3">
                    <li>
                        <a href="https://www.facebook.com/eSIM.md/" target="_blank">
                            <svg version="1.1" width="20px" height="20px" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 167.657 167.657">
                                <g>
                                    <path style="fill:#3a589b;" d="M83.829,0.349C37.532,0.349,0,37.881,0,84.178c0,41.523,30.222,75.911,69.848,82.57v-65.081H49.626
                                    v-23.42h20.222V60.978c0-20.037,12.238-30.956,30.115-30.956c8.562,0,15.92,0.638,18.056,0.919v20.944l-12.399,0.006
                                    c-9.72,0-11.594,4.618-11.594,11.397v14.947h23.193l-3.025,23.42H94.026v65.653c41.476-5.048,73.631-40.312,73.631-83.154
                                    C167.657,37.881,130.125,0.349,83.829,0.349z"></path>
                                </g>
                            </svg>
                        </a>
                    </li>
                    <li>
                        <a href="https://www.instagram.com/esim.md/" target="_blank">
                            <svg width="20px" height="20px" version="1.1" viewBox="0 0 128 128" xmlns="http://www.w3.org/2000/svg">
                                <g>
                                    <rect clip-rule="evenodd" fill="none" fill-rule="evenodd" height="128" width="128"></rect>
                                    <radialGradient cx="19.1111" cy="128.4444" gradientUnits="userSpaceOnUse" id="Instagram_2_" r="163.5519">
                                        <stop offset="0" style="stop-color:#FFB140"></stop>
                                        <stop offset="0.2559" style="stop-color:#FF5445"></stop>
                                        <stop offset="0.599" style="stop-color:#FC2B82"></stop>
                                        <stop offset="1" style="stop-color:#8E40B7"></stop>
                                    </radialGradient>
                                    <path clip-rule="evenodd" d="M105.843,29.837    c0,4.242-3.439,7.68-7.68,7.68c-4.241,0-7.68-3.438-7.68-7.68c0-4.242,3.439-7.68,7.68-7.68    C102.405,22.157,105.843,25.595,105.843,29.837z M64,85.333c-11.782,0-21.333-9.551-21.333-21.333    c0-11.782,9.551-21.333,21.333-21.333c11.782,0,21.333,9.551,21.333,21.333C85.333,75.782,75.782,85.333,64,85.333z M64,31.135    c-18.151,0-32.865,14.714-32.865,32.865c0,18.151,14.714,32.865,32.865,32.865c18.151,0,32.865-14.714,32.865-32.865    C96.865,45.849,82.151,31.135,64,31.135z M64,11.532c17.089,0,19.113,0.065,25.861,0.373c6.24,0.285,9.629,1.327,11.884,2.204    c2.987,1.161,5.119,2.548,7.359,4.788c2.24,2.239,3.627,4.371,4.788,7.359c0.876,2.255,1.919,5.644,2.204,11.884    c0.308,6.749,0.373,8.773,0.373,25.862c0,17.089-0.065,19.113-0.373,25.861c-0.285,6.24-1.327,9.629-2.204,11.884    c-1.161,2.987-2.548,5.119-4.788,7.359c-2.239,2.24-4.371,3.627-7.359,4.788c-2.255,0.876-5.644,1.919-11.884,2.204    c-6.748,0.308-8.772,0.373-25.861,0.373c-17.09,0-19.114-0.065-25.862-0.373c-6.24-0.285-9.629-1.327-11.884-2.204    c-2.987-1.161-5.119-2.548-7.359-4.788c-2.239-2.239-3.627-4.371-4.788-7.359c-0.876-2.255-1.919-5.644-2.204-11.884    c-0.308-6.749-0.373-8.773-0.373-25.861c0-17.089,0.065-19.113,0.373-25.862c0.285-6.24,1.327-9.629,2.204-11.884    c1.161-2.987,2.548-5.119,4.788-7.359c2.239-2.24,4.371-3.627,7.359-4.788c2.255-0.876,5.644-1.919,11.884-2.204    C44.887,11.597,46.911,11.532,64,11.532z M64,0C46.619,0,44.439,0.074,37.613,0.385C30.801,0.696,26.148,1.778,22.078,3.36    c-4.209,1.635-7.778,3.824-11.336,7.382C7.184,14.3,4.995,17.869,3.36,22.078c-1.582,4.071-2.664,8.723-2.975,15.535    C0.074,44.439,0,46.619,0,64c0,17.381,0.074,19.561,0.385,26.387c0.311,6.812,1.393,11.464,2.975,15.535    c1.635,4.209,3.824,7.778,7.382,11.336c3.558,3.558,7.127,5.746,11.336,7.382c4.071,1.582,8.723,2.664,15.535,2.975    C44.439,127.926,46.619,128,64,128c17.381,0,19.561-0.074,26.387-0.385c6.812-0.311,11.464-1.393,15.535-2.975    c4.209-1.636,7.778-3.824,11.336-7.382c3.558-3.558,5.746-7.127,7.382-11.336c1.582-4.071,2.664-8.723,2.975-15.535    C127.926,83.561,128,81.381,128,64c0-17.381-0.074-19.561-0.385-26.387c-0.311-6.812-1.393-11.464-2.975-15.535    c-1.636-4.209-3.824-7.778-7.382-11.336c-3.558-3.558-7.127-5.746-11.336-7.382c-4.071-1.582-8.723-2.664-15.535-2.975    C83.561,0.074,81.381,0,64,0z" fill="url(#Instagram_2_)" fill-rule="evenodd"></path>
                                </g>
                            </svg>
                        </a>
                    </li>
                </ul>
            </div>
            <ul class="pay-list pl-md-2 h-list mb-2 order-2 order-md-3">
                <li><a href="#"><img loading="lazy" src="/images/visa-pay-2.png" alt="icon" style="height:15px !important;"></a></li>
                <li><a href="#"><img loading="lazy" src="/images/mm-pay-2.png" alt="icon" style="height:15px !important;"></a></li>
            </ul>
        </div>
    </div>
</footer>