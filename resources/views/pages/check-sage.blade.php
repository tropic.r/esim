@extends('layouts.app')
@include('meta::manager', [])

@push('before-styles')
    <link rel="stylesheet" href="/css/global.css" />
@endpush

@section('content')
    <section class="py-4">
        <div class="container">
            <h2 class="h4 font-weight-500">{{ __('check-sage.title_page') }}</h2>
            <p>{{ __('check-sage.text1') }}</p>
            <p>{{ __('check-sage.text2') }}</p>
            <form id="search-form" class="search-form" method="get" action="{{ route('check-usage') }}">
                <div class="form-group d-md-flex">
                    <input type="text" placeholder="MM-12345" class="form-control country-search" id="searchbar"  name="orderId" value="{{ isset($orderId) ? $orderId : '' }}">
                    </input>
                    <div class="input-group-append pt-3 pt-md-0 ml-0 ml-md-3">
                        <button onclick="CheckBalance()" id="searchButton" type="button" class="btn btn-primary btn-block">{{ __('check-sage.button') }}</button>
                    </div>
                </div>
            </form>
            <div>
                @if($data)
                    <div class="card-body side-bar font-size-14 p-3">
                        <ul class="v-list justify-content-between flex-wrap text-break">
                            <li>
                                <span>
                                    <img src="{{ $data->provider->attachment->url() }}" width="auto" height="65">
                                </span>
                            </li>
                            <li>
                                <span>{{ __('check-sage.provider_name') }}: </span>
                                <span class="font-weight-500 text-primary">{{ $data->provider->name }}</span>
                            </li>
                            <li>
                                <span>{{ __('check-sage.plan_name') }}: </span>
                                <span class="font-weight-500 text-primary">{{ $data->translation->name }}</span>
                            </li>   
                            <li>
                                <span>{{ __('check-sage.plan_data') }}: </span>
                                <span class="font-weight-500 text-primary">{{ $data->plan_data_limit }} {{ $data->plan_data_unit }}</span>
                            </li>
                            <li>
                                <span>{{ __('check-sage.validity') }}: </span>
                                <span class="font-weight-500 text-primary">{{ $data->plan_validity / 24 }} {{ __('check-sage.days') }}</span>
                            </li>
                        </ul>                    
                        <p></p>
                        <!-- <h6 class="font-weight-500">The operator does not yet support package status check, please refer to your device settings to check your data usage</h6> -->
                    </div>
                @endif
            </div>
        </div>
    </section>
@endsection

@push('before-styles')
    <script src="/js/jquery-v3.4.0.js"></script>
    <script>
        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        });
        setTimeout(function () {
            $("#alert-message").fadeOut(500);
        }, 5000);
        if ($(window).width() > 992) {
            $(window).scroll(function(){  
                if ($(this).scrollTop() > 40) {
                    $('#navbar_top').addClass("fixed-top");
                    $('body').css('padding-top', $('.navbar').outerHeight() + 'px');
                } else {
                    $('#navbar_top').removeClass("fixed-top");
                    $('body').css('padding-top', '0');
                }   
            });
        }
    </script>
    <script>
        function CheckBalance() {
            var oid = $("#searchbar").val();
            window.location.href = "{{ route('check-usage') }}?orderId=" + oid;
        } 

        var searchVal = "";
        if (searchVal !== undefined)
            var searchBar = $("#searchbar").val(searchVal);
        
        $("#searchbar").on("keydown", function(event) {
            if (event.keyCode === 13) {
                event.preventDefault();
                CheckBalance();
            }
        });
    </script>
@endpush