@extends('layouts.app')

@include('meta::manager', $seo)

@push('before-styles')
<style>

    :root {
        --blue: #007bff;
        --indigo: #6610f2;
        --purple: #6f42c1;
        --pink: #e83e8c;
        --red: #dc3545;
        --orange: #fd7e14;
        --yellow: #ffc107;
        --green: #28a745;
        --teal: #20c997;
        --cyan: #17a2b8;
        --white: #fff;
        --gray: #6c757d;
        --gray-dark: #343a40;
        --primary: #007bff;
        --secondary: #6c757d;
        --success: #28a745;
        --info: #17a2b8;
        --warning: #ffc107;
        --danger: #dc3545;
        --light: #f8f9fa;
        --dark: #343a40;
        --breakpoint-xs: 0;
        --breakpoint-sm: 576px;
        --breakpoint-md: 768px;
        --breakpoint-lg: 992px;
        --breakpoint-xl: 1200px;
        --font-family-sans-serif: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, "Noto Sans", sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji";
        --font-family-monospace: SFMono-Regular, Menlo, Monaco, Consolas, "Liberation Mono", "Courier New", monospace;
    }

    ::after, ::before {
        box-sizing: border-box;
    }

    html {
        font-family: sans-serif;
        line-height: 1.15;
        -webkit-text-size-adjust: 100%;
    }

    figure,
    header,
    main,
    nav,
    section {
        display: block;
    }

    body {
        margin: 0;
        font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, "Noto Sans", sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji";
        font-size: 1rem;
        font-weight: 400;
        line-height: 1.5;
        color: #212529;
        text-align: left;
        background-color: #fff;
    }

    h1,
    h2 {
        margin-top: 0;
        margin-bottom: 0.5rem;
    }
    p {
        margin-top: 0;
        margin-bottom: 1rem;
    }
    ul {
        margin-top: 0;
        margin-bottom: 1rem;
    }
    strong {
        font-weight: bolder;
    }
    small {
        font-size: 80%;
    }
    a {
        color: #007bff;
        text-decoration: none;
        background-color: transparent;
    }
    a:not([href]) {
        color: inherit;
        text-decoration: none;
    }
    figure {
        margin: 0 0 1rem;
    }
    img {
        vertical-align: middle;
        border-style: none;
    }
    svg {
        overflow: hidden;
        vertical-align: middle;
    }
    button {
        border-radius: 0;
    }
    button,
    select {
        margin: 0;
        font-family: inherit;
        font-size: inherit;
        line-height: inherit;
    }
    button {
        overflow: visible;
    }
    button,
    select {
        text-transform: none;
    }
    select {
        word-wrap: normal;
    }
    [type="button"],
    button {
        -webkit-appearance: button;
    }
    [type="button"]::-moz-focus-inner,
    button::-moz-focus-inner {
        padding: 0;
        border-style: none;
    }
    ::-webkit-file-upload-button {
        font: inherit;
        -webkit-appearance: button;
    }
    h1,
    h2 {
        margin-bottom: 0.5rem;
        font-weight: 500;
        line-height: 1.2;
    }
    h1 {
        font-size: 2.5rem;
    }
    h2 {
        font-size: 2rem;
    }
    small {
        font-size: 80%;
        font-weight: 400;
    }
    .container {
        width: 100%;
        padding-right: 15px;
        padding-left: 15px;
        margin-right: auto;
        margin-left: auto;
    }
    @media (min-width: 576px) {
        .container {
            max-width: 540px;
        }
    }
    @media (min-width: 768px) {
        .container {
            max-width: 720px;
        }
    }
    @media (min-width: 992px) {
        .container {
            max-width: 960px;
        }
    }
    @media (min-width: 1200px) {
        .container {
            max-width: 1140px;
        }
    }
    @media (min-width: 576px) {
        .container {
            max-width: 540px;
        }
    }
    @media (min-width: 768px) {
        .container {
            max-width: 720px;
        }
    }
    @media (min-width: 992px) {
        .container {
            max-width: 960px;
        }
    }
    @media (min-width: 1200px) {
        .container {
            max-width: 1140px;
        }
    }
    .row {
        display: -ms-flexbox;
        display: flex;
        -ms-flex-wrap: wrap;
        flex-wrap: wrap;
        margin-right: -15px;
        margin-left: -15px;
    }
    .col-md-5,
    .col-md-7 {
        position: relative;
        width: 100%;
        padding-right: 15px;
        padding-left: 15px;
    }
    @media (min-width: 768px) {
        .col-md-5 {
            -ms-flex: 0 0 41.666667%;
            flex: 0 0 41.666667%;
            max-width: 41.666667%;
        }
        .col-md-7 {
            -ms-flex: 0 0 58.333333%;
            flex: 0 0 58.333333%;
            max-width: 58.333333%;
        }
    }
    .form-control {
        display: block;
        width: 100%;
        height: calc(1.5em + 0.75rem + 2px);
        padding: 0.375rem 0.75rem;
        font-size: 1rem;
        font-weight: 400;
        line-height: 1.5;
        color: #495057;
        background-color: #fff;
        background-clip: padding-box;
        border: 1px solid #ced4da;
        border-radius: 0.25rem;
    }
    .form-control::-ms-expand {
        background-color: transparent;
        border: 0;
    }
    .form-control:-moz-focusring {
        color: transparent;
        text-shadow: 0 0 0 #495057;
    }
    .form-control::-webkit-input-placeholder {
        color: #6c757d;
        opacity: 1;
    }
    .form-control::-moz-placeholder {
        color: #6c757d;
        opacity: 1;
    }
    .form-control:-ms-input-placeholder {
        color: #6c757d;
        opacity: 1;
    }
    .form-control::-ms-input-placeholder {
        color: #6c757d;
        opacity: 1;
    }
    select.form-control[multiple] {
        height: auto;
    }
    .form-group {
        margin-bottom: 1rem;
    }
    .btn {
        display: inline-block;
        font-weight: 400;
        color: #212529;
        text-align: center;
        vertical-align: middle;
        background-color: transparent;
        border: 1px solid transparent;
        padding: 0.375rem 0.75rem;
        font-size: 1rem;
        line-height: 1.5;
        border-radius: 0.25rem;
    }
    .btn-primary {
        color: #fff;
        background-color: #007bff;
        border-color: #007bff;
    }
    .collapse:not(.show) {
        display: none;
    }
    .dropdown {
        position: relative;
    }
    .dropdown-menu {
        position: absolute;
        top: 100%;
        left: 0;
        z-index: 1000;
        display: none;
        float: left;
        min-width: 10rem;
        padding: 0.5rem 0;
        margin: 0.125rem 0 0;
        font-size: 1rem;
        color: #212529;
        text-align: left;
        list-style: none;
        background-color: #fff;
        background-clip: padding-box;
        border: 1px solid rgba(0, 0, 0, 0.15);
        border-radius: 0.25rem;
    }
    .dropdown-menu-right {
        right: 0;
        left: auto;
    }
    .dropdown-item {
        display: block;
        width: 100%;
        padding: 0.25rem 1.5rem;
        clear: both;
        font-weight: 400;
        color: #212529;
        text-align: inherit;
        white-space: nowrap;
        background-color: transparent;
        border: 0;
    }
    .dropdown-item.disabled {
        color: #6c757d;
        background-color: transparent;
    }
    .input-group-append {
        display: -ms-flexbox;
        display: flex;
    }
    .input-group-append .btn {
        position: relative;
        z-index: 2;
    }
    .input-group-append {
        margin-left: -1px;
    }
    .nav-link {
        display: block;
        padding: 0.5rem 1rem;
    }
    .navbar {
        position: relative;
        display: -ms-flexbox;
        display: flex;
        -ms-flex-wrap: wrap;
        flex-wrap: wrap;
        -ms-flex-align: center;
        align-items: center;
        -ms-flex-pack: justify;
        justify-content: space-between;
        padding: 0.5rem 1rem;
    }
    .navbar .container {
        display: -ms-flexbox;
        display: flex;
        -ms-flex-wrap: wrap;
        flex-wrap: wrap;
        -ms-flex-align: center;
        align-items: center;
        -ms-flex-pack: justify;
        justify-content: space-between;
    }
    .navbar-brand {
        display: inline-block;
        padding-top: 0.3125rem;
        padding-bottom: 0.3125rem;
        margin-right: 1rem;
        font-size: 1.25rem;
        line-height: inherit;
        white-space: nowrap;
    }
    .navbar-nav {
        display: -ms-flexbox;
        display: flex;
        -ms-flex-direction: column;
        flex-direction: column;
        padding-left: 0;
        margin-bottom: 0;
        list-style: none;
    }
    .navbar-nav .nav-link {
        padding-right: 0;
        padding-left: 0;
    }
    .navbar-nav .dropdown-menu {
        position: static;
        float: none;
    }
    .navbar-collapse {
        -ms-flex-preferred-size: 100%;
        flex-basis: 100%;
        -ms-flex-positive: 1;
        flex-grow: 1;
        -ms-flex-align: center;
        align-items: center;
    }
    .navbar-toggler {
        padding: 0.25rem 0.75rem;
        font-size: 1.25rem;
        line-height: 1;
        background-color: transparent;
        border: 1px solid transparent;
        border-radius: 0.25rem;
    }
    .navbar-toggler-icon {
        display: inline-block;
        width: 1.5em;
        height: 1.5em;
        vertical-align: middle;
        content: "";
        background: no-repeat center center;
        background-size: 100% 100%;
    }
    @media (max-width: 767.98px) {
        .navbar-expand-md > .container {
            padding-right: 0;
            padding-left: 0;
        }
    }
    @media (min-width: 768px) {
        .navbar-expand-md {
            -ms-flex-flow: row nowrap;
            flex-flow: row nowrap;
            -ms-flex-pack: start;
            justify-content: flex-start;
        }
        .navbar-expand-md .navbar-nav {
            -ms-flex-direction: row;
            flex-direction: row;
        }
        .navbar-expand-md .navbar-nav .dropdown-menu {
            position: absolute;
        }
        .navbar-expand-md .navbar-nav .nav-link {
            padding-right: 0.5rem;
            padding-left: 0.5rem;
        }
        .navbar-expand-md > .container {
            -ms-flex-wrap: nowrap;
            flex-wrap: nowrap;
        }
        .navbar-expand-md .navbar-collapse {
            display: -ms-flexbox !important;
            display: flex !important;
            -ms-flex-preferred-size: auto;
            flex-basis: auto;
        }
        .navbar-expand-md .navbar-toggler {
            display: none;
        }
    }
    .navbar-light .navbar-brand {
        color: rgba(0, 0, 0, 0.9);
    }
    .navbar-light .navbar-nav .nav-link {
        color: rgba(0, 0, 0, 0.5);
    }
    .navbar-light .navbar-toggler {
        color: rgba(0, 0, 0, 0.5);
        border-color: rgba(0, 0, 0, 0.1);
    }
    .navbar-light .navbar-toggler-icon {
        background-image: url("data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' width='30' height='30' viewBox='0 0 30 30'%3e%3cpath stroke='rgba(0, 0, 0, 0.5)' stroke-linecap='round' stroke-miterlimit='10' stroke-width='2' d='M4 7h22M4 15h22M4 23h22'/%3e%3c/svg%3e");
    }
    .bg-white {
        background-color: #fff !important;
    }
    .d-inline-block {
        display: inline-block !important;
    }
    .d-flex {
        display: -ms-flexbox !important;
        display: flex !important;
    }
    @media (min-width: 768px) {
        .d-md-flex {
            display: -ms-flexbox !important;
            display: flex !important;
        }
        .d-md-inline-flex {
            display: -ms-inline-flexbox !important;
            display: inline-flex !important;
        }
    }
    .flex-wrap {
        -ms-flex-wrap: wrap !important;
        flex-wrap: wrap !important;
    }
    .flex-grow-1 {
        -ms-flex-positive: 1 !important;
        flex-grow: 1 !important;
    }
    .justify-content-start {
        -ms-flex-pack: start !important;
        justify-content: flex-start !important;
    }
    .justify-content-center {
        -ms-flex-pack: center !important;
        justify-content: center !important;
    }
    .align-items-center {
        -ms-flex-align: center !important;
        align-items: center !important;
    }
    @media (min-width: 768px) {
        .justify-content-md-end {
            -ms-flex-pack: end !important;
            justify-content: flex-end !important;
        }
    }
    .shadow-none {
        box-shadow: none !important;
    }
    .mb-2 {
        margin-bottom: 0.5rem !important;
    }
    .ml-2 {
        margin-left: 0.5rem !important;
    }
    .mb-3 {
        margin-bottom: 1rem !important;
    }
    .p-0 {
        padding: 0 !important;
    }
    .pt-3 {
        padding-top: 1rem !important;
    }
    @media (min-width: 768px) {
        .mb-md-3 {
            margin-bottom: 1rem !important;
        }
        .mb-md-4 {
            margin-bottom: 1.5rem !important;
        }
    }
    .text-center {
        text-align: center !important;
    }
    .text-dark {
        color: #343a40 !important;
    }
    #header {
        z-index: 1030;
        position: relative;
    }
    .navbar-light .navbar-nav {
        color: rgba(0, 0, 0, 0.5);
    }
    .navbar-light .navbar-nav .nav-link {
        color: #000;
    }
    :root {
        --body-color: #f2f6f8;
        --primary-color: 0, 174, 239;
        --primary-color-defult: #00aeef;
        --secondary-color: 248, 145, 0;
        --success-color: #24b502;
        --secondary-badge-color: #ff9124;
        --dark-gray: #959b94;
        --dimgray-color: #707070;
        --danger-color: #f50000;
        --solidgray-color: #898989;
        --silver-color: #b7b7b7;
        --bg-light: #cbcbcb;
        --primary-hover-color: #0096ce;
        --dark-color: 0, 0, 0;
        --black-color: #000000;
        --dark-color-50: rgba(var(--dark-color), 0.5);
        --font-family: "Open Sans", sans-serif;
        --font-family-rubik: "Rubik", sans-serif;
        --normal-font-size: 16px;
        --normal-box-shadow: 0px 6px 6px 0px rgba(0, 0, 0, 0.16);
        --button-height: 41px;
        --input-height: 41px;
    }
    html {
        position: relative;
        min-height: 100%;
    }
    body {
        font-family: var(--font-family-rubik);
        background: var(--body-color);
        font-size: var(--normal-font-size);
    }
    img {
        max-width: 100%;
    }
    h1,
    h2 {
        font-weight: 500;
    }
    h1 {
        font-size: 2.5rem;
    }
    h2 {
        font-size: 2rem;
    }
    a {
        color: rgb(var(--primary-color));
        text-decoration: none;
        background-color: transparent;
    }
    .btn {
        font-size: var(--normal-font-size);
        font-weight: 500;
    }
    .btn-primary {
        color: #fff;
        background-color: rgb(var(--primary-color));
        border-color: rgb(var(--primary-color));
    }
    .h-list {
        display: flex;
        align-items: center;
        margin: 0;
        padding: 0;
        line-height: 1.4;
        --item-gap: 15px;
    }
    .h-list {
        line-height: inherit;
        margin-right: var(--item-gap);
        list-style-type: none;
    }
    .h-list *:last-child {
        margin-right: 0 !important;
    }
    .form-control {
        height: var(--input-height);
        color: rgb(var(--dark-color));
        border: 1px solid rgb(var(--primary-color));
        border-radius: 0.25rem;
    }
    .form-control::-webkit-input-placeholder {
        color: rgb(var(--dark-color-50));
        opacity: 0.5;
    }
    .form-control::-moz-placeholder {
        color: rgb(var(--dark-color-50));
        opacity: 0.5;
    }
    .form-control:-ms-input-placeholder {
        color: rgb(var(--dark-color-50));
        opacity: 0.5;
    }
    .form-control::-ms-input-placeholder {
        color: rgb(var(--dark-color-50));
        opacity: 0.5;
    }
    #main {
        min-height: calc(100vh - 152px);
    }
    .search-form .form-group {
        --size: 55px;
    }
    .search-form .input-group-append {
        margin-left: 15px;
    }
    .search-form select {
        height: var(--size);
        min-width: 360px;
    }
    .search-form .btn {
        min-width: 168px;
    }
    .search-form select {
        background-image: url("data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='17.356' height='17.977' viewBox='0 0 17.356 17.977'%3E%3Cpath id='search' d='M18.076,16.374,13.8,11.924a7.255,7.255,0,1,0-5.555,2.592A7.18,7.18,0,0,0,12.4,13.2l4.311,4.484a.947.947,0,1,0,1.365-1.312ZM8.242,1.893A5.365,5.365,0,1,1,2.877,7.258,5.371,5.371,0,0,1,8.242,1.893Z' transform='translate(-0.984)' opacity='0.5'/%3E%3C/svg%3E%0A");
        background-repeat: no-repeat;
        background-position: 20px 50%;
        padding: 0.375rem 2.85rem;
    }
    .dropdown .dropdown-menu a {
        border-bottom: 1px solid rgba(var(--dark-color), 0.15);
        padding: 0.35rem 0.8rem;
    }
    .dropdown .dropdown-menu a:last-child {
        border: 0;
    }
    @media only screen and (max-width: 767px) {
        body {
            font-size: 16px;
        }

        h1 {
            font-size: 1.9rem;
        }
    }
    #overlay {
        background: #ffffff;
        color: #000;
        position: fixed;
        height: 100%;
        width: 100%;
        z-index: 5000;
        top: 0;
        left: 0;
        text-align: center;
        opacity: 0.8;
    }
    .overlay-loader-inner{
        position: relative;
        top: 50%;
        transform: translateY(-50%);
    }
    .spinner {
        margin: 0 auto;
        height: 64px;
        width: 64px;
        animation: rotate 0.8s infinite linear;
        border: 5px solid rgb(var(--primary-color));
        border-right-color: transparent;
        border-radius: 50%;
    }
    @keyframes rotate {
        0% {
            transform: rotate(0deg);
        }

        100% {
            transform: rotate(360deg);
        }
    }
    body {
        color: var(--dark-color-50);
        font-size: 14px;
    }
    .title {
        color: var(--black-color);
        font-size: 18px;
        font-weight: 500;
    }
    .banner {
        --height-size: 500px;
        position: relative;
        color: #fff;
        height: var(--height-size);
        overflow: hidden;
    }
    .banner .banner-holder {
        position: absolute;
        top: 0;
        right: 0;
        bottom: 0;
        left: 0;
        display: flex;
        align-items: center;
        z-index: 9;
    }
    @media only screen and (max-width: 767px) {
        .banner {
            text-align: center;
        }
        .banner h1 {
            font-size: 25px;
        }
        .search-form .form-group {
            flex-wrap: wrap;
            --size: 38px;
        }
        .search-form .input-group-append {
            height: 51px;
            margin: 10px 0 0;
            width: 100%;
        }
        .search-form .btn,
        .search-form select {
            margin: 0 auto;
        }
        .banner .search-form select {
            min-width: auto;
        }
    }
    .form-control {
        border-color: var(--silver-color);
    }
    .banner-holder .banner-tab-list .slick-track {
        width: unset!important;
    }
    .banner-tab-list {
        justify-content: center;
        margin: 0 auto;
        text-align: center;
        display: flex;
        align-items: center;
    }
    .banner-tab-list .nav-item {
        outline: none;
    }
    .banner-tab-list .nav-link {
        color: #fff;
        position: relative;
        outline: none;
    }
    .banner-slider .slide {
        padding: 30px 0 60px;
        background-size: cover;
        background-position: 43% 0;
        min-height: var(--height-size);
    }
    .banner .slide.local-bundles-banner {
        background-position: 60% 0;
    }
    .local-offers-list li,
    .checkout-offers-list li {
        margin-bottom: 15px;
    }
    .local-offers-list a,
    .checkout-offers-list a {
        display: block;
        background: #fff;
        border: 2px solid var(--primary-color-defult);
        color: var(--black-color);
        padding: 5px 10px;
        min-width: 85px;
        border-radius: 5px;
        outline: none;
        position: relative;
    }
    .h-list.checkout-offers-list a img:last-child {
        margin-right: 5px !important;
        vertical-align: text-top;
    }
    .checkout-offers-list a img {
        height: 13px;
        width: 19px;
        box-shadow: var(--normal-box-shadow);
        margin-right: 5px !important;
    }
    .h-list.local-offers-list a img:last-child,
    .local-offers-list a img {
        margin-right: 5px !important;
    }
    .coming-soon.local-offers-list a:after {
        content: "";
        background: rgba(255, 255, 255, 0.5);
        position: absolute;
        left: 0;
        right: 0;
        bottom: 0;
        top: 0;
        z-index: 10;
    }
    .sect-our-partners {
        padding: 20px 0;
        text-align: center;
    }
    .slider-our-partners {
        color: var(--black-color);
        padding: 0 15px;
        text-align: center;
        height: 138px;
        overflow: hidden;
    }
    .slider-our-partners.slick-initialized {
        height: auto;
        overflow: visible;
    }
    .slider-our-partners figure {
        margin: 0 0 4px;
    }
    .slider-our-partners figure img {
        min-width: 98px;
        min-height: 98px;
        background: url(/images/placeholder-mm.png);
        background-repeat: no-repeat;
    }
    @media only screen and (min-width: 768px) {
        body {
            font-size: 16px;
        }
        .title {
            font-size: 24px;
            font-weight: 700;
        }
        .checkout-offer-text {
            font-size: 18px;
            margin-right: 22px;
            flex-shrink: 0;
        }
        .sect-our-partners {
            text-align: left;
            padding: 60px 0;
        }
        .checkout-offers-list a img {
            height: 15px;
            width: 22px;
        }
        .banner-holder .nav-item {
            display: inline-block;
        }
    }
</style>
<link rel="stylesheet" href="/css/global.css">
    <link rel="stylesheet" href="/css/home.css" />
@endpush

@section('content')

    <section class="banner">
        <div class="banner-slider" style="">
            <div class="slide travel-esim-banner" style="background-image: url(/images/mm-img-18-compressor.jpg);"></div>
            <div class="slide local-bundles-banner" style="background-image: url(/images/mm-img-17-compressor.jpg);"></div>
        </div>
        <div class="banner-holder">
            <div class="container">
                <h1 class="mb-3 mb-md-4">{!! __('banner.title') !!}</h1>
                <div class="banner-tab-list slick-list mb-3">
                    <div class="nav-item">
                        <span class="nav-link" id="travel-eSIMs-tab-2" onclick="internationalOffers();">{{ __('banner.buy_a_new_eSIM') }}</span>
                    </div>
                    <div class="nav-item">
                        <span class="nav-link" id="local-bundles-tab-2" onclick="localbundlesOffer();">{{ __('banner.top_up_your_esim') }}</span>
                    </div>
                </div>
                <form id="search-form" class="search-form" method="get">
                    <div class="form-group d-md-flex">
                        <select class="form-control top-up-provider" id="provider" name="provider" style="display: none;">
                            <option value="3hk" selected>3HK</option>
                            <option value="globalesim">GlobaleSIM</option>
                            <option value="flexiroam">Flexiroam</option>
                        </select>
                        <select class="form-control country-search" id="demo" multiple="multiple" name="country">
                        </select>
                        <div class="input-group-append">
                            <button id="searchButton" type="button" class="btn btn-primary">{{ __('banner.button') }}</button>
                        </div>
                    </div>
                </form>
                <div class="international-offers">
                    <div class="text-center d-md-flex align-items-center">
                        <span class="checkout-offer-text d-inline-block mb-2 mb-md-3">{{ __('banner.jump_to') }}</span>
                        <ul class="h-list flex-wrap checkout-offers-list justify-content-center">
                            <li><a href="{{ route('travel-esim', ['slug' => 'united-states']) }}"><img src="/images/flag/US.png" alt="image" style="width:20px;height:15px;"> USA</a></li>
                            <li><a href="{{ route('travel-esim', ['slug' => 'japan']) }}"><img src="/images/flag/JP.png" alt="image" style="width:20px;height:15px;">Japan</a></li>
                            <li><a href="{{ route('travel-esim', ['slug' => 'spain']) }}"><img src="/images/flag/ES.png" alt="image" style="width:20px;height:15px;">Spain</a></li>
                            <li><a href="{{ route('travel-esim', ['slug' => 'asia']) }}"><img class="shadow-none" src="images/japan.png" alt="image" style="width:20px;height:15px;">Asia</a></li>
                            <li><a href="{{ route('travel-esim', ['slug' => 'europe']) }}"><img class="shadow-none" src="images/stonehenge.png" alt="image" style="width:20px;height:15px;">Europe</a></li>
                        </ul>
                    </div>
                </div>
                <div class="local-offers" style="display: none;">
                    <div class="text-center d-md-flex align-items-center">
                        <span class="checkout-offer-text d-inline-block mb-2 mb-md-3">{{ __('banner.jump_to') }}</span>
                        <ul class="h-list checkout-offers-list flex-wrap justify-content-center">
                            <li><a href="{{ route('topup') }}"><img src="/images/travel-esim.png" alt="image" style="width:20px;height:15px;">Topup store</a></li>
                            <li><a href="{{ route('topup_catalog', ['provider' => '3hk']) }}"><img loading="lazy" src="https://mobimatterstorage.blob.core.windows.net/mobimatter-assests/assets/3HK.png" alt="image" style="width:15px;height:15px;"> 3HK</a></li>
                            <li><a href="{{ route('topup_catalog', ['provider' => 'globalesim']) }}"><img loading="lazy" src="https://mobimatterstorage.blob.core.windows.net/mobimatter-assests/assets/globalesim.png" alt="image" style="width:15px;height:15px;"> GlobaleSIM</a></li>
                            <li><a href="{{ route('topup_catalog', ['provider' => 'flexiroam']) }}"><img loading="lazy" src="https://mobimatterstorage.blob.core.windows.net/mobimatter-assests/assets/flexiroam.png" alt="image" style="width:15px;height:15px;"> Flexiroam</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="py-4">
        <div class="container">
            @include('includes.popular_product')
            @include('includes.card_regional')
        </div>
    </section>
@endsection

@push('after-scripts')

<script rel="preload" type="application/javascript" src="/js/home-min.js" defer></script>
<script>

function localbundlesOffer() {
    $('.international-offers').css('display', 'none');
    $('.top-up-provider').css('display', 'block');
    $('.local-offers').css('display', 'block');
    $(".search-form").removeClass("disable-form");
    flag = false;
}
function internationalOffers() {
    $('.international-offers').css('display', 'block');
    $('.local-offers').css('display', 'none');
    $('.top-up-provider').css('display', 'none');
    $(".search-form").removeClass("disable-form");
    flag = true;
}

function onCarouselClick() {
    if (!flag) {
        internationalOffers();

    }
    else {
        localbundlesOffer();
    }
}


window.onload = function () {
    var time = sessionStorage.getItem("timeBeforeReload");
    if (time != null && time != undefined) {
        sessionStorage.removeItem("timeBeforeReload");
        sessionStorage.removeItem("setOnLoad");
    }
};
</script>
<script>

    var observer = lozad();
    observer.observe();
    var elementId = 'discounterPrice';
    var counterElement = 6;
    function ToSeoUrl(url) {
        // make the url lowercase
        var encodedUrl = url.toString().toLowerCase();
        encodedUrl = encodedUrl.replace("//", "-");
        // replace & with and
        encodedUrl = encodedUrl.split(/\&+/).join("-and-");
        // remove invalid characters
        encodedUrl = encodedUrl.split(/[^a-z0-9.]/).join("-");
        // remove duplicates
        encodedUrl = encodedUrl.split(/-+/).join("-");
        // trim leading & trailing characters
        encodedUrl = encodedUrl.trim('-');
        return encodedUrl;
    }
</script>
@endpush
