@extends('layouts.app')
@include('meta::manager', [
    'title' => $product->translation->meta_title,
    'description' => $product->translation->meta_description,
    'image' => $product->provider->attachment->url()
])
@push('before-styles')
<style>
    :root{--blue:#007bff;--indigo:#6610f2;--purple:#6f42c1;--pink:#e83e8c;--red:#dc3545;--orange:#fd7e14;--yellow:#ffc107;--green:#28a745;--teal:#20c997;--cyan:#17a2b8;--white:#fff;--gray:#6c757d;--gray-dark:#343a40;--primary:#007bff;--secondary:#6c757d;--success:#28a745;--info:#17a2b8;--warning:#ffc107;--danger:#dc3545;--light:#f8f9fa;--dark:#343a40;--breakpoint-xs:0;--breakpoint-sm:576px;--breakpoint-md:768px;--breakpoint-lg:992px;--breakpoint-xl:1200px;--font-family-sans-serif:-apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,"Noto Sans",sans-serif,"Apple Color Emoji","Segoe UI Emoji","Segoe UI Symbol","Noto Color Emoji";--font-family-monospace:SFMono-Regular,Menlo,Monaco,Consolas,"Liberation Mono","Courier New",monospace}*,::after,::before{box-sizing:border-box}html{font-family:sans-serif;line-height:1.15;-webkit-text-size-adjust:100%}figure,header,main,nav,section{display:block}body{margin:0;font-family:-apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,"Noto Sans",sans-serif,"Apple Color Emoji","Segoe UI Emoji","Segoe UI Symbol","Noto Color Emoji";font-size:1rem;font-weight:400;line-height:1.5;color:#212529;text-align:left;background-color:#fff}h2,h4{margin-top:0;margin-bottom:.5rem}p{margin-top:0;margin-bottom:1rem}ul{margin-top:0;margin-bottom:1rem}strong{font-weight:bolder}a{color:#007bff;text-decoration:none;background-color:transparent}figure{margin:0 0 1rem}img{vertical-align:middle;border-style:none}svg{overflow:hidden;vertical-align:middle}button{border-radius:0}button,select{margin:0;font-family:inherit;font-size:inherit;line-height:inherit}button{overflow:visible}button,select{text-transform:none}select{word-wrap:normal}[type=button],[type=submit],button{-webkit-appearance:button}[type=button]::-moz-focus-inner,[type=submit]::-moz-focus-inner,button::-moz-focus-inner{padding:0;border-style:none}::-webkit-file-upload-button{font:inherit;-webkit-appearance:button}.h4,h2,h4{margin-bottom:.5rem;font-weight:500;line-height:1.2}h2{font-size:2rem}.h4,h4{font-size:1.5rem}.list-unstyled{padding-left:0;list-style:none}.container{width:100%;padding-right:15px;padding-left:15px;margin-right:auto;margin-left:auto}@media (min-width:576px){.container{max-width:540px}}@media (min-width:768px){.container{max-width:720px}}@media (min-width:992px){.container{max-width:960px}}@media (min-width:1200px){.container{max-width:1140px}}@media (min-width:576px){.container{max-width:540px}}@media (min-width:768px){.container{max-width:720px}}@media (min-width:992px){.container{max-width:960px}}@media (min-width:1200px){.container{max-width:1140px}}.row{display:-ms-flexbox;display:flex;-ms-flex-wrap:wrap;flex-wrap:wrap;margin-right:-15px;margin-left:-15px}.col-lg-4,.col-lg-8{position:relative;width:100%;padding-right:15px;padding-left:15px}@media (min-width:768px){.order-md-3{-ms-flex-order:3;order:3}}@media (min-width:992px){.col-lg-4{-ms-flex:0 0 33.333333%;flex:0 0 33.333333%;max-width:33.333333%}.col-lg-8{-ms-flex:0 0 66.666667%;flex:0 0 66.666667%;max-width:66.666667%}}.form-control{display:block;width:100%;height:calc(1.5em + .75rem + 2px);padding:.375rem .75rem;font-size:1rem;font-weight:400;line-height:1.5;color:#495057;background-color:#fff;background-clip:padding-box;border:1px solid #ced4da;border-radius:.25rem}.form-control::-ms-expand{background-color:transparent;border:0}.form-control:-moz-focusring{color:transparent;text-shadow:0 0 0 #495057}.form-control::-webkit-input-placeholder{color:#6c757d;opacity:1}.form-control::-moz-placeholder{color:#6c757d;opacity:1}.form-control:-ms-input-placeholder{color:#6c757d;opacity:1}.form-control::-ms-input-placeholder{color:#6c757d;opacity:1}select.form-control[multiple]{height:auto}.form-group{margin-bottom:1rem}.btn{display:inline-block;font-weight:400;color:#212529;text-align:center;vertical-align:middle;background-color:transparent;border:1px solid transparent;padding:.375rem .75rem;font-size:1rem;line-height:1.5;border-radius:.25rem}.btn-primary{color:#fff;background-color:#007bff;border-color:#007bff}.btn-block{display:block;width:100%}.collapse:not(.show){display:none}.dropdown{position:relative}.dropdown-menu{position:absolute;top:100%;left:0;z-index:1000;display:none;float:left;min-width:10rem;padding:.5rem 0;margin:.125rem 0 0;font-size:1rem;color:#212529;text-align:left;list-style:none;background-color:#fff;background-clip:padding-box;border:1px solid rgba(0,0,0,.15);border-radius:.25rem}.dropdown-menu-right{right:0;left:auto}.dropdown-item{display:block;width:100%;padding:.25rem 1.5rem;clear:both;font-weight:400;color:#212529;text-align:inherit;white-space:nowrap;background-color:transparent;border:0}.input-group-append{display:-ms-flexbox;display:flex}.input-group-append{margin-left:-1px}.custom-select{display:inline-block;width:100%;height:calc(1.5em + .75rem + 2px);padding:.375rem 1.75rem .375rem .75rem;font-size:1rem;font-weight:400;line-height:1.5;color:#495057;vertical-align:middle;background:#fff url("data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' width='4' height='5' viewBox='0 0 4 5'%3e%3cpath fill='%23343a40' d='M2 0L0 2h4zm0 5L0 3h4z'/%3e%3c/svg%3e") no-repeat right .75rem center/8px 10px;border:1px solid #ced4da;border-radius:.25rem;-webkit-appearance:none;-moz-appearance:none;appearance:none}.custom-select::-ms-expand{display:none}.custom-select:-moz-focusring{color:transparent;text-shadow:0 0 0 #495057}.nav-link{display:block;padding:.5rem 1rem}.navbar{position:relative;display:-ms-flexbox;display:flex;-ms-flex-wrap:wrap;flex-wrap:wrap;-ms-flex-align:center;align-items:center;-ms-flex-pack:justify;justify-content:space-between;padding:.5rem 1rem}.navbar .container{display:-ms-flexbox;display:flex;-ms-flex-wrap:wrap;flex-wrap:wrap;-ms-flex-align:center;align-items:center;-ms-flex-pack:justify;justify-content:space-between}.navbar-brand{display:inline-block;padding-top:.3125rem;padding-bottom:.3125rem;margin-right:1rem;font-size:1.25rem;line-height:inherit;white-space:nowrap}.navbar-nav{display:-ms-flexbox;display:flex;-ms-flex-direction:column;flex-direction:column;padding-left:0;margin-bottom:0;list-style:none}.navbar-nav .nav-link{padding-right:0;padding-left:0}.navbar-nav .dropdown-menu{position:static;float:none}.navbar-collapse{-ms-flex-preferred-size:100%;flex-basis:100%;-ms-flex-positive:1;flex-grow:1;-ms-flex-align:center;align-items:center}.navbar-toggler{padding:.25rem .75rem;font-size:1.25rem;line-height:1;background-color:transparent;border:1px solid transparent;border-radius:.25rem}@media (max-width:767.98px){.navbar-expand-md>.container{padding-right:0;padding-left:0}}@media (min-width:768px){.navbar-expand-md{-ms-flex-flow:row nowrap;flex-flow:row nowrap;-ms-flex-pack:start;justify-content:flex-start}.navbar-expand-md .navbar-nav{-ms-flex-direction:row;flex-direction:row}.navbar-expand-md .navbar-nav .dropdown-menu{position:absolute}.navbar-expand-md .navbar-nav .nav-link{padding-right:.5rem;padding-left:.5rem}.navbar-expand-md>.container{-ms-flex-wrap:nowrap;flex-wrap:nowrap}.navbar-expand-md .navbar-collapse{display:-ms-flexbox!important;display:flex!important;-ms-flex-preferred-size:auto;flex-basis:auto}.navbar-expand-md .navbar-toggler{display:none}}.navbar-light .navbar-brand{color:rgba(0,0,0,.9)}.navbar-light .navbar-nav .nav-link{color:rgba(0,0,0,.5)}.navbar-light .navbar-toggler{color:rgba(0,0,0,.5);border-color:rgba(0,0,0,.1)}.card-body{-ms-flex:1 1 auto;flex:1 1 auto;min-height:1px;padding:1.25rem}.bg-white{background-color:#fff!important}.border-top{border-top:1px solid #dee2e6!important}.border-bottom{border-bottom:1px solid #dee2e6!important}.d-none{display:none!important}.d-block{display:block!important}.d-flex{display:-ms-flexbox!important;display:flex!important}@media (min-width:768px){.d-md-none{display:none!important}.d-md-block{display:block!important}.d-md-inline-flex{display:-ms-inline-flexbox!important;display:inline-flex!important}}.flex-wrap{-ms-flex-wrap:wrap!important;flex-wrap:wrap!important}.flex-grow-1{-ms-flex-positive:1!important;flex-grow:1!important}.flex-shrink-0{-ms-flex-negative:0!important;flex-shrink:0!important}.justify-content-start{-ms-flex-pack:start!important;justify-content:flex-start!important}.justify-content-end{-ms-flex-pack:end!important;justify-content:flex-end!important}.align-items-center{-ms-flex-align:center!important;align-items:center!important}@media (min-width:768px){.justify-content-md-end{-ms-flex-pack:end!important;justify-content:flex-end!important}}.position-relative{position:relative!important}.fixed-bottom{position:fixed;right:0;bottom:0;left:0;z-index:1030}@supports ((position:-webkit-sticky) or (position:sticky)){.sticky-top{position:-webkit-sticky;position:sticky;top:0;z-index:1020}}.shadow-sm{box-shadow:0 .125rem .25rem rgba(0,0,0,.075)!important}.shadow-none{box-shadow:none!important}.w-100{width:100%!important}.m-0{margin:0!important}.mb-2{margin-bottom:.5rem!important}.ml-2{margin-left:.5rem!important}.mr-3{margin-right:1rem!important}.mb-3{margin-bottom:1rem!important}.mb-4{margin-bottom:1.5rem!important}.p-0{padding:0!important}.px-0{padding-right:0!important}.px-0{padding-left:0!important}.py-1{padding-top:.25rem!important}.py-1{padding-bottom:.25rem!important}.pt-2{padding-top:.5rem!important}.pb-2{padding-bottom:.5rem!important}.pl-2{padding-left:.5rem!important}.p-3{padding:1rem!important}.pt-3,.py-3{padding-top:1rem!important}.py-3{padding-bottom:1rem!important}.pl-3{padding-left:1rem!important}.py-4{padding-top:1.5rem!important}.py-4{padding-bottom:1.5rem!important}.ml-auto{margin-left:auto!important}@media (min-width:768px){.m-md-0{margin:0!important}.pt-md-0{padding-top:0!important}.mr-md-auto{margin-right:auto!important}.ml-md-auto{margin-left:auto!important}}.text-primary{color:#007bff!important}.text-dark{color:#343a40!important}.text-body{color:#212529!important}#header{z-index:1030;position:relative}.navbar-light .navbar-nav{color:rgba(0,0,0,.5)}.navbar-light .navbar-nav .nav-link{color:#000}#header .topbar .custom-select,.custom-select.multi-currency-select{border:0;box-shadow:none;padding:.375rem .85rem .375rem .35rem;background-image:url("data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='9.02' height='6.297' viewBox='0 0 9.02 6.297'%3E%3Cpath id='Path_2774' data-name='Path 2774' d='M937.06,39.291l3.492,4.079,3.414-4.079' transform='translate(-936.003 -38.233)' fill='none' stroke='%23fff' stroke-linecap='round' stroke-width='1.5' /%3E%3C/svg%3E");background-color:var(--primary-color-defult);color:#fff;font-size:14px}#header .topbar{background:var(--primary-color-defult);color:#fff;font-size:14px}#header .topbar .nav-item{margin-right:10px}#header .topbar .nav-item:last-child{margin-right:0}.navbar-toggler .bar1,.navbar-toggler .bar2,.navbar-toggler .bar3{width:24px;height:4px;background-color:var(--primary-color-defult);margin:4px 0;display:block;border-radius:50rem}.navbar-toggler .bar1{-webkit-transform:rotate(-45deg) translate(-6px,6px);transform:rotate(-45deg) translate(-6px,6px)}.navbar-toggler .bar2{opacity:0}.navbar-toggler .bar3{-webkit-transform:rotate(45deg) translate(-5px,-5px);transform:rotate(45deg) translate(-5px,-5px)}.navbar-toggler.collapsed .bar1,.navbar-toggler.collapsed .bar2,.navbar-toggler.collapsed .bar3{-webkit-transform:rotate(0) translate(0);transform:rotate(0) translate(0);opacity:1}#header .navbar-toggler{border:0;outline:0;padding:0}@media only screen and (max-width:767px){#header{position:fixed;left:0;right:0}#main{padding-top:61px}#header .navbar-collapse{position:fixed;top:61px;z-index:11;bottom:0;left:0;right:0;padding:25px 15px;background:#fff;color:var(--primary-color-defult);overflow:auto}#header .navbar-nav{font-weight:500;color:var(--black-color);margin-bottom:12px;align-items:flex-start!important;flex-direction:column-reverse}#header .navbar-nav .nav-link.text-dark{color:var(--primary-color-defult)!important}#header .navbar-nav .nav-link{color:var(--primary-color-defult)}.navbar-nav .dropdown-menu{border:0;border-radius:0;padding-bottom:12px!important}#header .dropdown .dropdown-menu a{border-bottom:0;padding:.35rem 0}#header .custom-select,#header .custom-select.multi-currency-select{background-image:url("data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='9.02' height='6.297' viewBox='0 0 9.02 6.297'%3E%3Cpath id='Path_2774' data-name='Path 2774' d='M937.06,39.291l3.492,4.079,3.414-4.079' transform='translate(-936.003 -38.233)' fill='none' stroke='%23000' stroke-linecap='round' stroke-width='1.5' /%3E%3C/svg%3E");background-color:#fff;color:#000;border:0;width:auto!important}.mobile-cl-navbar{border-top:1px solid #707070;padding-top:12px}.shopbyregion-dropdown{display:block;width:100%}.shopbyregion-dropdown .dropdown-menu{display:block;border-bottom:1px solid #707070}.shopbyregion-dropdown svg{transform:rotate(180deg)}}:root{--body-color:#F2F6F8;--primary-color:0,174,239;--primary-color-defult:#00AEEF;--secondary-color:248,145,0;--success-color:#24B502;--secondary-badge-color:#FF9124;--dark-gray:#959B94;--dimgray-color:#707070;--danger-color:#F50000;--solidgray-color:#898989;--silver-color:#B7B7B7;--bg-light:#CBCBCB;--primary-hover-color:#0096ce;--dark-color:0,0,0;--black-color:#000000;--dark-color-50:rgba(var(--dark-color), 0.5);--font-family:'Open Sans',sans-serif;--font-family-rubik:'Rubik',sans-serif;--normal-font-size:16px;--normal-box-shadow:0px 6px 6px 0px rgba(0, 0, 0, 0.16);--button-height:41px;--input-height:41px}html{position:relative;min-height:100%}body{font-family:var(--font-family-rubik);background:var(--body-color);font-size:var(--normal-font-size)}img{max-width:100%}.h4,h2,h4{font-weight:500}h2{font-size:2rem}.h4,h4{font-size:1.5rem}a{color:rgb(var(--primary-color));text-decoration:none;background-color:transparent}.btn{font-size:var(--normal-font-size);font-weight:500}.text-primary{color:rgb(var(--primary-color))!important}.text-dark-gray{color:var(--dark-gray)!important}.btn-primary{color:#fff;background-color:rgb(var(--primary-color));border-color:rgb(var(--primary-color))}.text-underline{text-decoration:underline}.font-weight-400{font-weight:400!important}.font-weight-500{font-weight:500!important}.font-size-14{font-size:14px}.font-size-18{font-size:18px}.h-list *{line-height:inherit;margin-right:var(--item-gap);list-style-type:none}.h-list :last-child{margin-right:0!important}.form-control{height:var(--input-height);color:rgb(var(--dark-color));border:1px solid rgb(var(--primary-color));border-radius:.25rem}.form-control::-webkit-input-placeholder{color:rgb(var(--dark-color-50));opacity:.5}.form-control::-moz-placeholder{color:rgb(var(--dark-color-50));opacity:.5}.form-control:-ms-input-placeholder{color:rgb(var(--dark-color-50));opacity:.5}.form-control::-ms-input-placeholder{color:rgb(var(--dark-color-50));opacity:.5}#main{min-height:calc(100vh - 152px)}.grid-4{--item-gap:16px;display:grid;grid-template-columns:1fr;grid-gap:var(--item-gap)}@media only screen and (min-width:768px){.grid-4{grid-template-columns:1fr 1fr}.grid-4{grid-template-columns:1fr 1fr 1fr}}@media only screen and (min-width:992px){.grid-4{grid-template-columns:1fr 1fr 1fr}.grid-4{grid-template-columns:1fr 1fr 1fr 1fr}}.dropdown .dropdown-menu a{border-bottom:1px solid rgba(var(--dark-color),.15);padding:.35rem .8rem}.dropdown .dropdown-menu a:last-child{border:0}.card-body{border-radius:5px;background-color:#fff;flex:1 1 100%;margin-bottom:30px}.side-bar.sticky-top{top:85px}@media only screen and (max-width:767px){body{font-size:16px}}#overlay,.overlay-loader{background:#fff;color:#000;position:fixed;height:100%;width:100%;z-index:5000;top:0;left:0;text-align:center;opacity:.8}.overlay-loader-inner{position:relative;top:50%;transform:translateY(-50%)}.spinner{margin:0 auto;height:64px;width:64px;animation:rotate .8s infinite linear;border:5px solid rgb(var(--primary-color));border-right-color:transparent;border-radius:50%}@keyframes rotate{0%{transform:rotate(0)}100%{transform:rotate(360deg)}}.header-searchholder{--width-size:370px;border:1px solid var(--primary-color-defult);border-radius:50rem;width:100%;max-width:var(--width-size);margin:0;font-size:12px;overflow:hidden}.header-searchholder .form-control{background:0 0;border:0;box-shadow:none;outline:0}.header-searchholder select{height:100%;min-width:100%;padding:.399rem 1.205rem;border-radius:50rem;display:flex;align-items:center;background:0 0}.shiner_laoder .shine{background:#f6f7f8;background-image:linear-gradient(to right,#f6f7f8 0,#e9eaec 20%,#f6f7f8 40%,#f6f7f8 100%);background-repeat:no-repeat;background-size:800px 100%;display:inline-block;position:relative;-webkit-animation-duration:1s;-webkit-animation-fill-mode:forwards;-webkit-animation-iteration-count:infinite;-webkit-animation-name:placeholderShimmer;-webkit-animation-timing-function:linear;overflow:hidden}.shiner_laoder box,.shiner_laoder lines{width:100%}@-webkit-keyframes placeholderShimmer{0%{background-position:-468px 0}100%{background-position:468px 0}}.understand-fixed-banner{z-index:2147483003;background:#fff;border-radius:10px 10px 0 0;display:flex;align-items:center;font-size:18px;color:var(--black-color);padding:20px;box-shadow:0 -3px 30px 0 rgba(0,0,0,.16)}@media (max-width:767px){.understand-fixed-banner{font-size:12px;flex-wrap:wrap;text-align:center}.understand-fixed-banner .btn{font-size:12px;margin:0 auto}}body{overflow-x:hidden}@media only screen and (max-width:767px){#main{padding-top:114px}}.success-amount{color:var(--success-color);flex-shrink:0;font-weight:500;text-transform:uppercase}.side-bar .shoping-logo{width:23px;flex-shrink:0}.side-bar .shoping-logo img{max-height:40px}.side-bar .border-bottom{border-color:rgba(var(--dark-color),.5)!important}.warning-outofstock{text-align:center;position:absolute;left:0;bottom:0;top:0;right:0;background:#fff;border-radius:5px;padding:30px;align-items:center;justify-content:center}.warning-outofstock .text{margin-bottom:40px}@media only screen and (max-width:767px){#header{overflow:hidden}.header-search-form{width:100%}}.faqs-accordion .acc-card{padding:0 13px 13px}.faqs-accordion .acc-card p{margin:0}.faqs-accordion .acc-card p+p{margin-top:7px}.shimmer-countries-list li{border-radius:4px;height:24px}.something-wrong-mmassage{position:fixed;top:0;left:0;right:0;bottom:0;z-index:1030;padding:0 15px;display:none}.something-wrong-mmassage .text-holder{max-width:460px;border-radius:5px;color:#fff;background:var(--secondary-badge-color);margin:85px auto;padding:16px 16px 0;box-shadow:0 .5rem 1rem rgba(0,0,0,.15);display:flex;align-items:center;-webkit-transform:translate(0,-25%);transform:translate(0,-25%)}
</style>
<link rel="stylesheet" href="/css/detail.css">
@endpush

@section('content')
<div id="overlay-buyorder" class="overlay-loader" style="display:none;">

    <div class="overlay-loader-inner">
        <div class="spinner"></div>
        <div class="text-change pt-3">{{ __('product.cart.createOrder') }}</div>
    </div>
</div>
<section class="py-4 shiner_laoder">
        <div class="container">
            <div class="row" id="offer-section">
                <div class="col-lg-8">
                    <div class="card-body d-flex flex-wrap wholesale-price-card" id="price-card">
                        <div class="d-flex">
                            <figure class="mr-1 ubigi-logo">
                                <img class="lozad" data-src="{{$product->provider->attachment->url()}}" alt="icon" width="20" src="{{$product->provider->attachment->url()}}" data-loaded="true">
                            </figure>
                            <div class="text pl-2">
                                <h2 class="font-size-18 m-0">{{$product->translation->name}}</h2>
                                <span class="text-dark-gray">
                                    {{ $product->provider->name }}
                                    @if(json_decode($product->tags))
                                    <span class="badge badge-secondary" style="background:{{ json_decode($product->tags)->color }};">{{ json_decode($product->tags)->item }}</span>
                                    @endif
                                </span>
                            </div>
                        </div>
                        <ul class="h-list justify-content-between pl-md-3 flex-wrap">
                            <li>
                                <small class="font-size-16 d-block">{{ __('product.mini.validity') }}</small>
                                <span class="font-weight-500 text-primary h4">{{ $product->plan_validity / 24 }} {{ __('product.mini.date_d') }}</span>
                            </li>
                            <li>
                                <small class="font-size-16 d-block">{{ __('product.mini.data') }}</small>
                                <span class="font-weight-500 text-primary h4">{{ $product->plan_data_limit }} {{ $product->plan_data_unit }}</span>
                            </li>
                            <li>
                                <small class="font-size-14 d-block">{{ __('product.mini.price') }}</small>
                                <span class="font-weight-500 text-success h4"><span id="price-1">{{ $product->currencyCode }} {{ $product->retailPrice }}</span> </span>
                                <span class="font-size-14 currency-text" id="currencyAmountElement-1" style="display: none;"><strong>({{ $product->currencyCode }} {{ $product->retailPrice }})</strong></span>
                            </li>
                        </ul>
                    </div>

                    <h2 class="h4 mb-4">{{ __('product.cart.detail') }}</h2>
                    <div class="card-body readytoconnect-detail" id="detail-card">
                        {!!$product->translation->description!!}
                    </div>

                    <h2 class="h4 mb-4">{{ __('product.cart.rabota') }}</h2>
                    <div class="card-body" id="country-list">
                        <ul class="grid-4 list-unstyled countries-list m-0">
                            @foreach($product->counties as $county)
                            <li>
                                <figure class="image-holder">
                                    <img class="lozad" data-src="/images/flag/{{ $county->country->two_letter_abbreviation }}.png" alt="" width="30" height="23" src="/images/flag/{{ $county->country->two_letter_abbreviation }}.png" data-loaded="true">
                                </figure>
                                <span>{{ $county->country->name }}</span>
                            </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
                <div class="col-lg-4 shiner_laoder">
                    <div class="side-bar sticky-top" id="main-class">
                        <div class="card-body font-size-14 p-3 position-relative">
                            <div class="d-flex align-items-center pb-2" id="side-card">
                        <div class="d-flex">
                            <figure class="ubigi-logo">
                                <img class="lozad" data-src="{{$product->provider->attachment->url()}}" alt="icon" width="20" src="{{$product->provider->attachment->url()}}" data-loaded="true">
                            </figure>
                            <div class="text pl-2">
                                <strong class="font-weight-500 m-0 d-block">{{ $product->provider->name }}</strong>
                                <span class="text-dark-gray">{{ __('product.cart.count') }} 1</span>
                            </div>
                        </div>
                        <div class="success-amount font-size-14 ml-auto">{{ $product->currencyCode }} {{ $product->retailPrice }}</div>
                        </div>
                            <div class="d-flex align-items-center pb-2 border-bottom">
                                <div class="d-flex align-items-center">
                                    <figure class="esim-logo m-0 shadow-sm">
                                        <svg class="flex-shrink-0" width="22px" height="22px" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 22.941 23">
                                            <g data-name="Group 4552" transform="translate(-976.879 -171.854)">
                                                <g id="Group_7918" data-name="Group 7918">
                                                    <path id="Path_2041" data-name="Path 2041" d="M0,2.868V20.073a2.874,2.874,0,0,0,2.875,2.868h17.25A2.874,2.874,0,0,0,23,20.073V2.868A2.874,2.874,0,0,0,20.125,0H2.875A2.874,2.874,0,0,0,0,2.868Z" transform="translate(976.879 194.854) rotate(-90)" fill="#303c42"></path>
                                                </g>
                                                <path id="Path_2042" data-name="Path 2042" d="M0,3.833V0H4.779V4.792H.956A.958.958,0,0,1,0,3.833Z" transform="translate(978.791 188.145)" fill="#ffca28"></path>
                                                <rect id="Rectangle_1514" data-name="Rectangle 1514" width="4.779" height="5.75" transform="translate(978.791 180.478)" fill="#ffca28"></rect>
                                                <path id="Path_2043" data-name="Path 2043" d="M3.823,4.792H0V0H4.779V3.833A.958.958,0,0,1,3.823,4.792Z" transform="translate(993.129 188.145)" fill="#ffca28"></path>
                                                <rect id="Rectangle_1515" data-name="Rectangle 1515" width="4.779" height="5.75" transform="translate(993.129 180.478)" fill="#ffca28"></rect>
                                                <path id="Path_2044" data-name="Path 2044" d="M12.426.958V4.792H6.691a.957.957,0,0,0-.956.958V19.167H0V0H11.47A.958.958,0,0,1,12.426.958Z" transform="translate(985.482 173.77)" fill="#ffca28"></path>
                                                <path id="Path_2045" data-name="Path 2045" d="M.956,0H4.779V4.792H0V.958A.958.958,0,0,1,.956,0Z" transform="translate(978.791 173.77)" fill="#ffca28"></path>
                                            </g>
                                        </svg>
                                    </figure>
                                    <div class="text pl-2">
                                        <strong class="font-weight-500 m-0">eSIM <span class="text-dark-gray font-weight-400"> x 1</span></strong>
                                    </div>
                                </div>
                                <div class="success-amount font-size-14 ml-auto">
                                    {{ __('product.cart.free') }}
                                </div>
                            </div>
                            <div class="d-flex align-items-center py-3 border-bottom">
                                <div class="d-flex align-items-center">
                                    <figure class="shoping-logo m-0">
                                        <img class="w-100" src="/images/shoping-icon-comp.png" alt="cart-icon" width="23" height="20">
                                    </figure>
                                    <div class="text pl-2">
                                        <strong class="font-weight-500 m-0">{{ __('product.cart.total') }}</strong>
                                    </div>
                                </div>
                                <div class="font-size-14 ml-auto">
                                    <div class="success-amount" id="price-2">{{ $product->currencyCode }} {{ $product->retailPrice }}</div>
                                    <span class="currency-text" id="currencyAmountElement-2" style="display: none;"><strong>({{ $product->currencyCode }} {{ $product->retailPrice }})</strong></span>
                                </div>
                            </div>
                            <div class="form-holder pt-3">
                                <form id="offer-detail">
                                    <div class="button-holder">
                                        <input type="hidden" name="productId" value="{{ $product->productId }}">
                                        <input type="hidden" name="productCategory" value="{{ $product->productCategory }}">
                                        @csrf
                                        <button type="submit" class="btn btn-primary btn-block" id="buy-button">{{ __('product.cart.buy') }} </button>
                                    </div>
                                </form>
                            </div>
                            <div class="warning-outofstock" id="out-of-stock" style="display:none;">
                                <div class="text">
                                    <img class="lozad" data-src="/images/mm-img-4-comp.png" alt="image">
                                    <h4 class="font-size-18 font-weight-500">
                                    {!! __('product.cart.invistion') !!}
                                    </h4>
                                    <p>{!! __('product.cart.invistion_text') !!}</p>
                                    <a id="out-of-stock-button" href="{{ route('home') }}" class="btn btn-primary btn-block">{{ __('product.cart.search_buy') }}</a>
                                </div>
                            </div>
                        </div>
                        <div class="card-body font-size-12 px-0 pb-2">
                            <h4 class="font-size-12 px-3 mb-3">{{ __('product.faq.title') }}</h4>
                            <div class="accordion faqs-accordion" id="faqaccordion">
                                <div class="card">
                                    <button class="btn btn-opner collapsed" type="button" data-toggle="collapse" data-target="#acc_collapse1" aria-expanded="true" aria-controls="acc_collapse1">{{ __('product.faq.faq_1.title') }}</button>
                                    <div id="acc_collapse1" class="collapse" data-parent="#faqaccordion">
                                        <div class="acc-card">{!! __('product.faq.faq_1.desc') !!}</div>
                                    </div>
                                    <div class="card">
                                        <button class="btn btn-opner collapsed" type="button" data-toggle="collapse" data-target="#acc_collapse2" aria-expanded="false" aria-controls="acc_collapse2">
                                        {{ __('product.faq.faq_2.title') }}</button>
                                        <div id="acc_collapse2" class="collapse" data-parent="#faqaccordion">
                                            <div class="acc-card">{!! __('product.faq.faq_2.desc') !!}</div>
                                        </div>
                                    </div>
                                    <div class="card">
                                        <button class="btn btn-opner collapsed" type="button" data-toggle="collapse" data-target="#acc_collapse3" aria-expanded="false" aria-controls="acc_collapse3">
                                        {{ __('product.faq.faq_3.title') }}
                                        </button>
                                        <div id="acc_collapse3" class="collapse" data-parent="#faqaccordion">
                                            <div class="acc-card">{!! __('product.faq.faq_3.desc') !!}</div>
                                        </div>
                                    </div>
                                    <div class="card">
                                        <button class="btn btn-opner collapsed" type="button" data-toggle="collapse" data-target="#acc_collapse4" aria-expanded="false" aria-controls="acc_collapse4">
                                        {{ __('product.faq.faq_4.title') }}
                                        </button>
                                        <div id="acc_collapse4" class="collapse" data-parent="#faqaccordion">
                                            <div class="acc-card">{!! __('product.faq.faq_4.desc') !!}</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@push('before-scripts')
    <script>
        $("#buy-button").click(function (e) {
            e.preventDefault();
            $("#buy-button").prop('disabled', true);
            $('#overlay-buyorder').fadeIn();

            setTimeout(function () {
                var reservehtml = `<div class="overlay-loader-inner"><div class="spinner"></div><div class="text-change pt-3">{{ __('product.cart.booking') }}</div></div>`;
                $('#overlay-buyorder').empty();
                $('#overlay-buyorder').append(reservehtml);

                let token = $('meta[name="csrf-token"]').attr('content');
                let product_id = '{{ $product->productId }}';
                let product_category = '{{ $product->productCategory }}';

                $.post({
                    url: "{{ route('ajaxcreateorder') }}",
                    data: {
                        productId:product_id,
                        productCategory:product_category,
                        currency:"{{$product->currencyCode}}",
                        price:"{{$product->retailPrice}}"
                    },
                    headers: {
                        'X-CSRF-TOKEN': token
                    },
                    success: function (data) {
                        console.log(data)
                        $("#buy-button").prop('disabled', false);
                        if(data.split(':')[0].trim()=='TRANSACTION_ID'){
                            const TRANS_ID = data.split(':')[1].trim()
                            window.location.href = `https://maib.ecommerce.md:21443/ecomm/ClientHandler?trans_id=${TRANS_ID}`
                            //sessionStorage.setItem("orderId", TRANS_ID);
                            window.localStorage.setItem("orderId", TRANS_ID);
                            window.localStorage.setItem("token", token);
                            window.localStorage.setItem("price", {{$product->retailPrice}});
                        }

                        $("#overlay-buyorder").hide();

                    },
                    error: function (errMsg) {
                        console.log(errMsg)
                        $("#buy-button").prop('disabled', false);
                        $("#overlay-buyorder").hide();
                    }
                });
            }, 2000);
        });



    </script>

@endpush
