<a href="{{ route('travel-product',['slug'=>$product->slug]) }}" class="grid-item text-decoration-none text-body">
    <div class="d-flex align-items-start justify-content-between mb-3">
        <div class="d-flex align-items-start">
            <img class="flex-shrink-0 network-logo lozad" data-src="{{$product->provider->attachment->url()}}" alt="img" width="22">
            <div class="d-inline-block pl-2">
                <span class="font-weight-500 d-block font-size-14">{{ $product->translation->name }}</span>
                <span class="text-dark-light">{{ $product->provider->name }}</span>
            </div>
        </div>
        @if(json_decode($product->tags))
            <span class="badge badge-secondary" style="background:{{ json_decode($product->tags)->color }};">{{ json_decode($product->tags)->item }}</span>
        @endif
    </div>
    <ul class="h-list justify-content-between mb-2">
        <li>
            <small class="font-size-12 d-block">{{ __('product.mini.validity') }}</small>
            <span class="font-weight-500 text-primary font-size-18">{{ $product->plan_validity / 24 }} {{ __('product.mini.date_d') }}</span>
        </li>
        <li>
            <small class="font-size-12 d-block">{{ __('product.mini.data') }}</small>
            <span class="font-weight-500 text-primary font-size-18">{{ $product->plan_data_limit }} {{ $product->plan_data_unit }}</span>
        </li>
        <li>
            <small class="font-size-12 d-block">{{ __('product.mini.price') }}</small>
            <span class="font-weight-500 text-success font-size-18 mr-0">
                <span id="discounterPrice-{{ $product->key }}">{{ $product->currencyCode }} {{ $product->retailPrice }}</span>
            </span>
            <span class="font-size-14 currency-text" id="currencyAmountElement-{{ $product->key }}"></span>
        </li>
    </ul>
    @if(count($product->counties) > 3)
    <p id="countryFlags">{{ __('product.mini.works_in') }} 
        @foreach($product->counties as $key => $county)    
            <img class="mr-1 flag lozad" data-src="/images/flag/{{ $county->country->two_letter_abbreviation }}.png" alt="{{ $county->country->name }}" width="19" height="14"> 
            @php
                if($key == 2){break;}
            @endphp
        @endforeach 
    & {{count($product->counties) - 3 }} {{ __('product.mini.more_three_countries') }}</p>
    @else
    <p id="countryFlags">{{ __('product.mini.works_in') }} 
        @foreach($product->counties as $county)
            <img class="mr-1 flag lozad" data-src="/images/flag/{{ $county->country->two_letter_abbreviation }}.png" alt="{{ $county->country->name }}" width="19" height="14">
        @endforeach
        {{ __('product.mini.less_three_countries') }}</p>
    @endif
    <div class="btn btn-primary btn-block">{{ __('product.mini.readmore') }}</div>
</a>