@extends('layouts.app')
@include('meta::manager', $seo)

@push('before-styles')
<link rel="stylesheet" type="text/css" href="/css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="/css/site.css">
<link rel="stylesheet" type="text/css" href="/css/header-footer.css">
<link rel="stylesheet" type="text/css" href="/css/faqs-page.css">

@endpush

@section('content')
    <section class="faq_banner text-center bg-primary py-4 overflow-hidden">
        <div class="container">
            <figure><img src="/images/faq-header.png" alt="faqs"></figure>
        </div>
    </section>
    <section class="faqs text-body bg-light py-3 py-md-5">
        <div class="container" id="myAccordion">
            @foreach($faqs as $faq)
            <h2 class="h5 mb-4 category-opner collapsed" data-toggle="collapse" data-target="#category{{ $faq->id }}-accordion" aria-expanded="false" aria-controls="category{{ $faq->id }}-accordion">{{ $faq->translation->name }}</h2>
                @foreach($faq->faqs as $item)
                <div class="faqs-accordion mb-4 collapse" id="category{{ $faq->id }}-accordion">
                    <div class="panel">
                        <button class="btn btn-link text-decoration-none btn-opner collapsed"type="button" data-toggle="collapse" data-target="#faq_{{ $faq->id }}_colapse{{ $item->id }}" aria-expanded="true"aria-controls="faq_{{ $faq->id }}_colapse{{ $item->id }}">{{ $item->translation->name }}</button>
                        <div id="faq_{{ $faq->id }}_colapse{{ $item->id }}" class="collapse" data-parent="#category{{ $faq->id }}-accordion">
                            <div class="text">
                                <p>{!! $item->translation->description !!}</p>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            @endforeach
        </div>
    </section>
@endsection

@push('before-styles')
<script src="/js/jquery-v3.4.0.js"></script>
<script src="/js/bootstrap.bundle.min.js" type="text/javascript"></script>
<script src="https://cdn.jsdelivr.net/npm/alertifyjs@1.11.2/build/alertify.min.js"></script>
@endpush