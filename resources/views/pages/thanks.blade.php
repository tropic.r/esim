@extends('layouts.app')
@push('before-styles')
    <style>
        .res {
            width: 300px;
            padding-top: 50px;
            margin: 0 auto;
        }
    </style>
    <link rel="stylesheet" href="/css/detail.css">
@endpush
@section('content')

    <section class="py-4 shiner_laoder">
        <div class="container">
            <h3>Result: </h3>
            <div class="result">
                Load...
            </div>

            <button id="reverse">transaction reverse</button>
        </div>
    </section>
@endsection


@push('before-scripts')
    <script>
        let token = $('meta[name="csrf-token"]').attr('content');

        setTimeout(function () {
            $.post({
                url: "{{ route('ajaxconfirm') }}",
                data: {
                    trans_id: window.localStorage.getItem("orderId")
                },
                headers: {
                    'X-CSRF-TOKEN': token
                },
                success: function (data) {
                    console.log(data)
                    $('.result').html('<pre>'+data+'</pre>')
                },
                error: function (errMsg) {
                    console.log(errMsg)
                }
            });
        }, 500);


        $('#reverse').click(function () {
            let price = Math.floor(window.localStorage.getItem("price"))
            console.log(price)
            console.log(window.localStorage.getItem("orderId"))

            $.post({
                url: "{{ route('reverse') }}",
                data: {
                    trans_id: window.localStorage.getItem("orderId"),
                    price: price
                },
                headers: {
                    'X-CSRF-TOKEN': token
                },
                success: function (data) {
                    console.log(data)
                    $('.result').html('<pre>'+data+'</pre>')
                },
                error: function (errMsg) {
                    console.log(errMsg)
                }
            });
        })
    </script>

@endpush
