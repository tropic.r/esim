@extends('layouts.app')
@include('meta::manager', [
    'title' => 'Oформление Esim'
    ])

@push('before-styles')
    <link rel="stylesheet" href="css/check-out.css" media="nope!" onload="this.media='all'">
    <link rel="stylesheet" href="css/applepay.css" />
@endpush
@section('content')

<section class="py-4">
        <div class="container">
            <ul class="progressbar" id="pm-progressbar">
                <li id="detail-progress" class="active">{{ __('checkout.progressbar.detail') }}</li>
                <li id="payment-progress" class="">{{ __('checkout.progressbar.payment') }}</li>
                <li id="detail-progress">{{ __('checkout.progressbar.detail_step3') }}</li>
            </ul>
            <div class="row">
                <div class="col-lg-8">
                    <div class="your-detal-form" id="email-section">
                        <div class="text mb-4">
                            <h2 class="h4 mb-0">{{ __('checkout.step1.detail_title') }}</h2>
                            @auth
                            @else
                                <p class="mb-0 text-black-50">
                                    {!! __('checkout.step1.detail_auth', ['link' => route('auth.login')]) !!}
                                </p>
                            @endauth
                        </div>
                        <form method="post" class="form-step1">
                            <div class="form-group" id="name-field">
                                <label for="name-field">{{ __('checkout.step1.name') }}</label>
                                <input for="UserName" value="{{ Auth::user() ? Auth::user()->name : ''}}" type="text" class="form-control border-0 shadow-sm" id="name_field">
                            </div>
                            <div class="form-group" id="email-field">
                                <label for="email_field">
                                    {{ __('checkout.step1.email') }}
                                    <span class="font-weight-400">{{ __('checkout.step1.email_after') }}</span>
                                </label>
                                <input for="Email" type="email" value="{{ Auth::user() ? Auth::user()->email : ''}}" class="form-control border-0 shadow-sm" id="email_field" style="text-transform: none !important;">
                            </div>
                            <div class="row mb-4 mb-md-3">
                                <div class="col-lg-12 pt-3 pb-2">
                                    <p class="mb-1 text-black-50">{{ __('checkout.step1.before_button') }}</p>
                                    <button id="user-form-button" type="button" class="btn btn-primary btn-block">{{ __('checkout.step1.button_step1') }}</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    
                    <div id="pay-section" class="your-detal-form" style="display:none;">
                        <div class="text mb-4">
                            <a href="#" id="go-back" class="text-underline">< Go back</a>
                        </div>
                        <div class="text mb-4">
                            <h2 class="h4 mb-0">Select a payment method</h2>
                        </div>   
                        <meta property="product:payment_method" content="ApplePay" />
                        <meta name="payment-country-code" content="US" />
                        <meta name="payment-currency-code" content="USD" />
                        <meta name="base-url" content="https://mobimatter.com" />
                        <meta name="apple-pay-merchant-id" content="2146EDAF067B0E6CDF4F6604D2421C0D3BABAA487F402F9B9B8151F5F3E543FB" />
                        <meta name="apple-pay-store-name" content="MobiMatter" />
                        <link rel="merchant-validation" href="/applepay/validate" />
                        <ul class="select-paymentmethod">
                            <li id="showapplepay">
                                <span class="frame">
                                    <span class="mr-2">Pay with Apple Pay</span>
                                    <div id="set-up-apple-pay-button" class="apple-pay apple-pay-set-up-button apple-pay-set-up-button-black input-block-level d-none" lang="US"></div>
                                    <figure style="height: 50px;"><button type="button" id="apple-pay-button" class="apple-pay input-block-level d-none" lang="US"></button></figure>
                                </span>
                            </li>
                            <li>
                                <span class="frame" onclick="onGooglePaymentButtonClicked()">
                                    <span> Pay with Google Pay </span>
                                    <figure id="gpay" style="height: 50px;"></figure>
                                </span>
                            </li>
                            <li>
                                <button type="button" class="frame border-0" id="rak-pay-button">
                                    <span class="d-none d-md-block mr-2">Оплата с помощью кредитной или дебетовой карты </p></span>
                                    <span class="d-md-none mr-2">Примечание: Если вы используете VPN транзакции может получить отклонено платежный шлюз <p class="text-dark-gray font-size-14 m-0">Note: Using VPN may result in payment failure</p></span>
                                    <figure><span class="master-card-btn"><img class="lozad" data-src="/images/kisspng-mastercard-credit-card.png" alt="image"></span></figure>
                                </button>
                            </li>
                            <li id="notsafari" style="display:none;">
                                <span class="frame shadow-none" id="apple-pay-button" style="opacity: .65;">
                                    <span>Apple Pay available on Safari browser and MobiMatter iOS app</span>
                                    <figure><img class="lozad" data-src="/images/Apple_Pay_mark.png" alt="image"></figure>
                                </span>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="sticky-top side-bar-sticky">
                        <div class="d-flex align-items-center">
                            <h2 class="h4 mb-3">&#x438;&#x442;&#x43E;&#x433; &#x437;&#x430;&#x43A;&#x430;&#x437;&#x430;</h2>
                            <span class="text-dark-light font-size-14 ml-auto mb-3">&#x421;&#x435;&#x441;&#x441;&#x438;&#x44F; &#x438;&#x441;&#x442;&#x435;&#x43A;&#x430;&#x435;&#x442; &#x432; <span id="se-timer" class="text-secondary">00:00</span></span>
                        </div>
                        <div id="order-summary-card">
                        <div class="card-body side-bar font-size-14 p-3">
                                                    <div class="pb-2 border-bottom mb-3">
                                                        <div class="d-flex">
                                                            <figure class="ubigi-logo">
                                                                <img class="lozad" data-src="https://mobimatterstorage.blob.core.windows.net/mobimatter-assests/assets/esimgo-logo.png" alt="icon" src="https://mobimatterstorage.blob.core.windows.net/mobimatter-assests/assets/esimgo-logo.png" data-loaded="true">
                                                            </figure>
                                                            <div class="text pl-2">
                                                                <strong class="font-weight-500 m-0 d-block">Europe Plus 1 GB</strong>
                                                                <span class="text-dark-gray">eSIMGo</span>
                                                            </div>
                                                        </div>
                                                        <ul class="h-list justify-content-between flex-wrap" data-item-gap="15">
                                                            <li>
                                                                <small class="font-size-14 d-block">Срок действия:</small>
                                                                <span class="font-weight-500 text-primary h6">7 дней</span>
                                                            </li>
                                                            <li>
                                                                <small class="font-size-14 d-block">Данные:</small>
                                                                <span class="font-weight-500 text-primary h6">1 GB</span>
                                                            </li>
                                                            <li>
                                                                <span class="font-weight-500 text-success h6">USD 3.5 </span>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <div class="pb-3 border-bottom">
                                                        <div class="d-flex align-items-center pb-2">
                                                            <div class="d-flex">
                                                                <figure class="ubigi-logo">
                                                                    <img class="lozad" data-src="https://mobimatterstorage.blob.core.windows.net/mobimatter-assests/assets/esimgo-logo.png" alt="icon" src="https://mobimatterstorage.blob.core.windows.net/mobimatter-assests/assets/esimgo-logo.png" data-loaded="true">
                                                                </figure>
                                                                <div class="text pl-2">
                                                                    <strong class="font-weight-500 m-0 d-block">Europe Plus 1 GB</strong>
                                                                    <span class="text-dark-gray">Quantity x 1</span>
                                                                </div>
                                                            </div>
                                                            <div class="success-amount font-size-14 ml-auto">USD 3.5</div>
                                                        </div>
                                                        <div class="d-flex align-items-center">
                                                            <div class="d-flex align-items-center">
                                                                <figure class="esim-logo m-0 shadow-sm">
                                                                    <svg class="flex-shrink-0" width="22px" height="22px" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 22.941 23">
                                                                        <g data-name="Group 4552" transform="translate(-976.879 -171.854)">
                                                                            <g id="Group_7918" data-name="Group 7918">
                                                                                <path id="Path_2041" data-name="Path 2041" d="M0,2.868V20.073a2.874,2.874,0,0,0,2.875,2.868h17.25A2.874,2.874,0,0,0,23,20.073V2.868A2.874,2.874,0,0,0,20.125,0H2.875A2.874,2.874,0,0,0,0,2.868Z" transform="translate(976.879 194.854) rotate(-90)" fill="#303c42"></path>
                                                                            </g>
                                                                            <path id="Path_2042" data-name="Path 2042" d="M0,3.833V0H4.779V4.792H.956A.958.958,0,0,1,0,3.833Z" transform="translate(978.791 188.145)" fill="#ffca28"></path>
                                                                            <rect id="Rectangle_1514" data-name="Rectangle 1514" width="4.779" height="5.75" transform="translate(978.791 180.478)" fill="#ffca28"></rect>
                                                                            <path id="Path_2043" data-name="Path 2043" d="M3.823,4.792H0V0H4.779V3.833A.958.958,0,0,1,3.823,4.792Z" transform="translate(993.129 188.145)" fill="#ffca28"></path>
                                                                            <rect id="Rectangle_1515" data-name="Rectangle 1515" width="4.779" height="5.75" transform="translate(993.129 180.478)" fill="#ffca28"></rect>
                                                                            <path id="Path_2044" data-name="Path 2044" d="M12.426.958V4.792H6.691a.957.957,0,0,0-.956.958V19.167H0V0H11.47A.958.958,0,0,1,12.426.958Z" transform="translate(985.482 173.77)" fill="#ffca28"></path>
                                                                            Modelpath id="Path_2045" data-name="Path 2045" d="M.956,0H4.779V4.792H0V.958A.958.958,0,0,1,.956,0Z" transform="translate(978.791 173.77)" fill="#ffca28" /&gt;
                                                                        </g>
                                                                    </svg>
                                                                </figure>
                                                                <div class="text pl-2">
                                                                    <strong class="font-weight-500 m-0">eSIM <span class="text-dark-gray font-weight-400"> x 1</span></strong>
                                                                </div>
                                                            </div>
                                                            <div class="success-amount font-size-14 ml-auto">Свободный</div>
                                                        </div>
                                                    </div>
                                                    <div class="d-flex align-items-center py-3">
                                                        <div class="d-flex align-items-center">
                                                            <figure class="shoping-logo m-0">
                                                                <img class="lozad" data-class="w-100" src="/images/shoping-icon-comp.png" alt="shoping" data-loaded="true">
                                                            </figure>
                                                            <div class="text pl-2">
                                                                <strong class="font-weight-500 m-0">Итого</strong>
                                                            </div>
                                                        </div>
                                                        <div class="font-size-14 ml-auto">
                                                            <div class="success-amount"><span id="price-1">USD 3.5</span></div>
                                                            <span class="currency-text" id="currencyAmountElement-1" style="display: none;"></span>
                                                        </div>
                                                    </div>
                                                </div>
                        </div>
                            <div class="card-body font-size-12 px-0 pb-2" id="faqs-card">
                            <h4 class="font-size-12 px-3 mb-3">&#x427;&#x430;&#x441;&#x442;&#x43E; &#x437;&#x430;&#x434;&#x430;&#x432;&#x430;&#x435;&#x43C;&#x44B;&#x435; &#x432;&#x43E;&#x43F;&#x440;&#x43E;&#x441;&#x44B;</h4>
                            <div class="accordion faqs-accordion" id="faqaccordion">
                                <div class="card">
                                    <button class="btn btn-opner collapsed" type="button" data-toggle="collapse" data-target="#acc_collapse1"
                                            aria-expanded="true" aria-controls="acc_collapse1">
                                    &#x41A;&#x430;&#x43A;&#x438;&#x435; &#x441;&#x43C;&#x430;&#x440;&#x442;&#x444;&#x43E;&#x43D;&#x44B; ESIM &#x43F;&#x440;&#x430;&#x432;&#x43E;?
                                    </button>
                                    <div id="acc_collapse1" class="collapse" data-parent="#faqaccordion">
                                        <div class="acc-card">
                                            <p>
                                                <span class="text-primary">A:</span> iPhone: XR, XS, 11, 12, 13 &#x438; SE &#x441;&#x435;&#x440;&#x438;&#x438; 2020; Google Pixel 3 &#x438; &#x431;&#x43E;&#x43B;&#x435;&#x435; &#x43F;&#x43E;&#x437;&#x434;&#x43D;&#x438;&#x435; &#x43C;&#x43E;&#x434;&#x435;&#x43B;&#x438;; &#x421;&#x435;&#x440;&#x438;&#x44F; Samsung S20, &#x441;&#x433;&#x438;&#x431;&#x430;&#x43D;&#x438;&#x435; &#x438; &#x424;&#x43B;&#x438;&#x43F;; &#x441;&#x435;&#x440;&#x438;&#x44F; P40 Huawei; IPADS: 10,2, &#x41A;&#x43E;&#x43D;&#x434;&#x438;&#x446;&#x438;&#x43E;&#x43D;&#x435;&#x440;, &#x41C;&#x438;&#x43D;&#x438;-2019, Pro 11, Pro 12,9.
                                            </p>
                                            <p>
                                                Smartwatches with eSIM capability (except Apple Watch) are also supported
                                            </p>
                                            <p>
                                                &#x41F;&#x440;&#x438;&#x43C;&#x435;&#x447;&#x430;&#x43D;&#x438;&#x435;: &#x41D;&#x435;&#x43A;&#x43E;&#x442;&#x43E;&#x440;&#x44B;&#x435; &#x438;&#x437; &#x44D;&#x442;&#x438;&#x445; &#x443;&#x441;&#x442;&#x440;&#x43E;&#x439;&#x441;&#x442;&#x432; &#x438;&#x43C;&#x435;&#x44E;&#x442; &#x432;&#x430;&#x440;&#x438;&#x430;&#x43D;&#x442;&#x44B;, &#x43A;&#x43E;&#x442;&#x43E;&#x440;&#x44B;&#x435; &#x43D;&#x435; &#x43F;&#x43E;&#x434;&#x434;&#x435;&#x440;&#x436;&#x438;&#x432;&#x430;&#x44E;&#x442; ESIM, &#x43F;&#x43E;&#x436;&#x430;&#x43B;&#x443;&#x439;&#x441;&#x442;&#x430;, &#x43F;&#x440;&#x43E;&#x432;&#x435;&#x440;&#x44C;&#x442;&#x435; <a href="/faqs" target="_blank">&#x41F;&#x43E;&#x43C;&#x43E;&#x449;&#x44C; &#x438; &#x447;&#x430;&#x441;&#x442;&#x43E; &#x437;&#x430;&#x434;&#x430;&#x432;&#x430;&#x435;&#x43C;&#x44B;&#x435; &#x432;&#x43E;&#x43F;&#x440;&#x43E;&#x441;&#x44B;</a> &#x435;&#x441;&#x43B;&#x438; &#x441;&#x43E;&#x43C;&#x43D;&#x435;&#x432;&#x430;&#x435;&#x448;&#x44C;&#x441;&#x44F;.

                                            </p>
                                        </div>
                                    </div>
                                    <div class="card">
                                        <button class="btn btn-opner collapsed" type="button" data-toggle="collapse"
                                                data-target="#acc_collapse2" aria-expanded="false" aria-controls="acc_collapse2">
                                        &#x41C;&#x43E;&#x433;&#x443; &#x43B;&#x438; &#x44F; &#x438;&#x441;&#x43F;&#x43E;&#x43B;&#x44C;&#x437;&#x43E;&#x432;&#x430;&#x442;&#x44C; &#x444;&#x438;&#x437;&#x438;&#x447;&#x435;&#x441;&#x43A;&#x443;&#x44E; SIM &#x432;&#x43C;&#x435;&#x441;&#x442;&#x435; &#x441; ESIM?
                                        </button>
                                        <div id="acc_collapse2" class="collapse" data-parent="#faqaccordion">
                                            <div class="acc-card">
                                                <p><span class="text-primary">A:</span> &#x414;&#x430;! &#x421; &#x444;&#x443;&#x43D;&#x43A;&#x446;&#x438;&#x43E;&#x43D;&#x430;&#x43B;&#x44C;&#x43D;&#x43E;&#x441;&#x442;&#x44C;&#x44E; &#x434;&#x432;&#x43E;&#x439;&#x43D;&#x44B;&#x43C; SIM &#x432;&#x44B; &#x43C;&#x43E;&#x436;&#x435;&#x442;&#x435; &#x438;&#x43C;&#x435;&#x442;&#x44C; &#x438; &#x441;&#x432;&#x43E;&#x44E; &#x444;&#x438;&#x437;&#x438;&#x447;&#x435;&#x441;&#x43A;&#x443;&#x44E; SIM &#x438; ESIM &#x430;&#x43A;&#x442;&#x438;&#x432;&#x43D;&#x443;&#x44E; &#x43E;&#x434;&#x43D;&#x43E;&#x432;&#x440;&#x435;&#x43C;&#x435;&#x43D;&#x43D;&#x43E;.</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card">
                                        <button class="btn btn-opner collapsed" type="button" data-toggle="collapse"
                                                data-target="#acc_collapse3" aria-expanded="false" aria-controls="acc_collapse3">
                                        &#x423; &#x43C;&#x435;&#x43D;&#x44F; &#x443;&#x436;&#x435; &#x435;&#x441;&#x442;&#x44C; &#x430;&#x43A;&#x442;&#x438;&#x432;&#x43D;&#x44B;&#x439; ESIM &#x432; &#x43C;&#x43E;&#x435;&#x43C; &#x442;&#x435;&#x43B;&#x435;&#x444;&#x43E;&#x43D;&#x435;, &#x43C;&#x43E;&#x433;&#x443; &#x43B;&#x438; &#x44F; &#x432;&#x43E;&#x441;&#x43F;&#x43E;&#x43B;&#x44C;&#x437;&#x43E;&#x432;&#x430;&#x442;&#x44C;&#x441;&#x44F; &#x432;&#x430;&#x448;&#x438;&#x43C;&#x438; &#x443;&#x441;&#x43B;&#x443;&#x433;&#x430;&#x43C;&#x438;?
                                        </button>
                                        <div id="acc_collapse3" class="collapse" data-parent="#faqaccordion">
                                            <div class="acc-card">
                                                <p><span class="text-primary">A:</span> &#x414;&#x430;, &#x432;&#x430;&#x448; &#x442;&#x435;&#x43B;&#x435;&#x444;&#x43E;&#x43D; &#x43C;&#x43E;&#x436;&#x435;&#x442; &#x445;&#x440;&#x430;&#x43D;&#x438;&#x442;&#x44C; &#x43C;&#x43D;&#x43E;&#x433;&#x43E; &#x43F;&#x440;&#x43E;&#x444;&#x438;&#x43B;&#x435;&#x439; ESIM &#x441;&#x440;&#x430;&#x437;&#x443;. &#x412;&#x44B; &#x43C;&#x43E;&#x436;&#x435;&#x442;&#x435; &#x432;&#x44B;&#x431;&#x440;&#x430;&#x442;&#x44C; &#x43E;&#x434;&#x438;&#x43D;, &#x447;&#x442;&#x43E;&#x431;&#x44B; &#x438;&#x441;&#x43F;&#x43E;&#x43B;&#x44C;&#x437;&#x43E;&#x432;&#x430;&#x442;&#x44C; &#x432; &#x432; &#x43D;&#x430;&#x441;&#x442;&#x440;&#x43E;&#x439;&#x43A;&#x430;&#x445; &#x442;&#x435;&#x43B;&#x435;&#x444;&#x43E;&#x43D;&#x430;.</p>
                        </div>
                                        </div>
                                    </div>
                                    <div class="card">
                                        <button class="btn btn-opner collapsed" type="button" data-toggle="collapse"
                                                data-target="#acc_collapse4" aria-expanded="false" aria-controls="acc_collapse4">
                                        &#x423; &#x43C;&#x435;&#x43D;&#x44F; &#x435;&#x441;&#x442;&#x44C; &#x434;&#x440;&#x443;&#x433;&#x438;&#x435; &#x432;&#x43E;&#x43F;&#x440;&#x43E;&#x441;&#x44B;
                                        </button>
                                        <div id="acc_collapse4" class="collapse" data-parent="#faqaccordion">
                                            <div class="acc-card">
                                                <p><span class="text-primary">A:</span> &#x417;&#x430;&#x434;&#x430;&#x439;&#x442;&#x435; &#x43D;&#x430;&#x43C; &#x43B;&#x44E;&#x431;&#x43E;&#x439; &#x432;&#x43E;&#x43F;&#x440;&#x43E;&#x441;, &#x438;&#x441;&#x43F;&#x43E;&#x43B;&#x44C;&#x437;&#x443;&#x44F; &#x43D;&#x430;&#x448; &#x447;&#x430;&#x442; &#x444;&#x443;&#x43D;&#x43A;&#x446;&#x438;&#x438; &#x438;&#x43B;&#x438; &#x43D;&#x430;&#x43F;&#x438;&#x448;&#x438;&#x442;&#x435; &#x43D;&#x430;&#x43C; &#x43F;&#x43E; &#x430;&#x434;&#x440;&#x435;&#x441;&#x443; support@mobimatter.com. &#x41C;&#x44B; &#x431;&#x443;&#x434;&#x435;&#x43C; &#x440;&#x430;&#x434;&#x44B; &#x43F;&#x43E;&#x43C;&#x43E;&#x447;&#x44C;.</p>
                        </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


<!-- The Modal -->
<div class="modal fade confirmation-modal" id="confirmation-modal" tabindex="-1" role="dialog" aria-labelledby="confirmation-modal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div id="modal-body" class="modal-body text-center">
                <svg class="mb-2" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="32" height="28" viewBox="0 0 164.229 145.144">
                    <defs>
                        <filter id="triangle_2789" x="0" y="0" width="164.229" height="145.144" filterUnits="userSpaceOnUse">
                            <feOffset dy="3" input="SourceAlpha" />
                            <feGaussianBlur stdDeviation="3" result="blur" />
                            <feFlood flood-opacity="0.161" />
                            <feComposite operator="in" in2="blur" />
                            <feComposite in="SourceGraphic" />
                        </filter>
                    </defs>
                    <g data-name="Group 7910" transform="translate(9 6)">
                        <g data-name="Group 7909" transform="translate(0 0)">
                            <g data-name="Group 7907">
                                <g transform="matrix(1, 0, 0, 1, -9, -6)" filter="url(#triangle_2789)">
                                    <path id="triangle_2789-2" data-name="Path 2789" d="M1.573,143.76,63.661,38.825a10.961,10.961,0,0,1,18.907,0L144.657,143.76a11.106,11.106,0,0,1-9.454,16.8H11.026a11.106,11.106,0,0,1-9.453-16.8Z" transform="translate(9 -27.41)" fill="#566081" />
                                </g>
                                <g data-name="Group 7906" transform="translate(8.799 8.808)">
                                    <path id="Path_2790" data-name="Path 2790" d="M180.893,244.8h83.539a11.106,11.106,0,0,0,9.454-16.8l-45.364-76.67a78.491,78.491,0,0,1,1.722,25.8c-4.455,44.752-45.214,65.618-47.855,66.914C181.881,244.3,181.384,244.555,180.893,244.8Z" transform="translate(-138.029 -126.462)" fill="#576082" />
                                    <path id="Path_2791" data-name="Path 2791" d="M33.035,173.779a2.208,2.208,0,0,1-1.857-3.474L93.266,65.37a2.1,2.1,0,0,1,3.714,0L159.068,170.3a2.2,2.2,0,0,1-1.857,3.474H33.035Z" transform="translate(-30.81 -64.25)" fill="#00aeef" />
                                    <path id="Path_2792" data-name="Path 2792" d="M266.044,224.261c-.058,1.63-.166,3.286-.333,4.967-2.963,29.766-21.985,48.961-35.171,58.862H299.9a2.2,2.2,0,0,0,1.857-3.474Z" transform="translate(-173.496 -178.561)" fill="#0098d1" />
                                </g>
                            </g>
                            <g data-name="Group 7908" transform="translate(67.187 40.58)">
                                <path data-name="Path 2793" d="M241.173,221.034a3.017,3.017,0,0,1-3.007-2.777l-2.9-36.362a5.927,5.927,0,1,1,11.817,0l-2.9,36.362A3.017,3.017,0,0,1,241.173,221.034Z" transform="translate(-235.246 -175.496)" fill="#fff" />
                                <circle id="Ellipse_659" data-name="Ellipse 659" cx="4.738" cy="4.738" r="4.738" transform="translate(1.19 54.163)" fill="#fff" />
                            </g>
                        </g>
                    </g>
                </svg>
                <h4 class="h5">Пожалуйста подтвердите:</h4>
                <br/>
                <p class="font-size-14 mb-2">1. You have a <strong>eSIM capable device that is unlocked.</strong></p>
                <p class="font-size-14 mb-2">2. You agree to our <a href="https://mobimatter.com/privacy-policy" target="_blank">terms and conditions</a> and the <a href="https://mobimatter.com/delivery-refunds" target="_blank">cancellations, delivery and refunds policy</a>.</strong></p>
                <p class="font-size-14">3. Перепроверьте свой адрес электронной почты для любых опечаток:</p>
                <input for="Email" type="email" class="confirm-email form-control shadow-sm" id="confirm-email-input" style="text-transform: none !important;">
                <button id="modal-button" type="button" class="btn btn-primary btn-block" data-dismiss="modal">Confirm</button>
                <br/>
                <button id="modal-button" type="button" class="btn btn-accent btn-block" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade session-expired-modal" id="session-expired-modal" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body text-center">
                <svg class="mb-2" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="46.495" height="49.344" viewBox="0 0 46.495 49.344"><defs><filter id="a" x="5.699" y="0" width="35.097" height="46.495" filterUnits="userSpaceOnUse"><feOffset dy="3" input="SourceAlpha" /><feGaussianBlur stdDeviation="3" result="b" /><feFlood flood-opacity="0.161" /><feComposite operator="in" in2="b" /><feComposite in="SourceGraphic" /></filter><filter id="c" x="0" y="27.925" width="46.495" height="21.419" filterUnits="userSpaceOnUse"><feOffset dy="3" input="SourceAlpha" /><feGaussianBlur stdDeviation="3" result="d" /><feFlood flood-opacity="0.161" /><feComposite operator="in" in2="d" /><feComposite in="SourceGraphic" /></filter></defs><g transform="translate(4 3.151)"><g transform="matrix(1, 0, 0, 1, -4, -3.15)" filter="url(#a)"><path d="M30.306,14.743A4.025,4.025,0,0,0,32.1,11.4V5H15v6.4a4.025,4.025,0,0,0,1.791,3.348l5.617,3.745v1.52l-5.617,3.745A4.023,4.023,0,0,0,15,27.1v6.4H32.1V27.1a4.025,4.025,0,0,0-1.791-3.348l-5.617-3.745v-1.52Z" transform="translate(-0.3 1)" fill="#00aeef" /></g><path d="M13.761,72.526c-1.368.7-.056,1.119-1.157,3.276s3.247.83,1.749.137a1.569,1.569,0,0,0-2.246,1.339V77.3L10.082,80.78H27.567l-1.784-6.526V74.23s-1.077-1.893-2.16-1.7l-1.168.2c-1.536.267-.768-2.422-2.941-2.473S15.128,71.83,13.761,72.526Z" transform="translate(0.304 -49.305)" fill="#424a60" /><rect width="22.488" height="2.3" transform="translate(8.003 31.383)" fill="#424a60" /><g transform="matrix(1, 0, 0, 1, -4, -3.15)" filter="url(#c)"><path d="M32.925,56.28h-1.71V54H7.28v2.28H5.57a.57.57,0,1,0,0,1.14H32.925a.57.57,0,0,0,0-1.14ZM8.419,55.14H30.076v1.14H8.419Z" transform="translate(4 -20.07)" fill="#556080" /></g><rect width="22.488" height="2.3" transform="translate(8.003 0.462)" fill="#424a60" /><path d="M32.925,0H5.57a.57.57,0,0,0,0,1.14H7.28v2.28H31.215V1.14h1.71a.57.57,0,0,0,0-1.14ZM30.076,2.28H8.419V1.14H30.076Z" transform="translate(0 0)" fill="#556080" /></g></svg>
                <h4 class="h5">Время сеанса истекло</h4>
                <p class="text-dark-light font-weight-500">Сеанс имеет тайм-аут. пожалуйста, попробуйте еще раз приобрести предложение.</p>
                <a href="/travel-esim" class="btn btn-primary btn-block py-3">Попробуйте снова</a>
            </div>
        </div>
    </div>
</div>

@endsection

@push('before-scripts')
    <script src="/js/check-out-min.js"></script>
    <script>
        var elementId = 'price';
        var counterElement = 1;

        var productDetail = '';

        var currency = '';
        var amount = '';

        var url = window.location.pathname.split("/");
        var orderId = url.pop();
        var userId = url.pop()
        var postBody = {
            "OrderId": orderId,
            "Customer": {
                "Name": null,
                "Email": null,
                "Phone":null
            }
        };
        $(document).ready(function () {
            
            var cntryLang = "";
            if (cntryLang == null || cntryLang == undefined) {
                cntryLang = "en";
            }
            var cntryLangStorage = sessionStorage.getItem("cntryLang");
            if (cntryLangStorage) {
                if (cntryLangStorage!=cntryLang)
                {
                    sessionStorage.setItem("cntryLang", cntryLang);
                }
            }
            // navigation api of browser
            if (performance.navigation.type == performance.navigation.TYPE_RELOAD) {
                sessionStorage.setItem("setOnLoad", 2);

            }
            if (performance.navigation.type == performance.navigation.TYPE_BACK_FORWARD)
            {
                sessionStorage.setItem("setOnLoad", 2);
            }
            // order ata from local storage
            productDetail = JSON.parse(sessionStorage.getItem("productDetail"));
            if (productDetail == null) {
                cancelOrder(orderId);
            }

            amount = productDetail.price;
            currency = productDetail.currency;
            
            observer.observe();
            var curnyValfrmSesinStrg = sessionStorage.getItem("currencyChageVal");
            if (curnyValfrmSesinStrg != '' && curnyValfrmSesinStrg != null && curnyValfrmSesinStrg != undefined) {
                if (curnyValfrmSesinStrg == 'USD') {
                    $('.currency-text').hide();
                } else {
                    currencyConverion(counterElement, curnyValfrmSesinStrg, elementId);
                }
            }
        })

        if (sessionStorage.getItem("step") != null) {
            if (sessionStorage.getItem("step") == 2) {
                $("#email-section").hide();
                $("#pay-section").show();
                $("#detail-progress").addClass("success");
                $("#payment-progress").addClass("active");
            }
        }
        else {
            sessionStorage.setItem("step", "1");
        }
        function validateEmail(email) {
            var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(email);
        }
        $("#email_field").click(function () {
            $('#error-span').remove();
        });
        $("#name_field").click(function () {
            $('#error-span').remove();
        });
        $("#user-form-button").click(function (event) {
            var username = $("#name_field").val();
            var email = $("#email_field").val();
            $('#error-span').remove();
            if (username == "" || username == null) {
                $('#name_field').focus();
                $("#name-field").append('<span id="error-span" class="text-danger">This field should not be empty</span>');
            }
            else if (email != "" && email != null) {
                if (validateEmail(email)) {
                    $('#confirmation-modal').modal('show');
                    $("#confirm-email-input").val(email.trim());
                    event.preventDefault();
                }
                else {
                    $("#email-field").append('<span id="error-span" class="text-danger">Not a valid email</span>');
                }
            } else {
                $('#email_field').focus();
                $("#email-field").append('<span id="error-span" class="text-danger">This field should not be empty</span>');
            }
        });
        $('#modal-button').click(function (event) {
            event.preventDefault();
            $('#modal-button').addClass('disabled');
            $('#error-span').remove();
            $(window).scrollTop(0);
            var username = $("#name_field").val();
            var email = $("#confirm-email-input").val();

            if (email != "" && email != null) {
                if (validateEmail(email)) {
                    postBody = {
                        "OrderId": orderId,
                        "UserId": userId,
                        "Customer": {
                            "Name": username,
                            "Email": email
                        }
                    };
                    // $.ajax({
                    //     url: "https://mobimatter.com/api/v2/order/update",
                    //     method: "PUT",
                    //     contentType: "application/json",
                    //     data: JSON.stringify(postBody),
                    //     dataType: "json",
                    //     success: function (data) {
                    //         $("#email-section").hide();
                    //         $("#pay-section").show();
                    //         sessionStorage.setItem("step", "2");
                    //         $("#detail-progress").addClass("success");
                    //         $("#payment-progress").addClass("active");
                    //         $('#modal-button').removeClass('disabled');

                    
                    //         $("#faqs").hide();
                    //     },
                    //     error: function (errMsg) {
                    //         window.location.href = "/something-went-wrong?err=" + "Unable to save your contact information";
                    //     }
                    // });
                    sessionStorage.setItem("customerOrderData", JSON.stringify(postBody));
                }
                else {
                    $("#modal-body").append('<span id="error-span" class="text-danger">Not a valid email</span>');
                }
            } else {
                $('#confirm-email-input').focus();
                $("#modal-body").append('<span id="error-span" class="text-danger">This field should not be empty</span>');
            }

        });
        $("#go-back").click(function (event) {
            event.preventDefault();
            sessionStorage.setItem("step", "1");
            $("#email-section").show();
            $("#pay-section").hide();
            $("#detail-progress").addClass("active");
            $("#detail-progress").removeClass("success");
            $("#payment-progress").removeClass("active");
        });


        function startTimer(duration, display) {
            var timer = duration, minutes, seconds;
            var i = setInterval(function () {
                minutes = parseInt(timer / 60, 10)
                seconds = parseInt(timer % 60, 10);
                minutes = minutes < 10 ? "0" + minutes : minutes;
                seconds = seconds < 10 ? "0" + seconds : seconds;
                display.textContent = minutes + ":" + seconds;
                if (--timer < 0) {
                    timer = duration;
                    clearInterval(i);
                    sessionStorage.removeItem("timeBeforeReload");
                    var orderId = sessionStorage.getItem("orderId");
                    cancelOrder(orderId);
                }
            }, 1000);

        }
        window.onload = function () {
            var beforeReload = sessionStorage.getItem("timeBeforeReload");
            if (sessionStorage.getItem("orderId") === null)
            {
                $('#session-expired-modal').modal('show');
            }

            if (beforeReload == null || beforeReload == this.undefined)
            {
                var timeBeforeReload = new Date();
                sessionStorage.setItem("timeBeforeReload", timeBeforeReload);
                //sessionStorage.setItem("visited",1);
                sessionStorage.setItem("setOnLoad", 1);
            }

            var minutes = setTimerLimit();
            display = document.querySelector('#se-timer');
            startTimer(minutes, display);

        };
        function setTimerLimit() {
            var limit = 1800;

            var flagSetOnLoad=sessionStorage.getItem("setOnLoad");
            if (flagSetOnLoad != null && flagSetOnLoad != undefined && flagSetOnLoad!=1)
            {
                var beforeReload = sessionStorage.getItem("timeBeforeReload");
                var timeAfterReload = new Date();
                beforeReload = Date.parse(beforeReload); // parse to date object
                var diff = Math.abs(timeAfterReload - beforeReload);
                var seconds = diff / 1000;
                seconds =1800-seconds;
                if (seconds <= 0) {
                    sessionStorage.removeItem("timeBeforeReload");
                    sessionStorage.removeItem("setOnLoad");
                    var orderId = sessionStorage.getItem("orderId");
                    cancelOrder(orderId);
                    return 0;
                }
                return seconds;
            }
            return limit;
        }
        function cancelOrder(orderId) {
            $("#user-form-button").prop('disabled', true);
            $.ajax({
                url: "/api/v2/order/cancel/",
                type: "PUT",
                traditional: true,
                contentType: 'application/json',
                data: JSON.stringify({ "orderId": orderId }),
                success: function (res) {
                    sessionStorage.removeItem("orderId");
                    // sessionStorage.removeItem("visited");
                    sessionStorage.removeItem("timeBeforeReload");
                    sessionStorage.removeItem("setOnLoad");
                    $('#session-expired-modal').modal('show');

                }
            });
        }

        function onSavedCardChoose(id) {
            $("#orderId").attr("value", orderId);
            $("#methodId").attr("value", id);
            $('#cvv-modal').modal('show');
        }

    </script>
@endpush