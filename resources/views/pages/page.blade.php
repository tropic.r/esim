@extends('layouts.app')
@include('meta::manager', [
  'title' => $page->translation->meta_title,
  'description' => $page->translation->meta_description,
])

@push('before-styles')

@endpush

@section('content')
<section class="text-dark py-5 l-h-2">
    <div class="container">
      <div class="row text-center mb-3 pt-md-4">
        <div class="col-12 col-md-11 col-lg-9 mx-auto">
          @if($page->translation->meta_h1)
            <h2 class="font-weight-bold mb-3 h1">{{$page->translation->meta_h1}}</h2>
          @else
            <h2 class="font-weight-bold mb-3 h1">{{$page->translation->name}}</h2>
          @endif
        </div>
      </div>
      <div class="row">
        <div class="col-12">
            {!! $page->translation->description !!}
        </div>
      </div>
    </div>
</section>
@endsection