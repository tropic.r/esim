<!doctype html>
<html lang="{{ htmlLang() }}">
    <head>
        @section('metaLabels')
        <meta charset="utf-8">
        <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
        <meta http-equiv="Pragma" content="no-cache" />
        <meta http-equiv="Expires" content="0" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <!-- Favicon -->
        <link rel="apple-touch-icon" sizes="180x180" href="/images/icon.png">
        <link rel="icon" type="image/png" sizes="32x32" href="/images/icon.png">
        <link rel="icon" type="image/png" sizes="16x16" href="/images/icon.png">

        @if(View::hasSection('canonical'))
            @yield('canonical')
        @else
            <link rel="canonical" href="{{ url()->current() }}"/>
        @endif


        @if (config('app.env') == 'local')
            <meta name="googlebot" content="noindex, nofollow"/>
            <meta name="yandex" content="none"/>
        @else
            <meta name="robots" content="all" />
        @endif
        <link rel="preload" href="{{ mix('css/app.css') }}" as="style" />
        @yield('meta')
        <link rel="stylesheet" href="/public/css/global.css" />
        @stack('before-styles')
        <link rel="stylesheet" href="/css/index.css" />
        <link rel="stylesheet" href="/css/custome.css" />
        @stack('after-styles')

    </head>
    <body>
        <div id="overlay" style="display:none;">
            <div class="overlay-loader-inner">
                <div class="spinner"></div>
                <br />
                Loading...
            </div>
        </div>

        @include('includes.header_desktop')

        <main id="main" role="main">
            @yield('content')
        </main>

        @include('includes.footer')
        <script>
            window._defaultLocale = '{{ config('app.locale') }}';
            window._locale = '{{ app()->getLocale() }}';
            window._banner_placeholder = '{{ __('banner.search_for_a_country') }}';
            window._currencies = @php echo json_encode(json_currencies()) @endphp
        </script>
        <script src="/js/global1-min.js"></script>

        <script type="application/javascript" src="/js/lozad.js"></script>

        <script>

            $(function () {
                $('[data-toggle="tooltip"]').tooltip()
            });
            setTimeout(function () {
                $("#alert-message").fadeOut(500);
            }, 5000);
            if ($(window).width() > 992) {
                $(window).scroll(function(){
                    if ($(this).scrollTop() > 40) {
                        $('#navbar_top').addClass("fixed-top");
                        $('body').css('padding-top', $('.navbar').outerHeight() + 'px');
                    } else {
                        $('#navbar_top').removeClass("fixed-top");
                        $('body').css('padding-top', '0');
                    }
                });
            }
        </script>
        @stack('before-scripts')

        @stack('after-scripts')
        <script>
            $(window).on('load', function () {
                var curnyValfrmSesinStrg = sessionStorage.getItem("currencyChageVal");
                if (curnyValfrmSesinStrg != '' && curnyValfrmSesinStrg != null && curnyValfrmSesinStrg != undefined) {
                    setTimeout(currencyConverion(counterElement, curnyValfrmSesinStrg, elementId), 10000);
                    $('.multi-currency-select').find('option[value="' + curnyValfrmSesinStrg + '"]').attr('selected', 'selected');
                }
                $('.multi-currency-select').change(function () {
                    var option = $(this).find('option:selected');
                    var text = $(this).find('option:selected').text()
                    var val = $('.multi-currency-select').val();
                    sessionStorage.setItem("currencyChageVal", val);
                    var $aux = $('<select/>').append($('<option/>').text(text))
                    $(this).after($aux)
                    $(this).width($aux.width())
                    $aux.remove();
                    $('.currency-text').css('display', 'block');
                    if (val == 'USD') {
                        $('.currency-text').hide();
                    } else {
                        currencyConverion(counterElement, val, elementId);
                    }
                });
            });

            function currencyConverion(counterElement, val, elementId) {
                var actualAmount = '';
                var rate = '';
                var currencyAmount = '';
                var currentRate = sessionStorage.getItem('currencyChageVal');
                if (currentRate != null && currentRate != undefined) {
                    rate = window._currencies[currentRate];
                } else {
                    rate = window._currencies[val];
                }
                for (var i = 1; i <= counterElement; i++) {
                    actualAmount = $("#" + elementId + "-" + i).text();
                    actualAmount = actualAmount.substring(4);
                    var firstValue = 1;
                    if (rate != "error") {
                        firstValue = rate;
                    } else {
                        firstValue = 1;
                        val = "USD";
                        $('.multi-currency-select').find('option[value="' + val + '"]').attr('selected', 'selected')
                    }
                    console.log(firstValue, actualAmount);
                    currencyAmount = firstValue * actualAmount;
                    $("#currencyAmountElement-" + i).empty();
                    $("#currencyAmountElement-" + i).append(`<strong>(${val} ${currencyAmount.toFixed(2)})</strong>`);
                    $('#currencyAmountElement-' + i).show();
                    currencyAmount = 0;
                    if (val == "USD") {
                        $('.currency-text').hide();
                    }
                }
            }
        </script>
    </body>
</html>
