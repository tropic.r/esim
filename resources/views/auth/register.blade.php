@extends('layouts.app')

@push('before-styles')
<link rel="stylesheet" href="/css/login-page.css" />
@endpush

@section('content')
<section class="sect-login">
    <div class="row m-3 login-frame flex-grow-1">
        <div class="col-md-7 d-none d-lg-block img-holder text-white p-3">
            <div class="outer-frame">
                <h1><strong class="d-block">Welcome to E-sim</strong> Get access to travel eSIMs for over a hundred countries</h1>
            </div>
        </div>
        <div class="col-md-5 p-3 d-flex align-items-center flex-wrap justify-content-center flex-column">
            <div class="outer-frame">
                <a class="col-md-12 d-inline-flex mb-3 justify-content-center" href="{{ route('home') }}">
                    <img src="/images/logo_login.png" >
                </a>
                <div class="d-block mb-3 text-center">Sign Up or <a href="{{ route('auth.login') }}">Log in</a></div>
                <div id="firebaseui-auth-container" lang="{{ htmlLang() }}">
                    <div class="firebaseui-container firebaseui-page-provider-sign-in firebaseui-id-page-provider-sign-in firebaseui-use-spinner">
                        <div class="firebaseui-card-content">
                            <form id="autorizationForm_second" method="POST" action="{{ route('login.store') }}">
                                @csrf
                                <div class="row">
                                    <div class="col-lg-12 form-group">
                                        <label for="email_field">Email</label>
                                        <input type="email" class="form-control border-0 shadow-sm" id="email_field" placeholder="john@e-sim.md" name="email" value="">
                                    </div>
                                    <div class="col-lg-12 form-group">
                                        <label for="exi_password_field">Password</label>
                                        <input type="password" class="form-control border-0 shadow-sm" id="new_password_field" placeholder="********"  name="password">
                                        <span class="text-danger field-validation-valid" data-valmsg-replace="true"></span>
                                    </div>
                                    <div class="col-lg-12 justify-content-center">
                                        <button type="submit" class="btn btn-primary btn-block">Login</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <br>
                        <div class="firebaseui-card-footer firebaseui-provider-sign-in-footer">
                            <p class="firebaseui-tos firebaseui-tospp-full-message">By continuing, you are indicating that you accept our <a href="{{ route('page',['slug'=>'terms-condition']) }}" class="firebaseui-link firebaseui-tos-link" target="_blank">Terms of Service</a> and <a href="{{ route('page',['slug'=>'privacy-policy']) }}" class="firebaseui-link firebaseui-pp-link" target="_blank">Privacy Policy</a>.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection