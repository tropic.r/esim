@extends('layouts.app')
@section('metaLabels')
    @parent
    @include('meta::manager', [
        'title' => __('Not Found'),
    ])
@stop
@section('title', 'Error')
@section('code', '404')
@push('before-styles')
    <link rel="stylesheet" href="/css/eror.css">
@endpush
@section('content')
    <section class="py-4 eror-massage">
        <div class="container">

                <div class="eror-404">
                    <strong class="text-primary text-404">404</strong>
                    <h2 class="h1 title">Page not found</h2>
                    <p>Visit <a href="{{ route('home') }}">E-sim.md</a></p>
                </div>

        </div>
    </section>
@endsection
@push('before-scripts')
    <script src="js/global1-min.js"></script>
    <script type="application/javascript" src="/js/lozad.js"></script>

    <script src="/js/bootstrap.bundle.min.js"></script>

    <script>
        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        });
        setTimeout(function () {
            $("#alert-message").fadeOut(500);
        }, 5000);
        if ($(window).width() > 992) {
            $(window).scroll(function(){  
            if ($(this).scrollTop() > 40) {
            $('#navbar_top').addClass("fixed-top");
            // add padding top to show content behind navbar
            $('body').css('padding-top', $('.navbar').outerHeight() + 'px');
        }else{
            $('#navbar_top').removeClass("fixed-top");
            // remove padding top from body
            $('body').css('padding-top', '0');
        }   
        });
        }
    </script>

@endpush