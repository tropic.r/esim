<?php

namespace Database\Seeders;

use App\Domains\Region\Models\Region;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

use Illuminate\Support\Str;

class RegionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $XML = file_get_contents(public_path().'/country_list.xml');
        $random = collect(json_decode(json_encode((array) simplexml_load_string($XML)), true));
        foreach($random['country'] as $country){
            if($country['location'] != null){
                Region::updateOrCreate([
                    'slug' => Str::slug($country['location'])
                ], [
                    'name' => $country['location'],
                    'active' => 1
                ]);
            }
        }
    }
}
