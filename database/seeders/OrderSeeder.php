<?php

namespace Database\Seeders;

use App\Domains\Order\Models\Order;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

use Illuminate\Support\Str;

class OrderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $curl = $this->curl();
        Order::updateOrCreate([
            'orderId' => 'STV-570887',
        ],[
            'user_id' => 1,
            'product_id' => 399 , 
            'name' => 'admin', 
            'email' => 'admin@admin.admin', 
            'status' => 'Completed', 
            'currencyCode' => 'USD', 
            'retailPrice' => 6.6, 
            'data' => $curl
        ]);
    }

    public function curl(){
        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => 'https://api.mobimatter.com/mobimatter/api/v2/order/STV-570887',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'GET',
        CURLOPT_HTTPHEADER => array(
            'Content-Type: application/json',
            'api-key:'. env('MOBIMATTER_API_KEY'),
            'MerchantId:'. env('MOBIMATTER_API_MERCHANT_ID'),
            'Cookie: ARRAffinity=645e8f754b5c169bbf500aca97480ed8507d6dded47a9bdf4d4ce5a70e419436; ARRAffinitySameSite=645e8f754b5c169bbf500aca97480ed8507d6dded47a9bdf4d4ce5a70e419436'
        ),
        ));

        $response = curl_exec($curl);
        curl_close($curl);
        return json_decode($response)->result;
    }
}
