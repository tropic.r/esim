<?php

namespace Database\Seeders;

use App\Domains\Region\Models\Region;
use App\Domains\Country\Models\Country;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

use Illuminate\Support\Str;

class CountriSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $JSON = file_get_contents(public_path().'/country.json');
        $random = collect(json_decode($JSON, true));
        foreach($random as $country){
            if($country['name'] != null){
                Country::updateOrCreate([
                    'slug' => Str::slug($country['name'])
                ], [
                    'calling_code' => $country['calling_code'],
                    'currency' => (isset($country['currency'])) ? $country['currency'] : '',
                    'name' => $country['name'],
                    'three_letter_abbreviation' => $country['three_letter_abbreviation'],
                    'two_letter_abbreviation' => $country['two_letter_abbreviation'],
                    'objectID' => (isset($country['objectID'])) ? $country['objectID'] : '0',
                    '_highlightResult' => (isset($country['_highlightResult'])) ? $country['_highlightResult'] : 'NULL',
                    'isOfferCountry' => (isset($country['isOfferCountry'])) ? $country['isOfferCountry'] : 'false',
                    'isTopUpCountry' => (isset($country['isTopUpCountry'])) ? $country['isTopUpCountry'] : 'false',
                    'active' => 1
                ]);
            } 
        }
    }
}
