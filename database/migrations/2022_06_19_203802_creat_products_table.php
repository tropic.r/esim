<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
        /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('uniqueId');
            $table->string('merchantId');
            $table->string('productId');
            $table->string('i_product');
            $table->string('rank');
            $table->unsignedBigInteger('category')->nullable();
            $table->string('productCategory')->nullable();
            $table->foreignId("provider_id")->constrained("provider")->onDelete('cascade');
            $table->string('slug');
            $table->float('retailPrice')->default(0);
            $table->float('wholesalePrice')->default(0);
            $table->float('currencyCode')->default(0);
            $table->float('plan_data_limit')->default(0);
            $table->string('plan_data_unit')->nullable();
            $table->string('plan_validity')->nullable();
            $table->jsonb('tags');
            $table->boolean('active')->default(true);
            $table->jsonb('data');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
};
