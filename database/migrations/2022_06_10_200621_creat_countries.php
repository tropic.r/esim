<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('countries', function (Blueprint $table) {
            $table->id();
            $table->string('slug');
            $table->string('calling_code');
            $table->string('currency');
            $table->string('name');
            $table->string('three_letter_abbreviation');
            $table->string('two_letter_abbreviation');
            $table->string('objectID');
            $table->string('_highlightResult')->nullable();
            $table->string('isOfferCountry');
            $table->string('isTopUpCountry');
            $table->boolean('active')->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('countries');
    }
};
