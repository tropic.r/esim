<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('activity')->default(null)->nullable();
            $table->string('google_id')->default(null)->nullable()->after('activity');
            $table->string('facebook_id')->default(null)->nullable()->after('activity');
            $table->tinyInteger('receive_emails')->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('activity');
            $table->dropColumn('google_id');
            $table->dropColumn('facebook_id');
            $table->dropColumn('receive_emails');
        });
    }
};
