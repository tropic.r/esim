<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order', function (Blueprint $table) {
            $table->id();
            $table->string('orderId');
            $table->string('user_id')->constrained("users")->onDelete('cascade');
            $table->string('product_id')->constrained("products")->onDelete('cascade');
            $table->string('name');
            $table->string('email')->unique()->nullable();
            $table->string('status')->nullable();
            $table->string('currencyCode')->nullable();
            $table->string('retailPrice')->default(0);
            $table->jsonb('data')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order');
    }
};
