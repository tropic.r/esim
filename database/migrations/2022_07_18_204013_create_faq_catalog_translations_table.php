<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('faq_catalog_translations', function (Blueprint $table) {
            $table->id();
            $table->foreignId('faq_catalog_id')->constrained("faq_catalog")->onDelete('cascade');
            $table->string('locale')->default('en');
            $table->string('name');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('faq_catalog_translations');
    }
};
